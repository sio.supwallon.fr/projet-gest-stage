<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Droit;
use GEST_STAGE\kernel\View;
use PDO;

class DroitDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `droit`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Droit::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `droit`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Droit::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByGroupe($id_groupe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `droit`.* 
                      FROM `droit`
                      INNER JOIN `groupe_droit` ON `droit`.`id` = `id_droit`
                      WHERE `id_groupe` = :id_groupe;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_groupe", $id_groupe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Droit::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByUtilisateur($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `droit`.* 
                      FROM `droit`
                      INNER JOIN `groupe_droit` ON `droit`.`id` = `id_droit`
                      INNER JOIN `utilisateur_groupe` ON `utilisateur_groupe`.`id_groupe` = `groupe_droit`.`id_groupe`
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Droit::class);

        $array = $sth->fetchAll();

        return $array;
    }
}