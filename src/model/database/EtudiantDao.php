<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Etudiant;
use GEST_STAGE\kernel\View;
use PDO;

class EtudiantDao extends UtilisateurDao
{
    public static function get($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etudiant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etudiant`
                      INNER JOIN `utilisateur` 
                      ON `etudiant`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etudiant::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etudiant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etudiant`
                      INNER JOIN `utilisateur` 
                      ON `etudiant`.`id_utilisateur` = `utilisateur`.`id`
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etudiant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByClasse($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etudiant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etudiant`
                      INNER JOIN `utilisateur` 
                      ON `etudiant`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_classe` = :id_classe
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etudiant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function update($etudiant)
    {
        parent::update($etudiant);

        $cnx = Database::get_connection();

        $statement = "UPDATE `etudiant`
                      SET
                        `sexe` = :sexe,
                        `num_secu` = :num_secu,
                        `date_naissance` = :date_naissance,
                        `adresse` = :adresse,";
        if (isset($etudiant->signature)) {
            $statement .= " `signature` = :signature,";
        }
        $statement .= " `id_ville` = :id_ville,
                        `id_ville_naissance` = :id_ville_naissance,
                        `id_classe` = :id_classe
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":sexe", $etudiant->sexe, PDO::PARAM_STR, 1);
        $sth->bindParam(":num_secu", $etudiant->num_secu, PDO::PARAM_STR, 15);
        $sth->bindParam(":date_naissance", $etudiant->date_naissance, PDO::PARAM_STR, 10);
        $sth->bindParam(":adresse", $etudiant->adresse, PDO::PARAM_STR, 255);
        if (isset($etudiant->signature)) {
            $sth->bindParam(":signature", $etudiant->signature, PDO::PARAM_STR, 50);
        }
        $sth->bindParam(":id_ville", $etudiant->id_ville, PDO::PARAM_INT);
        $sth->bindParam(":id_ville_naissance", $etudiant->id_ville_naissance, PDO::PARAM_INT);
        $sth->bindParam(":id_classe", $etudiant->id_classe, PDO::PARAM_INT);
        $sth->bindParam(":id_utilisateur", $etudiant->id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function insert($etudiant)
    {
        parent::insert($etudiant);
        $etudiant->id_utilisateur = $etudiant->id;

        $cnx = Database::get_connection();

        $statement = "INSERT INTO `etudiant`
                      (`id_utilisateur`)
                      VALUES
                      (:id_utilisateur);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $etudiant->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}