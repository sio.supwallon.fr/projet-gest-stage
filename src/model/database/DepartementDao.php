<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Departement;
use GEST_STAGE\kernel\View;
use PDO;

class DepartementDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `departement` WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Departement::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `departement`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Departement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByRegion($id_region)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `departement`
                      WHERE `id_region` = :id_region
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_region", $id_region, PDO::PARAM_INT);
        $result = $sth->execute();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Departement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByPays($id_pays)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `departement`.* 
                      FROM `departement`
                      INNER JOIN `region` ON `id_region` = `region`.`id`
                      WHERE `id_pays` = :id_pays
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_pays", $id_pays, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Departement::class);

        $array = $sth->fetchAll();

        return $array;
    }
}