<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\TypeUtilisateur;
use GEST_STAGE\kernel\View;
use PDO;

class TypeUtilisateurDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `type_utilisateur`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, TypeUtilisateur::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getBySlug($slug)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `type_utilisateur`
                      WHERE `slug` = :slug;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":slug", $slug, PDO::PARAM_STR, 50);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, TypeUtilisateur::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `type_utilisateur`
                      ORDER BY `niveau_habilitation` DESC;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, TypeUtilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByGroupe($id_groupe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `type_utilisateur`
                      WHERE `id_groupe` = :id_groupe
                      ORDER BY `niveau_habilitation` DESC;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(':id_groupe', $id_groupe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, TypeUtilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }
}