<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Accompagnement;
use GEST_STAGE\kernel\View;
use PDO;

class AccompagnementDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `accompagnement`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Accompagnement::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `accompagnement`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Accompagnement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByClasse($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `accompagnement`.*
                      FROM `accompagnement`
                      INNER JOIN `classe_accompagnement` ON `id_accompagnement` = `accompagnement`.`id`
                      WHERE `id_classe` = :id_classe;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Accompagnement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllCheckedByClasse($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *, EXISTS
                        (
                            SELECT * 
                            FROM `classe_accompagnement` 
                            WHERE `id_accompagnement` = `accompagnement`.`id` 
                            AND `id_classe` = :id_classe
                        ) as `checked`
                      FROM `accompagnement`;";
        
        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Accompagnement::class);

        $array = $sth->fetchAll();

        return $array;
    }
}