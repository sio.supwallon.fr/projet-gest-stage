<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\TypeUtilisateur;
use GEST_STAGE\model\classes\Utilisateur;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\classes\Enseignant;
use GEST_STAGE\model\classes\Etablissement;
use GEST_STAGE\model\classes\Etudiant;
use PDO;

class UtilisateurDao
{
    public static function countByIdentifiant($identifiant)
    {
        $identifiant = filter_var($identifiant, FILTER_SANITIZE_STRING);

        $cnx = Database::get_connection();

        $statement = "SELECT COUNT(*) as 'count'
                      FROM `utilisateur` 
                      WHERE REGEXP_LIKE(`identifiant`, :pattern);";
        
        $pattern = "^({$identifiant})[0-9]*$";
        $sth = $cnx->prepare($statement);
        $sth->bindParam(":pattern", $pattern, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $row = $sth->fetch();
        $result = $row['count'];

        return $result;
    }

    public static function getLastIdentifiant($identifiant)
    {
        $identifiant = filter_var($identifiant, FILTER_SANITIZE_STRING);

        $cnx = Database::get_connection();

        $statement = "SELECT `identifiant`
                      FROM `utilisateur` 
                      WHERE REGEXP_LIKE(`identifiant`, :pattern)
                      ORDER BY `id` DESC
                      LIMIT 1;";
        
        $pattern = "^({$identifiant})[0-9]*$";
        $sth = $cnx->prepare($statement);
        $sth->bindParam(":pattern", $pattern, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $row = $sth->fetch();
        if ($row) {
            $result = $row['identifiant'];
        } else {
            $result = null;
        }

        return $result;
    }

    public static function getByIdentifiant($identifiant)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `utilisateur` 
                      WHERE `identifiant` = :identifiant;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":identifiant", $identifiant, PDO::PARAM_STR, 50);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        if ($sth->rowCount() == 1) {
            // si on a un résultat
            $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);
            $object = $sth->fetch();
        } else {
            // si erreur d'identifiant ou de mot de passe
            $object = null;
        }

        return $object;
    }

    public static function getByIdentifiantMotdepasse($identifiant, $motdepasse)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `utilisateur` 
                      WHERE `identifiant` = :identifiant
                      AND   `motdepasse` = :motdepasse
                      AND   (
                          `nb_erreurs` < 3 
                          OR DATE_ADD(derniere_erreur, INTERVAL 3 MINUTE) < NOW()
                      );";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":identifiant", $identifiant, PDO::PARAM_STR, 50);
        $sth->bindParam(":motdepasse", $motdepasse, PDO::PARAM_STR, 40);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        if ($sth->rowCount() == 1) {
            // si on a un résultat
            $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);
            $object = $sth->fetch();
        } else {
            // si erreur d'identifiant ou de mot de passe
            $object = null;
        }

        return $object;
    }

    public static function getByIdentifiantJeton($identifiant, $jeton_activation)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `utilisateur` 
                      WHERE `identifiant` = :identifiant
                      AND   `jeton_activation` = :jeton_activation;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":identifiant", $identifiant, PDO::PARAM_STR, 50);
        $sth->bindParam(":jeton_activation", $jeton_activation, PDO::PARAM_STR, 256);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        if ($sth->rowCount() == 1) {
            // si on a un résultat
            $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);
            $object = $sth->fetch();
        } else {
            // si erreur d'identifiant ou de mot de passe
            $object = null;
        }

        return $object;
    }

    public static function getByIdentifiantEmail($identifiant, $email)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `utilisateur` 
                      WHERE `identifiant` = :identifiant
                      AND   `email` = :email;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":identifiant", $identifiant, PDO::PARAM_STR, 50);
        $sth->bindParam(":email", $email, PDO::PARAM_STR, 250);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        if ($sth->rowCount() == 1) {
            // si on a un résultat
            $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);
            $object = $sth->fetch();
        } else {
            // si erreur d'identifiant ou de mot de passe
            $object = null;
        }

        return $object;
    }

    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `utilisateur`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $object = $sth->fetch();

        $type = TypeUtilisateur::get($object->id_type_utilisateur);

        if (isset(Utilisateur::SUBCLASSES[$type->slug])) {
            $class = Utilisateur::SUBCLASSES[$type->slug];
            $object = $class::get($id);
        }

        return $object;
    }

    public static function getIdByNomPrenom($nom, $prenom) 
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `id`
                      FROM `utilisateur`
                      WHERE `nom` = :nom AND `prenom` = :prenom ;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":nom", $nom, PDO::PARAM_STR);
        $sth->bindParam(":penom", $prenom, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $val = $sth->fetch();

        return $val;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `utilisateur`
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByFonction($fonction)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `utilisateur`
                      WHERE `fonction` = :fonction
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":fonction", $fonction);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        switch ($fonction) {
            case 'Proviseur':
                $sth->setFetchMode(PDO::FETCH_CLASS, Etablissement::class);
            break;
            case 'Enseignant':
                $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);
            break;
            case 'Etudiant':
                $sth->setFetchMode(PDO::FETCH_CLASS, Etudiant::class);
            break;
            default:
                $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);
            break;
        }

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByGroupe($id_groupe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `utilisateur`.*
                      FROM `utilisateur`
                      INNER JOIN `utilisateur_groupe` ON `id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_groupe` = :id_groupe
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_groupe", $id_groupe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByTypeUtilisateur($id_type_utilisateur)
    {
        $type = TypeUtilisateur::get($id_type_utilisateur);
        
        if (isset(Utilisateur::SUBCLASSES[$type->slug])) {
            $class = Utilisateur::SUBCLASSES[$type->slug];
        } else {
            $class = Utilisateur::class;
        }

        return $class::getAll();
    }

    public static function getAllByLettre($lettre)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `utilisateur`
                      WHERE LEFT(`nom`, 1) = :lettre
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":lettre", $lettre, PDO::PARAM_STR_CHAR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByTypeLettre($type, $lettre)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `utilisateur`.*
                      FROM `utilisateur`
                      INNER JOIN `type_utilisateur` ON `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id`
                      WHERE LEFT(`utilisateur`.`nom`, 1) = :lettre
                      AND   `slug` = :type
                      ORDER BY `utilisateur`.`nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":lettre", $lettre, PDO::PARAM_STR_CHAR);
        $sth->bindParam(":type", $type, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getLettres()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT DISTINCT LEFT(`utilisateur`.`nom`, 1) as lettre 
                      FROM `utilisateur` 
                      ORDER BY 1;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getLettresByType($type)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT DISTINCT LEFT(`utilisateur`.`nom`, 1) as lettre 
                      FROM `utilisateur`
                      INNER JOIN `type_utilisateur` ON `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id`
                      WHERE `slug` = :type
                      ORDER BY 1;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":type", $type);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getPremiereLettre()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT DISTINCT LEFT(`utilisateur`.`nom`, 1) as lettre 
                      FROM `utilisateur` 
                      ORDER BY 1
                      LIMIT 1;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $row = $sth->fetch();

        return $row['lettre'];
    }

    public static function getPremiereLettreByType($type)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT DISTINCT LEFT(`utilisateur`.`nom`, 1) as lettre 
                      FROM `utilisateur` 
                      INNER JOIN `type_utilisateur` ON `utilisateur`.`id_type_utilisateur` = `type_utilisateur`.`id`
                      WHERE `slug` = :type
                      ORDER BY 1
                      LIMIT 1;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":type", $type);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $row = $sth->fetch();

        if (isset($row['lettre'])) {
            $result = $row['lettre'];
        } else {
            $result = false;
        }

        return $result;
    }

    public static function getAllByNom($nom){

        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `utilisateur` 
                      WHERE `nom` LIKE :pattern
                      ORDER BY prenom;";

        $pattern = "{$nom}%";
        $sth = $cnx->prepare($statement);
        $sth->bindParam(':pattern', $pattern, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Utilisateur::class);

        $array = $sth->fetchAll();


        return $array;
    }

    public static function update($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `identifiant` = :identifiant,
                        `motdepasse` = :motdepasse,
                        `nom` = :nom,
                        `prenom` = :prenom,
                        `fonction` = :fonction,
                        `telephone` = :telephone,
                        `email` = :email,
                        `active` = :active,
                        `jeton_activation` = :jeton_activation
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $utilisateur->id, PDO::PARAM_INT);
        $sth->bindParam(":identifiant", $utilisateur->identifiant, PDO::PARAM_STR, 50);
        $sth->bindParam(":motdepasse", $utilisateur->motdepasse, PDO::PARAM_STR, 40);
        $sth->bindParam(":nom", $utilisateur->nom, PDO::PARAM_STR, 50);
        $sth->bindParam(":prenom", $utilisateur->prenom, PDO::PARAM_STR, 50);
        $sth->bindParam(":fonction", $utilisateur->fonction, PDO::PARAM_STR, 50);
        $sth->bindParam(":telephone", $utilisateur->telephone, PDO::PARAM_STR, 20);
        $sth->bindParam(":email", $utilisateur->email, PDO::PARAM_STR, 250);
        $sth->bindParam(":active", $utilisateur->active, PDO::PARAM_INT);
        $sth->bindParam(":jeton_activation", $utilisateur->jeton_activation, PDO::PARAM_STR, 256);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function insert($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `utilisateur`
                      (`identifiant`, `nom`, `prenom`, `email`, `jeton_activation`, `id_type_utilisateur`)
                      VALUES
                      (:identifiant, :nom, :prenom, :email, :jeton_activation, :id_type_utilisateur);";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":identifiant", $utilisateur->identifiant, PDO::PARAM_STR, 50);
        $sth->bindParam(":nom", $utilisateur->nom, PDO::PARAM_STR, 50);
        $sth->bindParam(":prenom", $utilisateur->prenom, PDO::PARAM_STR, 50);
        $sth->bindParam(":email", $utilisateur->email, PDO::PARAM_STR, 250);
        $sth->bindParam(":jeton_activation", $utilisateur->jeton_activation, PDO::PARAM_STR, 256);
        $sth->bindParam(":id_type_utilisateur", $utilisateur->id_type_utilisateur, PDO::PARAM_INT);
        
        $result = $sth->execute();
        $utilisateur->id = $cnx->lastInsertId();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        // add the default group
        Utilisateur::addGroupe($utilisateur->id, $utilisateur->typeUtilisateur->groupe->id);

        return $utilisateur->id;
    }

    public static function update_my_password($id, $motdepasse)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `motdepasse` = :motdepasse,
                        `jeton_activation` = NULL
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":motdepasse", $motdepasse, PDO::PARAM_STR, 40);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function login($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `ip` = :ip,
                        `connexion` = NOW(),
                        `nb_erreurs` = 0,
                        `derniere_erreur` = NULL
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $utilisateur->id, PDO::PARAM_INT);
        $sth->bindParam(":ip", $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR, 39);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function loginError($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `ip` = :ip,
                        `nb_erreurs` = :nb_erreurs,
                        `derniere_erreur` = NOW()
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $utilisateur->id, PDO::PARAM_INT);
        $sth->bindParam(":nb_erreurs", $utilisateur->nb_erreurs, PDO::PARAM_INT);
        $sth->bindParam(":ip", $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR, 39);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function resetErrors($id)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `nb_erreurs` = 0,
                        `derniere_erreur` = NULL
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function active($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `active` = 1
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $utilisateur->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function desactive($utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `active` = 0
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $utilisateur->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function addGroupe($id_utilisateur, $id_groupe)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `utilisateur_groupe` 
                      (`id_utilisateur`, `id_groupe`)
                      VALUES
                      (:id_utilisateur, :id_groupe);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $sth->bindParam(":id_groupe", $id_groupe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function removeGroupe($id_utilisateur, $id_groupe)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `utilisateur_groupe` 
                      WHERE `id_utilisateur` = :id_utilisateur
                      AND   `id_groupe` = :id_groupe;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $sth->bindParam(":id_groupe", $id_groupe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function setJetonActivation($id, $jeton)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `utilisateur`
                      SET
                        `jeton_activation` = :jeton
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":jeton", $jeton, PDO::PARAM_STR, 256);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function delete($id)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `utilisateur`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}