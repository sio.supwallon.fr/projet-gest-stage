<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Pays;
use GEST_STAGE\kernel\View;
use PDO;

class PaysDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `pays`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Pays::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `pays`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Pays::class);

        $array = $sth->fetchAll();

        return $array;
    }
}