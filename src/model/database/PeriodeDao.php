<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Periode;
use GEST_STAGE\kernel\View;
use PDO;

class PeriodeDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `periode` 
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Periode::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `periode`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Periode::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByConvention($id_convention)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `periode`
                      WHERE `id_convention` = :id_convention;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_convention", $id_convention, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Periode::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function insert($debut, $fin, $id_tuteur, $periode_lieu, $id_convention)
    {
        $cnx = Database::get_connection();

        // *********************************************
        // insertion de la période dans la table période
        // *********************************************

        $requete = "INSERT INTO periode (debut, fin, lieu, id_tuteur, id_convention)
                    VALUE (:debut, :fin, :lieu, :tuteur, :convention);";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":debut", $debut, PDO::PARAM_STR, 10);
        $sth->bindParam(":fin", $fin, PDO::PARAM_STR, 10);
        $sth->bindParam(":lieu", $periode_lieu, PDO::PARAM_STR, 200);
        $sth->bindParam(":tuteur", $id_tuteur, PDO::PARAM_INT);
        $sth->bindParam(":convention", $id_convention, PDO::PARAM_INT);


        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function deleteByConvention($id_convention)
    {
        $cnx = Database::get_connection();

        // ***********************************************
        // suppression de la période dans la table période
        // ***********************************************

        $requete = "DELETE FROM periode
                    WHERE id_convention = :convention;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":convention", $id_convention, PDO::PARAM_INT);


        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function unlockPeriode ($id_convention)
    {
        $cnx = Database::get_connection();

        // *************************************************
        // remplacement des signatures par NULL dans periode
        // *************************************************

        $requete = "UPDATE periode
                    SET signature_tuteur = NULL
                    WHERE id_convention=:id_convention;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id_convention", $id_convention, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function signer_tuteurPeriode($id_convention,$id_user)
    {
        $cnx = Database::get_connection();

        // *************************************************
        // remplacement des signatures par NULL dans periode
        // *************************************************

        $requete = "UPDATE periode
                    SET signature_tuteur = NOW()
                    WHERE id_convention=:id_convention
                    AND id_tuteur = :id_user;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id_convention", $id_convention, PDO::PARAM_INT);
        $sth->bindParam(":id_user", $id_user, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}