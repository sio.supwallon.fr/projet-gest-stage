<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\kernel\View;
use PDO;
use PDOException;

class Database
{
    private static $_driver   = "mysql";
    private static $_host     = "host=localhost;";
    private static $_port     = "port=3306;";
    private static $_charset  = "charset=utf8;";
    private static $_database = "dbname=gest_stage;";
    private static $_user     = "gest_stage";
    private static $_password = "c7osTvS3OqtHGbEX";

    private static $_dsn;
    private static $_connection;
    
    public static function get_connection()
    {
        if(!isset(self::$_connection)) {
            self::$_dsn = self::$_driver . ":" 
                        . self::$_host 
                        . self::$_port 
                        . self::$_charset 
                        . self::$_database;
            try {
                self::$_connection = new PDO(self::$_dsn, self::$_user, self::$_password);
            } catch (PDOException $e) {
                echo $e->getMessage();
                var_dump($e->errorInfo);
                View::setTemplate('error-db-connection');
                View::display();
                die();
            }
        }
        return self::$_connection;
    }
}