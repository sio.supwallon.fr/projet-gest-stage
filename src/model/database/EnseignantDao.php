<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Enseignant;
use GEST_STAGE\kernel\View;
use PDO;

class EnseignantDao extends UtilisateurDao
{
    public static function get($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `enseignant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `enseignant`
                      INNER JOIN `utilisateur` ON `enseignant`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `enseignant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `enseignant`
                      INNER JOIN `utilisateur` ON `enseignant`.`id_utilisateur` = `utilisateur`.`id`
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByClasse($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `enseignant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `enseignant`
                      INNER JOIN `utilisateur` ON `enseignant`.`id_utilisateur` = `utilisateur`.`id`
                      INNER JOIN `enseignant_classe` ON `id_enseignant` = `enseignant`.`id_utilisateur`
                      WHERE `id_classe` = :id_classe
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByClasseNonAffectes($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `enseignant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `enseignant`
                      INNER JOIN `utilisateur` ON `enseignant`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `enseignant`.`id` NOT IN (
                          SELECT `id_enseignant`
                          FROM `enseignant_classe`
                          WHERE `id_classe` = :id_classe
                      )
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllBySection($id_section)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `enseignant`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `enseignant`
                      INNER JOIN `utilisateur` ON `enseignant`.`id_utilisateur` = `utilisateur`.`id`
                      INNER JOIN `enseignant_classe` ON `id_enseignant` = `enseignant`.`id_utilisateur`
                      INNER JOIN `classe` ON `id_classe` = `classe`.`id`
                      WHERE `id_section` = :id_section
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_section", $id_section, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Enseignant::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function update($enseignant)
    {
        parent::update($enseignant);

        if (isset($enseignant->signature)) {

            $cnx = Database::get_connection();

            $statement = "UPDATE `enseignant`
                          SET 
                            `signature` = :signature
                          WHERE `id_utilisateur` = :id_utilisateur;";
    
            $sth = $cnx->prepare($statement);
            $sth->bindParam(":signature", $enseignant->signature, PDO::PARAM_STR, 50);
            $sth->bindParam(":id_utilisateur", $enseignant->id_utilisateur, PDO::PARAM_INT);
            $result = $sth->execute();
    
            if (! $result)
            {
                $errorInfo = $sth->errorInfo();
                View::setTemplate('error-db-statement');
                View::bindParam("errorInfo", $errorInfo);
                View::display();
                die();
            }
        }
    }

    public static function insert($enseignant)
    {
        parent::insert($enseignant);
        $enseignant->id_utilisateur = $enseignant->id;

        $cnx = Database::get_connection();

        $statement = "INSERT INTO `enseignant`
                      (`id_utilisateur`)
                      VALUES
                      (:id_utilisateur);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $enseignant->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}