<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Etablissement;
use GEST_STAGE\kernel\View;
use PDO;

class EtablissementDao extends UtilisateurDao
{
    public static function get($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etablissement`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etablissement`
                      INNER JOIN `utilisateur` ON `etablissement`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etablissement::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etablissement`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etablissement`
                      INNER JOIN `utilisateur` ON `etablissement`.`id_utilisateur` = `utilisateur`.`id`
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etablissement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByVille($id_ville)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etablissement`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etablissement`
                      INNER JOIN `utilisateur` ON `etablissement`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `id_ville` = :id_ville
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_ville", $id_ville, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etablissement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByFonction($fonction)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `etablissement`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `etablissement`
                      INNER JOIN `utilisateur` ON `etablissement`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `fonction` = :fonction
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":fonction", $fonction, PDO::PARAM_STR, 50);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Etablissement::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function update($etablissement)
    {
        parent::update($etablissement);

        if (isset($etablissement->signature)) {

            $cnx = Database::get_connection();

            $statement = "UPDATE `etablissement`
                          SET 
                            `signature` = :signature
                          WHERE `id_utilisateur` = :id_utilisateur;";
    
            $sth = $cnx->prepare($statement);
            $sth->bindParam(":signature", $etablissement->signature, PDO::PARAM_STR, 50);
            $sth->bindParam(":id_utilisateur", $etablissement->id_utilisateur, PDO::PARAM_INT);
            $result = $sth->execute();
    
            if (! $result)
            {
                $errorInfo = $sth->errorInfo();
                View::setTemplate('error-db-statement');
                View::bindParam("errorInfo", $errorInfo);
                View::display();
                die();
            }
        }
    }

    public static function insert($etablissement)
    {
        parent::insert($etablissement);
        $etablissement->id_utilisateur = $etablissement->id;

        $cnx = Database::get_connection();

        $statement = "INSERT INTO `etablissement`
                      (`id_utilisateur`)
                      VALUES
                      (:id_utilisateur);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $etablissement->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}