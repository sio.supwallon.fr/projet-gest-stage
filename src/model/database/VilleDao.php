<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Ville;
use GEST_STAGE\kernel\View;
use PDO;

class VilleDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `ville`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getIdByNom($ville)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `id`
                      FROM `ville`
                      WHERE `nom` = :ville;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":ville", $ville, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $val = $sth->fetch();

        return $val;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `ville`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByDepartement($id_departement)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `ville`
                      WHERE `id_departement` = :id_departement
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(':id_departement', $id_departement, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByRegion($id_region)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `ville`.* 
                      FROM `ville`
                      INNER JOIN `departement` ON `id_departement` = `departement`.`id`
                      WHERE `id_region` = :id_region
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(':id_region', $id_region, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByPays($id_pays)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `ville`.* 
                      FROM `ville`
                      INNER JOIN `departement` ON `id_departement` = `departement`.`id`
                      INNER JOIN `region` ON `id_region` = `region`.`id`
                      WHERE `id_pays` = :id_pays
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_pays", $id_pays, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByCodePosal($code_postal)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `ville`
                      WHERE `code_postal` LIKE :pattern
                      ORDER BY `nom`;";

        $pattern = "{$code_postal}%";
        $sth = $cnx->prepare($statement);
        $sth->bindParam(':pattern', $pattern, PDO::PARAM_STR, 6);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Ville::class);

        $array = $sth->fetchAll();

        return $array;
    }
}