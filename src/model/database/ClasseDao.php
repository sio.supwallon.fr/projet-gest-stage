<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Classe;
use GEST_STAGE\kernel\View;
use PDO;

class ClasseDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `classe` WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Classe::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `classe`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Classe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllBySection($id_section)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `classe`
                      WHERE `id_section` = :id_section
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_section", $id_section, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Classe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEnseignant($id_enseignant)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `classe`.* 
                      FROM `classe`
                      INNER JOIN `enseignant_section` ON `enseignant_section`.`id_section` = `classe`.`id_section`
                      WHERE `id_enseignant` = :id_enseignant
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_enseignant", $id_enseignant, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Classe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByAccompagnement($id_accompagnement)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `classe`.*
                      FROM `classe`
                      INNER JOIN `classe_accompagnement` ON `id_classe` = `classe`.`id`
                      WHERE `id_accompagnement` = :id_accompagnement;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_accompagnement", $id_accompagnement, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Classe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function delete($id)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `classe`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function insert($classe)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `classe`
                      (`nom`, `slug`, `id_section`)
                      VALUES
                      (:nom, :slug, :id_section);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":nom", $classe->nom, PDO::PARAM_STR, 20);
        $sth->bindParam(":slug", $classe->slug, PDO::PARAM_STR, 20);
        $sth->bindParam(":id_section", $classe->id_section, PDO::PARAM_INT);
        $result = $sth->execute();
        $classe->id = $cnx->lastInsertId();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function update($classe)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `classe`
                      SET
                        `nom` = :nom,
                        `slug` = :slug,
                        `id_section` = :id_section
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":nom", $classe->nom, PDO::PARAM_STR, 20);
        $sth->bindParam(":slug", $classe->slug, PDO::PARAM_STR, 20);
        $sth->bindParam(":id_section", $classe->id_section, PDO::PARAM_INT);
        $sth->bindParam(":id", $classe->id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function addAccompagnement($id_classe, $id_accompagnement)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `classe_accompagnement`
                      (`id_classe`, `id_accompagnement`)
                      VALUES
                      (:id_classe, :id_accompagnement);";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        $sth->bindParam(":id_accompagnement", $id_accompagnement, PDO::PARAM_INT);
        
        $result = $sth->execute();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function clearAccompagnement($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `classe_accompagnement`
                      WHERE `id_classe` = :id_classe;";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        
        $result = $sth->execute();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function addEnseignant($id_classe, $id_enseignant)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `enseignant_classe`
                      (`id_enseignant`, `id_classe`)
                      VALUES
                      (:id_enseignant, :id_classe);";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":id_enseignant", $id_enseignant, PDO::PARAM_INT);
        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        
        $result = $sth->execute();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function clearEnseignant($id_classe)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `enseignant_classe`
                      WHERE `id_classe` = :id_classe;";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":id_classe", $id_classe, PDO::PARAM_INT);
        
        $result = $sth->execute();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}