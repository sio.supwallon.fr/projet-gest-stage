<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Entreprise;
use GEST_STAGE\kernel\View;
use PDO;

class EntrepriseDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT * 
                    FROM `entreprise` 
                    WHERE `id` = :id;";

        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $requete = "SELECT *
                    FROM  `entreprise`
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEntrepriseSiege($id_siege)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT *
                    FROM  `entreprise`
                    WHERE `id_siege` = :id_siege
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_siege", $id_siege, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByVille($id_ville)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT *
                    FROM  `entreprise`
                    WHERE `id_ville` = :id_ville
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_ville", $id_ville, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByDepartement($id_departement)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT `entreprise`.*
                    FROM  `entreprise`
                    INNER JOIN `ville` ON `id_ville` = `ville`.`id`
                    WHERE `id_departement` = :id_departement
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_departement", $id_departement, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByRegion($id_region)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT `entreprise`.*
                    FROM  `entreprise`
                    INNER JOIN `ville` ON `id_ville` = `ville`.`id`
                    INNER JOIN `departement` ON `id_departement` = `departement`.`id`
                    WHERE `id_region` = :id_region
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_region", $id_region, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByPays($id_pays)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT `entreprise`.*
                    FROM  `entreprise`
                    INNER JOIN `ville` ON `id_ville` = `ville`.`id`
                    INNER JOIN `departement` ON `id_departement` = `departement`.`id`
                    INNER JOIN `region` ON `id_region` = `region`.`id`
                    WHERE `id_pays` = :id_pays
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_pays", $id_pays, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllBySection($id_section)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT DISTINCT `entreprise`.*
                    FROM  `entreprise`
                    INNER JOIN `convention` ON `id_entreprise` = `entreprise`.`id`
                    INNER JOIN `classe` ON `id_classe` = `classe`.`id`
                    WHERE `id_section` = :id_section
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_section", $id_section, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByNaf($id_naf)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT `entreprise`.*
                    FROM  `entreprise`
                    WHERE `id_naf` = :id_naf
                    ORDER BY `raison_sociale`;";
        
        $sth = $cnx->prepare($requete);
        $sth->bindParam(":id_naf", $id_naf, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllBySiege($id_entreprise)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `entreprise` 
                      WHERE `id_siege` = :entreprise
                      ORDER BY raison_sociale;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":entreprise", $id_entreprise, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();


        return $array;
    }

    public static function getEntrepriseByFiltre($id_region, $id_departement, $id_ville, $code_naf, $section){

        $cnx = Database::get_connection();
    
        $requete = "SELECT `entreprise`.*
                    FROM  `entreprise`
                    LEFT JOIN naf ON entreprise.id_naf = naf.id
                    LEFT JOIN `convention` ON `id_entreprise` = `entreprise`.`id`
                    LEFT JOIN `classe` ON `id_classe` = `classe`.`id`
                    INNER JOIN ville ON entreprise.id_ville = ville.id
                    INNER JOIN departement ON ville.id_departement = departement.id
                    WHERE 1 ";
    
        if($id_region != null){
            $requete .= "AND departement.id_region = :id_region ";
        }
    
        if($id_departement != null){
                $requete .= "AND ville.id_departement = :id_departement ";
        }
    
        if($id_ville != null){
                $requete .= "AND entreprise.id_ville = :id_ville ";
        }
    
        if($code_naf != null){
                $requete .= "AND naf.code = :code_naf ";
        }

        if($section != null){
            $requete .= "AND classe.id_section = :id_section ";
        }
    
        $reqFinal = $requete . ";";
        
        $sth = $cnx->prepare($reqFinal);
        if($id_region != null){
            $sth->bindParam(":id_region", $id_region, PDO::PARAM_INT);
        }
        if($id_departement != null){
            $sth->bindParam(":id_departement", $id_departement, PDO::PARAM_INT);
        }
        if($id_ville != null){
            $sth->bindParam(":id_ville", $id_ville, PDO::PARAM_INT);
        }
        if($code_naf != null){
            $sth->bindParam(":code_naf", $code_naf, PDO::PARAM_STR,5);
        } 
        if($section != null){
            $sth->bindParam(":id_section", $section, PDO::PARAM_INT);
        }
    
        $result = $sth->execute();
    
        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    
        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);
    
        $array = $sth->fetchAll();
    
        return $array;
    }

    public static function update($entreprise)
    {
        $cnx = Database::get_connection();

        $statement = "UPDATE `entreprise`
                      SET
                        `raison_sociale` = :raisonSocial,
                        `adresse` = :adresse,
                        `telephone` = :telephone,
                        `email` = :email,
                        `id_naf` = :naf,
                        `id_ville` = :ville,
                        `id_responsable` = :resp,
                        `est_siege` = :estSiege,
                        `id_siege` = :siege,
                        `maps` = :gmaps
                        WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":id", $entreprise->id, PDO::PARAM_INT);
        $sth->bindParam(":raisonSocial", $entreprise->raison_sociale, PDO::PARAM_STR, 100);
        $sth->bindParam(":adresse", $entreprise->adresse, PDO::PARAM_STR, 255);
        $sth->bindParam(":telephone", $entreprise->telephone, PDO::PARAM_STR, 20);
        $sth->bindParam(":email", $entreprise->email, PDO::PARAM_STR, 25);
        $sth->bindParam(":naf", $entreprise->id_naf, PDO::PARAM_INT);
        $sth->bindParam(":ville", $entreprise->id_ville, PDO::PARAM_INT);
        $sth->bindParam(":resp", $entreprise->id_responsable, PDO::PARAM_INT);
        $sth->bindParam(":estSiege", $entreprise->est_siege, PDO::PARAM_INT);
        $sth->bindParam(":siege", $entreprise->id_siege, PDO::PARAM_INT);
        $sth->bindParam(":gmaps", $entreprise->maps, PDO::PARAM_STR);
        

        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function insert($entreprise)
    {
        $cnx = Database::get_connection();

        $statement = "INSERT INTO `entreprise`
                      (`raison_sociale`, `adresse`, `telephone`, `email`, `est_siege`, `id_naf`, `id_ville`, `id_responsable`, `id_siege` , `maps`)
                      VALUES
                      (:raison_sociale, :adresse, :telephone, :email, :est_siege, :id_naf, :id_ville, :id_responsable, :id_siege, :maps);";

        $sth = $cnx->prepare($statement);

        $sth->bindParam(":raison_sociale", $entreprise->raison_sociale, PDO::PARAM_STR, 100);
        $sth->bindParam(":adresse", $entreprise->adresse, PDO::PARAM_STR, 255);
        $sth->bindParam(":telephone", $entreprise->telephone, PDO::PARAM_STR, 20);
        $sth->bindParam(":email", $entreprise->email, PDO::PARAM_STR, 250);
        $sth->bindParam(":est_siege", $entreprise->est_siege, PDO::PARAM_INT);
        $sth->bindParam(":id_naf", $entreprise->id_naf, PDO::PARAM_INT);
        $sth->bindParam(":id_ville", $entreprise->id_ville, PDO::PARAM_INT);
        $sth->bindParam(":id_responsable", $entreprise->id_responsable, PDO::PARAM_INT);
        $sth->bindParam(":id_siege", $entreprise->id_siege, PDO::PARAM_INT);
        $sth->bindParam(":maps", $entreprise->maps, PDO::PARAM_STR, 350);
        
        
        $result = $sth->execute();
        $entreprise->id = $cnx->lastInsertId();
        
        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        return $entreprise->id;
    }

    public static function delete($id)
    {
        $cnx = Database::get_connection();

        $statement = "DELETE FROM `entreprise`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function getByRaisonSociale($RaisonSocial)
    {
        $cnx = Database::get_connection();

        $requete = "SELECT `id` 
                    FROM `entreprise` 
                    WHERE `raison_sociale` = :raisonSociale;";

        $sth = $cnx->prepare($requete);
        $sth->bindParam(":raisonSociale", $RaisonSocial, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $val = $sth->fetch();

        return $val;
    }

    public static function getAllByEstSiege()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `entreprise` 
                      WHERE `est_siege` = 1
                      ORDER BY raison_sociale;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Entreprise::class);

        $array = $sth->fetchAll();


        return $array;
    }

    
}

