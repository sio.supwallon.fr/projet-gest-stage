<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Section;
use GEST_STAGE\kernel\View;
use PDO;

class SectionDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `section`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Section::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `section`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Section::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEnseignant($id_enseignant)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `section`.* 
                      FROM `section`
                      INNER JOIN `enseignant_section` ON `id_section` = `section`.`id`
                      WHERE `id_enseignant` = :id_enseignant
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_enseignant", $id_enseignant, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Section::class);

        $array = $sth->fetchAll();

        return $array;
    }
}