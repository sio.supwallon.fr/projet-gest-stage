<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Groupe;
use GEST_STAGE\kernel\View;
use PDO;

class GroupeDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `groupe` WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Groupe::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getByNom($nom)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * FROM `groupe` WHERE `nom` = :nom;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":nom", $nom, PDO::PARAM_STR, 50);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Groupe::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `groupe`
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Groupe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByUtilisateur($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `groupe`.* 
                      FROM `groupe`
                      INNER JOIN `utilisateur_groupe` ON `groupe`.`id` = `id_groupe`
                      WHERE `id_utilisateur` = :id_utilisateur
                      ORDER BY `nom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Groupe::class);

        $array = $sth->fetchAll();

        return $array;
    }
}