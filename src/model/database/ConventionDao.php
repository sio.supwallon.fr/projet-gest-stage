<?php
namespace GEST_STAGE\model\database;

use Exception;
use GEST_STAGE\model\classes\Convention;
use GEST_STAGE\model\classes\Periode;
use GEST_STAGE\kernel\View;
use PDO;

class ConventionDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `convention` 
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `convention`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEmployeTuteur($id_tuteur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `convention`.* 
                      FROM `convention`
                      INNER JOIN `periode` ON `id_convention` = `convention`.`id`
                      WHERE `id_tuteur` = :id_tuteur
                      AND `signature_enseignant` IS NOT NULL;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_tuteur", $id_tuteur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEmployeResponsable($id_responsable)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `convention`.* 
                      FROM `convention`
                      INNER JOIN `entreprise` ON `id_entreprise` = `entreprise`.`id`
                      WHERE `id_responsable` = :id_responsable
                      AND `signature_enseignant` IS NOT NULL;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_responsable", $id_responsable, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEnseignantReferent($id_referent)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `convention`.* 
                      FROM `convention`
                      INNER JOIN `classe` ON `convention`.`id_classe` = `classe`.`id`
                      WHERE `convention`.`id_enseignant` = :id_enseignant
                      OR (
                          `signature_enseignant` IS NULL
                          AND `signature_etudiant` IS NOT NULL
                          AND `classe`.`id_referent` = :id_referent
                      );";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_referent", $id_referent, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEtablissement()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `convention`.* 
                      FROM `convention`
                      WHERE `signature_responsable` IS NOT NULL
                      AND NOT EXISTS(
                          SELECT *
                          FROM `periode` 
                          WHERE `id_convention` = `convention`.`id`
                          AND `signature_tuteur` IS NULL
                      );";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEtat($id_etat)
    {
        $cnx = Database::get_connection();
        $class = __CLASS__;
        $method = __METHOD__;

        switch($id_etat) {
            case 1: // en préparation
                $statement = "SELECT * 
                FROM `convention`
                WHERE `signature_etudiant` IS NULL;";
            break;
            case 2: // validé par l'étudiant
                $statement = "SELECT * 
                FROM `convention`
                WHERE `signature_etudiant` IS NOT NULL
                AND   `signature_enseignant` IS NULL;";
            break;
            case 3: // validé par l'enseignant référent
                $statement = "SELECT * 
                FROM `convention`
                WHERE `signature_enseignant` IS NOT NULL
                AND ( `signature_responsable` IS NULL
                OR EXISTS (
                    SELECT *
                    FROM `periode`
                    WHERE `id_convention` = `convention`.`id`
                    AND   `signature_tuteur` IS NULL
                ));";
            break;
            case 4: // validé par l'entreprise (responsable + tous les tuteurs)
                $statement = "SELECT * 
                FROM `convention`
                WHERE `signature_responsable` IS NOT NULL
                AND NOT EXISTS (
                    SELECT *
                    FROM `periode`
                    WHERE `id_convention` = `convention`.`id`
                    AND   `signature_tuteur` IS NULL
                )
                AND   `signature_etablissement` IS NULL;";
            break;
            case 5: // validé par l'établissement
                $statement = "SELECT * 
                FROM `convention`
                WHERE `signature_etablissement` IS NOT NULL;";
            break;
            default:
                throw new Exception("Unknown state $id_etat in {$class}::{$method}()");
            break;
        }

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEtudiant($id_etudiant)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `convention`
                      WHERE `id_etudiant` = :id_etudiant;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_etudiant", $id_etudiant, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Convention::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function insertConvention (
        $lst_entreprise, $entreprise_service_stage, 
        $entreprise_lieu_stage, $lst_classe, $lst_etudiant, $CPAM,  
        $stage_activite, $stage_competence, $stage_presence, 
        $stage_nb_jours, $condition_avantage1, $condition_avantage2,
        $condition_maladie, $condition_autorisation, $condition_cas,
        $debut_arr, $fin_arr, $id_tuteur_arr, $periode_lieu_arr)
    {
        $cnx = Database::get_connection();

        // ***************************************************
        // insertion d'une convention dans la table convention
        // ***************************************************

        $requete = "INSERT INTO convention (`service`, lieu, cpam, activites, competences, jours_particuliers, duree_jours, avantage1, avantage2, protection_maladie, jours_conge, 
                    nb_ects, id_etudiant, id_classe, id_entreprise)
                    VALUE (:service, :lieu, :cpam, :activites, :competences, :jours_particuliers, :duree_jours, :avantage1, :avantage2, :maladie, 
                    :jours_conge, :nb_etcs, :id_etudiant, :id_classe, :id_entreprise);";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":service", $entreprise_service_stage, PDO::PARAM_STR, 50);
        $sth->bindParam(":lieu", $entreprise_lieu_stage, PDO::PARAM_STR, 50);
        $sth->bindParam(":cpam", $CPAM, PDO::PARAM_STR, 200);
        $sth->bindParam(":activites", $stage_activite, PDO::PARAM_STR, 200);
        $sth->bindParam(":competences", $stage_competence, PDO::PARAM_STR, 200);
        $sth->bindParam(":jours_particuliers", $stage_presence, PDO::PARAM_STR, 50);
        $sth->bindParam(":duree_jours", $stage_nb_jours, PDO::PARAM_INT);
        $sth->bindParam(":avantage1", $condition_avantage1, PDO::PARAM_STR, 100);
        $sth->bindParam(":avantage2", $condition_avantage2, PDO::PARAM_STR, 100);
        $sth->bindParam(":maladie", $condition_maladie, PDO::PARAM_STR, 3);
        $sth->bindParam(":jours_conge", $condition_autorisation, PDO::PARAM_INT);
        $sth->bindParam(":nb_etcs", $condition_cas, PDO::PARAM_INT);
        $sth->bindParam(":id_etudiant", $lst_etudiant, PDO::PARAM_INT);
        $sth->bindParam(":id_classe", $lst_classe, PDO::PARAM_INT);
        $sth->bindParam(":id_entreprise", $lst_entreprise, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $id_convention = $cnx->lastInsertId();
        foreach($debut_arr as $key => $value){
            //creer la methode dao pour la modifier dans la bdd
            Periode::insert($debut_arr[$key], $fin_arr[$key], $id_tuteur_arr[$key], $periode_lieu_arr[$key], $id_convention);
        }
    }


    public static function updateEntreprise (
        $id, $entreprise_service_stage, $entreprise_lieu_stage, $lst_entreprise)
    {
        $cnx = Database::get_connection();

        // ***********************************************************
        // modification des info d'entreprise dans la table convention
        // ***********************************************************

        $requete = "UPDATE convention
                    SET `service` = :service,
                    lieu = :lieu,
                    id_entreprise = :id_entreprise
                    WHERE id = :id;";
        
        $sth = $cnx->prepare($requete);
        
        ($entreprise_service_stage) ? $entreprise_service_stage : null;
        ($entreprise_lieu_stage) ? $entreprise_lieu_stage : null;


        $sth->bindParam(":service", $entreprise_service_stage, PDO::PARAM_STR, 50);
        $sth->bindParam(":lieu", $entreprise_lieu_stage, PDO::PARAM_STR, 50);
        $sth->bindParam(":id_entreprise", $lst_entreprise, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function updateStagiaire (
        $id, $lst_classe, $lst_etudiant, $CPAM)
    {
        $cnx = Database::get_connection();

        // ***************************************************
        // insertion d'une convention dans la table convention
        // ***************************************************

        $requete = "UPDATE convention
                    SET id_etudiant = :id_etudiant,
                    id_classe = :id_classe,
                    cpam = :cpam
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id_etudiant", $lst_etudiant, PDO::PARAM_INT);
        $sth->bindParam(":id_classe", $lst_classe, PDO::PARAM_INT);
        $sth->bindParam(":cpam", $CPAM, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function updateInformation (
        $id, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours)
    {
        $cnx = Database::get_connection();

        // ***************************************************
        // insertion d'une convention dans la table convention
        // ***************************************************

        $requete = "UPDATE convention
                    SET activites = :activites,
                    competences = :competences,
                    jours_particuliers = :jours_particuliers,
                    duree_jours = :duree_jours
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":activites", $stage_activite, PDO::PARAM_STR, 200);
        $sth->bindParam(":competences", $stage_competence, PDO::PARAM_STR, 200);
        $sth->bindParam(":jours_particuliers", $stage_presence, PDO::PARAM_STR, 50);
        $sth->bindParam(":duree_jours", $stage_nb_jours, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function updateCondition (
        $id, $condition_avantage1, $condition_avantage2,$condition_maladie, $condition_autorisation, $condition_cas)
    {
        $cnx = Database::get_connection();

        // ***************************************************
        // insertion d'une convention dans la table convention
        // ***************************************************

        $requete = "UPDATE convention
                    SET avantage1 = :avantage1,
                    avantage2 = :avantage2,
                    protection_maladie = :maladie,
                    jours_conge = :jours_conge,
                    nb_ects = :nb_ects
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":avantage1", $condition_avantage1, PDO::PARAM_STR, 100);
        $sth->bindParam(":avantage2", $condition_avantage2, PDO::PARAM_STR, 100);
        $sth->bindParam(":maladie", $condition_maladie, PDO::PARAM_STR, 3);
        $sth->bindParam(":jours_conge", $condition_autorisation, PDO::PARAM_INT);
        $sth->bindParam(":nb_ects", $condition_cas, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function deleteConvention ($id_convention)
    {
        $cnx = Database::get_connection();

        // *****************************************************
        // suppression d'une convention dans la table convention
        // *****************************************************

        $requete = "DELETE FROM convention
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id_convention, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function unlockConvention ($id_convention)
    {
        $cnx = Database::get_connection();

        // ****************************************************
        // remplacement des signatures par NULL dans convention
        // ****************************************************

        $requete = "UPDATE convention
                    SET signature_etudiant = NULL, signature_enseignant = NULL, signature_responsable = NULL, signature_etablissement = NULL
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id_convention, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function signer_etudiantConvention($id)
    {
        $cnx = Database::get_connection();

        // ****************************************************
        // remplacement des signatures par NULL dans convention
        // ****************************************************

        $requete = "UPDATE convention
                    SET signature_etudiant = NOW()
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function signer_enseignantConvention($id)
    {
        $cnx = Database::get_connection();

        // ****************************************************
        // remplacement des signatures par NULL dans convention
        // ****************************************************

        $requete = "UPDATE convention
                    SET signature_enseignant = NOW()
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function signer_responsableConvention($id)
    {
        $cnx = Database::get_connection();

        // ****************************************************
        // remplacement des signatures par NULL dans convention
        // ****************************************************

        $requete = "UPDATE convention
                    SET signature_responsable = NOW()
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function signer_etablissementConvention($id)
    {
        $cnx = Database::get_connection();

        // ****************************************************
        // remplacement des signatures par NULL dans convention
        // ****************************************************

        $requete = "UPDATE convention
                    SET signature_etablissement = NOW()
                    WHERE id=:id;";
        
        $sth = $cnx->prepare($requete);
        // `id` = :id;
        // $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);

        $result = $sth->execute();

        if(! $result){
            $errorInfo = $sth->errorInfo();
            View::setTemplate("error-db-statement");
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}
