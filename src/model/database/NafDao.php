<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Naf;
use GEST_STAGE\kernel\View;
use PDO;

class NafDao
{
    public static function get($id)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `naf`
                      WHERE `id` = :id;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id", $id, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Naf::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT *
                      FROM `naf`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Naf::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByNom($code)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT * 
                      FROM `naf`
                      WHERE `code` LIKE :pattern
                      ORDER BY `code`;";

        $pattern = "{$code}%";
        $sth = $cnx->prepare($statement);
        $sth->bindParam(':pattern', $pattern, PDO::PARAM_STR, 6);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Naf::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getIdByNom($code)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `id`
                      FROM `naf`
                      WHERE `code` = :code;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":code", $code, PDO::PARAM_STR);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $val = $sth->fetch();

        return $val;
    }
}