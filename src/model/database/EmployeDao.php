<?php
namespace GEST_STAGE\model\database;

use GEST_STAGE\model\classes\Employe;
use GEST_STAGE\kernel\View;
use PDO;

class EmployeDao extends UtilisateurDao
{
    public static function get($id_utilisateur)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `employe`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `jeton_activation`, `ip`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `employe`
                      INNER JOIN `utilisateur` ON `employe`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `employe`.`id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Employe::class);

        $object = $sth->fetch();

        return $object;
    }

    public static function getAll()
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `employe`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `employe`
                      INNER JOIN `utilisateur` ON `employe`.`id_utilisateur` = `utilisateur`.`id`
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Employe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByEntreprise($id_entreprise)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `employe`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `employe`
                      INNER JOIN `utilisateur` ON `employe`.`id_utilisateur` = `utilisateur`.`id`
                      WHERE `employe`.`id_entreprise` = :id_entreprise
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_entreprise", $id_entreprise, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Employe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function getAllByConventionTuteur($id_convention)
    {
        $cnx = Database::get_connection();

        $statement = "SELECT `employe`.*, `id`, `identifiant`, `civilite`, `nom`, `prenom`, `fonction`, `telephone`, `email`, `active`, `ip`, `jeton_activation`, `connexion`, `nb_erreurs`, `derniere_erreur`, `id_type_utilisateur`
                      FROM `employe`
                      INNER JOIN `utilisateur` ON `employe`.`id_utilisateur` = `utilisateur`.`id`
                      INNER JOIN `periode` ON `periode`.`id_tuteur` = `utilisateur`.`id`
                      WHERE `periode`.`id_convention` = :id_convention
                      ORDER BY `nom`, `prenom`;";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_convention", $id_convention, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }

        $sth->setFetchMode(PDO::FETCH_CLASS, Employe::class);

        $array = $sth->fetchAll();

        return $array;
    }

    public static function update($employe)
    {
        parent::update($employe);

        $cnx = Database::get_connection();

        $statement = "UPDATE `employe`
                      SET \n";
if (isset($employe->signature)) {
        $statement .= " `signature` = :signature,\n";
}
        $statement .= " `id_entreprise` = :id_entreprise
                      WHERE `id_utilisateur` = :id_utilisateur;";

        $sth = $cnx->prepare($statement);
        if (isset($employe->signature)) {
            $sth->bindParam(":signature", $employe->signature, PDO::PARAM_STR, 50);
        }
        $sth->bindParam(":id_entreprise", $employe->id_entreprise, PDO::PARAM_INT);
        $sth->bindParam(":id_utilisateur", $employe->id_utilisateur, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }

    public static function insert($employe)
    {
        parent::insert($employe);
        $employe->id_utilisateur = $employe->id;

        $cnx = Database::get_connection();

        $statement = "INSERT INTO `employe`
                      (`id_utilisateur`, `id_entreprise`)
                      VALUES
                      (:id_utilisateur, :id_entreprise);";

        $sth = $cnx->prepare($statement);
        $sth->bindParam(":id_utilisateur", $employe->id, PDO::PARAM_INT);
        $sth->bindParam(":id_entreprise", $employe->id_entreprise, PDO::PARAM_INT);
        $result = $sth->execute();

        if (! $result)
        {
            $errorInfo = $sth->errorInfo();
            View::setTemplate('error-db-statement');
            View::bindParam("errorInfo", $errorInfo);
            View::display();
            die();
        }
    }
}