<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\kernel\Date;
use GEST_STAGE\model\database\PeriodeDao;

class Periode extends Entity
{
    public $id;
    public $debut;
    public $fin;
    public $lieu;
    public $signature_tuteur;
    public $id_tuteur;
    public $id_convention;

    protected $debut_fr;
    protected function get_debut_fr() 
    {
        return Date::toFr($this->debut);
    }

    protected $fin_fr;
    protected function get_fin_fr() 
    {
        return Date::toFr($this->fin);
    }

    protected $signature_tuteur_fr;
    protected function get_signature_tuteur_fr() 
    {
        return Date::toFr($this->signature_tuteur);
    }

    protected $tuteur;
    protected $convention;

    public function __construct()
    {
        $this->linkManyToOne("tuteur", Employe::class, "id_tuteur");
        $this->linkManyToOne("convention", Convention::class, "id_convention");
    }

    public static function get($id) 
    {
        return PeriodeDao::get($id);
    }

    public static function getAll() 
    {
        return PeriodeDao::getAll();
    }

    public static function getAllByConvention($id_convention)
    {
        return PeriodeDao::getAllByConvention($id_convention);
    }

    public static function insert($debut, $fin, $id_tuteur, $periode_lieu, $id_convention)
    {
        return PeriodeDao::insert($debut, $fin, $id_tuteur, $periode_lieu, $id_convention);
    }

    public static function deleteByConvention($id_convention)
    {
        return PeriodeDao::deleteByConvention($id_convention);
    }

    public static function unlockPeriode($id_convention)
    {
        return PeriodeDao::unlockPeriode($id_convention);
    }

    public static function signer_tuteurPeriode($id_convention,$id_user)
    {
        return PeriodeDao::signer_tuteurPeriode($id_convention,$id_user);
    }
}
