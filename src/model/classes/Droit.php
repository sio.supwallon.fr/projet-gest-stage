<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\DroitDao;

class Droit extends Entity
{
    public $id;
    public $nom;

    public static function get($id) 
    {
        return DroitDao::get($id);
    }

    public static function getAll() 
    {
        return DroitDao::getAll();
    }

    public static function getAllByGroupe($id_groupe)
    {
        return DroitDao::getAllByGroupe($id_groupe);
    }

    public static function getAllByUtilisateur($id_utilisateur)
    {
        return DroitDao::getAllByUtilisateur($id_utilisateur);
    }

    public function __toString()
    {
        return $this->nom;
    }
}