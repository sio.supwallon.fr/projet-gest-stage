<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\JourFerieDao;

class JourFerie extends Entity
{
    public $id;
    public $nom;
    public $date;

    public static function get($id) 
    {
        return JourFerieDao::get($id);
    }

    public static function getAll() 
    {
        return JourFerieDao::getAll();
    }

    public function __toString()
    {
        return $this->date;
    }
}