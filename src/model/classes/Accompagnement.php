<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\AccompagnementDao;

class Accompagnement extends Entity
{
    public $id;
    public $nom;
    public $checked;

    protected $classes;

    public function __construct()
    {
        $this->linkManyToOne("classes", Classe::class, "id");
    }

    public static function get($id) 
    {
        return AccompagnementDao::get($id);
    }

    public static function getAll() 
    {
        return AccompagnementDao::getAll();
    }

    public static function getAllByClasse($id_classe)
    {
        return AccompagnementDao::getAllByClasse($id_classe);
    }

    public static function getAllCheckedByClasse($id_classe)
    {
        return AccompagnementDao::getAllCheckedByClasse($id_classe);
    }

    public function __toString()
    {
        return $this->nom;
    }
}