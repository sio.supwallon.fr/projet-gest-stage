<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\RegionDao;

class Region extends Entity
{
    public $id;
    public $code_insee;
    public $nom;
    public $slug;
    public $id_pays;

    protected $pays;
    protected $departements;
    protected $villes;
    protected $entreprises;

    public function __construct()
    {
        $this->linkManyToOne("pays", Pays::class, "id_pays");
        $this->linkOneToMany("departements", Departement::class, "id");
        $this->linkOneToMany("villes", Ville::class, "id");
        $this->linkOneToMany("entreprises", Entreprise::class, "id");
    }

    public static function get($id) 
    {
        return RegionDao::get($id);
    }

    public static function getAll() 
    {
        return RegionDao::getAll();
    }

    public static function getAllByPays($id_pays)
    {
        return RegionDao::getAllByPays($id_pays);
    }
}