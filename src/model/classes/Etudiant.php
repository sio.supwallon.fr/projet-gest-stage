<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\kernel\Date;
use GEST_STAGE\model\database\EtudiantDao;

class Etudiant extends Utilisateur
{
    public $id_utilisateur;
    public $sexe;
    public $num_secu;
    public $date_naissance;
    public $adresse;
    public $signature;
    public $id_ville;
    public $id_ville_naissance;
    public $id_classe;

    protected $date_naissance_fr;
    protected function get_date_naissance_fr() 
    {
        return Date::toFr($this->date_naissance);
    }

    protected $num_secu_fr;
    protected function get_num_secu_fr() 
    {
        $sexe = substr($this->num_secu, 0, 1);
        $annee = substr($this->num_secu, 1, 2);
        $mois = substr($this->num_secu, 3, 2);
        $dpt = substr($this->num_secu, 5, 2);
        $ville = substr($this->num_secu, 7, 3);
        $nb = substr($this->num_secu, 10, 3);
        $cle = substr($this->num_secu, 13, 2);
        return "{$sexe} {$annee} {$mois} {$dpt} {$ville} {$nb} {$cle}";
    }

    protected $ville;
    protected $ville_naissance;
    protected $classe;

    protected $conventions;

    public function __construct()
    {
        parent::__construct();
        $this->linkManyToOne("ville", Ville::class, "id_ville");
        $this->linkManyToOne("ville_naissance", Ville::class, "id_ville_naissance");
        $this->linkManyToOne("classe", Classe::class, "id_classe");
        $this->linkOneToMany("conventions", Convention::class, "id");
    }

    public static function get($id) 
    {
        return EtudiantDao::get($id);
    }

    public static function getAll() 
    {
        return EtudiantDao::getAll();
    }

    public static function getAllByClasse($id) 
    {
        return EtudiantDao::getAllByClasse($id);
    }

    public static function update($etudiant)
    {
        return EtudiantDao::update($etudiant);
    }

    public static function insert($etudiant)
    {
        return EtudiantDao::insert($etudiant);
    }
}