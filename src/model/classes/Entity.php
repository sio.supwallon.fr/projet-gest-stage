<?php
namespace GEST_STAGE\model\classes;

use Exception;

abstract class Entity
{
    private static $links = [];

    public function __set($name, $value)
    {
        $class = get_class($this);
        $method = "set_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not writable in $class.");
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $class = get_class($this);

        if (isset(self::$links[$class]['ManyToOne'][$name])) {
            return self::__get_many_to_one($name);
        }

        if (isset(self::$links[$class]['OneToMany'][$name])) {
            return self::__get_one_to_many($name);
        }

        $method = "get_" . $name;
        if(! property_exists($this, $name)) {
            throw new Exception("Property $name does not exist in $class.");
        }
        if (! method_exists($this, $method)) {
            throw new Exception("Property $name is not readable in $class.");
        }
        return $this->$method();
    }

    protected function linkManyToOne($name, $linked_class, $foreign_key, $role=null)
    {
        $class = get_class($this);
        self::$links[$class]['ManyToOne'][$name] = [
            "linked_class" => $linked_class,
            "foreign_key" => $foreign_key,
            "role" => $role,
        ];
    }

    protected function linkOneToMany($name, $linked_class, $primary_key, $role=null)
    {
        $class = get_class($this);
        self::$links[$class]['OneToMany'][$name] = [
            "linked_class" => $linked_class,
            "primary_key" => $primary_key,
            "role" => $role,
        ];
    }

    private function __get_many_to_one($name) {
        $class = get_class($this);

        if(! isset($this->$name)) {
            $linked_class = self::$links[$class]['ManyToOne'][$name]['linked_class'];
            $foreign_key = self::$links[$class]['ManyToOne'][$name]['foreign_key'];
            $role = self::$links[$class]['ManyToOne'][$name]['role'];

            if(! property_exists($class, $foreign_key)) {
                throw new Exception("Property $foreign_key does not exist in $class.");
            }

            if(isset($role)) {
                $role = ucfirst($role);
            }
            $method = "get{$role}";

            if (! method_exists($linked_class, $method)) {
                throw new Exception("Method $method does not exist in $linked_class.");
            }

            if(isset($this->$foreign_key)) {
                $this->$name = $linked_class::get($this->$foreign_key);
            }
        }
        return $this->$name;
    }

    private function __get_one_to_many($name) {
        $class = get_class($this);

        if(! isset($this->$name)) {
            $linked_class = self::$links[$class]['OneToMany'][$name]['linked_class'];
            $primary_key = self::$links[$class]['OneToMany'][$name]['primary_key'];
            $role = self::$links[$class]['OneToMany'][$name]['role'];

            if(! property_exists($class, $primary_key)) {
                throw new Exception("Property $primary_key does not exist in $class.");
            }
    
            if(isset($role)) {
                $role = ucfirst($role);
            }

            $class_arr = explode("\\", $class);
            $classname = array_pop($class_arr);
            $method = "getAllBy{$classname}{$role}";

            if (! method_exists($linked_class, $method)) {
                throw new Exception("Method $method does not exist in $linked_class.");
            }

            $this->$name = $linked_class::$method($this->$primary_key);
        }
        return $this->$name;
    }

    public function save()
    {
        $class = get_class($this);

        if (isset($this->id)) {
            // update
            $method = "update";
        } else {
            // insert
            $method = "insert";
        }

        if (! method_exists($class, $method)) {
            throw new Exception("Method $method does not exist in $class.");
        }

        $class::$method($this);
    }
}