<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\NafDao;

class Naf extends Entity
{
    public $id;
    public $code;
    public $intitule;
    public $intitule_65;
    public $intitule_40;

    protected $entreprises;

    public function __construct()
    {
        $this->linkOneToMany("entreprises", Entreprise::class, "id");
    }

    public static function get($id) 
    {
        return NafDao::get($id);
    }

    public static function getAll() 
    {
        return NafDao::getAll();
    }

    public static function getAllByNom($code) 
    {
        return NafDao::getAllByNom($code);
    }

    public static function getIdByNom($code) 
    {
        return NafDao::getIdByNom($code);
    }

    public function __toString()
    {
        return $this->intitule;
    }
}