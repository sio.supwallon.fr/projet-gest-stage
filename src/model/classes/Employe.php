<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\EmployeDao;

class Employe extends Utilisateur
{
    public $id_utilisateur;
    public $signature;
    public $id_entreprise;

    public function is_responsable($convention)
    {
        return ($convention->entreprise->id_responsable == $this->id);
    }

    public function is_tuteur($convention)
    {
        $periodes = $convention->periodes;
        $periodes_count = count($periodes);
        $is_tuteur = false;
        for($i = 0; $i < $periodes_count && ! $is_tuteur; $i++) {
            $is_tuteur = ($periodes[$i]->id_tuteur == $this->id);
        }
        return $is_tuteur;
    }

    protected $entreprise;
    protected $conventions_tuteur;
    protected $conventions_responsable;

    public function __construct()
    {
        parent::__construct();
        $this->linkManyToOne("entreprise", Entreprise::class, "id_entreprise");
        $this->linkOneToMany("conventions_tuteur", Convention::class, "id", "tuteur");
        $this->linkOneToMany("conventions_responsable", Convention::class, "id", "responsable");
    }

    public static function get($id) 
    {
        return EmployeDao::get($id);
    }

    public static function getAll() 
    {
        return EmployeDao::getAll();
    }

    public static function getAllByEntreprise($id_entreprise) 
    {
        return EmployeDao::getAllByEntreprise($id_entreprise);
    }

    public static function getAllByConventionTuteur($id_convention)
    {
        return EmployeDao::getAllByConventionTuteur($id_convention);
    }

    public static function update($employe)
    {
        return EmployeDao::update($employe);
    }

    public static function insert($employe)
    {
        return EmployeDao::insert($employe);
    }
}