<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\VilleDao;

class Ville extends Entity
{
    public $id;
    public $code_insee;
    public $nom;
    public $slug;
    public $gps_lat;
    public $gps_lng;
    public $code_postal;
    public $code_departement;
    public $id_departement;

    protected $departement;
    protected $entreprises;
    protected $etablissements;

    public function __construct()
    {
        $this->linkManyToOne("departement", Departement::class, "id_departement");
        $this->linkOneToMany("entreprises", Entreprise::class, "id");
        $this->linkOneToMany("etablissements", Etablissement::class, "id");
    }

    public static function get($id)
    {
        return VilleDao::get($id);
    }

    public static function getIdByNom($ville)
    {
        return VilleDAO::getIdByNom($ville);
    }

    public static function getAll()
    {
        return VilleDao::getAll();
    }

    public static function getAllByDepartement($code_departement)
    {
        return VilleDao::getAllByDepartement($code_departement);
    }

    public static function getAllByRegion($id_region)
    {
        return VilleDao::getAllByRegion($id_region);
    }

    public static function getAllByPays($id_pays)
    {
        return VilleDao::getAllByPays($id_pays);
    }

    public static function getAllByCodePosal($code_postal)
    {
        return VilleDao::getAllByCodePosal($code_postal);
    }
}