<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\EtablissementDao;

class Etablissement extends Utilisateur
{
    public $id_utilisateur;
    public $signature;
    public $adresse;
    public $id_ville;

    protected $ville;
    protected $conventions;

    public function __construct()
    {
        parent::__construct();
        $this->linkManyToOne("ville", Ville::class, "id_ville");
        $this->linkOneToMany("conventions", Convention::class, "id");
    }

    public static function get($id) 
    {
        return EtablissementDao::get($id);
    }

    public static function getAll() 
    {
        return EtablissementDao::getAll();
    }

    public static function getAllByVille($id_ville)
    {
        return EtablissementDao::getAllByVille($id_ville);
    }

    public static function getAllByFonction($fonction)
    {
        return EtablissementDao::getAllByFonction($fonction);
    }

    public static function update($etablissement)
    {
        return EtablissementDao::update($etablissement);
    }

    public static function insert($etablissement)
    {
        return EtablissementDao::insert($etablissement);
    }
}