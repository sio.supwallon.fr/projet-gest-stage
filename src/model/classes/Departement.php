<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\DepartementDao;

class Departement extends Entity
{
    public $id;
    public $code_insee;
    public $nom;
    public $slug;
    public $code_region;
    public $id_region;

    protected $region;
    protected $villes;
    protected $entreprises;

    public function __construct()
    {
        $this->linkManyToOne("region", Region::class, "id_region");
        $this->linkOneToMany("villes", Ville::class, "id");
        $this->linkOneToMany("entreprises", Entreprise::class, "id");
    }

    public static function get($id) 
    {
        return DepartementDao::get($id);
    }

    public static function getAll() 
    {
        return DepartementDao::getAll();
    }

    public static function getAllByRegion($id_region)
    {
        return DepartementDao::getAllByRegion($id_region);
    }
    
    public static function getAllByPays($id_pays)
    {
        return DepartementDao::getAllByPays($id_pays);
    }
}