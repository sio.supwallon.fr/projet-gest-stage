<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\ClasseDao;

class Classe extends Entity
{
    public $id;
    public $nom;
    public $slug;
    public $id_section;

    protected $section;
    protected $enseignants;
    protected $enseignants_non_affectes;
    protected $etudiants;
    protected $accompagnements;

    public function __construct()
    {
        $this->linkManyToOne("section", Section::class, "id_section");
        $this->linkOneToMany("enseignants", Enseignant::class, "id");
        $this->linkOneToMany("enseignants_non_affectes", Enseignant::class, "id", "NonAffectes");
        $this->linkOneToMany("etudiants", Etudiant::class, "id");
        $this->linkOneToMany("accompagnements", Accompagnement::class, "id");
    }

    public static function get($id)
    {
        return ClasseDao::get($id);
    }

    public static function getAll()
    {
        return ClasseDao::getAll();
    }

    public static function getAllBySection($id_section)
    {
        return ClasseDao::getAllBySection($id_section);
    }

    public static function getAllByEnseignant($id_enseignant)
    {
        return ClasseDao::getAllByEnseignant($id_enseignant);
    }

    public static function getAllByAccompagnement($id_accompagnement)
    {
        return ClasseDao::getAllByAccompagnement($id_accompagnement);
    }

    public static function insert($classe){
        return ClasseDao::insert($classe);
    }

    public static function update($classe){
        return ClasseDao::update($classe);
    }

    public static function delete($id){
        return ClasseDao::delete($id);
    }

    public static function addAccompagnement($id_classe, $id_accompagnement)
    {
        return ClasseDao::addAccompagnement($id_classe, $id_accompagnement);
    }

    public static function clearAccompagnement($id_classe)
    {
        return ClasseDao::clearAccompagnement($id_classe);
    }

    public static function addEnseignant($id_classe, $id_enseignant)
    {
        return ClasseDao::addEnseignant($id_classe, $id_enseignant);
    }

    public static function clearEnseignant($id_classe)
    {
        return ClasseDao::clearEnseignant($id_classe);
    }
}