<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\EnseignantDao;

class Enseignant extends Utilisateur
{
    public $id_utilisateur;
    public $signature;

    protected $classes;
    protected $sections;
    protected $conventions;

    public function __construct()
    {
        parent::__construct();
        $this->linkOneToMany("classes", Classe::class, "id");
        $this->linkOneToMany("sections", Section::class, "id");
        $this->linkOneToMany("conventions", Convention::class, "id", "referent");
    }

    public function is_referent($convention)
    {
        $is_referent = false;
        foreach($convention->classe->enseignants as $enseignant)
        {
            if ($enseignant->id == $this->id) {
                $is_referent = true;
                break;
            }
        }
        return $is_referent;
    }

    public static function get($id) 
    {
        return EnseignantDao::get($id);
    }

    public static function getAll() 
    {
        return EnseignantDao::getAll();
    }

    public static function getAllByClasse($id_classe)
    {
        return EnseignantDao::getAllByClasse($id_classe);
    }

    public static function getAllByClasseNonAffectes($id_classe)
    {
        return EnseignantDao::getAllByClasseNonAffectes($id_classe);
    }

    public static function getAllBySection($id_section)
    {
        return EnseignantDao::getAllBySection($id_section);
    }

    public static function update($enseignant)
    {
        return EnseignantDao::update($enseignant);
    }

    public static function insert($enseignant)
    {
        return EnseignantDao::insert($enseignant);
    }
}