<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\EntrepriseDao;

class Entreprise extends Entity
{
    public $id;
    public $raison_sociale;
    public $adresse;
    public $telephone;
    public $email;
    public $maps;
    public $est_siege;
    public $id_naf;
    public $id_ville;
    public $id_responsable;
    public $id_siege;

    protected $naf;
    protected $ville;
    protected $responsable;
    protected $siege;
    protected $agences;
    protected $employes;

    public function __construct()
    {
        $this->linkManyToOne("naf", Naf::class, "id_naf");
        $this->linkManyToOne("ville", Ville::class, "id_ville");
        $this->linkManyToOne("responsable", Employe::class, "id_responsable");
        $this->linkManyToOne("siege", Entreprise::class, "id_siege");
        $this->linkOneToMany("agences", Entreprise::class, "id", "Siege");
        $this->linkOneToMany("employes", Employe::class, "id");
    }

    public static function getByRaisonSociale($RaisonSocial)
    {
        return EntrepriseDao::getByRaisonSociale($RaisonSocial);
    }

    public static function get($id)
    {
        return EntrepriseDao::get($id);
    }

    public static function getAll()
    {
        return EntrepriseDao::getAll();
    }

    public static function getAllByVille($id_ville)
    {
        EntrepriseDao::getAllByVille($id_ville);
    }

    public static function getAllByDepartement($code_departement)
    {
        return EntrepriseDao::getAllByDepartement($code_departement);
    }

    public static function getAllByRegion($id_region)
    {
        return EntrepriseDao::getAllByRegion($id_region);
    }

    public static function getAllByPays($id_pays)
    {
        return EntrepriseDao::getAllByPays($id_pays);
    }

    public static function getAllByNaf($id_naf)
    {
        return EntrepriseDao::getAllByNaf($id_naf);
    }

    public static function getEntrepriseByFiltre($id_region, $id_departement, $id_ville, $code_naf, $section)
    {
        return EntrepriseDao::getEntrepriseByFiltre($id_region, $id_departement, $id_ville, $code_naf, $section);
    }

    public static function getAllBySiege($id_entreprise)
    {
        return EntrepriseDao::getAllBySiege($id_entreprise);
    }

    public static function update($entreprise)
    {
        return EntrepriseDao::update($entreprise);
    }

    public static function insert($entreprise)
    {
        return EntrepriseDao::insert($entreprise);
    }

    public static function delete($id)
    {
        return EntrepriseDao::delete($id);
    }

    public static function getAllByEstSiege()
    {
        return EntrepriseDao::getAllByEstSiege();
    }

    
}