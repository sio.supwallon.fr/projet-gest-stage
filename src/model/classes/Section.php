<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\SectionDao;

class Section extends Entity
{
    public $id;
    public $nom;
    public $slug;
    public $intitule;

    protected $classes;
    protected $enseignants;

    public function __construct()
    {
        $this->linkOneToMany("classes", Classe::class, "id");
        $this->linkOneToMany("enseignants", Enseignant::class, "id");
    }

    public static function get($id) 
    {
        return SectionDao::get($id);
    }

    public static function getAll() 
    {
        return SectionDao::getAll();
    }

    public static function getAllByEnseignant($id_enseignant)
    {
        return SectionDao::getAllByEnseignant($id_enseignant);
    }
}