<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\TypeUtilisateurDao;

class TypeUtilisateur extends Entity
{
    public $id;
    public $nom;
    public $slug;
    public $niveau_habilitation;
    public $id_groupe;

    protected $groupe;
    protected $utilisateurs;

    public function __construct()
    {
        $this->linkManyToOne("groupe", Groupe::class, "id_groupe");
        $this->linkOneToMany("utilisateurs", Utilisateur::class, "id");
    }

    public static function get($id) 
    {
        return TypeUtilisateurDao::get($id);
    }

    public static function getBySlug($slug)
    {
        return TypeUtilisateurDao::getBySlug($slug);
    }

    public static function getAll() 
    {
        return TypeUtilisateurDao::getAll();
    }

    public static function getAllByGroupe($id_groupe)
    {
        return TypeUtilisateurDao::getAllByGroupe($id_groupe);
    }

    public function __toString()
    {
        return $this->nom;
    }
}