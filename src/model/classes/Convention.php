<?php
namespace GEST_STAGE\model\classes;

use DateTime;
use GEST_STAGE\kernel\Date;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\database\ConventionDao;

class Convention extends Entity
{
    public $id;
    public $service;
    public $lieu;
    public $cpam;
    public $activites;
    public $competences;
    public $jours_particuliers;
    public $duree_jours;
    public $gratification_mensuelle;
    public $avantage1;
    public $avantage2;
    public $protection_maladie;
    public $jours_conge;
    public $nb_ects;
    public $signature_etudiant;
    public $signature_enseignant;
    public $signature_responsable;
    public $signature_etablissement;
    public $id_entreprise;
    public $id_responsable;
    public $id_etablissement;
    public $id_enseignant;
    public $id_classe;
    public $id_etudiant;

    protected $duree_totale;
    protected function get_duree_totale() 
    {
        $total = 0;
        foreach($this->periodes as $periode) {
            $debut = new DateTime($periode->debut);
            $fin = new DateTime($periode->fin);
            $interval = $debut->diff($fin);
            $total += $interval->format('%a');
        }
        $total = ceil($total / 7.0);
        return $total;
    }

    protected $nb_jours_ouvrables;
    protected function get_nb_jours_ouvrables() 
    {
        $jours_feries = JourFerie::getAll();
        $pdf = View::getParam('pdf');
        $nb = 0;
        foreach($this->periodes as $periode) {
            $dates = Date::getPeriod($periode->debut, $periode->fin);

            foreach($dates as $index => $date) {
                if (in_array($date->format('l'), $pdf['jours_ouvres'])) {
                    // jour ouvré
                    if (! in_array($date->format('Y-m-d'), $jours_feries)) {
                        // jour non férié
                        $nb++;
                    }
                }
            }
        }

        return $nb;
    }

    protected $date_fr;
    protected function get_date_fr() 
    {
        return Date::toFr((new DateTime())->format('y-m-d'));
    }

    protected $signature_etudiant_fr;
    protected function get_signature_etudiant_fr() 
    {
        return Date::toFr($this->signature_etudiant);
    }

    protected $signature_enseignant_fr;
    protected function get_signature_enseignant_fr() 
    {
        return Date::toFr($this->signature_enseignant);
    }

    protected $signature_responsable_fr;
    protected function get_signature_responsable_fr() 
    {
        return Date::toFr($this->signature_responsable);
    }

    protected $signature_etablissement_fr;
    protected function get_signature_etablissement_fr() 
    {
        return Date::toFr($this->signature_etablissement);
    }

    protected $entreprise;
    protected $responsable;
    protected $etablissement;
    protected $enseignant;
    protected $classe;
    protected $etudiant;

    protected $periodes;
    protected $tuteurs;

    public function __construct()
    {
        $this->linkManyToOne("entreprise", Entreprise::class, "id_entreprise");
        $this->linkManyToOne("responsable", Employe::class, "id_responsable");
        $this->linkManyToOne("etablissement", Etablissement::class, "id_etablissement");
        $this->linkManyToOne("enseignant", Enseignant::class, "id_enseignant");
        $this->linkManyToOne("classe", Classe::class, "id_classe");
        $this->linkManyToOne("etudiant", Etudiant::class, "id_etudiant");

        $this->linkOneToMany("periodes", Periode::class, "id");
        $this->linkOneToMany("tuteurs", Employe::class, "id", "Tuteur");
    }

    public static function get($id)
    {
        return ConventionDao::get($id);
    }

    public static function getAll()
    {
        return ConventionDao::getAll();
    }

    public static function getAllByEmployeTuteur($id_tuteur)
    {
        return ConventionDao::getAllByEmployeTuteur($id_tuteur);
    }

    public static function getAllByEmployeResponsable($id_responsable)
    {
        return ConventionDao::getAllByEmployeResponsable($id_responsable);
    }

    public static function getAllByEnseignantReferent($id_enseignant)
    {
        return ConventionDao::getAllByEnseignantReferent($id_enseignant);
    }

    public static function getAllByEtablissement()
    {
        return ConventionDao::getAllByEtablissement();
    }

    public static function getAllByEtudiant($id_etudiant)
    {
        return ConventionDao::getAllByEtudiant($id_etudiant);
    }

    public static function getAllByEtat($id_etat)
    {
        return ConventionDao::getAllByEtat($id_etat);
    }

    public static function insertConvention($lst_entreprise, $entreprise_service_stage, $entreprise_lieu_stage, $lst_classe, $lst_etudiant, $CPAM, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours, $condition_avantage1, $condition_avantage2, $condition_maladie, $condition_autorisation, $condition_cas, $debut_arr, $fin_arr, $id_tuteur_arr, $periode_lieu_arr)
    {
        return ConventionDao::insertConvention($lst_entreprise, $entreprise_service_stage, $entreprise_lieu_stage, $lst_classe, $lst_etudiant, $CPAM, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours, $condition_avantage1, $condition_avantage2, $condition_maladie, $condition_autorisation, $condition_cas, $debut_arr, $fin_arr, $id_tuteur_arr, $periode_lieu_arr);
    }

    public static function updateEntreprise ($id, $entreprise_service_stage, $entreprise_lieu_stage, $lst_entreprise)
    {
        return ConventionDao::updateEntreprise ($id, $entreprise_service_stage, $entreprise_lieu_stage, $lst_entreprise);
    }

    public static function updateStagiaire ($id, $lst_classe, $lst_etudiant, $CPAM)
    {
        return ConventionDao::updateStagiaire ($id, $lst_classe, $lst_etudiant, $CPAM);
    }

    public static function updateInformation ($id, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours)
    {
        return ConventionDao::updateInformation ($id, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours);
    }

    public static function updateCondition ($id, $condition_avantage1, $condition_avantage2,$condition_maladie, $condition_autorisation, $condition_cas)
    {
        return ConventionDao::updateCondition ($id, $condition_avantage1, $condition_avantage2,$condition_maladie, $condition_autorisation, $condition_cas);
    }

    public static function deleteConvention ($id_convention)
    {
        return ConventionDao::deleteConvention ($id_convention);
    }

    public static function unlockConvention ($id_convention)
    {
        return ConventionDao::unlockConvention ($id_convention);
    }

    public static function signer_etudiantConvention($id)
    {
        return ConventionDao::signer_etudiantConvention($id);
    }

    public static function signer_enseignantConvention($id)
    {
        return ConventionDao::signer_enseignantConvention($id);
    }

    public static function signer_responsableConvention($id)
    {
        return ConventionDao::signer_responsableConvention($id);
    }

    public static function signer_etablissementConvention($id)
    {
        return ConventionDao::signer_etablissementConvention($id);
    }
}
