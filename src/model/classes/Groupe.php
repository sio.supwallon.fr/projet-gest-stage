<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\GroupeDao;

class Groupe extends Entity
{
    public $id;
    public $nom;

    protected $utilisateurs;
    protected $droits;
    protected $typesUtilisateurs;

    public function __construct()
    {
        $this->linkOneToMany("utilisateurs", Utilisateur::class, "id");
        $this->linkOneToMany("droits", Droits::class, "id");
        $this->linkOneToMany("typesUtilisateurs", TypeUtilisateur::class, "id");
    }

    public static function get($id) 
    {
        return GroupeDao::get($id);
    }

    public static function getByNom($nom)
    {
        return GroupeDao::getByNom($nom);
    }

    public static function getAll() 
    {
        return GroupeDao::getAll();
    }

    public static function getAllByUtilisateur($id_utilisateur)
    {
        return GroupeDao::getAllByUtilisateur($id_utilisateur);
    }

    public static function getAllByEmploye($id_employe)
    {
        return GroupeDao::getAllByUtilisateur($id_employe);
    }

    public static function getAllByEnseignant($id_enseignant)
    {
        return GroupeDao::getAllByUtilisateur($id_enseignant);
    }

    public static function getAllByEtablissement($id_etablissement)
    {
        return GroupeDao::getAllByUtilisateur($id_etablissement);
    }

    public static function getAllByEtudiant($id_etudiant)
    {
        return GroupeDao::getAllByUtilisateur($id_etudiant);
    }

    public function __toString()
    {
        return $this->nom;
    }
}