<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\UtilisateurDao;

class Utilisateur extends Entity
{
    const SUBCLASSES =
    [
        'employe' => Employe::class,
        'enseignant' => Enseignant::class,
        'etablissement' => Etablissement::class,
        'etudiant' => Etudiant::class,
    ];

    public $id;
    public $identifiant;
    public $motdepasse;
    public $civilite;
    public $nom;
    public $prenom;
    public $fonction;
    public $telephone;
    public $email;
    public $active;
    public $jeton_activation;
    public $ip;

    protected $connexion;
    protected function get_connexion() {
        if ($this->connexion) {
            $date = substr($this->connexion, 0, 10);
            $time = substr($this->connexion, 11);
            $format = "{$date}T{$time}";
        } else {
            $format = null;
        }
        return $format;
    }
    protected function set_connexion($value) { $this->connexion = $value; }

    public $nb_erreurs;

    protected $derniere_erreur;
    protected function get_derniere_erreur() {
        if ($this->derniere_erreur) {
            $date = substr($this->derniere_erreur, 0, 10);
            $time = substr($this->derniere_erreur, 11);
            $format = "{$date}T{$time}";
        } else {
            $format = null;
        }
        return $format;
    }
    protected function set_derniere_erreur($value) { $this->derniere_erreur = $value; }

    public $id_type_utilisateur;

    protected $sel;
    protected function get_sel() {
        if (! isset($this->sel)) {
            $this->sel = $this->identifiant . "UneTrèsLonguePhraseHistoireDeSécuriserToutCa";
        }
        return $this->sel;
    }

    protected $typeUtilisateur;
    protected $groupes;
    protected $droits;

    public function __construct()
    {
        $this->linkManyToOne("typeUtilisateur", TypeUtilisateur::class, "id_type_utilisateur");
        $this->linkOneToMany("groupes", Groupe::class, "id");
        $this->linkOneToMany("droits", Droit::class, "id");
    }

    public static function countByIdentifiant($identifiant)
    {
        return UtilisateurDao::countByIdentifiant($identifiant);
    }

    public static function getLastIdentifiant($identifiant)
    {
        return UtilisateurDao::getLastIdentifiant($identifiant);
    }

    public static function getByIdentifiant($identifiant)
    {
        return UtilisateurDao::getByIdentifiant($identifiant);
    }

    public static function getByIdentifiantMotdepasse($identifiant, $motdepasse) 
    {
        return UtilisateurDao::getByIdentifiantMotdepasse($identifiant, $motdepasse);
    }

    public static function getByIdentifiantJeton($identifiant, $jeton_activation) 
    {
        return UtilisateurDao::getByIdentifiantJeton($identifiant, $jeton_activation);
    }

    public static function getByIdentifiantEmail($identifiant, $email)
    {
        return UtilisateurDao::getByIdentifiantEmail($identifiant, $email);
    }

    public static function getIdByNomPrenom($nom, $prenom) 
    {
        return UtilisateurDao::getIdByNomPrenom($nom, $prenom);
    }

    public static function get($id) 
    {
        return UtilisateurDao::get($id);
    }

    public static function getAll() 
    {
        return UtilisateurDao::getAll();
    }

    public static function getAllByFonction($fonction)
    {
        return UtilisateurDao::getAllByFonction($fonction);
    }

    public static function getAllByGroupe($id_groupe)
    {
        return UtilisateurDao::getAllByGroupe($id_groupe);
    }

    public static function getAllByTypeUtilisateur($id_type_utilisateur)
    {
        return UtilisateurDao::getAllByTypeUtilisateur($id_type_utilisateur);
    }

    public static function getLettres()
    {
        return UtilisateurDao::getLettres();
    }

    public static function getLettresByType($type)
    {
        return UtilisateurDao::getLettresByType($type);
    }

    public static function getAllByLettre($lettre)
    {
        return UtilisateurDao::getAllByLettre($lettre);
    }

    public static function getAllByTypeLettre($type, $lettre)
    {
        return UtilisateurDao::getAllByTypeLettre($type, $lettre);
    }

    public static function getAllByNom($nom)
    {
        return UtilisateurDao::getAllByNom($nom);
    }

    public static function getPremiereLettre()
    {
        return UtilisateurDao::getPremiereLettre();
    }

    public static function getPremiereLettreByType($type)
    {
        return UtilisateurDao::getPremiereLettreByType($type);
    }

    public static function update($utilisateur)
    {
        return UtilisateurDao::update($utilisateur);
    }

    public static function insert($utilisateur)
    {
        return UtilisateurDao::insert($utilisateur);
    }

    public static function update_my_password($id, $motdepasse)
    {
        UtilisateurDao::update_my_password($id, $motdepasse);
    }

    public static function login($utilisateur)
    {
        return UtilisateurDao::login($utilisateur);
    }

    public static function loginError($utilisateur)
    {
        return UtilisateurDao::loginError($utilisateur);
    }

    public static function resetErrors($id)
    {
        return UtilisateurDao::resetErrors($id);
    }

    public static function active($utilisateur)
    {
        return UtilisateurDao::active($utilisateur);
    }

    public static function desactive($utilisateur)
    {
        return UtilisateurDao::desactive($utilisateur);
    }

    public static function addGroupe($id_utilisateur, $id_groupe)
    {
        UtilisateurDao::addGroupe($id_utilisateur, $id_groupe);
    }

    public static function removeGroupe($id_utilisateur, $id_groupe)
    {
        UtilisateurDao::removeGroupe($id_utilisateur, $id_groupe);
    }

    public static function setJetonActivation($id, $jeton)
    {
        UtilisateurDao::setJetonActivation($id, $jeton);
    }

    public static function delete($id)
    {
        UtilisateurDao::delete($id);
    }
}