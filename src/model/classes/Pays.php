<?php
namespace GEST_STAGE\model\classes;

use GEST_STAGE\model\database\PaysDao;

class Pays extends Entity
{
    public $id;
    public $nom;
    public $slug;

    protected $regions;
    protected $departements;
    protected $villes;
    protected $entreprises;

    public function __construct()
    {
        $this->linkOneToMany("regions", Region::class, "id");
        $this->linkOneToMany("departements", Ville::class, "id");
        $this->linkOneToMany("villes", Ville::class, "id");
        $this->linkOneToMany("entreprises", Entreprise::class, "id");
    }

    public static function get($id) 
    {
        return PaysDao::get($id);
    }

    public static function getAll() 
    {
        return PaysDao::getAll();
    }
}