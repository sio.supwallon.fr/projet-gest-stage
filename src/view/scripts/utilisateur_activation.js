$(document).ready(function() {
    $("#motdepasse").keyup(checkPassword2);
    $("#motdepasse2").keyup(checkPassword2);
});

function checkPassword2()
{
    var element = $("#motdepasse2")[0];
    element.setCustomValidity(
        ($("#motdepasse").val() != $("#motdepasse2").val()) ? 'Les mots de passent de correspondent pas': ''
    );
}