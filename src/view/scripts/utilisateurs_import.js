$(document).ready(()=>{
    $('#id_type_utilisateur').change(id_type_utilisateur_change);
    $('#active').change(active_change);
    $('#id_section').change(id_section_change);
});

function id_type_utilisateur_change()
{
    var text = $("#id_type_utilisateur option:selected").text();
    //console.log(text);
    if (text == 'Etudiant') {
        $('#id_classe').attr('disabled', false);
        $('#info_etudiant').show();
    } else {
        $('#id_classe').attr('disabled', true);
        $('#info_etudiant').hide();
    }
}

function active_change()
{
    var value = $(this).val();
    if (value == 1) {
        $('#send_link').attr('disabled', true);
        $('#send_link').parent().hide();
    } else {
        $('#send_link').attr('disabled', false);
        $('#send_link').parent().show();
    }
}

function id_section_change()
{
    var id_section = $("#id_section option:selected").val();
    $select = $("#id_classe");
    if (id_section == '') {
        clear_select_classe($select)
    } else {
        ajax_classes_by_section(id_section, $select);
    }
}

function ajax_classes_by_section(id_section, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/classes/by-section/' + id_section,
        success: function(data) {
            // conversion des données en objet
            var classes = JSON.parse(data);

            // actualisation de la liste
            update_select_classe(classes, $select);
        }
    });
}

function update_select_classe(classes, $select)
{
    $select.find('option').remove();

    $.each(classes, (index, classe) => {
        $select.append($('<option value="' + classe.id + '">' + classe.nom + '</option>'));
    });
}

function clear_select_classe($select)
{
    $select.find('option').remove();
    $select.append($('<option value="">&lt;Aucune&gt;</option>'));
}
