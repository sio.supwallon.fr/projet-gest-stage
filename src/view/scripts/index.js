/* Common JS Functionalities */

(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

$(document).ready(function(){
    $(".custom-file-input").change(custom_file_input_change);
    $(".btn-back").click(btn_back_click);
    $("input.custom-control-input:checkbox").change(custom_checkbox_change)
});

function custom_checkbox_change(e)
{
    var hidden = $('#hidden_' + this.name)[0];
    this.value = (this.checked)?1:0;
    hidden.value = (this.checked)?1:0;
    hidden.disabled = this.checked;
    //console.log(hidden);
    //console.log(this);
}

function btn_back_click() {
    window.history.back();
}
  
function custom_file_input_change() 
{
    var $label = $(this).next('.custom-file-label');
    var path = $(this).val();
    var filename = path.split('\\').pop();
    if (filename == '') {
        filename = 'Aucun fichier choisi';
    }

    var input_width = 70;
    var label_width = $label.width();
    //console.log('label_width: ' + label_width);
    var max_label_size = (label_width - input_width) / (rem() / 1.5);
    if (filename.length > max_label_size) {
        filename = filename.substr(0,max_label_size) + '...';
    }
    
    $label.html(filename);
}

function rem() {
    var html = document.getElementsByTagName('html')[0];
    var rem = parseInt(window.getComputedStyle(html)['fontSize']);
    //console.log('rem: ' + rem);
    return rem;
}