$(document).ready(() => {
    $("#Nom_classe").change(nom_change);

    $('#btn_ajout_prof').click(btn_ajout_prof_click);
    $('#btn_supp_prof').click(btn_supp_prof_click);
    $('#btn_ajout_eleve').click(btn_ajout_eleve_click);
    $('#btn_supp_eleve').click(btn_supp_eleve_click);
});

function btn_ajout_prof_click()
{
    $('#modal_ajout_prof').modal('show');
}

function btn_supp_prof_click()
{
    $('#modal_supp_prof').modal('show');
}

function btn_ajout_eleve_click()
{
    $('#modal_ajout_eleve').modal('show');
}

function btn_supp_eleve_click()
{
    $('#modal_supp_eleve').modal('show');
}

function nom_change()
{
    $(this).val($(this).val().toUpperCase());
    update_slug();
}

function update_slug()
{
    var nom = $("#Nom_classe").val().toLowerCase();
    if (nom.length > 0) {
        var slug = nom;
        $('#slug_classe').val(slug);
    } else {
        $('#slug_classe').val('');
    }    
}
