$(document).ready(function() { 
    $("#code_postal").keyup(code_postal_keyup);
    $("#id_ville").change(id_ville_change);

    $("#nom").keyup(nom_keyup);
    $("#id_resp").change(id_resp_change);

    $("#siege_O").click(siege_hide);
    $("#siege_N").click(siege_show);

    $("#NewNaf").keyup(naf_keyup);
    $("#id_newNaf").change(id_newNaf_change);


});

//Selection des villes en fonctions de la saisie du code postal

function code_postal_keyup(e)
{
    var id = $(e.target).attr('id');
    var code_postal = $('#' + id).val();
    let $select;
    $select = $("#id_ville");
            
    if (code_postal.length >= 2) {
        ajax_ville_by_cp(code_postal, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Code Postal</option>'));
    }
}

function ajax_ville_by_cp(code_postal, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/ville/by-cp/' + code_postal,
        success: function(data) {
            // conversion des données en objet
            var villes = JSON.parse(data);

            // actualisation de la liste
            update_select_ville(villes, $select);
        }
    });
}

function update_select_ville(villes, $select)
{
    $select.find('option').remove();

    if (villes.length > 0) {
        $.each(villes, (index, ville) => {
            $select.append($('<option value="' + ville.id + '" data-code_postal="' + ville.code_postal + '">' + ville.nom + '</option>'));
        });
        if (villes.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function id_ville_change()
{
    var code_postal = $("#id_ville option:selected").data('code_postal');
    $("#code_postal").val(code_postal);
}

//Selection du prenom en fonction de la saisie du nom

function nom_keyup(e)
{
    var id = $(e.target).attr('id');
    var nom = $('#' + id).val();
    let $select;
    $select = $("#id_resp");
            
    if (nom.length >= 3) {
        ajax_resp_by_nom(nom, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Nom</option>'));
    }
}

function ajax_resp_by_nom(nom, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/utilisateur/by-nom/' + nom,
        success: function(data) {
            // conversion des données en objet
            var responsables = JSON.parse(data);

            // actualisation de la liste
            update_select_resp(responsables, $select);
        }
    });
}

function update_select_resp(responsables, $select)
{
    $select.find('option').remove();

    if (responsables.length > 0) {
        $.each(responsables, (index, responsable) => {
            $select.append($('<option value="' + responsable.id + '" data-nom="' + responsable.nom + '">' + responsable.prenom + '</option>'));
        });
        if (responsables.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function id_resp_change()
{
    var nom = $("#id_resp option:selected").data('nom');
    $("#nom").val(nom);
}

function siege_hide(){
    $("#les_sieges").hide();

}

function siege_show(){
    $("#les_sieges").show();

}

function naf_keyup(e)
{
    var id = $(e.target).attr('id');
    var naf = $('#' + id).val();
    let $select;
    $select = $("#id_newNaf");
            
    if (naf.length >= 2) {
        ajax_naf(naf, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Code</option>'));
    }
}

function ajax_naf(naf, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/entreprise/naf/' + naf,
        success: function(data) {
            // conversion des données en objet
            var nafs = JSON.parse(data);

            // actualisation de la liste
            update_select_naf(nafs, $select);
        }
    });
}

function update_select_naf(nafs, $select)
{
    $select.find('option').remove();

    if (nafs.length > 0) {
        $.each(nafs, (index, naf) => {
            $select.append($('<option value="' + naf.id + '" data-naf="' + naf.code + '">' + naf.code + '</option>'));
        });
        if (nafs.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function id_newNaf_change()
{
    var naf = $("#id_newNaf option:selected").data('naf');
    $("#NewNaf").val(naf);
}


//Modifie l'identifiant de l'employe en fonction du nom et prénom saisie

$(document).ready(() => {
    $("#nom").change(nom_change);
    $("#prenom").change(prenom_change);
});

function nom_change()
{
    $(this).val($(this).val().toUpperCase());
    check_identifiant();
}

function prenom_change()
{
    $(this).val($(this).val()[0].toUpperCase() + $(this).val().slice(1));
    check_identifiant();
}

function check_identifiant()
{
    var nom = $("#nom").val().toLowerCase();
    var prenom = $("#prenom").val().toLowerCase();
    if (nom.length > 0 && prenom.length > 0) {
        var identifiant = prenom + '.' + nom;
        ajax_identifiant(identifiant);
    } else {
        $('#identifiant').val('');
    }    
}

function ajax_identifiant(identifiant)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/utilisateur/count-by-identifiant/' + identifiant,
        success: function(data) {
            // conversion des données en objet
            var count = JSON.parse(data);

            // actualisation
            update_identifiant(identifiant, count);
        }
    });
}

function update_identifiant(identifiant, count)
{
    if (count == 0) {
        $('#identifiant').val(identifiant);
    } else {
        count ++;
        $('#identifiant').val(identifiant + count);
    }
}




