$(document).ready(() => {
    $("#nom").change(nom_change);
    $("#prenom").change(prenom_change);
});

function nom_change()
{
    $(this).val($(this).val().toUpperCase());
    check_identifiant();
}

function prenom_change()
{
    $(this).val($(this).val()[0].toUpperCase() + $(this).val().slice(1));
    check_identifiant();
}

function check_identifiant()
{
    var nom = $("#nom").val().toLowerCase();
    var prenom = $("#prenom").val().toLowerCase();
    if (nom.length > 0 && prenom.length > 0) {
        var identifiant = prenom + '.' + nom;
        ajax_count_identifiant(identifiant);
    } else {
        $('#identifiant').val('');
    }    
}

function ajax_count_identifiant(identifiant)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/utilisateur/count-by-identifiant/' + identifiant,
        success: function(data) {
            // conversion des données en objet
            var count = JSON.parse(data);

            if (count == 0) {
                // identifiant inconnu, donc c'est ok
                $('#identifiant').val(identifiant);
            } else {
                // identifiant déjà utilisé
                // récupération de la dernière valeur
                ajax_last_identifiant(identifiant)
            }
        }
    });
}

function ajax_last_identifiant(identifiant)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/utilisateur/get-last-identifiant/' + identifiant,
        success: function(data) {
            // conversion des données en objet
            var last = JSON.parse(data);
            let count;

            if (last == identifiant) {
                // identifiant identique
                count = 1;
            } else {
                // on doit récupérer le compteur dans l'identifiant
                var end = last.substring(identifiant.length);
                const parsed = parseInt(end);
                if (isNaN(parsed)) {
                    // erreur !
                    count = 5554;
                } else {
                    count = parsed;
                }
            }

            count ++;
            $('#identifiant').val(identifiant + count);
        }
    });
}
