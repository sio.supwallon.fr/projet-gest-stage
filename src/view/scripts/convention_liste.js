$(document).ready(function () {

    $('#btn_accordion_1').click(function () {
        if(!$("#fleche1").hasClass("fas fa-caret-down")){
            $("#fleche1").removeClass("fa-caret-up"),
            $("#fleche1").addClass("fa-caret-down");
        }
        else{
            $("#fleche1").removeClass("fa-caret-down"),
            $("#fleche1").addClass("fa-caret-up");
        }
    });

    $('#btn_accordion_2').click(function () {
        if(!$("#fleche2").hasClass("fas fa-caret-down")){
            $("#fleche2").removeClass("fa-caret-up"),
            $("#fleche2").addClass("fa-caret-down");
        }
        else{
            $("#fleche2").removeClass("fa-caret-down"),
            $("#fleche2").addClass("fa-caret-up");
        }
    });

    $('#btn_accordion_3').click(function () {
        if(!$("#fleche3").hasClass("fas fa-caret-down")){
            $("#fleche3").removeClass("fa-caret-up"),
            $("#fleche3").addClass("fa-caret-down");
        }
        else{
            $("#fleche3").removeClass("fa-caret-down"),
            $("#fleche3").addClass("fa-caret-up");
        }
    });

    $('#btn_accordion_4').click(function () {
        if(!$("#fleche4").hasClass("fas fa-caret-down")){
            $("#fleche4").removeClass("fa-caret-up"),
            $("#fleche4").addClass("fa-caret-down");
        }
        else{
            $("#fleche4").removeClass("fa-caret-down"),
            $("#fleche4").addClass("fa-caret-up");
        }
    });

    $('#btn_accordion_5').click(function () {
        if(!$("#fleche5").hasClass("fas fa-caret-down")){
            $("#fleche5").removeClass("fa-caret-up"),
            $("#fleche5").addClass("fa-caret-down");
        }
        else{
            $("#fleche5").removeClass("fa-caret-down"),
            $("#fleche5").addClass("fa-caret-up");
        }
    });
});