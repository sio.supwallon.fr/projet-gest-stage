$(document).ready(function(){
    $(document).attr('nb', 1);
    $("#nb").change(nb_change);
    $("#lst_entreprise").change(lst_entreprise_change);
    $("#lst_classe").change(lst_classe_change);
    $("#lst_etudiant").change(lst_etudiant_change);


    // chargement des étudiants de la classe
    ajax_etudiants($("#lst_classe").val());
});

function lst_classe_change() {
    // récup de la valeur de la liste déroulante
    var id = $("#lst_classe").val();
    ajax_etudiants(id);
}

function ajax_etudiants(id_classe)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/etudiants/' + id_classe,
        success: function(data) {
            // conversion des données en objet
            var etudiants = JSON.parse(data);

            // purge de la liste
            $("#lst_etudiant").empty();

            // actualisation de la liste// actualisation de la liste
            lst_etudiant_refresh(etudiants);

            // actualisation de l'étudiant sélectionné
            ajax_etudiant($("#lst_etudiant").val());
        }
    });
}

function lst_etudiant_change() {
    // récup de la valeur de la liste déroulante
    var id = $("#lst_etudiant").val();
    ajax_etudiant(id);
}

function ajax_etudiant(id)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/etudiant/' + id,
        success: function(data) {
            // conversion des données en objet
            var etudiant = JSON.parse(data);

            // actualisation de la liste
            update_stagiaire(etudiant);
        }
    });
}

function lst_etudiant_refresh(etudiants)
{
    etudiants.forEach(etudiant => {
        if (document.etudiant != undefined && etudiant.id == document.etudiant.id) {
            $("#lst_etudiant").append('<option value="' + etudiant.id + '" selected>' + etudiant.nom + ' ' + etudiant.prenom +'</option>');
            update_stagiaire(etudiant);
        } else {
            $("#lst_etudiant").append('<option value="' + etudiant.id + '">' + etudiant.nom + ' ' + etudiant.prenom +'</option>');
        }
    });
}

function ajax_ville_etudiant(id)
{
    var ville = null;
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/ville/' + id,
        success: function(data) {
            // conversion des données en objet
            ville = JSON.parse(data);
            update_ville(ville);
        }
    });
}

function update_ville(ville)
{    
    // actualisation du formumlaire
    $("#stagiaire_ville").val(ville.nom);
    $("#stagiaire_cp").val(ville.code_postal);
}

function ajax_ville_etudiant_naissance(id)
{
    var ville = null;
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/ville/' + id,
        success: function(data) {
            // conversion des données en objet
            ville = JSON.parse(data);
            update_ville_naissance(ville);
        }
    });
}

function update_ville_naissance(ville)
{    
    // actualisation du formumlaire
    $("#stagiaire_ne-ville").val(ville.nom);
}

function update_stagiaire(etudiant)
{
    if (etudiant.id_ville_naissance != null) {
        ajax_ville_etudiant_naissance(etudiant.id_ville_naissance);
    }
    if (etudiant.id_ville != null) {
        ajax_ville_etudiant(etudiant.id_ville);
    }

    // actualisation du formulaire
    $("#stagiaire_date").val(etudiant.date_naissance);
    $("#stagiaire_num-secu").val(etudiant.num_secu);
    $("#stagiaire_adresse").val(etudiant.adresse);
    $("#stagiaire_tel").val(etudiant.telephone);
    $("#stagiaire_mail").val(etudiant.email);
}

function lst_entreprise_change() {
    // récup de la valeur de la liste déroulante
    var id = $("#lst_entreprise").val();

    ajax_entreprise(id);
    ajax_tuteur(id)

}


function ajax_entreprise(id)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/entreprise/' + id,
        success: function(data) {
            console.log(id);
            // conversion des données en objet
            var entreprise = JSON.parse(data);
            console.log(entreprise);
            // actualisation du formumlaire
            
            $("#raison_sociale").val(entreprise.raison_sociale);
            $("#entreprise_tel").val(entreprise.telephone);
            $("#entreprise_email").val(entreprise.email);
            $("#entreprise_adresse").val(entreprise.adresse);

            // actualisation ville
            ajax_ville(entreprise);

            //récupération du responsable
            ajax_responsable(entreprise.id_responsable);
        }
    });
}


function ajax_responsable(id)
{
    $.ajax({
        url: document.basePath + '/ajax/responsable/' + id,
        success: function(data) {
            var responsable = JSON.parse(data);
            console.log(responsable);
            $("#entreprise_representent").val(responsable.nom + " " + responsable.prenom);
            $("#entreprise_fonction").val(responsable.fonction);
        }
    });
}

function ajax_tuteur(id)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/tuteurs/' + id,
        success: function(data) {
            // conversion des données en objet
            var tuteurs = JSON.parse(data);
            $('.lst_tuteur').find('option').remove();
            tuteurs.forEach(tuteur => {
                $(".lst_tuteur").append('<option value="' + tuteur.id + '">' + tuteur.nom + ' ' + tuteur.prenom +'</option>');
            });
        }
    });
}

function ajax_ville(entreprise)
{
    $.ajax({
        url: document.basePath + '/ajax/ville/' + entreprise.id_ville,
        success: function(data) {
            var ville = JSON.parse(data);
            $("#entreprise_cp").val(ville.code_postal);
            $("#entreprise_ville").val(ville.nom);
            $("#entreprise_lieu-stages").val(( entreprise.adresse ) ? entreprise.adresse : "" + " " + ( entreprise.adresse ) ? ville.nom : "" + " " + ( entreprise.adresse ) ? ville.code_postal : "");
        }
    });
}

function nb_change() {
    var _old = $(document).attr('nb');
    var _new = $("#nb").val();
    
    if(_new < 1)
    {
        $("#nb").val(1);
        _new = 1;
    }
    else if(_new > 4)
    {
        $("#nb").val(4);
        _new = 4;
    }
    if (_old < _new) {
        // ajoute 1
        for(_old++;_old <= _new; _old++)
        {
            $("#zone").append(
                '<div id="nb' + _old + '" class="form-row">'
                + '<div class="col-md-2 mb-3">'
                + '<label>debut-' + _old + '</label>'
                + '<input type="date" class="form-control is-invalid" name="periode_au[]" required>'
                + '</div>'
    
                + '<div class="col-md-2 mb-3">'
                + '<label>fin-' + _old + '</label>'
                + '<input type="date" class="form-control is-invalid" name="periode_du[]" required>'
                + '</div>'

                + '<div class="col-md-2 mb-3">'
                + '<label>lieu-' + _old + '</label>'
                + '<input type="text" class="form-control " name="periode_lieu[]" required>'
                + '</div>'

                + '<div class="col-md-6 mb-3">'
                + '<label>tuteur-' + _old + '</label>'
                + '<select name="lst_tuteur[]" class="form-control lst_tuteur" required>'
                + '<option value=""></option>'
                + '</select>'
                + '</div>'
                + '</div>'
            );
        }
    } else {
        // enlève 1
        for(;_old > _new; _old--)
        {
            $("#nb" + _old).remove();
        }
    }
    $(document).attr('nb', _new);
    var id = $("#lst_entreprise").val();
    ajax_tuteur(id)
}