$(document).ready(function(){
    $("#btn_modifier_entreprise").click(modifier);
    $('#btn_delete_entreprise').click(btn_delete_entreprise_click);

    $("#naf").keyup(naf_keyup);

    $("#code_postal").keyup(code_postal_keyup);
    $("#id_ville").change(id_ville_change);

    $("#siege").change(update_siege);

    if($("#siege").attr('checked') == 'checked')
    {
        $("#les_sieges").hide();
        $("#les_entreprises").show();
    }
    else
    {
        $("#les_sieges").show();
        $("#les_entreprises").hide();
    }

});

function modifier(){

    $("#raisonSocial").removeAttr("readonly");
    $("#adresse").removeAttr("readonly");
    $("#telephone").removeAttr("readonly");
    $("#email").removeAttr("readonly");
    $("#naf").removeAttr("readonly");
    $("#ville").removeAttr("readonly");
    $("#responsable").removeAttr("readonly");
}

function btn_delete_entreprise_click()
{
    $('#modal-entreprise-delete').modal('show');
}

function naf_keyup(e)
{
    var id = $(e.target).attr('id');
    var naf = $('#' + id).val();
    let $select;
    $select = $("#id_naf");
            
    if (naf.length >= 2) {
        ajax_naf(naf, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Code</option>'));
    }
}

function ajax_naf(naf, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/entreprise/naf/' + naf,
        success: function(data) {
            // conversion des données en objet
            var nafs = JSON.parse(data);

            // actualisation de la liste
            update_select_naf(nafs, $select);
        }
    });
}

function update_select_naf(nafs, $select)
{
    $select.find('option').remove();

    if (nafs.length > 0) {
        $.each(nafs, (index, naf) => {
            $select.append($('<option value="' + naf.id + '">' + naf.code + '</option>'));
        });
        if (nafs.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function code_postal_keyup(e)
{
    var id = $(e.target).attr('id');
    var code_postal = $('#' + id).val();
    let $select;
    $select = $("#id_ville");
            
    if (code_postal.length >= 2) {
        ajax_ville_by_cp(code_postal, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Code Postal</option>'));
    }
}

function ajax_ville_by_cp(code_postal, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/ville/by-cp/' + code_postal,
        success: function(data) {
            // conversion des données en objet
            var villes = JSON.parse(data);

            // actualisation de la liste
            update_select_ville(villes, $select);
        }
    });
}

function update_select_ville(villes, $select)
{
    $select.find('option').remove();

    if (villes.length > 0) {
        $.each(villes, (index, ville) => {
            $select.append($('<option value="' + ville.id + '" data-code_postal="' + ville.code_postal + '" data-code_insee="' + ville.code_insee + '">' + ville.nom + '</option>'));
        });
        if (villes.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function id_ville_change()
{
    var code_postal = $("#id_ville option:selected").data('code_postal');
    $("#code_postal").val(code_postal);
}

function update_siege(){
    var valeur = $("#siege").attr('checked');
    if(valeur == 'checked'){
        $("#les_sieges").hide();
        $("#les_entreprises").show();
    }else{
        $("#les_sieges").show();
        $("#les_entreprises").hide();
    }
}

