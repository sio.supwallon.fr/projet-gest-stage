$(document).ready(function() {
    $("#motdepasse").keyup(checkPassword2);
    $("#motdepasse2").keyup(checkPassword2);
    $("#change_password").click(change_password_click);
    $("#signature").change(signature_change);

    $("input[id^=num_secu_g]").keyup(changeFocus);
    $("#num_secu_cle").keyup(changeFocus);

    $("input[id^=num_secu_g]").keyup(checkSecu);
    $("#num_secu_cle").keyup(checkSecu);

    $("input[name='sexe']").change(checkSecuSexe);
    $("#num_secu_g1").keyup(checkSecuSexe);

    $("#date_naissance").change(checkSecuDateNaissance);
    $("#num_secu_g2").keyup(checkSecuDateNaissance);
    $("#num_secu_g3").keyup(checkSecuDateNaissance);

    $("#code_postal").keyup(code_postal_keyup);
    $("#id_ville").change(id_ville_change);

    $("#code_postal_naissance").keyup(code_postal_keyup);
    $("#id_ville_naissance").change(id_ville_naissance_change);

    $("#id_ville_naissance").change(checkSecuVilleNaissance);
    $("#num_secu_g4").keyup(checkSecuVilleNaissance);
    $("#num_secu_g5").keyup(checkSecuVilleNaissance);

    $("#id_section").change(id_section_change);

    $('#btn_copy').click(btn_copy_click);
    $('#btn_email').click(btn_email_click);
    
    $('#btn_raz_erreurs').tooltip('enable');
    $('#btn_copy').tooltip('enable');
    $('#btn_email').tooltip('enable');
    $('#btn_show_hide_password').tooltip('enable');
    $('#btn_show_hide_password2').tooltip('enable');

    $('#btn_delete').click(btn_delete_click);
    $('#confirm-value').keyup(confirm_value_keyup);

    $('#btn_show_hide_password').click(show_hide_password);
    $('#btn_show_hide_password2').click(show_hide_password);
});

function show_hide_password()
{
    var btn_id = $(this).attr('id');
    var input_id = $(this).attr('for');

    if($('#' + input_id).attr('type') == 'password') {
        // show password
        $('#' + input_id).attr('type', 'text');
        $('#' + btn_id + ' i').addClass( 'fa-eye-slash' );
        $('#' + btn_id + ' i').removeClass( 'fa-eye' );
        $(this).attr('title', 'Masquer')
            .tooltip('_fixTitle')
            .tooltip('show');
        $('#' + input_id).focus();
    } else {
        // hide password
        $('#' + input_id).attr('type', 'password');
        $('#' + btn_id + ' i').addClass( 'fa-eye' );
        $('#' + btn_id + ' i').removeClass( 'fa-eye-slash' );
        $(this).attr('title', 'Afficher')
            .tooltip('_fixTitle')
            .tooltip('show');
        $('#' + input_id).focus();
    }
}

function confirm_value_keyup()
{
    var match = $('#confirm-match').text();
    var value = $(this).val();

    console.log('match: ' + match);
    console.log('value: ' + value);
    if(match == value) {
        $('#confirm-button').prop('disabled', false);
    } else {
        $('#confirm-button').prop('disabled', true);
    }
}

function btn_delete_click()
{
    $('#confirm-value').val('');
    $('#modal-delete').modal('show');
}

function btn_copy_click() {
    var $jeton = $("#jeton_activation");
    $jeton.select();
    document.execCommand("copy");
    $(this).attr('title', 'Lien copié')
          .tooltip('_fixTitle')
          .tooltip('show');
    $(this).focus();
}

function btn_email_click() {
    $('#modal-sendmail-welcome').modal('show');
}

function id_section_change()
{
    var id_section = $("#id_section option:selected").val();
    $select = $("#id_classe");
    ajax_classes_by_section(id_section, $select);
}

function ajax_classes_by_section(id_section, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/classes/by-section/' + id_section,
        success: function(data) {
            // conversion des données en objet
            var classes = JSON.parse(data);

            // actualisation de la liste
            update_select_classe(classes, $select);
        }
    });
}

function update_select_classe(classes, $select)
{
    $select.find('option').remove();

    $.each(classes, (index, classe) => {
        $select.append($('<option value="' + classe.id + '">' + classe.nom + '</option>'));
    });
}

function code_postal_keyup(e)
{
    var id = $(e.target).attr('id');
    var code_postal = $('#' + id).val();
    let $select;
    switch(id) {
        case 'code_postal':
            $select = $("#id_ville");
            break;
        case 'code_postal_naissance':
            $select = $("#id_ville_naissance");
            break;
    }
    if (code_postal.length >= 2) {
        ajax_ville_by_cp(code_postal, $select);
    } else {
        $select.find('option').remove();
        $select.append($('<option value="" disabled selected>Saisir un Code Postal</option>'));
    }
}

function ajax_ville_by_cp(code_postal, $select)
{
    $.ajax({
        // url = la route à appeler
        url: document.basePath + '/ajax/ville/by-cp/' + code_postal,
        success: function(data) {
            // conversion des données en objet
            var villes = JSON.parse(data);

            // actualisation de la liste
            update_select_ville(villes, $select);
        }
    });
}

function update_select_ville(villes, $select)
{
    $select.find('option').remove();

    if (villes.length > 0) {
        $.each(villes, (index, ville) => {
            $select.append($('<option value="' + ville.id + '" data-code_postal="' + ville.code_postal + '" data-code_insee="' + ville.code_insee + '">' + ville.nom + '</option>'));
        });
        if (villes.length == 1) {
            $select.change();
        }
    } else {
        $select.append($('<option value="" disabled selected>Aucun résultat</option>'));
    }
}

function id_ville_change()
{
    var code_postal = $("#id_ville option:selected").data('code_postal');
    $("#code_postal").val(code_postal);
}

function id_ville_naissance_change()
{
    var code_postal = $("#id_ville_naissance option:selected").data('code_postal');
    $("#code_postal_naissance").val(code_postal);
}

function changeFocus(e)
{
    var id = $(this).attr('id');
    var maxlength = $(this).attr('maxlength');
    var length = $(this).val().length;
    var inputCompleted = (length == maxlength);

    if (e.which == 8 && this.value.length == 0) {
        $(this).prev().focus();
    } else if (inputCompleted) {
        var num = parseInt(id.substring(10,11));
        var next = num + 1;
        $(this).next().focus();
    }
}

function checkPassword2()
{
    var element = $("#motdepasse2")[0];
    element.setCustomValidity(
        ($("#motdepasse").val() != $("#motdepasse2").val()) ? 'Les mots de passent de correspondent pas': ''
    );
}

function change_password_click() 
{
    $(this).hide();
    $("#password-block").show();
}

function signature_change() {
    var input = $(this)[0];
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#img-signature').css('background-image', 'url(' + e.target.result + ')');  
        }

        // load file
        reader.readAsDataURL(input.files[0]); 
    }
}

function checkSecuVilleNaissance()
{
    var code_insee = $("#id_ville_naissance option:selected").data('code_insee').toString();
    console.log(code_insee);
    if(code_insee != null && code_insee != '') {
        var dept = code_insee.substring(0,2); // DDVVV
        var ville = code_insee.substring(2,5); // DDVVV
        var num_secu_dept = $("#num_secu_g4").val();
        var num_secu_ville = $("#num_secu_g5").val();

        var expected_dept = (dept == num_secu_dept);
        var expected_ville = (ville == num_secu_ville);
    
        // Check secu dept
        var element = $("#num_secu_g4")[0];
        element.setCustomValidity(
            (!expected_dept) ? 'Ne concorde pas avec la ville de naissance': ''
        );
    
        // Check secu ville
        element = $("#num_secu_g5")[0];
        element.setCustomValidity(
            (!expected_ville) ? 'Ne concorde pas avec la ville de naissance': ''
        );
    
        // Check ville
        element = $("#id_ville_naissance")[0];
        element.setCustomValidity(
            (!expected_dept || !expected_ville) ? 'Ne concorde pas avec le numéro de sécurité sociale': ''
        );
    }
}

function checkSecuDateNaissance()
{
    var naissance = $("#date_naissance").val();

    if (naissance != '') {
        var annee = naissance.substring(2,4); // YYYY-MM-DD
        var mois = naissance.substring(5,7);
        var num_secu_annee = $("#num_secu_g2").val();
        var num_secu_mois = $("#num_secu_g3").val();

        var expected_mois = (mois == num_secu_mois);
        var expected_annee = (annee == num_secu_annee);
    
        // Check secu annee
        var element = $("#num_secu_g2")[0];
        element.setCustomValidity(
            (!expected_annee) ? 'Ne concorde pas avec la date de naissance': ''
        );
    
        // Check secu mois
        element = $("#num_secu_g3")[0];
        element.setCustomValidity(
            (!expected_mois) ? 'Ne concorde pas avec la date de naissance': ''
        );
    
        // Check naissance
        element = $("#date_naissance")[0];
        element.setCustomValidity(
            (!expected_annee || !expected_mois) ? 'Ne concorde pas avec le numéro de sécurité sociale': ''
        );
    }
}

function checkSecuSexe()
{
    var sexe = $("input[name='sexe']:checked").val();
    var num = $("#num_secu_g1").val();
    var expected = (sexe == 'M' && num == '1' || sexe == 'F' && num == '2');

    var element = $("#num_secu_g1")[0];
    element.setCustomValidity(
        (!expected) ? 'Ne concorde pas avec le sexe': ''
    );

    element = $("#sexe_F")[0];
    element.setCustomValidity(
        (!expected) ? 'Ne concorde pas avec le numéro de sécurité sociale': ''
    );
    element = $("#sexe_M")[0];
    element.setCustomValidity(
        (!expected) ? 'Ne concorde pas avec le numéro de sécurité sociale': ''
    );
}

function checkSecu()
{
    var txt_nir = '';
    $("input[id^=num_secu_g").each((index, element) => {
        var value = $(element).val();
        txt_nir += value;
    });

    var txt_cle = $(num_secu_cle).val();

    $("#num_secu").val(txt_nir + txt_cle);

    var nir = parseInt(txt_nir);
    var cle_attendue = 97 - (nir % 97);
    var cle = parseInt($("#num_secu_cle").val());

    var element = $("#num_secu_cle")[0];
    element.setCustomValidity(
        (cle != cle_attendue) ? 'La clé ne correspond pas.': ''
    );
}
