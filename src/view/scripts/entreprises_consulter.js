$(document).ready(function(){
    $("#filtre_region").change(filtre_region_change);
    $("#filtre_departement").change(filtre_departement_change);
    $("#filtre_ville").change(filtre_change);
    $("#filtre_naf").change(filtre_change);
    $("#filtre_section").change(filtre_change);
 
    $('#lst_entreprise').DataTable();
    $('.dataTables_length').addClass('bs-select');
});

function filtre_region_change(){
    var id_region = $("#filtre_region").val();
    $selectDep =  $("#filtre_departement");
    
        ajax_departement_change(id_region, $selectDep);
    
    
}

function ajax_departement_change(id_region, $selectDep){
    $.ajax({

        // url = la route à appeler
        url: document.basePath + '/ajax/entreprises/departement/' + id_region,
        success: function(data) {
            // conversion des données en objet
            var departements = JSON.parse(data);

            // affiche la liste des département en fonction de la région
            update_select_departement(departements, $selectDep);

            filtre_change();
        }
    });
}

function update_select_departement(departements, $selectDep)
{
    $selectDep.find('.change_departement').remove();
    $.each(departements, (index, departement) => {
        $selectDep.append($('<option class="change_departement" value="' + departement.id + '">' + departement.nom + '</option>'));
    });
}

function filtre_departement_change(){
    var id_departement = $("#filtre_departement option:selected").val();
    $selectVille =  $("#filtre_ville");
    ajax_ville_change(id_departement, $selectVille);
}

function ajax_ville_change(id_departement, $selectVille){
    $.ajax({

        // url = la route à appeler
        url: document.basePath + '/ajax/entreprises/ville/' + id_departement,
        success: function(data) {
            // conversion des données en objet
            var villes = JSON.parse(data);

            // affiche la liste des villes en fonction du département
            update_select_ville(villes, $selectVille);

            filtre_change();
        }
    });
}

function update_select_ville(villes, $selectVille)
{
    $selectVille.find('.change').remove();

    $.each(villes, (index, ville) => {
        $selectVille.append($('<option class="change" value="' + ville.id + '">' + ville.nom + '</option>'));
    });
}

function filtre_change() {
    // récup de la valeur de la liste déroulante
    var id_region = $("#filtre_region option:selected").val();
    var id_departement = $("#filtre_departement option:selected").val();
    var id_ville = $("#filtre_ville option:selected").val();
    var code_naf = $("#filtre_naf").val();
    var section = $("#filtre_section").val();

    $tabEntreprise = $("#tabEntreprise");

    var filter = '';
        if (id_region != 0) {
            filter += '?id_region=' + id_region;
        }
        if (id_departement != 0) {
            filter += '&id_departement=' + id_departement;
        }
        if (id_ville != 0) {
            filter += '&id_ville=' + id_ville;
        }
        if (code_naf) {
            if (id_region != 0) {
                filter += '&code_naf=' + code_naf;
            } else {
                filter += '?code_naf=' + code_naf;
            }
        }
        if (section != 0) {
            if (id_region != 0) {
                filter += '&section=' + section;
            } else {
                filter += '?section=' + section;
            }
        }

        var url = '/ajax/entreprises' + filter;
        ajax_entreprise(url, $tabEntreprise);
        

}

function ajax_entreprise(url, $tabEntreprise){

    //$('#url').text(url);
    $.ajax({
        url :document.basePath + url,
        success: function(data){

            var entreprises = JSON.parse(data);
            update_tableau_entreprise(entreprises, $tabEntreprise);
        }
    });

}


function ajax_ville(entreprise, $tabEntreprise){

    $.ajax({
        url :document.basePath + '/ajax/ville/' + entreprise.id_ville,
        success: function(data){

            var ville = JSON.parse(data);

            $tabEntreprise.append($(
                '<tr><th scope="row">' + entreprise.id + '</th><td>' + entreprise.raison_sociale + '</td><td>' + ville.nom + '</td><td><a class="btn btn-outline-secondary btn-block" href="' + document.basePath + '/entreprise/' + entreprise.id +'">Consulter</a></td></tr>'
            ));

        }
    });
}

function update_tableau_entreprise(entreprises, $tabEntreprise){
    $tabEntreprise.find('tr').remove();
    
    $.each(entreprises, (index, entreprise) => {
        ajax_ville(entreprise, $tabEntreprise);
        
    });
}
