$(document).ready(() => {
    $("#New_nom_classe").change(nom_change);


    $("#addEns").click(add);
    $("#removeEns").click(remove);

    $("#addEtudiant").click(addEtud);
    $("#removeEtudiant").click(removeEtud);
    
});

function nom_change()
{
    $(this).val($(this).val().toUpperCase());
    update_slug();
}

function update_slug()
{
    var nom = $("#New_nom_classe").val().toLowerCase();
    if (nom.length > 0) {
        var slug = nom;
        $('#New_slug_classe').val(slug);
    } else {
        $('#New_slug_classe').val('');
    }    
}

function add(){
    var options = $('#id_nEns option:selected').sort().clone();
    $('#id_enseignant').append(options);
    $('#id_nEns option:selected').remove();
}

function remove(){
    var options = $('#id_enseignant option:selected').sort().clone();
    $('#id_nEns').append(options);
    $('#id_enseignant option:selected').remove();
}

function addEtud(){
    var options = $('#id_nEtudiant option:selected').sort().clone();
    $('#id_etudiant').append(options);
    $('#id_nEtudiant option:selected').remove();
}

function removeEtud(){
    var options = $('#id_etudiant option:selected').sort().clone();
    $('#id_nEtudiant').append(options);
    $('#id_etudiant option:selected').remove();
}

    
