<h1 class="mt-0">
    Mot de passe modifié
</h1>
<p>
    Vous venez de mettre-à-jour votre mot de passe.
</p>
<p>
    Voici un rappel de vos identifiants pour vous connecter 
    sur notre application.
</p>
<p>
    <label class="col-form-label">Identifiant&nbsp;:</label>
    <input class="form-control" value="<?= $receiver->identifiant; ?>" disabled>
</p>
<p>
    <label class="col-form-label">Mot de passe&nbsp;:</label>
    <input class="form-control" value="<?= $password; ?>" disabled>
</p>
<p>
    Pour vous connecter, rendez-vous sur notre site à l'adresse suivante&nbsp;:
    <a href="http://<?= "{$host}{$basePath}"; ?>">
        http://<?= "{$host}{$basePath}"; ?>
    </a>.
</p>
<p>
    Conservez bien ce message car il ne sera pas possible de vous redonner ces informations.
</p>
<div class="alert alert-warning mb-0">
    <p class="my-0">
        <strong >
            Ce n'est pas vous qui êtes 
            à l'origine de cette action&nbsp;?
        </strong><br>
        &rarr;&nbsp; Connectez-vous sur votre compte 
                     et modifiez votre mot de passe.
    </p>
    <p class="mb-0">
        <strong>
            Vous n'arrivez plus à vous connecter 
            sur votre compte&nbsp;?
        </strong><br>
        &rarr;&nbsp; Contactez l'administrateur 
                     de l'application pour y remédier.
    </p>
</div>
