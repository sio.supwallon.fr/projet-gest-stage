<h1>Nouvel Utilisateur</h1>
<form method="POST" class="needs-validation form-password-confirm" novalidate enctype="multipart/form-data">
    <!-- Compte Utilisateur -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Compte Utilisateur</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <!-- Identifiant -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="identifiant">Identifiant</label> 
                        <input type="text" class="form-control" id="identifiant" name="identifiant" readonly>
                    </div>

                    <!-- Type -->
                    <div class="form-group col-0 col-md-4">
                        <label for="type">Type</label>
                        <select id="id_type_utilisateur" name="id_type_utilisateur" class="form-control">
<?php foreach($types as $type): ?> 
<?php if($connected_user->typeUtilisateur->niveau_habilitation >= $type->niveau_habilitation): ?> 
                            <option value="<?= $type->id ?>"><?= $type->nom ?></option>
<?php endif; ?>
<?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Informations Personnelles -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Personnelles</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <!-- Nom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="nom">Nom</label> 
                        <input type="text" class="form-control" id="nom" name="nom" placeholder="Ex: DUPONT" required>
                    </div>

                    <!-- Prénom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="prenom">Prénom</label> 
                        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Ex: Bernard" required>
                    </div>

                    <!-- E-Mail -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="email">E-Mail</label> 
                        <input type="email" class="form-control" id="email" name="email" placeholder="Ex: bernard.dupond@domain.com" required>
                        <div class="invalid-tooltip">
                            Veuillez saisir une adresse email valide.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="row mb-3">
        <!-- Buttons -->
        <div class="col p-0 text-right">
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer" name="btn_enregistrer" formaction="<?= $basePath ?>/utilisateur/new/submit?callback=<?= $callback ?>">Enregistrer</button>
            <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>
        </div>
    </div>
</form>
<?php require_once "utilisateur_details_modal_delete.html.php"; ?>