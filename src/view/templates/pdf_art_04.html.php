<strong style="font-size: 11pt;">Article&nbsp;4&nbsp;-&nbsp;Accueil et encadrement du stagiaire</strong><br>
Le stagiaire est suivi par l’enseignant référent désigné dans la présente convention ainsi que par le service de l’établissement en charge des stages. Le tuteur de stage désigné par l’organisme d’accueil dans la présente convention est chargé d’assurer le suivi du stagiaire et d’optimiser les conditions de réalisation du stage conformément aux stipulations pédagogiques définies.<br>
Le stagiaire est autorisé à revenir dans son établissement d’enseignement pendant la durée du stage pour y suivre des cours demandés explicitement par le programme ou pour participer à des réunions. Les dates sont portées à la connaissance de l’organisme d’accueil par l’établissement.<br>
Toute difficulté survenue dans la réalisation et le déroulement du stage, qu’elle soit constatée par le stagiaire ou par le tuteur de stage, doit être portée à la connaissance de l’enseignant référent et de l’établissement d’enseignement afin d’être résolue au plus vite.<br>
<br>
<strong>Modalités d’accompagnement&nbsp;:</strong><br>
<table>
<?php foreach($accompagnements as $accompagnement): ?>    
    <tr><th style="width: 150px;"><?= $accompagnement->nom; ?></th>
    <td><img style="width: 10pt;" src="src/view/images/icons/checkbox-1016505-200-<?= ($accompagnement->checked)?"":"un"; ?>checked.png"></td></tr>
<?php endforeach; ?>    
</table>
<br>