<strong style="font-size: 10pt;">6.2&nbsp;-&nbsp;Protection Accident du travail du stagiaire à l’étranger</strong><br>
1)&nbsp;<u>Pour pouvoir bénéficier de la législation française</u> sur la couverture accident de travail, le présent stage doit&nbsp;:<br>
 •&nbsp;Être d’une durée au plus égale à 6 mois, prolongations incluses&nbsp;;<br>
 •&nbsp;Ne donner lieu à aucune rémunération susceptible d’ouvrir des droits à une protection accident de travail dans le pays d’accueil&nbsp; une indemnité ou gratification est admise dans la limite de 13,75% du plafond horaire de la sécurité sociale (cf. point 5), et sous réserve de l’accord de la caisse primaire d’assurance maladie  sur la demande de maintien de droit&nbsp;;<br>
 •&nbsp;Se dérouler exclusivement dans l’organisme signataire de la présente convention&nbsp;;<br>
 •&nbsp;Se dérouler exclusivement dans le pays d’accueil étranger cité.<br>
Lorsque ces conditions ne sont pas remplies, l’organisme d’accueil s’engage à cotiser pour la protection du stagiaire et à faire les déclarations nécessaires en cas d’accident de travail.<br>
2)&nbsp;<u>La déclaration des accidents de travail</u> incombe à l’établissement d’enseignement qui doit en être informé par l’organisme d’accueil par écrit dans un délai de 48 heures.<br>
3)&nbsp;<u>La couverture concerne les accidents survenus&nbsp;:</u><br>
 •&nbsp;Dans l’enceinte du lieu de stage et aux heures du stage&nbsp;;<br>
 •&nbsp;Sur le trajet aller-retour habituel entre la résidence du stagiaire sur le territoire  étranger et le lieu de stage&nbsp;;<br>
 •&nbsp;Dans le cadre d’une mission confiée par l’organisme  d’accueil du stagiaire et obligatoirement par ordre de mission&nbsp;;<br>
 •&nbsp;Lors du premier trajet pour se rendre depuis son domicile sur le lieu de sa résidence durant le stage (déplacement à la date du début du stage)&nbsp;;<br>
 •&nbsp;Lors du dernier trajet de retour depuis sa résidence durant le stage à son domicile personnel.<br>
4)&nbsp;<u>Pour le cas où l’une seule des conditions prévues au point 6.2-1) n’est pas remplie</u>, l’organisme d’accueil s’engage à couvrir le/la stagiaire contre le risque d’accident de travail, de trajet et les maladies professionnelles et à en assurer toutes les déclarations nécessaires.<br>
5)&nbsp;<u>Dans tous les cas&nbsp;:</u><br>
 •&nbsp;Si l’étudiant est victime d’un accident de travail durant le stage, l’organisme d’accueil doit impérativement signaler immédiatement cet accident à l’établissement d’enseignement&nbsp;;<br>
 •&nbsp;Si l’étudiant remplit des missions limitées en-dehors de l’organisme d’accueil ou en-dehors du pays du stage, l’organisme d’accueil doit prendre toutes les dispositions nécessaires pour lui fournir les assurances appropriées.<br>