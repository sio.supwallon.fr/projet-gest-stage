<h1 class="mt-0">
    Activation de votre compte
</h1>
<p>
    Vous venez de finaliser l'activation de votre compte.
</p>
<p>
    Voici un rappel de vos identifiants pour vous connecter 
    sur notre application.
</p>
<p>
    <label class="col-form-label">Identifiant&nbsp;:</label>
    <input class="form-control" value="<?= $receiver->identifiant; ?>" disabled>
</p>
<p>
    <label class="col-form-label">Mot de passe&nbsp;:</label>
    <input class="form-control" value="<?= $password; ?>" disabled>
</p>
<p>
    Pour vous connecter, rendez-vous sur notre site à l'adresse suivante&nbsp;:
    <a href="http://<?= "{$host}{$basePath}"; ?>">
        http://<?= "{$host}{$basePath}"; ?>
    </a>.
</p>
<p>
    Conservez bien ce message car il ne sera pas possible de vous redonner ces informations.
</p>