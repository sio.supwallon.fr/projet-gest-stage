<form class="form-inline" method="POST">
    <label class="sr-only" for="login">Identifiant</label>
    <div class="input-group input-group-sm mr-2">
        <div class="input-group-prepend">
            <label class="input-group-text" for="login"><i class="fas fa-user"></i></label>
        </div>
        <input type="text" class="form-control" id="login" name="identifiant" placeholder="identifiant">
    </div>
    
    <label class="sr-only" for="password">Mot de passe</label>
    <div class="input-group input-group-sm mr-2">
        <div class="input-group-prepend">
            <label class="input-group-text" for="password"><i class="fas fa-lock"></i></label>
        </div>
        <input type="password" class="form-control form-control-sm" id="password" name="motdepasse" placeholder="mot de passe">
        <div class="input-group-append">
            <div class="input-group-text"><a href="<?= $basePath ?>/utilisateur/mot-de-passe-oublie?callback=<?= $current_uri; ?>" title="Mot de passe oublié"><i class="fas fa-question"></i></a></div>
        </div>
    </div>

    <button type="submit" class="btn btn-sm btn-outline-secondary" name="btn_login" formaction="<?= $basePath; ?>/utilisateur/login?callback=<?= $router->getUri(); ?>">
        <i class="fas fa-sign-in-alt"></i>
    </button>
</form>