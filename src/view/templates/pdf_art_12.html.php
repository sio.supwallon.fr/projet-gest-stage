<strong style="font-size: 11pt;">Article&nbsp;12&nbsp;-&nbsp;Fin de stage - Rapport - Evaluation</strong><br>
1)&nbsp;<u>Attestation de stage</u>&nbsp;: à l’issue du stage, l’organisme d’accueil délivre une attestation dont le modèle figure dans la circulaire d’organisation annuelle du diplôme préparé, mentionnant au minimum la durée effective du stage et, le cas échéant, le montant de la gratification perçue. Le stagiaire devra produire cette attestation à l’appui de sa demande éventuelle d’ouverture  de droits au régime général d’assurance vieillesse  prévue à l’art. L351-17 du code de la sécurité sociale ;<br>
2)&nbsp;<u>Evaluation de l’activité du stagiaire</u>&nbsp;: à l’issue du stage, l’organisme  d’accueil renseigne une fiche d’évaluation de l’activité du stagiaire qu’il retourne à l’enseignant référent.<br>
4)&nbsp;<u>Modalités d’évaluation pédagogiques</u>&nbsp;:  le stagiaire devra réaliser tout travail demandé par l’équipe pédagogique de son établissement d’enseignement.<br>
5)&nbsp;<u>Remboursement des frais de déplacement</u>&nbsp;: le tuteur de l’organisme d’accueil ou tout membre de l’organisme d’accueil appelé à se rendre dans l’établissement d’enseignement dans le cadre de la préparation, du déroulement et de la validation du stage ne peut prétendre à une quelconque prise en charge ou indemnisation de la part de l’établissement d’enseignement.<br>
<br>
<strong>Nombre d’ECTS accordés</strong> (le cas échéant)&nbsp;: 
<?php if($convention->nb_ects): ?><?= $convention->nb_ects; ?>.<br>
<?php else: ?><i>Aucune indication.</i><br>
<?php endif; ?>