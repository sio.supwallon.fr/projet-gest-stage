<?php 
namespace GEST_STAGE\view\templates;

use GEST_STAGE\kernel\Session;

?>
<h1>Liste des utilisateurs</h1>

<a class="btn btn-outline-primary mb-3" href="<?= $basePath; ?>/utilisateur/new?callback=<?= $current_uri; ?>"><i class="fas fa-user-plus"></i> Ajouter</a>

<div class="row">

    <div class="col-4 col-md-3 col-lg-2">
    <div class="dropdown">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= (isset($selected_type))?$selected_type->nom:"&lt;Tous les types&gt;"; ?>        
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="<?= $basePath ?>/utilisateurs">&lt;Tous les types&gt;</a>
<?php foreach($types as $type): ?>
            <a class="dropdown-item" href="<?= $basePath ?>/utilisateurs/type/<?= $type->slug ?>"><?= $type->nom ?></a>
<?php endforeach; ?>
        </div>
    </div>
    </div>

    <div class="col-8 col-md-9 col-lg-10">
    <div class="row">

    <!-- Pagination 1 -->
    <div class="col-12 col-lg-5 col-xl-4">
    <nav>
        <ul class="pagination">
<?php
for($i = 0; $i < 9; $i++):
    $current = $lettres[$i];

    $enabled = in_array($current, $lettres_enabled);

    if($enabled && $uri == '/utilisateurs/'. ((!isset($selected_type))?"":$selected_type->slug."/") . $current): 
    $enabled_lettre = $current;
?>
            <li class="page-item text-center active" style="width: 2rem;">
<?php elseif ($enabled): ?>
            <li class="page-item text-center" style="width: 2rem;">
<?php else: ?>
            <li class="page-item text-center disabled" style="width: 2rem;">
<?php endif; ?>
                <a class="page-link" href="<?= $basePath ?><?= (!isset($selected_type))?"/utilisateurs/{$current}":"/utilisateurs/{$selected_type->slug}/{$current}"; ?>"><?= $current; ?></a>
            </li>
<?php endfor; ?>
        </ul>
    </nav>
    </div>

    <!-- Pagination 2 -->
    <div class="col-12 col-md-6 col-lg-5 col-xl-4">
    <nav>
        <ul class="pagination">
<?php
for(; $i < 18; $i++):
    $current = $lettres[$i];

    $enabled = in_array($current, $lettres_enabled);

    if($enabled && $uri == '/utilisateurs/'. ((!isset($selected_type))?"":$selected_type->slug."/") . $current): 
    $enabled_lettre = $current;
?>
            <li class="page-item text-center active" style="width: 2rem;">
<?php elseif ($enabled): ?>
            <li class="page-item text-center" style="width: 2rem;">
<?php else: ?>
            <li class="page-item text-center disabled" style="width: 2rem;">
<?php endif; ?>
                <a class="page-link" href="<?= $basePath ?><?= (!isset($selected_type))?"/utilisateurs/{$current}":"/utilisateurs/{$selected_type->slug}/{$current}"; ?>"><?= $current; ?></a>
            </li>
<?php endfor; ?>
        </ul>
    </nav>
    </div>

    <!-- Pagination 3 -->
    <div class="col-12 col-xl-4">
    <nav>
        <ul class="pagination">
<?php
for(; $i < 26; $i++):
    $current = $lettres[$i];
    
    $enabled = in_array($current, $lettres_enabled);

    if($enabled && $uri == '/utilisateurs/'. ((!isset($selected_type))?"":$selected_type->slug."/") . $current): 
    $enabled_lettre = $current;
?>
            <li class="page-item text-center active" style="width: 2rem;">
<?php elseif ($enabled): ?>
            <li class="page-item text-center" style="width: 2rem;">
<?php else: ?>
            <li class="page-item text-center disabled" style="width: 2rem;">
<?php endif; ?>
                <a class="page-link" href="<?= $basePath ?><?= (!isset($selected_type))?"/utilisateurs/{$current}":"/utilisateurs/{$selected_type->slug}/{$current}"; ?>"><?= $current; ?></a>
            </li>
<?php endfor; ?>
        </ul>
    </nav>
    </div>


    </div>
    </div>
</div>

<!-- Liste des Utilisateurs -->
<ul>
<?php foreach($utilisateurs as $utilisateur): ?>
    <li>
<?php if(Session::isGranted("/utilisateur/{id}")): ?>
        <a class="<?= ($utilisateur->active)?"text-dark":"text-danger"; ?>" href="<?= $basePath; ?>/utilisateur/<?= $utilisateur->id . "?callback=" . '/utilisateurs/'. ((!isset($selected_type))?"":"{$selected_type->slug}/") . $enabled_lettre; ?>">
        <?= $utilisateur->prenom; ?> <?= $utilisateur->nom; ?>
        </a>
<?php else: ?>
        <?= $utilisateur->prenom; ?> <?= $utilisateur->nom; ?>
<?php endif; ?>
    </li>
<?php endforeach; ?>
</ul>
