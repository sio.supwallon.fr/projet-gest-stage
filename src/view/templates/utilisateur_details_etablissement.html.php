    <!-- Informations Spécifiques Etablissement -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Spécifiques</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="form-row">
<?php require_once "utilisateur_details_signature.html.php" ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
