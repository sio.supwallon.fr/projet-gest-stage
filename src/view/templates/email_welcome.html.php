<h1 class="mt-0">
    Bienvenue 
    <?= !empty($receiver->civilite)?$receiver->civilite:""; ?>
    <?= !empty($receiver->prenom)?$receiver->prenom:""; ?>
    <?= !empty($receiver->nom)?$receiver->nom:""; ?>
</h1>
<p>
    <?= !empty($sender->civilite)?$sender->civilite:""; ?>
    <?= !empty($sender->prenom)?$sender->prenom:""; ?>
    <?= !empty($sender->nom)?$sender->nom:""; ?>
    vient de créer votre lien d'activation pour accéder à l'application <?= $application['title']; ?>.
</p>
<p>
    Cette application vous permet de gérer les conventions de stage avec l'établissement&nbsp;: <?= $etablissement['nom']; ?>
</p>
<p>
    Pour vous connecter, utiliser ce lien d'<a href="<?= "http://{$host}{$basePath}/utilisateur/link/{$receiver->identifiant}?jeton=" . $receiver->jeton_activation; ?>">activation</a>.
</p>
