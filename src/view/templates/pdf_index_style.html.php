        <style>
            *, *::after, *::before { box-sizing: border-box; }
            body { font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"; }
            .text-primary { color: #007bff!important; }
            .text-secondary { color: #6c757d!important; }
            .text-success { color: #28a745!important; }
            .text-danger { color: #dc3545!important; }
            .text-warning { color: #ffc107!important; }
            .text-info { color: #17a2b8!important; }
            .text-light { color: #f8f9fa!important;}
            .text-dark { color: #343a40!important;}
            .text-body { color: #212529!important;}
            .text-muted { color: #6c757d!important;}
            .text-white { color: #fff!important;}
            .text-black-50 { color: rgba(0,0,0,.5)!important;}
            .text-white-50 { color: rgba(255,255,255,.5)!important;}
            .bg-primary { background-color: #007bff!important; }
            .bg-secondary { background-color: #6c757d!important; }
            .bg-success { background-color: #28a745!important; }
            .bg-danger { background-color: #dc3545!important; }
            .bg-warning { background-color: #ffc107!important; }
            .bg-info { background-color: #17a2b8!important; }
            .bg-light { background-color: #f8f9fa!important; }
            .bg-dark { background-color: #343a40!important; }
            .bg-white { background-color: #fff!important; }
            .bg-transparent { background-color: transparent!important; }
            .border { border: 1px solid #dee2e6!important; }
            .border-top { border-top: 1px solid #dee2e6!important; }
            .border-right { border-right: 1px solid #dee2e6!important; }
            .border-bottom { border-bottom: 1px solid #dee2e6!important; }
            .border-left { border-left: 1px solid #dee2e6!important; }
            .border-0 { border: 0!important; }
            .border-top-0 { border-top: 0!important; }
            .border-right-0 { border-right: 0!important; }
            .border-bottom-0 { border-bottom: 0!important; }
            .border-left-0 { border-left: 0!important; }
            .border-primary { border-color: #007bff!important; }
            .border-secondary { border-color: #6c757d!important; }
            .border-success { border-color: #28a745!important; }
            .border-danger { border-color: #dc3545!important; }
            .border-warning { border-color: #ffc107!important; }
            .border-info { border-color: #17a2b8!important; }
            .border-light { border-color: #f8f9fa!important; }
            .border-dark { border-color: #343a40!important; }
            .border-white { border-color: #fff!important; }
            .rounded { border-radius: .25em!important; }
            .rounded-top { 
                border-top-left-radius: .25em!important; 
                border-top-right-radius: .25em!important; 
            }
            .rounded-right { 
                border-top-right-radius: .25em!important; 
                border-bottom-right-radius: .25em!important; 
            }
            .rounded-bottom { 
                border-bottom-right-radius: .25em!important; 
                border-bottom-left-radius: .25em!important; 
            }
            .rounded-left { 
                border-top-left-radius: .25em!important; 
                border-bottom-left-radius: .25em!important; 
            }
            .rounded-circle { border-radius: 50%!important; }
            .rounded-pill { border-radius: 50em!important; }
            .rounded-0 { border-radius: 0!important; }
            .m-0 { margin: 0!important; }
            .m-1 { margin: .25em!important; }
            .m-2 { margin: .5em!important; }
            .m-3 { margin: 1em!important; }
            .m-4 { margin: 1.5em!important; }
            .m-5 { margin: 3em!important; }
            .m-auto { margin: auto!important; }
            .mt-0 { margin-top: 0!important; }
            .mt-1 { margin-top: .25em!important; }
            .mt-2 { margin-top: .5em!important; }
            .mt-3 { margin-top: 1em!important; }
            .mt-4 { margin-top: 1.5em!important; }
            .mt-5 { margin-top: 3em!important; }
            .mt-auto { margin-top: auto!important; }
            .mr-0 { margin-right: 0!important; }
            .mr-1 { margin-right: .25em!important; }
            .mr-2 { margin-right: .5em!important; }
            .mr-3 { margin-right: 1em!important; }
            .mr-4 { margin-right: 1.5em!important; }
            .mr-5 { margin-right: 3em!important; }
            .mr-auto { margin-right: auto!important; }
            .mb-0 { margin-bottom: 0!important; }
            .mb-1 { margin-bottom: .25em!important; }
            .mb-2 { margin-bottom: .5em!important; }
            .mb-3 { margin-bottom: 1em!important; }
            .mb-4 { margin-bottom: 1.5em!important; }
            .mb-5 { margin-bottom: 3em!important; }
            .mb-auto { margin-bottom: auto!important; }
            .ml-0 { margin-left: 0!important; }
            .ml-1 { margin-left: .25em!important; }
            .ml-2 { margin-left: .5em!important; }
            .ml-3 { margin-left: 1em!important; }
            .ml-4 { margin-left: 1.5em!important; }
            .ml-5 { margin-left: 3em!important; }
            .ml-auto { margin-left: auto!important; }
            .mx-0 { 
                margin-left: 0!important; 
                margin-right: 0!important; 
            }
            .mx-1 { 
                margin-left: .25em!important; 
                margin-right: .25em!important; 
            }
            .mx-2 { 
                margin-left: .5em!important; 
                margin-right: .5em!important; 
            }
            .mx-3 { 
                margin-left: 1em!important; 
                margin-right: 1em!important; 
            }
            .mx-4 { 
                margin-left: 1.5em!important; 
                margin-right: 1.5em!important; 
            }
            .mx-5 { 
                margin-left: 3em!important; 
                margin-right: 3em!important; 
            }
            .mx-auto { 
                margin-left: auto!important; 
                margin-right: auto!important; 
            }
            .my-0 { 
                margin-top: 0!important; 
                margin-bottom: 0!important; 
            }
            .my-1 { 
                margin-top: .25em!important; 
                margin-bottom: .25em!important; 
            }
            .my-2 { 
                margin-top: .5em!important; 
                margin-bottom: .5em!important; 
            }
            .my-3 { 
                margin-top: 1em!important; 
                margin-bottom: 1em!important; 
            }
            .my-4 { 
                margin-top: 1.5em!important; 
                margin-bottom: 1.5em!important; 
            }
            .my-5 { 
                margin-top: 3em!important; 
                margin-bottom: 3em!important; 
            }
            .my-auto { 
                margin-top: auto!important; 
                margin-bottom: auto!important; 
            }
            .p-0 { padding: 0!important; }
            .p-1 { padding: .25em!important; }
            .p-2 { padding: .5em!important; }
            .p-3 { padding: 1em!important; }
            .p-4 { padding: 1.5em!important; }
            .p-5 { padding: 3em!important; }
            .pt-0 { padding-top: 0!important; }
            .pt-1 { padding-top: .25em!important; }
            .pt-2 { padding-top: .5em!important; }
            .pt-3 { padding-top: 1em!important; }
            .pt-4 { padding-top: 1.5em!important; }
            .pt-5 { padding-top: 3em!important; }
            .pr-0 { padding-right: 0!important; }
            .pr-1 { padding-right: .25em!important; }
            .pr-2 { padding-right: .5em!important; }
            .pr-3 { padding-right: 1em!important; }
            .pr-4 { padding-right: 1.5em!important; }
            .pr-5 { padding-right: 3em!important; }
            .pb-0 { padding-bottom: 0!important; }
            .pb-1 { padding-bottom: .25em!important; }
            .pb-2 { padding-bottom: .5em!important; }
            .pb-3 { padding-bottom: 1em!important; }
            .pb-4 { padding-bottom: 1.5em!important; }
            .pb-5 { padding-bottom: 3em!important; }
            .pl-0 { padding-left: 0!important; }
            .pl-1 { padding-left: .25em!important; }
            .pl-2 { padding-left: .5em!important; }
            .pl-3 { padding-left: 1em!important; }
            .pl-4 { padding-left: 1.5em!important; }
            .pl-5 { padding-left: 3em!important; }
            .px-0 { 
                padding-left: 0!important; 
                padding-right: 0!important; 
            }
            .px-1 { 
                padding-left: .25em!important; 
                padding-right: .25em!important; 
            }
            .px-2 { 
                padding-left: .5em!important; 
                padding-right: .5em!important; 
            }
            .px-3 { 
                padding-left: 1em!important; 
                padding-right: 1em!important; 
            }
            .px-4 { 
                padding-left: 1.5em!important; 
                padding-right: 1.5em!important; 
            }
            .px-5 { 
                padding-left: 3em!important; 
                padding-right: 3em!important; 
            }
            .py-0 { 
                padding-top: 0!important; 
                padding-bottom: 0!important; 
            }
            .py-1 { 
                padding-top: .25em!important; 
                padding-bottom: .25em!important; 
            }
            .py-2 { 
                padding-top: .5em!important; 
                padding-bottom: .5em!important; 
            }
            .py-3 { 
                padding-top: 1em!important; 
                padding-bottom: 1em!important; 
            }
            .py-4 { 
                padding-top: 1.5em!important; 
                padding-bottom: 1.5em!important; 
            }
            .py-5 { 
                padding-top: 3em!important; 
                padding-bottom: 3em!important; 
            }
            .shadow-none { box-shadow: none!important; }
            .shadow-sm { box-shadow: 0 .125em .25em rgba(0,0,0,.075)!important; }
            .shadow { box-shadow: 0 .5em 1em rgba(0,0,0,.15)!important; }
            .shadow-lg { box-shadow: 0 1em 3em rgba(0,0,0,.175)!important; }
            .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
                margin-bottom: .5em;
                font-weight: 500;
                line-height: 1.2;
            }
            .h1, h1 { font-size: 2.5em; }
            .h2, h2 { font-size: 2em; }
            .h3, h3 { font-size: 1.75em; }
            .h4, h4 { font-size: 1.5em; }
            .h5, h5 { font-size: 1.25em; }
            .h6, h6 { font-size: 1em; }
            .lead {
                font-size: 1.25em;
                font-weight: 300;
            }
            del, s { text-decoration: line-through; }
            ins, u { text-decoration: underline; }
            .small, small { font-size: 80%; font-weight: 400; }
            b, strong { font-weight: bolder; }
            em { font-style: italic; }
            .font-weight-bold { font-weight: bold; }
            .blockquote { margin-bottom: 1em; font-size: 1.25em; }
            blockquote { margin: 0 0 1em; }
            .text-center { text-align: center!important; }
            .text-right { text-align: right!important; }
            .text-left { text-align: left!important; }
            .navbar { position: relative; display: flex; padding: .5em 1em; }
            .navbar-light .navbar-brand { color: rgba(0,0,0,.9); }
            .navbar-brand {
                display: inline-block;
                padding-top: .3125em;
                padding-bottom: .3125em;
                margin-right: 1em;
                font-size: 1.25em;
                line-height: inherit;
                white-space: nowrap;
            }
            .alert {
                position: relative;
                padding: .75em 1.25em;
                margin-bottom: 1em;
                border: 1px solid transparent;
                border-radius: .25em;
            }
            .alert-primary {
                color: #004085;
                background-color: #cce5ff;
                border-color: #b8daff;
            }
            .alert-secondary {
                color: #383d41;
                background-color: #e2e3e5;
                border-color: #d6d8db;
            }
            .alert-success {
                color: #155724;
                background-color: #d4edda;
                border-color: #c3e6cb;
            }
            .alert-danger {
                color: #721c24;
                background-color: #f8d7da;
                border-color: #f5c6cb;
            }
            .alert-warning {
                color: #856404;
                background-color: #fff3cd;
                border-color: #ffeeba;
            }
            .alert-light {
                color: #818182;
                background-color: #fefefe;
                border-color: #fdfdfe;
            }
            .alert-dark {
                color: #1b1e21;
                background-color: #d6d8d9;
                border-color: #c6c8ca;
            }
            .row {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .col, .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                position: relative;
                width: 100%; min-height: 1px;
                padding-right: 15px; padding-left: 15px;
            }
            .col { flex-basis: 0; flex-grow: 1; max-width: 100%; }
            .col-1 { flex: 0 0 8.333333%; max-width: 8.333333%; }
            .col-2 { flex: 0 0 16.666667%; max-width: 16.666667%; }
            .col-3 { flex: 0 0 25%; max-width: 25%; }
            .col-4 { flex: 0 0 33.333333%; max-width: 33.333333%; }
            .col-5 { flex: 0 0 41.666667%; max-width: 41.666667%; }
            .col-6 {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 50%;
                flex: 0 0 50%;
                max-width: 50%;
            }
            .col-7 { flex: 0 0 58.333333%; max-width: 58.333333%; }
            .col-8 { flex: 0 0 66.666667%; max-width: 66.666667%; }
            .col-9 { flex: 0 0 75%; max-width: 75%; }
            .col-10 { flex: 0 0 83.333333%; max-width: 83.333333%; }
            .col-11 { flex: 0 0 91.666667%; max-width: 91.666667%; }
            .col-12 { flex: 0 0 100%; max-width: 100%; }
            .form-control { display: inline-block; padding: .375em .75em; font-size: 1em;
                line-height: 1.5; color: #495057; background-color: #e9ecef; 
                background-clip: padding-box; border: 1px solid #ced4da; 
                border-radius: .25em; }
            .col-form-label { 
                display: inline-block; width: 7em;
                padding-top: calc(.375em + 1px); padding-bottom: calc(.375em + 1px);
                margin-bottom: 0; font-size: inherit; line-height: 1.5; 
            }
        </style>
