<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>

<?php //if(Session::isGranted("/utilisateurs/{lettre}")): ?>
<?php if(isset($etudiant)): ?>
<script>
    document.etudiant = JSON.parse('<?= json_encode($etudiant); ?>');
</script>
<?php endif;
?>

<!-- Partie utilisateur et entreprise-->
<form method="POST" class="needs-validation">
    <div id="organisme_acceuil">
    <?php if (Session::isGranted("/convention/creer#organisme_acceuil")):?>
        <h2>L'organisme d'accueil</h2>
        <br>


        <div>
            <select id="lst_entreprise" name="lst_entreprise" class="form-control" required>
                <label>Pour sélectionner une entreprise qui est déjà utilisateur</label>
                <option disabled selected> sélectionner une entreprise </option>
                <?php foreach ($entreprises as $entreprise): ?> 
                        <option value="<?= $entreprise->id; ?>"> <?= $entreprise->raison_sociale; ?> </option>
                <?php endforeach ?>
            </select>
        </div>
        <br>
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Raison Sociale" id="raison_sociale" name="raison_sociale" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="tel" class="form-control " placeholder="Tel" name="entreprise_tel" id="entreprise_tel" pattern="^((+\d{1,3}((-)\d{3})?(-| )?(?\d)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$" id="stagiaire_tel" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="email" class="form-control " placeholder="Mèl" name="entreprise_email" id="entreprise_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Adresse" name="entreprise_adresse" id="entreprise_adresse" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="CP" name="entreprise_cp" id="entreprise_cp" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville" name="entreprise_ville" id="entreprise_ville" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Représenter par" name="entreprise_representent" id="entreprise_representent" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Fonction"name="entreprise_fonction" id="entreprise_fonction" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-6 mb-3">
                <input type="text" class="form-control " placeholder="Service dans lequel le stage sera effectué" name="entreprise_service_stage" id="entreprise_service-stage">
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Lieu du stage" name="entreprise_lieu_stage" id="entreprise_lieu-stages">
            </div>
        </div>
    <?php endif; ?>


    <?php if (! Session::isGranted("/convention/creer#organisme_acceuil")):?>
        <h2>L'organisme d'accueil</h2>
        <br>


        <div>
            <select id="lst_entreprise" name="lst_entreprise" class="form-control" disabled required>
                <label>Pour sélectionner une entreprise qui est déjà utilisateur</label>
                <option disabled selected> sélectionner une entreprise </option>
                <?php foreach ($entreprises as $entreprise): ?> 
                        <option value="<?= $entreprise->id; ?>"> <?= $entreprise->raison_sociale; ?> </option>
                <?php endforeach ?>
            </select>
        </div>
        <br>
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Raison Sociale" id="raison_sociale" name="raison_sociale" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="tel" class="form-control " placeholder="Tel" name="entreprise_tel" id="entreprise_tel" pattern="^((+\d{1,3}((-)\d{3})?(-| )?(?\d)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$" id="stagiaire_tel" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="email" class="form-control " placeholder="Mèl" name="entreprise_email" id="entreprise_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Adresse" name="entreprise_adresse" id="entreprise_adresse" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="CP" name="entreprise_cp" id="entreprise_cp" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville" name="entreprise_ville" id="entreprise_ville" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Représenter par" name="entreprise_representent" id="entreprise_representent" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Fonction"name="entreprise_fonction" id="entreprise_fonction" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-6 mb-3">
                <input type="text" class="form-control " placeholder="Service dans lequel le stage sera effectué" name="entreprise_service_stage" id="entreprise_service-stage" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Lieu du stage" name="entreprise_lieu_stage" id="entreprise_lieu-stages" readonly>
            </div>
        </div>
    <?php endif; ?>
    </div>



<!-- Partie étudiant -->
    <div id="stagiaire">
    <?php if (Session::isGranted("/convention/creer#stagiaire")):?>
        <h2>Le stagiaire</h2>
        <br>

        <div>
            <select id="lst_classe" name="lst_classe" class="form-control" required>
                <?php foreach ($classes as $classe): ?>
                <?php if(isset($etudiant) && $etudiant->id_classe == $classe->id): ?>
                <option value="<?= $classe->id; ?>" selected><?= $classe->nom; ?></option>
                <?php else: ?>
                <option value="<?= $classe->id; ?>"><?= $classe->nom; ?></option>
                <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <br>
        <div>
            <select id="lst_etudiant" name="lst_etudiant" class="form-control" required>
            </select>
        </div>
        <br>
    
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="date" class="form-control " name="stagiaire_date" id="stagiaire_date" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville de naissance" name="stagiaire_ne_ville" id="stagiaire_ne-ville" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="N° Sécurité social" name="stagiaire_num_secu" id="stagiaire_num-secu" readonly>
            </div>
        </div>
    

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Adresse" name="stagiaire_adresse" id="stagiaire_adresse" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="CP" name="stagiaire_cp" id="stagiaire_cp" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville" name="stagiaire_ville" id="stagiaire_ville" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
            <input type="tel" class="form-control " placeholder="Tel" name="stagiaire_tel" pattern="^((+\d{1,3}((-)\d{3})?(-| )?(?\d)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$" id="stagiaire_tel" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="email" class="form-control " placeholder="Mèl" name="stagiaire_mail" id="stagiaire_mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="CPAM" name="stagiaire_cpam">
            </div>
        </div>
        <?php else: ?>


        <h2>Le stagiaire</h2>
        <br>

        <div>
            <select id="lst_classe" name="lst_classe" class="form-control" disabled required>
                <?php foreach ($classes as $classe): ?>
                <?php if(isset($etudiant) && $etudiant->id_classe == $classe->id): ?>
                <option value="<?= $classe->id; ?>" selected><?= $classe->nom; ?></option>
                <?php else: ?>
                <option value="<?= $classe->id; ?>"><?= $classe->nom; ?></option>
                <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <br>
        <div>
            <select id="lst_etudiant" name="lst_etudiant" class="form-control" disabled required>
            </select>
        </div>
        <br>
    
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="date" class="form-control " name="stagiaire_date" id="stagiaire_date" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville de naissance" name="stagiaire_ne_ville" id="stagiaire_ne-ville" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="N° Sécurité social" name="stagiaire_num_secu" id="stagiaire_num-secu" readonly>
            </div>
        </div>
    

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Adresse" name="stagiaire_adresse" id="stagiaire_adresse" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="CP" name="stagiaire_cp" id="stagiaire_cp" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Ville" name="stagiaire_ville" id="stagiaire_ville" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-4 mb-3">
            <input type="tel" class="form-control " placeholder="Tel" name="stagiaire_tel" pattern="^((+\d{1,3}((-)\d{3})?(-| )?(?\d)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$" id="stagiaire_tel" readonly>
            </div>
            <div class="col-md-4 mb-3">
                <input type="email" class="form-control " placeholder="Mèl" name="stagiaire_mail" id="stagiaire_mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$" readonly>
            </div>
        </div>
    <?php endif; ?>
    </div>





<!-- Partie utilisateur, tuteur et entreprise-->
    <div id="periode_stage">
        <h2>Periode(s) de stage</h2>
        <br>


        <div class="col-4">
            Nombre de période:<input id="nb" type="number" value="1" min="1" max="4" class="form-control">
        </div>
            <div id="zone">
                <div class="form-row">
                    <div class="col-md-2 mb-3">
                        <label>debut-1</label>
                        <input type="date" class="form-control " name="periode_du[]" required>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label>fin-1</label>
                        <input type="date" class="form-control " name="periode_au[]" required>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label>lieu-1</label>
                        <input type="text" class="form-control " name="periode_lieu[]" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>tuteur-1</label>
                        <select name="lst_tuteur[]" class="form-control lst_tuteur" required>
                            <option class="form-control" selected required></option>
                        </select>
                    </div>
                </div>
            </div>
        <p>Représentant une durée totale de 4  semaines</p>
    </div>



<!-- Partie tuteur et entreprise-->
    <div id="stage">
    <?php if (Session::isGranted("/convention/creer#stage")):?>
        <h2>Le stage</h2>
        <br>
    

        <div class="form-row">
            <div class="col-md-4 mb-3">
                <textarea class="form-control " placeholder="Activités confiées" name="stage_activite"></textarea>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Compétences à acquérir ou à développer" name="stage_competence">
            </div>
            <p>Si le stagiaire doit être présent dans l’organisme d’accueil la nuit, le dimanche ou un jour férié, préciser les cas particuliers :</p>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Présence ou aucune modification" name="stage_presence">
            </div>
            <p>Soit une durée totale en jour ouvrables de :</p> 
            <div class="col-md-auto mb-3">
                <input type="number" class="form-control " placeholder="nb de jours" name="stage_nb_jours" min="0" max="100">
            </div>
        </div>
    <?php else: ?>
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Activités confiées" name="stage_activite">
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Compétences à acquérir ou à développer" name="stage_competence">
            </div>
            <p>Si le stagiaire doit être présent dans l’organisme d’accueil la nuit, le dimanche ou un jour férié, préciser les cas particuliers :</p>
            <div class="col-md-4 mb-3">
                <input type="text" class="form-control " placeholder="Présence ou aucune modification" name="stage_presence">
            </div>
            <p>Soit une durée totale en jour ouvrables de :</p> 
            <div class="col-md-auto mb-3">
                <input type="number" class="form-control " placeholder="nb de jours" name="stage_nb_jours" min="0" max="100">
            </div>
        </div>
    <?php endif; ?>
    </div>




<!-- Partie entreprise-->
    <div id="condition_stage">
    <?php if (Session::isGranted("/convention/creer#condition_stage")):?>
        <h2>Les conditions du stage</h2>
        <br>


        <p>
            Article 5bis-Accès aux droits des salariés – Avantages (Organisme de droit privé en France sauf en cas de règles particulières applicables dans certaines collectivités d’outre-mer  françaises) :
            <br>
            Le stagiaire bénéficie des protections et droits mentionnés aux articles L1121-1, L1152-1  et 11 L1153-1 du code du travail, dans les mêmes conditions que les salariés.
            Le stagiaire a accès au restaurant d’entreprise ou aux titres-restaurants prévus à l’article L3262-1 du code du travail, dans les mêmes conditions que les salariés de l’organisme d’accueil.
            Il bénéficie également de la prise en charge des frais de transport prévue à l’article L3261-2 du même code.
            Le stagiaire accède aux activités sociales et culturelles mentionnées à l’article L2323-83 du code du travail dans les mêmes conditions que les salariés.
        </p>
        <div class="col-md-auto mb-3">
            <label>Autres avantages accordés :</label>
            <input type="text" class="form-control " placeholder="avantage ou aucune modification" name="condition_avantage1">
        </div>

        <p>
            Article 5ter-Accès aux droits des agents-Avantages (Organisme de droit public en France sauf en cas de règles particulières applicables dans certaines collectivités d’outre-mer françaises) :
            <br>
            Les trajets effectués par le stagiaire d’un organisme de droit public entre leur domicile et leur lieu de stage sont pris en charge dans les conditions fixées par le décret n°2010-676 du 21 juin 2010 
            instituant une prise en charge partielle du prix des titres d’abonnement correspondant aux déplacements effectués par les agents publics entre leur résidence administrative le lieu de stage indiqué dans la présente convention.
        </p>
        <div class="col-md-auto mb-3">
            <label>Autres avantages accordés :</label>
            <input type="text" class="form-control " placeholder="avantage ou aucune modification" name="condition_avantage2">
        </div>

        <p>
            6.1-Protection maladie du/de la stagiaire à l’étranger
            <br>
            1)Protection issue du régime étudiant français
            - pour les stages au sein de l’Espace Economique Européen (EEE) effectués par des ressortissants d’un état de l’union européenne ou de la Norvège, de l’Islande, du Liechtenstein ou de la Suisse, ou encore de tout autre état 
            (dans ce dernier cas, cette disposition n’est pas applicable pour un stage au Danemark, Norvège, Islande, Liechtenstein ou Suisse) l’étudiant doit demander la carte européenne d’assurance maladie (CEAM)
            - pour les stages effectués au Québec par les étudiant(e)s de nationalité française, l’étudiant doit demander le formulaire SE401Q (104 pour les stages en entreprises, 106 pour les stages en université) ;
            - dans tous les autres cas les étudiants qui engagent des frais de santé peuvent être remboursés auprès de la mutuelle qui leur tient lieu de caisse de sécurité sociale étudiante, au retour et sur présentation des justificatifs : 
            le remboursement s’effectue alors sur la base des tarifs de soins français. Des écarts importants peuvent exister entre les frais engagés et les tarifs français base du remboursement. 
            Il est donc fortement conseillé aux étudiants de souscrire une assurance maladie complémentaire spécifique valable pour le pays et la durée du stage, auprès de l’organisme d’assurance de son choix (mutuelle étudiante, mutuelle des parents, compagnie privée ad hoc…) 
            ou éventuellement et après vérification de l’étendue des garanties proposées, auprès de l’organisme d’accueil si celui-ci fournit au stagiaire une couverture maladie en vertu du droit local (voir 2° ci-dessous)
        </p>
        <p>
            2) Protection sociale issue de l’organisme d’accueil
            En cochant la case appropriée, l’organisme d’accueil indique ci-après s’il fournit une protection maladie au stagiaire, en vertu du droit local :
        </p>
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customControlValidation1" name="condition_maladie">
            <label class="custom-control-label" for="customControlValidation1">OUI : cette protection s’ajoute au maintien, à l’étranger, des droits issus du droit français</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customControlValidation2" name="condition_maladie">
            <label class="custom-control-label" for="customControlValidation2">NON : la protection découle alors exclusivement du maintien, à l’étranger, des droits issus du régime français étudiant).</label>
        </div>
        <div class="custom-control">
            <p>Si aucune case n’est cochée, le 6.3-1 s’applique.</p>
        </div>

        <p>
            Article 9-Congés-Interruption du stage
            <br>
            En France (sauf en cas de règles particulières  applicables  dans certaines collectivités d’outre-mer françaises ou dans les organismes de droit public), en cas de grossesse, de paternité ou d’adoption, le stagiaire bénéficie de congés 
            et d’autorisations d’absence d’une durée équivalente à celle prévues pour les salariés aux articles  L1225-16 à L1225-28, L1225-35, L1225-37, L1225-46 du code du travail.
            Pour les stages dont la durée est supérieure à deux mois et dans la limite de la durée maximale de 6 mois, des congés ou autorisations d’absence sont possibles.
        </p>
        <div class="col-md-auto mb-3">
            <label>Nombre de jours de congès autorisés / ou modalités des congés et autorisations d’absence durant le stage :</label>
            <input type="number" class="form-control " min="0" name="condition_autorisation">
        </div>

        <p>
        Article 12-Fin de stage-Rapport-Evaluation
        <br>
        1)Attestation de stage : à l’issue du stage, l’organisme d’accueil délivre une attestation dont le modèle figure dans la circulaire d’organisation annuelle du diplôme préparé, mentionnant au minimum la durée effective du stage et, le cas échéant, 
        le montant de la gratification perçue. Le stagiaire devra produire cette attestation à l’appui de sa demande éventuelle d’ouverture  de droits au régime général d’assurance vieillesse  prévue à l’art. L351-17 du code de la sécurité sociale ;
        2)Evaluation de l’activité du stagiaire : à l’issue du stage, l’organisme  d’accueil renseigne une fiche d’évaluation de l’activité du stagiaire qu’il retourne à l’enseignant référent. 
        4)Modalités d’évaluation pédagogiques :  le stagiaire devra réaliser tout travail demandé par l’équipe pédagogique de son établissement d’enseignement.
        </p>
        <div class="col-md-auto mb-3">
            <label>Nombre d’ECTS  (le cas échéant) :</label>
            <input type="number" class="form-control " name="condition_cas" min="0">
        </div>
    </div>
    <?php endif; ?>
    <div class="form-group col text-right">
        <button type="submit" class="btn btn-outline-success mr-3" name="btn_enregistrer" formaction="<?= $basePath ?>/convention/creer/submit" >Enregistrer</button>
        <a class="btn btn-outline-secondary" href="<?= $basePath ?>">Retour</a>
    </div>
</form>