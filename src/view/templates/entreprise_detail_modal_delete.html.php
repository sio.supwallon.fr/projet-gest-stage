<div id="modal-entreprise-delete" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-exclamation-triangle text-danger"></i>
                    Confirmation
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold text-danger">
                    Êtes-vous sûr de vouloir supprimer l'entreprise '<?= $entreprise->raison_sociale; ?>'&nbsp;
                </p>
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button id="confirm-button" class="btn btn-outline-danger mr-3" formaction="<?= $basePath; ?>/entreprise/supprimer/<?= $entreprise->id; ?>?callback=<?= $callback; ?>" >Confirmer</button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                </form>
            </div>
        </div>
    </div>
</div>