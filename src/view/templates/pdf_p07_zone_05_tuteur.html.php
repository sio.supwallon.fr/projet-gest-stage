<strong><u><span style="font-size: 8pt;">LE TUTEUR DE L’ORGANISME D’ACCUEIL</span></u></strong><br>
<?php $periode = $convention->periodes[0]; ?>
<?= !empty($periode->tuteur->civilite)?$periode->tuteur->civilite." ":""; ?><?= !empty($periode->tuteur->prenom)?$periode->tuteur->prenom." ":""; ?><?= !empty($periode->tuteur->nom)?$periode->tuteur->nom:""; ?>,
<?= !empty($periode->tuteur->fonction)?$periode->tuteur->fonction:""; ?><br>
le <?= $periode->signature_tuteur_fr; ?><br>