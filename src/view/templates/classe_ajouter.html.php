<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>

<form method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    <?php if (Session::isGranted("/classes")):?>
        <h2 class="mb-3 user-select-none">Nouvelle Classe</h2>


<fieldset class="form-group">
    <div class="row">
        <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Classe</legend>
        <div class="col-md-10 border rounded pt-3">
            <div class="form-row">

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="Nom_classe">Nom</label> 
                        <input type="text" class="form-control" id="New_nom_classe" name="New_nom_classe" required/>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="adresse">slug</label> 
                        <input type="text" class="form-control" id="New_slug_classe" name="New_slug_classe"  readonly/>
                    </div>

                </div>

            <div class="form-row">

                <div class="form-group user-select-none col-12 col-sm-6">
                    <label for="id_acomp">Accompagnement</label> 
                    
                            <?php foreach ($Accompagnements as $Accompagnement): ?>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="accp" name="id_accompagnements[]" value="<?= $Accompagnement->id; ?>" checked /> 
                                    <label class="form-check-label" for="accp">
                                        <?= $Accompagnement->nom;?>
                                    </label>
                                </div>
                            <?php endforeach ?>
                        </select>
                            
                    
                </div>

                <div class="form-group user-select-none col-12 col-sm-6">
                    <label for="id_sect">Section</label> 
                    <div class="form-inline">
                        <select class="form-control w-100" name='id_sect' id="id_sect">
                        <option value="0" disabled selected>&lt;Aucune section&gt;</option>
                            <?php foreach ($sections as $section): ?>
                                        <option value="<?= $section->id; ?>"> <?= $section->nom;?> </option>
                                <?php endforeach ?>                      
                        </select>

                    </div>
                </div>
  

            </div>

        </div>
    </div>
</fieldset>

<fieldset class="form-group">
    <div class="row">
        <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Enseignants</legend>
        <div class="col-md-10 border rounded pt-3">
            <div class="form-row">

                <div class="form-group user-select-none w-100">
                    <label for="id_acomp">Enseignant</label> 
                    <div class="form-inline w-100">
                        
                            <select  class="form-control list-group list-group-flush w-40 " name="id_nEns" id="id_nEns" multiple>
                            <?php foreach ($Enseignants as $Enseignant): ?>
                                <option class="list-group-item" value="<?= $Enseignant->id; ?>"> <?= $Enseignant->prenom . " " . $Enseignant->nom;?> </option>     
                            <?php endforeach ?> 
                            </select>
                        
                        <div class="btn_ens text-center">
                            <button type="button" class="btn btn-outline-secondary mb-1" id="removeEns">&lt;</button>
                            <button type="button" class="btn btn-outline-secondary mt-1" id="addEns">&gt;</button>                       
                        </div>
                        
                        
                        <select  class="form-control list-group list-group-flush w-40" name="id_enseignant[]" id="id_enseignant" multiple>
                        </select>

                    </div>
                </div>

               
            </div>
        </div>    
    </div>
</fieldset>

<div class="row mb-3">
    <div class="col p-0 text-right">
        <?php if(Session::isGranted("/classe/ajouter/submit")): ?>
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_ajouter_classe" name="btn_ajouter_classe" formaction="<?= $basePath ?>/classe/ajouter/submit?callback=<?= $current_uri; ?>" >Enregistrer</button>
        <?php endif; ?>
        <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>  
    </div>
    

    <?php endif; ?>
</form>