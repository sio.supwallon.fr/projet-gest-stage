<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
<?php require "email_index_style.html.php"; ?>
    </head>
    <body>
        <div class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">
                <img style="width:20px; height: 20px; vertical-align:-.1rem;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/File_alt_font_awesome.svg/240px-File_alt_font_awesome.svg.png">
                <?= $application['title']?>
            </span>
        </div> 
        <div class="border rounded p-3 mt-3">
<?php require $template; ?>
        </div>
        <p class="text-muted">
            Ceci est un message automatique, merci de ne pas y répondre.
        </p>
        <p class="text-muted">
            En cas de problème, contacter l'administrateur de l'application : <?= $contact['nom']; ?><br>
            <?= (isset($contact['suffixe']))?$contact['suffixe']:""; ?>
        </p>
        <ul class="text-muted">
            <li >
                Par courrier : <?= $etablissement['nom']; ?><br>
                <?= $etablissement['adresse']; ?> - 
                <?= $etablissement['code_postal']; ?>
                <?= $etablissement['ville']; ?>
            </li>
            <li>
                Par téléphone : <?= $contact['telephone']; ?>
            </li>
            <li>
                Par email : <?= $contact['email']; ?>
            </li>
        </ul>
        <p class="text-danger font-weight-bold mt-0">
            Attention
        </p>
        <p class="text-muted">
            Ce message ainsi que les éventuelles pieces jointes constituent une correspondance privee et confidentielle a l'attention exclusive du destinataire designe ci-dessus. Si vous n'etes pas le destinataire du present message ou une personne susceptible de pouvoir le lui delivrer, il vous est signifie que toute divulgation, distribution ou copie de cette transmission est strictement interdite. Si vous avez recu ce message par erreur, nous vous remercions d'en informer l'expediteur par telephone ou de lui retourner le present message, puis d'effacer immediatement ce message de votre systeme.
        </p>
        <p class="text-muted mb-0">
            *** This e-mail and any attachments is a confidential correspondence intended only for use of the individual or entity named above. If you are not the intended recipient or the agent responsible for delivering the message to the intended recipient, you are hereby notified that any disclosure, distribution or copying of this communication is strictly prohibited. If you have received this communication in error, please notify the sender by phone or by replying this message, and then delete this message from your system.
        </p>
    </body>
</html>