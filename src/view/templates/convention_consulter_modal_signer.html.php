<?php
?>

<div id="modal-signer" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-info">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-signature"></i>
                    Signature
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold text-danger">
                    
                    Êtes-vous sûr de vouloir signer cette convention &nbsp;?
                </p>
                <p>
                    Signer cette convention est un geste important car il vous engage envers elle.<br>
                </p>
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button id="confirm_button_signer" class="btn btn-outline-info mr-3" formaction="<?= $basePath; ?>/convention/signer/<?= $convention->id; ?>/">Signer</button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                </form>
            </div>
        </div>
    </div>
</div>