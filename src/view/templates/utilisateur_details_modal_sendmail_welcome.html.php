<div id="modal-sendmail-welcome" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-info">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-info-circle text-info"></i>
                    Confirmation
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Souhaitez-vous que l'utilisateur reçoive un email de bienvenue avec le lien d'activation&nbsp;?
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button class="btn btn-outline-primary mr-3" formaction="<?= $basePath; ?>/utilisateur/sendmail/welcome/<?= $utilisateur->id; ?>?callback=<?= $current_uri; ?>&callback2=<?= $callback; ?>">Oui</button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Non</button>
                </form>
            </div>
        </div>
    </div>
</div>
