<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>

<form method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    <?php if (Session::isGranted("/entreprises")):?>
        <input type="hidden" name="id_entreprise" value="<?= $entreprise->id; ?>">
        <h2 class="mb-3 user-select-none">Entreprise : <?= $entreprise->raison_sociale; ?></h2>

        <div class="row mb-3">
            <div class="col-xl-8 border rounded pt-3">
                <div class="form-row">

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="raisonSocial">Raison Sociale</label> 
                        <input type="text" class="form-control" id="raisonSocial" name="raisonSocial" value="<?= $entreprise->raison_sociale; ?>"  required/>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="adresse">adresse</label> 
                        <input type="text" class="form-control" id="adresse" name="adresse" value="<?= $entreprise->adresse; ?>" required />
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="telephone">Téléphone</label> 
                        <input type="text" class="form-control" id="telephone" name="telephone" value="<?= $entreprise->telephone; ?>" pattern="^((\+\d{1,3}((-)\d{3})?(-| )?\(?\d\)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$" />
                        <div class="invalid-tooltip">
                            Veuillez saisir un numéro de téléphone valide.
                        </div>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="email">Email</label> 
                        <input type="text" class="form-control" id="email" name="email" value="<?= $entreprise->email; ?>" />
                        <div class="invalid-tooltip">
                            Veuillez saisir une adresse email valide.
                        </div>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="naf">Code Naf</label>
                        <div class="form-inline">
                            <?php if($entreprise->id_naf != null): ?>
                                <input type="text" class="form-control w-25" id="naf" name="naf"  value="<?= $entreprise->naf->code ?>">
                            <?php else: ?>
                                <input type="text" class="form-control w-25" id="naf" name="naf"  placeholder="EX: 1020Z">
                            <?php endif; ?>
                            <select class="form-control w-75" id="id_naf" name="id_naf">
                             <?php if($entreprise->naf->code): ?>
                                <option value="<?= $entreprise->naf->id; ?>"><?= $entreprise->naf->code ?></option>
                            <?php else: ?>
                                <option value="" disabled selected>Saisir un Code</option>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label for="adresse">Ville</label> 
                        <div class="form-inline">
                            <?php if($entreprise->ville->code_postal): ?>
                                <input type="text" class="form-control w-25" id="code_postal" name="code_postal"  maxlength="5" pattern="\d{5}" value="<?= $entreprise->ville->code_postal; ?>" required>
                            <?php else: ?>
                                <input type="text" class="form-control w-25" id="code_postal" name="code_postal"  maxlength="5" pattern="\d{5}" placeholder="CP" required>
                            <?php endif; ?>
                            <select class="form-control w-75" id="id_ville" name="id_ville">
                             <?php if($entreprise->ville): ?>
                                <option value="<?= $entreprise->ville->id; ?>"><?= $entreprise->ville->nom; ?></option>
                            <?php else: ?>
                                <option value="" disabled selected>Saisir un Code Postal</option>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-row">

                    
                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="adresse">Responsable</label> 
                        <div class="form-inline">
                            <select class="form-control w-100" name='id_resp' id="id_resp">
                                <?php foreach ($employes as $employe): ?>
                                        <?php if($employe->id_utilisateur == $entreprise->id_responsable): ?>
                                            <option value="<?= $employe->id_utilisateur; ?>" selected> <?= $employe->prenom .  " " . $employe->nom; ?> </option>
                                        <?php else: ?>
                                            <option value="<?= $employe->id_utilisateur;?>"> <?= $employe->prenom . " " . $employe->nom; ?> </option>
                                        <?php endif; ?>
                                 <?php endforeach ?>                      
                            </select>

                        </div>
                    </div>
                </div>

                <div class="form-row">

                    <div class="form-group col-12 col-md-6 ">
                        <label for="sexe">Siège</label>
                        <div id="sexe" class="form-control">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input siege" type="radio" name="siege" id="siege" value="1" <?= ($entreprise->est_siege == 1)?'checked':''; ?>>
                                <label class="form-check-label" for="siege_O">Oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input siege" type="radio" name="siege" id="siege" value="0" <?= ($entreprise->est_siege == 0)?'checked':''; ?>>
                                <label class="form-check-label" for="siege_N">Non</label>
                            </div>
                        </div>
                    </div>

                    
                        <div class="form-group user-select-none col-12 col-sm-6" id="les_sieges">
                        
                            <label for="id_siege">Les sièges</label> 
                            <div class="form-inline">
                                <select class="form-control w-100" name='id_siege' id="id_siege">
                                    
                                        <option value="0" selected>&lt;Aucun sièges&gt;</option>
                                        <?php foreach ($sieges as $siege): ?>
                                                <?php if($entreprise->id_siege == $siege->id): ?>
                                                    <option value="<?= $siege->id; ?>" selected> <?= $siege->raison_sociale; ?> </option>
                                                <?php else: ?>
                                                    <option value="<?= $siege->id; ?>"> <?= $siege->raison_sociale; ?> </option>
                                                <?php endif; ?>
                                        <?php endforeach ?>
                                </select>      
                            </div>
                        </div>
                    
                        <div class="form-group user-select-none col-12 col-sm-6" id="les_entreprises">
                            <label for="adresse">Les entreprises liés</label> 
                            <div class="form-inline">
                                <select class="form-control w-100" name='id_entre' id="id_entre" readonly>
                                        <?php foreach ($lst_entreprise as $lst_entreprise): ?>
                                            <option value="<?= $lst_entreprise->id; ?>"> <?= $lst_entreprise->raison_sociale; ?> </option>
                                        <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    
                </div>

                <div class="form-row">
                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="UrlGmap">Adresse URL Google Map</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="urlIcon">@</span>
                            </div>
                            <input type="text" class="form-control" id="UrlGmap" name="UrlGmap" aria-describedby="urlIcon" value="<?= $entreprise->maps; ?>"/>
                        </div>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="UrlGmap">Lien vers Google Map</label>
                        <div class="input-group-prepend">
                            <a href="https://www.google.com/maps" class="btn btn-outline-secondary w-50" target="_blank"><i class="fa fa-globe-europe" aria-hidden="true"></i> Google Map</a>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="col-xl-4 border rounded pt-3">
                <iframe class="text-center" src="<?= $entreprise->maps ?>" width="100%" height="95%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

        </div>
        
        <div class="row mb-3">
            <div class="col p-0 text-right">
            <?php if(Session::isGranted("/entreprise/supprimer/{id}")): ?>           
                <button type="button" class="btn btn-outline-danger mr-3" id="btn_delete_entreprise"><i class="fa fa-window-close" aria-hidden="true"></i> Supprimer</button>
            <?php endif; ?>
            <?php if(Session::isGranted("/entreprise/update")): ?>
                <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer_entreprise" name="btn_enregistrer_entreprise" formaction="<?= $basePath ?>/entreprise/update?callback=<?= $current_uri; ?>" >Enregistrer</button>
            <?php endif; ?>
            <a class="btn btn-outline-secondary" href="<?= $basePath; ?>/entreprises">Retour</a>
            
        </div>
    </div>

    <?php endif; ?>
</form>

<?php require_once "entreprise_detail_modal_delete.html.php"; ?>