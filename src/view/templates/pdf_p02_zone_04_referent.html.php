<u>ENCADREMENT DU STAGIAIRE<br>
PAR L’ÉTABLISSEMENT D’ENSEIGNEMENT</u><br>
<br>
Référent&nbsp;:
<strong>
<?= !empty($convention->enseignant->civilite)?$convention->enseignant->civilite." ":""; ?>
<?= !empty($convention->enseignant->prenom)?$convention->enseignant->prenom." ":""; ?>
<?= !empty($convention->enseignant->nom)?$convention->enseignant->nom:""; ?>
</strong><br>
Fonction : <?= $convention->enseignant->fonction; ?><br>
Tel.&nbsp;: <?= $convention->enseignant->telephone; ?><br>
Mél&nbsp;: <?= $convention->enseignant->email; ?>