    <!-- Informations Spécifiques Etudiant -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Spécifiques</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <div class="col-12 col-sm-6 col-md-8">
                        <div class="form-row">

                            <!-- Adresse -->
                            <div class="form-group col-12 col-lg-6">
                                <label for="adresse">Adresse</label> 
                                <input type="text" class="form-control" id="adresse" name="adresse" value="<?= $utilisateur->adresse; ?>" placeholder="Voie (Ex: 16 place de la république)">
                            </div>

                            <!-- Ville -->
                            <div class="form-group col-12 col-lg-6">
                                <label for="adresse">Ville</label> 
                                <div class="form-inline">
                                    <input type="text" class="form-control w-25" id="code_postal" value="<?= ($utilisateur->ville)?$utilisateur->ville->code_postal:""; ?>" maxlength="5" pattern="\d{5}" placeholder="CP">
                                    <select class="form-control w-75" id="id_ville" name="id_ville">
<?php if($utilisateur->ville): ?>
                                        <option value="<?= $utilisateur->ville->id; ?>"><?= $utilisateur->ville->nom; ?></option>
<?php else: ?>
                                        <option value="" disabled selected>Saisir un Code Postal</option>
<?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Date de Naissance -->
                            <div class="form-group col-12 col-md-6">
                                <label for="date_naissance">Date de Naissance</label> 
                                <input type="date" class="form-control" id="date_naissance" name="date_naissance" value="<?= $utilisateur->date_naissance; ?>">
                            </div>

                            <!-- Ville Naissance -->
                            <div class="form-group col-12 col-lg-6">
                                <label for="code_postal_naissance">Ville de Naissance</label> 
                                <div class="form-inline">
                                    <input type="text" class="form-control w-25" id="code_postal_naissance" value="<?= ($utilisateur->ville_naissance)?$utilisateur->ville_naissance->code_postal:""; ?>" maxlength="5" pattern="\d{5}" placeholder="CP">
                                    <select class="form-control w-75" id="id_ville_naissance" name="id_ville_naissance">
<?php if($utilisateur->ville_naissance): ?>
                                        <option value="<?= $utilisateur->ville_naissance->id; ?>"><?= $utilisateur->ville_naissance->nom; ?></option>
<?php else: ?>
                                        <option value="" disabled selected>Saisir un Code Postal</option>
<?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- Sexe -->
                            <div class="form-group col-12 col-md-6">
                                <label for="sexe">Sexe</label>
                                <div id="sexe" class="form-control">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexe" id="sexe_F" value="F" <?= ($utilisateur->sexe == 'F')?'checked':''; ?>>
                                        <label class="form-check-label" for="sexe_F">F</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexe" id="sexe_M" value="M" <?= ($utilisateur->sexe == 'M')?'checked':''; ?>>
                                        <label class="form-check-label" for="sexe_M">M</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Numéro Sécurité Sociale -->
                            <div class="form-group col-12 col-lg-6">
                                <label for="num_secu">Sécurité Sociale</label>
                                <div class="form-inline">
                                    <input type="hidden" id="num_secu" name="num_secu" value="<?= $utilisateur->num_secu; ?>">
                                    <input type="text" class="form-control num-secu px-1 mr-2 mr-sm-0 mr-md-2 mr-lg-0 mr-xl-2 text-center" style="width: 1.5rem;" maxlength="1" pattern="[1-2]" id="num_secu_g1" value="<?= substr($utilisateur->num_secu, 0, 1); ?>">
                                    <input type="text" class="form-control num-secu px-1 mr-2 mr-sm-0 mr-md-2 mr-lg-0 mr-xl-2 text-center" style="width: 2rem;" maxlength="2" pattern="\d{2}" id="num_secu_g2" value="<?= substr($utilisateur->num_secu, 1, 2); ?>">
                                    <input type="text" class="form-control num-secu px-1 mr-2 mr-sm-0 mr-md-2 mr-lg-0 mr-xl-2 text-center" style="width: 2rem;" maxlength="2" pattern="\d{2}" id="num_secu_g3" value="<?= substr($utilisateur->num_secu, 3, 2); ?>">
                                    <input type="text" class="form-control num-secu px-1 mr-2 mr-sm-0 mr-md-2 mr-lg-0 mr-xl-2 text-center" style="width: 2rem;" maxlength="2" pattern="\d{2}" id="num_secu_g4" value="<?= substr($utilisateur->num_secu, 5, 2); ?>">
                                    <input type="text" class="form-control num-secu px-1 mr-2 mr-sm-0 mr-md-2 mr-lg-0 mr-xl-2 text-center" style="width: 2.5rem;" maxlength="3" pattern="\d{3}" id="num_secu_g5" value="<?= substr($utilisateur->num_secu, 7, 3); ?>">
                                    <input type="text" class="form-control num-secu px-1 text-center mr-4 mr-sm-3 mr-md-4 mr-lg-3 mr-xl-4" style="width: 2.5rem;" maxlength="3" pattern="\d{3}" id="num_secu_g6" value="<?= substr($utilisateur->num_secu, 10, 3); ?>">
                                    <input type="text" class="form-control num-secu-cle px-1 text-center" style="width: 2rem;" maxlength="2" pattern="\d{2}" id="num_secu_cle" value="<?= substr($utilisateur->num_secu, 13, 2); ?>">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <div class="form-row">
                            <!-- Classe -->
                            <div class="form-group col-12">
                                <label for="id_classe">Classe</label>
                                <div class="form-inline">
                                    <select class="form-control w-50" id="id_section" name="id_section">
<?php if(! $utilisateur->classe): ?>
                                        <option value="" disabled selected>Choisir une section</option>
<?php endif; ?>
<?php foreach($sections as $section): ?>
<?php if($utilisateur->classe->id_section == $section->id): ?>
                                        <option value="<?= $section->id; ?>" selected><?= $section->nom; ?></option>
<?php else: ?>
                                        <option value="<?= $section->id; ?>"><?= $section->nom; ?></option>
<?php endif; ?>
<?php endforeach; ?>
                                    </select>
                                    <select class="form-control w-50" id="id_classe" name="id_classe">
<?php foreach($utilisateur->classe->section->classes as $classe): ?>
<?php if($utilisateur->classe->id == $classe->id): ?>
                                        <option value="<?= $classe->id; ?>" selected><?= $classe->nom; ?></option>
<?php else: ?>
                                        <option value="<?= $classe->id; ?>"><?= $classe->nom; ?></option>
<?php endif; ?>
<?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
<?php require_once "utilisateur_details_signature.html.php" ?>                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
