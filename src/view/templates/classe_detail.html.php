<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>

<form method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    <?php if (Session::isGranted("/classes")):?>
        <input type="hidden" name="id_classe" value="<?= $classe->id; ?>">
        <h2 class="mb-3 user-select-none">Classe : <?= $classe->nom; ?></h2>


<fieldset class="form-group">
    <div class="row">
        <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Classe</legend>
        <div class="col-md-10 border rounded pt-3">
            <div class="form-row">

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="Nom_classe">Nom</label> 
                        <input type="text" class="form-control" id="Nom_classe" name="Nom_classe" value="<?= $classe->nom; ?>"  required/>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="adresse">slug</label> 
                        <input type="text" class="form-control" id="slug_classe" name="slug_classe" value="<?= $classe->slug; ?>" readonly/>
                    </div>

                </div>

            <div class="form-row">

                <div class="form-group user-select-none col-12 col-sm-6">
                    <label for="id_sect">Section</label> 
                    <div class="form-inline">
                        <select class="form-control w-100" name='id_sect' id="id_sect">
                            <?php foreach ($sections as $section): ?>
                                    <?php if($section->id == $classe->id_section): ?>
                                        <option value="<?= $section->id; ?>" selected> <?= $section->nom;?> </option>
                                    <?php else: ?>
                                        <option value="<?= $section->id; ?>"> <?= $section->nom;?> </option>
                                    <?php endif; ?>
                                <?php endforeach ?>                      
                        </select>

                    </div>
                </div>

                <div class="form-group user-select-none col-12 col-sm-6">
                    <label for="id_acomp">Acompagnement</label> 
                    <div class="form-inline">
                        <select class="form-control w-100" name='id_acomp' id="id_acomp">
                            <?php foreach ($Accompagnements as $Accompagnement): ?>
                                    <?php if($Accompagnement->id == $Accompagnement->id): ?>
                                        <option value="<?= $Accompagnement->id; ?>" selected> <?= $Accompagnement->nom;?> </option>
                                    <?php else: ?>
                                        <option value="<?= $Accompagnement->id; ?>"> <?= $Accompagnement->nom;?> </option>
                                    <?php endif; ?>
                                <?php endforeach ?>                      
                        </select>

                    </div>
                </div>

            </div>

        </div>
    </div>
</fieldset>

<fieldset class="form-group">
    <div class="row">
        <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Enseignants et Etudiants</legend>
        <div class="col-md-10 border rounded pt-3">
            <div class="form-row">

                <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="id_ens">Enseignants</label>
                        <div class="form-inline">
                            <ul class="list-group list-group-horizontal">
                                <?php foreach ($Enseignants as $Enseignant): ?>
                                    <li class="list-group-item">
                                        <a href="<?= $basePath; ?>/utilisateur/<?= $Enseignant->id_utilisateur; ?>"><?= $Enseignant->prenom. " " . $Enseignant->nom; ?></a>
                                    </li>
                                <?php endforeach ?>
                                <button type="button" class="btn btn-outline-success ml-3" id="btn_ajout_prof"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                <button type="button" class="btn btn-outline-danger ml-1" id="btn_supp_prof"><i class="fa fa-minus-square" aria-hidden="true"></i></button>
                            </ul>
                        </div>
                </div>

            </div>

            <div class="form-row">

                <div class="form-group user-select-none col-12 col-sm-6">
                    <label for="etudiant">Etudiants</label>
                    <div class="form-inline">
                        <ul class="list-group list-group-horizontal">
                            <?php foreach($Etudiants as $Etudiant): ?>
                                <li class="list-group-item">
                                    <a href="<?= $basePath; ?>/utilisateur/<?= $Etudiant->id; ?>"><?= $Etudiant->prenom . " " . $Etudiant->nom ?></a>
                                </li>
                            <?php endforeach ?>
                            <button type="button" class="btn btn-outline-success ml-3" id="btn_ajout_eleve"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                            <button type="button" class="btn btn-outline-danger ml-1" id="btn_supp_eleve"><i class="fa fa-minus-square" aria-hidden="true"></i></button>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<div class="row mb-3">
    <div class="col p-0 text-right">
        <?php if(Session::isGranted("/classe/supprimer/{id}")): ?>           
            <button type="button" class="btn btn-outline-danger mr-3" id="btn_delete_classe"><i class="fa fa-window-close" aria-hidden="true"></i> Supprimer</button>
        <?php endif; ?>
        <?php if(Session::isGranted("/classe/modifier/submit")): ?>
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer_classe" name="btn_enregistrer_classe" formaction="<?= $basePath ?>/classe/modifier/submit?callback=<?= $current_uri; ?>" >Enregistrer</button>
        <?php endif; ?>
        <a class="btn btn-outline-secondary" href="<?= $basePath; ?>/classes">Retour</a>  
    </div>
    

    <?php endif; ?>
</form>
