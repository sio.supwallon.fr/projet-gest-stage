<u>2&nbsp;-&nbsp;L’ORGANISME D’ACCUEIL</u><br>
<br>
<span style="font-weight: bold;"><?= $convention->entreprise->raison_sociale; ?></span><br>
<?= $convention->entreprise->adresse; ?><br>
<?= $convention->entreprise->ville->code_postal . " ". $convention->entreprise->ville->nom; ?><br>
<br>
Représenté par&nbsp;:
<strong>
<?= !empty($convention->responsable->civilite)?$convention->responsable->civilite." ":""; ?>
<?= !empty($convention->responsable->prenom)?$convention->responsable->prenom." ":""; ?>
<?= !empty($convention->responsable->nom)?$convention->responsable->nom:""; ?>
</strong><br>
Fonction : <?= $convention->responsable->fonction; ?><br>
Tel.&nbsp;: <?= $convention->responsable->telephone; ?><br>
Mél&nbsp;: <?= $convention->responsable->email; ?><br>
Le stage est effectué<br>
au service&nbsp;: <?= $convention->service; ?><br>
situé<sup style="font-size: 7pt;">1</sup>&nbsp;: <?= $convention->lieu; ?><br>
<span style="text-indent: 1pt; font-size: 7pt; line-height:7pt;">
    (1)&nbsp;: En cas de stage à l’étranger, une fiche concernant
     les droits et obligations du stagiaire dans le pays
     d’accueil sera annexée à la convention.
</span>