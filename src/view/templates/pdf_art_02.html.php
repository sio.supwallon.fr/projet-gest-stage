<strong style="font-size: 11pt;">Article&nbsp;2&nbsp;-&nbsp;Objectif du stage</strong><br>
Le stage correspond à une période temporaire de mise en situation en milieu professionnel au cours de laquelle l’étudiant(e) acquiert des compétences professionnelles et met en oeuvre les acquis de sa formation en vue de l’obtention d’un diplôme ou d’une certification et de favoriser son insertion professionnelle. Le stagiaire se voit confier une ou des missions conformes au projet pédagogique défini par son établissement d’enseignement et approuvées par l’organisme d’accueil.<br>
Le programme est établi par l’établissement d’enseignement et l’organisme d’accueil en fonction du programme général de la formation dispensée.<br>
<br>
<strong>Activités confiées&nbsp;:</strong><br>
<?php if($convention->activites): ?><?= $convention->activites; ?><br>
<?php else: ?><i>Aucune activité indiquée.</i><br>
<?php endif; ?>
<br>
<strong>Compétences à acquérir ou à développer&nbsp;:</strong><br>
<?php if($convention->competences): ?><?= $convention->competences; ?><br>
<?php else: ?><i>Aucune compétence indiquée.</i><br>
<?php endif; ?>