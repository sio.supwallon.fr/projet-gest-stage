<h1 class="offset-lg-1 offset-xl-2">Importation Etudiants</h1>
<form method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $max_file_size; ?>">

    <!-- Compte Utilisateur -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="offset-lg-1 offset-xl-2 col col-md-2 col-form-label pt-0 font-weight-bold">Compte Utilisateur</legend>
            <div class="col-md-9 col-xl-6 border rounded pt-3">
                <div class="form-row">

                    <!-- Type -->
                    <div class="form-group col-12 col-md-6">
                        <label for="id_type_utilisateur">Type</label>
                        <select id="id_type_utilisateur" name="id_type_utilisateur" class="form-control">
                            <option value="<?= $typeUtilisateur_arr['enseignant']->id; ?>"><?= $typeUtilisateur_arr['enseignant']->nom; ?></option>
                            <option value="<?= $typeUtilisateur_arr['etudiant']->id; ?>" selected><?= $typeUtilisateur_arr['etudiant']->nom; ?></option>
                        </select>
                    </div>

                    <!-- Activation -->
                    <div class="form-group col-12 col-md-6">
                        <label for="nom">Activation</label>
                        <div class="form-control custom-control custom-control-right custom-switch mb-1">
                            <input type="hidden" id="hidden_active" name="active" value="0">
                            <input type="checkbox" class="custom-control-input" id="active" name="active">
                            <label class="custom-control-label" for="active">Activer compte(s)</label>
                        </div>
                        <div class="form-control custom-control custom-control-right custom-switch mb-1">
                            <input type="hidden" id="hidden_send_link" name="send_link" value="1">
                            <input type="checkbox" class="custom-control-input" id="send_link" name="send_link" value="1" checked>
                            <label class="custom-control-label" for="send_link">Envoyer email d'activation</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Informations Spécifiques Etudiant -->
    <fieldset id="info_etudiant" class="form-group">
        <div class="row">
            <legend class="offset-lg-1 offset-xl-2 col col-md-2 col-form-label pt-0 font-weight-bold">Informations Spécifiques Etudiant</legend>
            <div class="col-md-9 col-xl-6 border rounded pt-3">
                <div class="form-row">

                    <div class="form-group col-12 col-md-6">
                        <div class="form-row">
                            <!-- Sections / Classe -->
                            <div class="form-group col-12">
                                <div class="row">
                                    <div class="col-6 pr-1">
                                        <label for="id_section">Section</label>
                                        <select class="form-control" id="id_section" name="id_section">
                                        <option value="">&lt;Aucune&gt;</option>
<?php foreach($sections as $section): ?>
                                            <option value="<?= $section->id; ?>"><?= $section->nom; ?></option>
<?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-6 pl-1">
                                        <label for="id_classe">Classe</label>
                                        <select class="form-control" id="id_classe" name="id_classe">
                                            <option value="">&lt;Aucune&gt;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Importation -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="offset-lg-1 offset-xl-2 col col-md-2 col-form-label pt-0 font-weight-bold">Importation</legend>
            <div class="col-md-9 col-xl-6 border rounded pt-3">
                <div class="form-row">

                    <!-- Fichier CSV -->
                    <div class="form-group col-12 col-md-6">
                        <label for="signature">Sélectionner un fichier</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="csv_file" name="csv_file" accept=".csv" lang="fr" required>
                            <label class="custom-file-label" for="csv_file">Aucun fichier choisi</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    
    <div class="row mb-3">
        <!-- Buttons -->
        <div class="offset-lg-1 offset-xl-2 col-12 col-md-11 col-xl-8 p-0 text-right">
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer" name="btn_enregistrer" formaction="<?= $basePath ?>/utilisateurs/import/submit?callback=/utilisateurs/import">Importer</button>
            <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>
        </div>
    </div>
</form>