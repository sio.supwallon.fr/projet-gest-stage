<strong style="font-size: 11pt;">Article&nbsp;7&nbsp;-&nbsp;Responsabilité et assurance</strong><br>
Le stagiaire, pendant la durée de son séjour dans l'organisme d’accueil, demeure étudiant(e) au Lycée Henri Wallon mais sera placé sous la direction du Chef d'organisme d’accueil ou de son délégué (le tuteur généralement).<br>
<br>
L’organisme d’accueil déclare être garanti au titre de la responsabilité civile. Le lycée Henri Wallon a contracté une assurance auprès de la <?= $etablissement['assurance']; ?> qui couvre la responsabilité civile du stagiaire.<br>
<br>
Il appartient au responsable de l’Organisme d’accueil ou à son représentant, ou à toute autre personne ayant eu connaissance d'un accident survenu au stagiaire participant à la réalisation de la présente convention, d'en informer immédiatement, par tout moyen, le chef de l'établissement à l’adresse suivante&nbsp;:<br>