<?php 
namespace GEST_STAGE\view\templates;

use GEST_STAGE\kernel\Session;

//var_dump($_SESSION);

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title><?= $application['title'] ?></title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Favicon -->
        <link rel="icon" href="<?= "{$basePath}/src/view/images/icons/{$application['favicon']['href']}"; ?>" sizes="<?= $application['favicon']['sizes']; ?>" type="<?= $application['favicon']['type']; ?>">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= $basePath; ?>/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font-Awesome CSS -->
        <link rel="stylesheet" href="<?= $basePath; ?>/vendor/components/font-awesome/css/all.css">
        <!-- Common CSS -->
        <link rel="stylesheet" type="text/css" href="<?= $basePath; ?>/src/view/css/index.css" media="all"/>
<?php if(file_exists("src/view/css/" . $style)): ?>
        <!-- Template CSS -->
        <link rel="stylesheet" type="text/css" href="<?= $basePath; ?>/src/view/css/<?= $style ?>" media="all"/>
<?php endif; ?>
    </head>
    <body>
<?php require_once 'navbar.html.php'; ?>
        <main class="container">
<?php require_once $template; ?>
        </main>
<?php if(isset($modals['error'])): ?>
<?php require_once 'index_modal_error.html.php'; ?>
<?php endif; ?>
<?php if(isset($modals['notify'])): ?>
<?php require_once 'index_modal_notify.html.php'; ?>
<?php endif; ?>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?= $basePath; ?>/vendor/components/jquery/jquery.min.js"></script>
        <script src="<?= $basePath; ?>/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="<?= $basePath; ?>/src/view/scripts/index.js"></script>
<?php if(file_exists("src/view/scripts/" . $script)): ?>        
        <script src="<?= $basePath; ?>/src/view/scripts/<?= $script ?>"></script>
<?php endif; ?>
        <script>
                document.basePath = '<?= $basePath; ?>';
<?php foreach($modals as $name => $data): ?>
<?php if(is_array($data) || $data === true): ?>
                $('#modal-<?= $name; ?>').modal('show');
<?php Session::clearModal($name); ?>
<?php endif; ?>
<?php endforeach; ?>
        </script>

    </body>
</html>
