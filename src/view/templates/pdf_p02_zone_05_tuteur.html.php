<u>ENCADREMENT DU STAGIAIRE<br>
PAR L’ORGANISME D’ACCUEIL</u><br>
<br>
Tuteur<sup style="font-size: 7pt;">2</sup>&nbsp;:
<strong>
<?= !empty($convention->periodes[0]->tuteur->civilite)?$convention->periodes[0]->tuteur->civilite." ":""; ?>
<?= !empty($convention->periodes[0]->tuteur->prenom)?$convention->periodes[0]->tuteur->prenom." ":""; ?>
<?= !empty($convention->periodes[0]->tuteur->nom)?$convention->periodes[0]->tuteur->nom:""; ?>
</strong><br>
Fonction : <?= $convention->periodes[0]->tuteur->fonction; ?><br>
Tel.&nbsp;: <?= $convention->periodes[0]->tuteur->telephone; ?><br>
Mél&nbsp;: <?= $convention->periodes[0]->tuteur->email; ?><br>
<span style="text-indent: 1pt; font-size: 7pt; line-height:7pt;">
    (2)&nbsp;: Tuteur de la première période de stage. Les informations sur les éventuels autres tuteurs seront annexées à la convention.
</span>