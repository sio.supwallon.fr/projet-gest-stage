<h1 >Mot de passe oublié</h1>

<div class="row">
    <div class="col offset-md-3 col-md-6 offset-lg-4 col-lg-4">
        <p class="text-danger mb-0">
            <strong>Attention</strong>
        </p>
        <p class="mb-0">
            Pour des raisons de sécurité, cette procédure va :
        </p>
        <ul>
            <li>Désactiver votre compte</li>
            <li>Effacer votre mot de passe actuel</li>
            <li>Envoyer un email de réinitialisation de mot de passe</li>
        </ul>
        <form method="POST" class="needs-validation" novalidate>
            <label class="sr-only" for="login">Identifiant</label>
            <div class="input-group mb-1">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="my-login"><i class="fas fa-user"></i></label>
                </div>
                <input type="text" class="form-control" id="my-login" name="identifiant" placeholder="identifiant" pattern="^[a-z]+(\.){1}[a-z]+$" required>
            </div>
            
            <label class="sr-only" for="email">Email</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="email"><i class="fas fa-at"></i></label>
                </div>
                <input type="email" class="form-control" id="email" name="email" placeholder="adresse email associée" required>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-outline-primary mr-3" name="btn_login" formaction="<?= $basePath; ?>/utilisateur/mot-de-passe-oublie/submit?callback=<?= $callback; ?>">
                    <i class="far fa-paper-plane"></i> Réinitialiser
                </button>            
                <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>
            </div>

        </form>
    </div>
</div>
