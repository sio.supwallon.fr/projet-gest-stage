<ul class="navbar-nav mr-1">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="rounded-circle mr-1" src="https://www.gravatar.com/avatar/<?= md5($connected_user->email); ?>?s=20&d=<?= urldecode("https://gitlab.com/sio.supwallon.fr/projet-gest-stage/-/raw/dev/src/view/images/logos/LogoLyceeHW-020px.png"); ?>" />
            <?= $connected_user->identifiant; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?= $basePath; ?>/profil?callback=<?= $current_uri; ?>">Profil</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item text-danger" href="<?= $basePath; ?>/utilisateur/logout">Déconnexion</a>
        </div>
    </li>
</ul>
