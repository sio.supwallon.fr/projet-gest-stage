<strong style="font-size: 11pt;">Article&nbsp;13&nbsp;-&nbsp;Droit applicable - Tribunaux compétents</strong><br>
La présente convention est régie exclusivement par le droit français. Tout litige non résolu par voie amiable sera soumis à la compétence de la juridiction française compétente.<br>
