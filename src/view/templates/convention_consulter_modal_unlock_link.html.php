<div id="modal-unlock" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-question"></i>
                    Confirmation
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold text-danger">
                    Déverouiller cette convention est irréversible.
                    Êtes-vous sûr de vouloir déverouiller cette convention &nbsp;?
                </p>
                <p>
                    Déverouiller cette convention la fait repartir de zéro au niveau des signatures.<br>
                </p>
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button class="btn btn-outline-primary mr-3" formaction="<?= $basePath; ?>/convention/unlock/<?= $convention->id; ?>">Déverouiller</button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                </form>
            </div>
        </div>
    </div>
</div>