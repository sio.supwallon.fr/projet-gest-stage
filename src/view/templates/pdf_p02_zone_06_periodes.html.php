<u>PÉRIODE(S) DE STAGE</u><br>
<table style="border: 1px solid lightgray;">
<?php foreach($convention->periodes as $index => $periode): ?>
    <tr style="<?= ($index % 2 == 1)?"background-color: lightgray;":""; ?>">
        <th style="width: 30px;">du&nbsp;:</th>
        <td style="width: 70px;"><?= $periode->debut_fr ?></td>
        <th style="width: 30px;">au&nbsp;:</th>
        <td style="width: 70px;"><?= $periode->fin_fr ?></td>
        <th style="width: 50px;">tuteur&nbsp;:</th>
        <td style="width: 300px;"><strong><?= !empty($periode->tuteur->civilite)?$periode->tuteur->civilite." ":""; ?><?= !empty($periode->tuteur->prenom)?$periode->tuteur->prenom." ":""; ?><?= !empty($periode->tuteur->nom)?$periode->tuteur->nom:""; ?></strong></td>
    </tr>
<?php endforeach; ?>
</table>
<br>
Représentant une durée totale de <?= $convention->duree_totale; ?> semaines.<br>
Commentaire&nbsp;: l’équipe pédagogique et le tuteur définissent en concertation les activités confiées au stagiaire en fonction des
objectifs de formation.