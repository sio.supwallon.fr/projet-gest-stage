<h1>Mon profil</h1>
<form method="POST" class="needs-validation form-password-confirm" novalidate>
    <input type="hidden" name="id" value="<?= $utilisateur->id; ?>">
    <input type="hidden" name="jeton" value="<?= $utilisateur->jeton_activation; ?>">

    <!-- Compte Utilisateur -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Compte Utilisateur</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <!-- Identifiant -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="identifiant">Identifiant</label> 
                        <input type="text" class="form-control" id="identifiant" name="identifiant" value="<?= $utilisateur->identifiant; ?>" readonly>
                    </div>

                    <!-- Mot de passe -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="motdepasse">Mot de passe</label>
                        <input type="password" class="form-control mb-1" id="motdepasse" name="motdepasse" placeholder="mot de passe" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
                        <input type="password" class="form-control" id="motdepasse2" name="motdepasse2" placeholder="vérification" required>
                        <div class="invalid-tooltip">
                            Veuillez saisir un mot de passe d'au moins 8 caractères avec des minuscules, majuscules, nombres et caractères spéciaux.
                        </div>
                    </div>

                    <!-- Type -->
                    <div class="form-group col-0 col-md-4">
                        <label for="type">Type</label>
                        <select id="type" class="form-control" disabled>
                            <option value="" selected><?= $utilisateur->typeUtilisateur; ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="row mb-3">
        <!-- Buttons -->
        <div class="col p-0 text-right">
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer" name="btn_enregistrer" formaction="<?= $basePath ?>/utilisateur/update/my/password">Enregistrer</button>
        </div>
    </div>
</form>