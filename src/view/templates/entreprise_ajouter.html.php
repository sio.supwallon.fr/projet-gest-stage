<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>
<?php if (Session::isGranted("/entreprise/ajouter")):?>
<h1 class="user-select-none">Nouvelle Entreprise</h1>
<form method="POST" class="needs-validation form-password-confirm user-select-none" novalidate enctype="multipart/form-data">
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">

                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="NewRaisonSocial">Raison Sociale</label> 
                        <input type="text" class="form-control" id="NewRaisonSocial" name="NewRaisonSocial" placeholder="EX: Lycée H. Wallon" required  />
                    </div>

                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="NewTelephone">Téléphone</label> 
                        <input type="text" class="form-control" id="NewTelephone" name="NewTelephone" placeholder="EX: 03 56 18 26 15" pattern="^((\+\d{1,3}((-)\d{3})?(-| )?\(?\d\)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$"/>
                        <div class="invalid-tooltip">
                            Veuillez saisir un numéro de téléphone valide.
                        </div>
                    </div>

                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="NewEmail">Email</label> 
                        <input type="email" class="form-control" id="NewEmail" name="NewEmail" placeholder="EX: raison.sociale@gmail.com"  />
                        <div class="invalid-tooltip">
                            Veuillez saisir une adresse email valide.
                        </div>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-12 col-lg-6">
                        <label for="NewNaf">Code Naf</label>
                        <div class="form-inline">
                            <input type="text" class="form-control w-25" id="NewNaf" name="NewNaf" placeholder="EX: 1057Z" />
                            <select class="form-control w-75" id="id_newNaf" name="id_newNaf">
                             <?php if($entreprise->naf->code): ?>
                                <option value="<?= $entreprise->naf->id; ?>"><?= $entreprise->naf->code ?></option>
                            <?php else: ?>
                                <option value="" disabled selected>Saisir un Code</option>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    

                </div>

                <div class="form-row">

                    <div class="form-group col-12 col-md-6 ">
                        <label for="sexe">Siège</label>
                        <div id="sexe" class="form-control">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="siege" id="siege_O" value="1">
                                <label class="form-check-label" for="siege_O">Oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="siege" id="siege_N" value="0" checked>
                                <label class="form-check-label" for="siege_N">Non</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-12 col-lg-6" id="les_sieges">
                        <label for="adresse">Les sièges</label> 
                        <div class="form-inline">
                            <select class="form-control w-75" name='id_siege' id="id_siege">
                                <option value="0" selected>&lt;Aucun siege&gt;</option>
                                <?php foreach ($sieges as $siege): ?>
                                    <option value="<?= $siege->id; ?>"> <?= $siege->raison_sociale; ?> </option>
                                 <?php endforeach ?>                      
                            </select>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </fieldset>


    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Adresse</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">

                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="adresse">adresse</label> 
                        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="EX: 30 rue du moulin" required/>
                    </div>

                    <div class="form-group col-12 col-lg-6">
                        <label for="adresse">Ville</label> 
                        <div class="form-inline">
                            <input type="text" class="form-control w-25" id="code_postal" name="code_postal"  maxlength="5" pattern="\d{5}" placeholder="CP" required>
                            <select class="form-control w-75" id="id_ville" name="id_ville">
                             <?php if($entreprise->ville->nom): ?>
                                <option value="<?= $entreprise->ville->id; ?>"><?= $entreprise->ville->nom; ?></option>
                            <?php else: ?>
                                <option value="" disabled selected>Saisir un Code Postal</option>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    

                </div>

                <div class="form-row">
                
                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="UrlGmap">Adresse URL Google Map</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="urlIcon">@</span>
                            </div>
                            <input type="text" class="form-control" id="UrlGmap" name="UrlGmap" aria-describedby="urlIcon" />
                        </div>
                    </div>

                    <div class="form-group user-select-none col-12 col-sm-6">
                        <label for="UrlGmap">Lien vers Google Map</label>
                        <div class="input-group-prepend">
                            <a href="https://www.google.com/maps" class="btn btn-outline-secondary w-50" target="_blank"><i class="fa fa-globe-europe" aria-hidden="true"></i> Google Map</a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </fieldset>

    <fieldset class="form-group">

        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Employe</legend>
            <div class="col-md-10 border rounded pt-3">

                <div class="form-row">
                        <!-- Identifiant -->
                        <div class="form-group col-12 col-sm-6 col-md-4">
                            <label for="identifiant">Identifiant</label> 
                            <input type="text" class="form-control" id="identifiant" name="identifiant" readonly>
                        </div> 
                </div>

                <div class="form-row">
                    <!-- Nom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="nom">Nom</label> 
                        <input type="text" class="form-control" id="nom" name="nom" placeholder="Ex: DUPONT" required>
                    </div>

                    <!-- Prénom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="prenom">Prénom</label> 
                        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Ex: Bernard" required>
                    </div>

                    <!-- E-Mail -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="email">E-Mail</label> 
                        <input type="email" class="form-control" id="email" name="email" placeholder="Ex: bernard.dupond@domain.com" required>
                        <div class="invalid-tooltip">
                            Veuillez saisir une adresse email valide.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="row mb-3">
        <!-- Buttons -->
        <div class="col p-0 text-right">
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_ajouter_entreprise" name="btn_ajouter_entreprise" formaction="<?= $basePath ?>/entreprise/new/submit?callback=<?= $callback ?>">Enregistrer</button>
            <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>
        </div>
    </div>

</form>
<?php endif; ?>