<h1 class="mt-0">
    Réinitialisation de votre mot de passe 
</h1>
<p>
    Vous venez de procéder à la réinitialisation de votre 
    mot de passe.<br>
    L'application <?= $application['title']; ?> 
    a généré un lien d'activation pour vous
    permettre d'accéder à nouveau à votre compte.
</p>
<p>
    Pour finaliser la procédure et définir un nouveau mot de passe,
    veuillez utiliser ce lien 
    d'<a href="<?= "http://{$host}{$basePath}/utilisateur/link/{$receiver->identifiant}?jeton=" . $receiver->jeton_activation; ?>">activation</a>.
</p>
<div class="alert alert-warning mb-0">
    <p class="my-0">
        <strong >
            Ce n'est pas vous qui êtes 
            à l'origine de cette procédure&nbsp;?
        </strong><br>
        &rarr;&nbsp; Finalisez la procédure de 
                     réinitialisation de votre mot de passe.<br>
        &rarr;&nbsp; Contactez l'administrateur 
                     de l'application pour et demandez 
                     à changer d'identifiant.
    </p>
</div>