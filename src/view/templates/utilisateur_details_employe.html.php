    <!-- Informations Spécifiques Employé -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Spécifiques</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">

                
                    <!-- Entreprise -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="entreprise">Entreprise</label> 
                        <input type="hidden" name="id_entreprise" value="<?= $utilisateur->id_entreprise; ?>">
                        <?php if($utilisateur->id_entreprise != null): ?>
                            <input type="text" class="form-control" id="entreprise" value="<?= $utilisateur->entreprise->raison_sociale; ?>" readonly>
                        <?php else: ?>
                            <input type="text" class="form-control" id="entreprise" readonly>
                        <?php endif ?>
                    </div>

                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="form-row">
<?php require_once "utilisateur_details_signature.html.php" ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
