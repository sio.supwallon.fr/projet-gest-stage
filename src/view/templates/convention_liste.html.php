<?php

namespace GEST_STAGE\view\templates;

use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Employe;

?>
<h2 class="mb-3">Liste des conventions</h2>

<div id="accordion">
  <?php $valid_VIP = Session::isAffected("Administrateur(s)") ?>
  <?php $valid_contrainte = Session::isAffected("Etudiant(s)") || Session::isAffected("Enseignant(s)") || Session::isAffected("Employe(s)") ?>
  <?php if($valid_VIP == true || $valid_contrainte == true): ?>
    <div class="alert alert-danger" role="alert">
      <button class="btn shadow-none" id="btn_accordion_1" data-toggle="collapse" data-target="#collapse1">
        <i id="fleche1" class="fas fa-caret-down"></i>
        En cour de préparation.
      </button>
    </div>

    <div class="collapse" id="collapse1" aria-labelledby="headingOne" data-parent="#accordion">
    <div class="row">
      <?php

      
      $id_etat=1;
      foreach($conventions[$id_etat] as $convention):
        if($connected_user->typeUtilisateur->slug == 'etudiant' && $connected_user->id == $convention->id_etudiant || $connected_user->typeUtilisateur->slug == 'enseignant' && $connected_user->id == $convention->classe->referents->id_utilisateur || $connected_user->typeUtilisateur->slug == 'employe' && $connected_user->id == $convention->entreprise->id_responsable || $valid_VIP == true ):
        // récupération des informations pour chaque convention

        $N_P_etudiant = "{$convention->etudiant->nom} {$convention->etudiant->prenom}";
        $lieu = $convention->lieu;
        $Nom_entreprise = $convention->entreprise->raison_sociale;
        $id_convention = $convention->id;

        $link_consulter = "$basePath/convention/$id_convention";

        ?>
        <!-- card -->
          <div class="col-3 mb-3">
            <div class="card alert-danger h-100">
              <div class="card-header"><?= $N_P_etudiant ?></div>
              <div class="card-body">
                  <div style="height: 7rem;">
                    <p class="card-text">Lieu:</p>
                    <p class="card-text"><?= $lieu ?></p>
                  </div>
                  <div>
                    <p class="card-text">Entreprise:</p>
                    <p class="card-text"><?= $Nom_entreprise ?></p>
                  </div>
              </div>
              <?php if($connected_user->typeUtilisateur->slug == 'etudiant' || $valid_VIP == true): ?>
                <div class="card-footer">
                  <a class="btn btn-dark mr-auto ml-auto col" href="<?= $link_consulter ?>">consulter</a>
                </div>
              <?php endif; ?>
            </div>
          </div>
        <?php endif; ?>
      <?php endforeach;?>
      </div>
    </div>
  <?php endif; ?>



  <?php $valid_VIP = Session::isAffected("Administrateur(s)")?>
  <?php $valid_contrainte = Session::isAffected("Etudiant(s)") || Session::isAffected("Enseignant(s)") || Session::isAffected("Employe(s)") ?>
  <?php if($valid_VIP == true || $valid_contrainte == true): ?>
    <div class="alert alert-warning" role="alert">
      <button class="btn shadow-none" id="btn_accordion_2" data-toggle="collapse" data-target="#collapse2">
        <i id="fleche2" class="fas fa-caret-down"></i>
        En attente de validation par l'enseignant
      </button>
    </div>

    <div class="collapse" id="collapse2" class="collapse show" data-parent="#accordion">
    <div class="row">
    
    <?php

    $id_etat=2;
    foreach($conventions[$id_etat] as $convention):
      if($connected_user->typeUtilisateur->slug == 'etudiant' && $connected_user->id == $convention->id_etudiant || $connected_user->typeUtilisateur->slug == 'enseignant' && $connected_user->id == $convention->classe->referents->id_utilisateur || $connected_user->typeUtilisateur->slug == 'employe' && $connected_user->id == $convention->entreprise->id_responsable || $valid_VIP == true ):
        // récupération des informations pour chaque convention

        $N_P_etudiant = "{$convention->etudiant->nom} {$convention->etudiant->prenom}";
        $lieu = $convention->lieu;
        $Nom_entreprise = $convention->entreprise->raison_sociale;
        $id_convention = $convention->id;

        $link_consulter = "$basePath/convention/$id_convention";

        ?>
        <!-- card -->
          <div class="col-3 mb-3">
            <div class="card alert-warning h-100">
              <div class="card-header"><?= $N_P_etudiant ?></div>
              <div class="card-body">
                <div style="height: 7rem;">
                  <p class="card-text">Lieu:</p>
                  <p class="card-text"><?= $lieu ?></p>
                </div>
                <div>
                  <p class="card-text">Entreprise:</p>
                  <p class="card-text"><?= $Nom_entreprise ?></p>
                </div> 
              </div>
              <?php if($valid_VIP == true || $connected_user->typeUtilisateur->slug == 'enseignant'): ?>
              <div class="card-footer">
                <a class="btn btn-dark mr-auto ml-auto col" href="<?= $link_consulter ?>">consulter</a>
              </div>
              <?php endif; ?>
            </div>
          </div>
      <?php endif;?>
    <?php endforeach;?>
    </div>
    </div>
  <?php endif; ?>


  <?php $valid_VIP = Session::isAffected("Administrateur(s)")?>
  <?php $valid_contrainte = Session::isAffected("Etudiant(s)") || Session::isAffected("Enseignant(s)") || Session::isAffected("Employe(s)") ?>
  <?php if($valid_VIP == true || $valid_contrainte == true): ?>
  <div class="alert alert-primary" role="alert">
  <button class="btn shadow-none" id="btn_accordion_3" data-toggle="collapse" data-target="#collapse3">
    <i id="fleche3" class="fas fa-caret-down"></i>
    En attente de validation par l'entreprise
  </button>
  </div>

  <div class="collapse" id="collapse3" class="collapse show" data-parent="#accordion">
  <div class="row">
  
  <?php


  $id_etat=3;
  foreach($conventions[$id_etat] as $convention):
    if($connected_user->typeUtilisateur->slug == 'etudiant' && $connected_user->id == $convention->id_etudiant || $connected_user->typeUtilisateur->slug == 'enseignant' && $connected_user->id == $convention->classe->referents->id_utilisateur || $connected_user->typeUtilisateur->slug == 'employe' && $connected_user->id == $convention->entreprise->id_responsable || $utilisateur instanceof Employe && $utilisateur->is_tuteur($convention) || $valid_VIP == true ):
      // récupération des informations pour chaque convention

      $N_P_etudiant = "{$convention->etudiant->nom} {$convention->etudiant->prenom}";
      $lieu = $convention->lieu;
      $Nom_entreprise = $convention->entreprise->raison_sociale;
      $id_convention = $convention->id;

      $link_consulter = "$basePath/convention/$id_convention";



      ?>
      <!-- card -->
        <div class="col-3 mb-3">
          <div class="card alert-primary h-100">
            <div class="card-header"><?= $N_P_etudiant ?></div>
            <div class="card-body">
              <div style="height: 7rem;">
                <p class="card-text">Lieu:</p>
                <p class="card-text"><?= $lieu ?></p>
              </div>
              <div>
                <p class="card-text">Entreprise:</p>
                <p class="card-text"><?= $Nom_entreprise ?></p>
              </div>
            </div>
            <?php if($valid_VIP == true || $connected_user->typeUtilisateur->slug == 'employe'): ?>
              <div class="card-footer">
                <a class="btn btn-dark mr-auto ml-auto col" href="<?= $link_consulter ?>">consulter</a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>
  </div>
  <?php endif; ?>


  <?php $valid_VIP = Session::isAffected("Administrateur(s)") || Session::isAffected("Etablissement(s)")?>
  <?php $valid_contrainte = Session::isAffected("Etudiant(s)") || Session::isAffected("Enseignant(s)") || Session::isAffected("Employe(s)") ?>
  <?php if($valid_VIP == true || $valid_contrainte == true): ?>  
  <div class="alert alert-secondary" role="alert">
  <button class="btn shadow-none" id="btn_accordion_4" data-toggle="collapse" data-target="#collapse4">
    <i id="fleche4" class="fas fa-caret-down"></i>
    En attente de validation par l'établissement
  </button>
  </div>

  <div class="collapse" id="collapse4" class="collapse show" data-parent="#accordion">
  <div class="row">
  
  <?php

  $id_etat=4;
  foreach($conventions[$id_etat] as $convention):
    if($connected_user->typeUtilisateur->slug == 'etudiant' && $connected_user->id == $convention->id_etudiant || $connected_user->typeUtilisateur->slug == 'enseignant' && $connected_user->id == $convention->classe->referents->id_utilisateur || $connected_user->typeUtilisateur->slug == 'employe' && $connected_user->id == $convention->entreprise->id_responsable || $valid_VIP == true ):

      // récupération des informations pour chaque convention

      $N_P_etudiant = "{$convention->etudiant->nom} {$convention->etudiant->prenom}";
      $lieu = $convention->lieu;
      $Nom_entreprise = $convention->entreprise->raison_sociale;
      $id_convention = $convention->id;

      $link_consulter = "$basePath/convention/$id_convention";



      ?>
      <!-- card -->
        <div class="col-3 mb-3">
          <div class="card alert-secondary h-100">
            <div class="card-header"><?= $N_P_etudiant ?></div>
            <div class="card-body">
              <div style="height: 7rem;">
                <p class="card-text">Lieu:</p>
                <p class="card-text"><?= $lieu ?></p>
              </div>
              <div>
                <p class="card-text">Entreprise:</p>
                <p class="card-text"><?= $Nom_entreprise ?></p>
              </div>
            </div>
            <?php if($valid_VIP == true): ?>
              <div class="card-footer">
                <a class="btn btn-dark mr-auto ml-auto col" href="<?= $link_consulter ?>">consulter</a>
              </div>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>
    <?php endforeach;?>
  </div>
  </div>
  <?php endif; ?>



  <?php $valid_VIP = Session::isAffected("Administrateur(s)") || Session::isAffected("Etablissement(s)") || Session::isAffected("Secretaire(s)")?>
  <?php $valid_contrainte = Session::isAffected("Etudiant(s)") || Session::isAffected("Enseignant(s)") || Session::isAffected("Employe(s)") ?>
  <?php if($valid_VIP == true || $valid_contrainte == true): ?>  
  <div class="alert alert-success" role="alert">
  <button class="btn shadow-none" id="btn_accordion_5" data-toggle="collapse" data-target="#collapse5">
    <i id="fleche5" class="fas fa-caret-down"></i>
    Près a être exporter
  </button>
  </div>

  <div class="collapse" id="collapse5" class="collapse show" data-parent="#accordion">
  <div class="row">
  
  <?php

  $id_etat=5;
  foreach($conventions[$id_etat] as $convention):
    if($connected_user->typeUtilisateur->slug == 'etudiant' && $connected_user->id == $convention->id_etudiant || $connected_user->typeUtilisateur->slug == 'enseignant' && $connected_user->id == $convention->classe->referents->id_utilisateur || $connected_user->typeUtilisateur->slug == 'employe' && $connected_user->id == $convention->entreprise->id_responsable || $valid_VIP == true ):

      // récupération des informations pour chaque convention

      $N_P_etudiant = "{$convention->etudiant->nom} {$convention->etudiant->prenom}";
      $lieu = $convention->lieu;
      $Nom_entreprise = $convention->entreprise->raison_sociale;
      $id_convention = $convention->id;

      $link_PDF = "$basePath/convention/pdf/$id_convention";



      ?>
      <!-- card -->
        <div class="col-3 mb-3">
          <div class="card alert-success h-100">
            <div class="card-header"><?= $N_P_etudiant ?></div>
            <div class="card-body">
              <div style="height: 7rem;">
                <p class="card-text">Lieu:</p>
                <p class="card-text"><?= $lieu ?></p>
              </div>
              <div>
                <p class="card-text">Entreprise:</p>
                <p class="card-text"><?= $Nom_entreprise ?></p>
              </div>
            </div>
            <div class="card-footer">
              <a class="btn btn-danger mr-auto ml-auto col" href="<?= $link_PDF ?>" target="_blank">PDF</a>
            </div>
          </div>
        </div>
      <?php endif; ?>
    <?php endforeach;?>
  </div>
  </div>
<?php endif; ?>
</div>