<?php
namespace GEST_STAGE\view\templates;

use GEST_STAGE\kernel\Session;

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<?php if(!$application['navbar']['enabled']): ?>
    <a class="navbar-brand" href="#"><i class="fas fa-file-alt"></i>&nbsp;<?= $application['title'] ?></a>
<?php else: ?>
    <a class="navbar-brand" href="<?= $basePath; ?>"><i class="fas fa-file-alt"></i>&nbsp;<?= $application['title'] ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
<?php if(Session::isGranted("/utilisateurs")): ?>  
            <li class="nav-item dropdown <?= ($current_uri == '/classe')?" active":""; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Utilisateurs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $basePath; ?>/utilisateurs">Consulter</a>
<?php if(Session::isGranted("/utilisateur/new")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/utilisateur/new">Ajouter</a>
<?php  endif; ?>
<?php if(Session::isGranted("/utilisateurs/import")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/utilisateurs/import">Importer</a>
<?php  endif; ?>
                </div>
            </li>
<?php  endif; ?>
<?php if(Session::isGranted("/classes")): ?>  
            <li class="nav-item dropdown <?= ($current_uri == '/classe')?" active":""; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Classes
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $basePath; ?>/classes">Consulter</a>
<?php if(Session::isGranted("/classe/ajouter")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/classe/ajouter">Ajouter</a>
<?php  endif; ?>
                </div>
            </li>
<?php  endif; ?>
<?php if(Session::isGranted("/etudiants/import")): ?>  
            <li class="nav-item dropdown <?= ($current_uri == '/etudiants/import')?" active":""; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Etudiants
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php if(Session::isGranted("/etudiants/import")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/etudiants/import">Importer</a>
<?php  endif; ?>
                </div>
            </li>
<?php  endif; ?>
<?php if(Session::isGranted("/entreprises")): ?>  
            <li class="nav-item dropdown <?= ($current_uri == '/entreprise')?" active":""; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Entreprises
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $basePath; ?>/entreprises">Consulter</a>
<?php if(Session::isGranted("/entreprise/ajouter")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/entreprise/ajouter">Ajouter</a>
<?php  endif; ?>
                </div>
            </li>
<?php  endif; ?>
<?php if(Session::isGranted("/conventions")): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Convention
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $basePath; ?>/conventions">Consulter</a>
<?php if(Session::isGranted("/convention/creer")): ?>
                    <a class="dropdown-item" href="<?= $basePath; ?>/convention/creer">Ajouter</a>
<?php  endif; ?>
                </div>
            </li>
<?php  endif; ?>
        </ul>
<?php 
if($connected) {
  require_once "navbar_logout.html.php";
} else {
  require_once "navbar_login.html.php";
}
?>
    </div>
<?php endif; ?>
</nav>