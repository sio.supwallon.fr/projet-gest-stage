<h5>Article 1 - Objet de la convention</h5>
<p>
    La présente convention règle les rapports de l'organisme d’accueil 
    avec l'établissement d'enseignement et le stagiaire;
</p>
<h5>Article 2 - Objectif du stage</h5>
<p>
    Le stage correspond à une période temporaire de mise en situation 
    en milieu professionnel au cours de laquelle l'étudiant(e) acquiert 
    des compétences professionnelles et met en œuvre les acquis de sa 
    formation en vue de l'obtention d’un diplôme ou d'une certification 
    et de favoriser son insertion professionnelle. Le stagiaire se voit 
    confier une ou des missions conformes au projet pédagogique défini 
    par son établissement d'enseignement et approuvées par l'organisme 
    d'accueil.
</p>
<p>
    Le programme est établi par l'établissement d'enseignement et 
    l'organisme d'accueil en fonction du programme général de la formation 
    dispensée.
</p>
<h5>Article 3 - Modalités du stage</h5>
<p>
    La durée hebdomadaire de présence du stagiaire dans l’organisme d’accueil 
    sera de <?= $heures; ?> heures.  
    Si le stagiaire doit être présent dans l’organisme d’accueil la nuit, 
    le dimanche ou un jour férié, préciser les cas particuliers :
</p>
<h5>Article 4 - Accueil et encadrement du stagiaire</h5>
<p>
    Le stagiaire est suivi par l'enseignant référent désigné dans la présente 
    convention ainsi que par le service de l'établissement en charge des stages. 
    Le tuteur de stage désigné par l'organisme d'accueil dans la présente 
    convention est chargé d'assurer le suivi du stagiaire et d'optimiser 
    les conditions de réalisation du stage conformément aux stipulations 
    pédagogiques définies.
</p>
<p>
    Le stagiaire est autorisé à revenir dans son établissement d'enseignement 
    pendant la durée du stage pour y suivre des cours demandés explicitement 
    par le programme ou pour participer à des réunions. Les dates sont portées 
    à la connaissance de l'organisme  d’accueil par l'établissement.
</p>
<p>
    Toute difficulté survenue dans la réalisation et le déroulement du stage, 
    qu'elle soit constatée par le stagiaire ou par le tuteur de stage, doit 
    être portée à la connaissance de l'enseignant référent et de l'établissement 
    d'enseignement afin d'être résolue au plus vite.
</p>
<h5>Article 5 - Gratification-Avantages</h5>
<p>
    En France, lorsque la durée du stage est supérieure à deux mois consécutifs 
    ou non, celui-ci fait obligatoirement l'objet d’une gratification, sauf en 
    cas de règles particulières applicables dans certaines collectivités 
    d'outre-mer françaises et pour les stages relevant de l'article  L4381-1 du 
    code de la santé publique.
</p>
<p>
    Le montant horaire de la gratification est fixé à 13,75 % du plafond horaire 
    de la sécurité sociale défini en application de l'article L241-3 du code de 
    la santé publique.
</p>
<p>
    Une convention de branche ou un accord professionnel peut définir un montant 
    supérieur à ce taux.
</p>
<p>
    La gratification due par un organisme de droit public ne peut être cumulée avec 
    une rémunération versée par ce même organisme au cours de la période concernée.
    La gratification est due sans préjudice du remboursement des frais engagés par 
    le stagiaire pour effectuer son stage et des avantages offerts, le cas échéant, 
    pour la restauration, l’hébergement et le transport. L'organisme peut décider de 
    verser une gratification pour les stages dont la durée est inférieure ou égale à 
    deux mois. En cas de suspension ou de réalisation de la présente convention, le 
    montant de la gratification due au stagiaire est proratisé en fonction de la durée 
    du stage effectué.
</p>
<p>
    La durée donnant droit à gratification s'apprécie compte tenu de la présente 
    convention et de ses avenants éventuels, ainsi que du nombre de jours de présence 
    effective du/de la stagiaire dans l'organisme.
</p>
<p>
    Le montant de la gratification est fixé à <?= $gratification ?> € par mois.
</p>
<h5>
    Article 5bis - Accès aux droits des salariés – Avantages 
    (Organisme de droit privé en France sauf en cas de règles particulières 
    applicables dans certaines collectivités d’outre-mer  françaises)
</h5>
<p>
    Le stagiaire bénéficie des protections et droits mentionnés aux articles L1121-1, 
    L1152-1  et 11 L1153-1 du code du travail, dans les mêmes conditions que les salariés.
</p>
<p>
    Le stagiaire a accès au restaurant d’entreprise ou aux titres-restaurants prévus 
    à l’article L3262-1 du code du travail, dans les mêmes conditions que les salariés 
    de l’organisme d’accueil. Il bénéficie également de la prise en charge des frais 
    de transport prévue à l’article L3261-2 du même code.
</p>
<p>
    Le stagiaire accède aux activités sociales et culturelles mentionnées à l’article 
    L2323-83 du code du travail dans les mêmes conditions que les salariés.
</p>
<h5>
    Article 5ter - Accès aux droits des agents-Avantages 
    (Organisme de droit public en France sauf en cas de règles particulières applicables 
    dans certaines collectivités d’outre-mer françaises)
</h5>
<p>
    Les trajets effectués par le stagiaire d’un organisme de droit public entre leur 
    domicile et leur lieu de stage sont pris en charge dans les conditions fixées par 
    le décret n°2010-676 du 21 juin 2010 instituant une prise en charge partielle du 
    prix des titres d’abonnement correspondant aux déplacements effectués par les agents 
    publics entre leur résidence administrative le lieu de stage indiqué dans la présente 
    convention.
</p>
<h5>Article 6 - Régime de protection sociale</h5>
<p>
    Pendant la durée du stage, le stagiaire reste affilé à son régime de sécurité sociale 
    antérieur.
</p>
<p>
    Le responsable de l'organisme d’accueil, ou son représentant, doit déclarer tout 
    accident dont il a eu connaissance à la Caisse Primaire d'Assurance Maladie dans 
    les délais légaux, en donnant toutes précisions utiles concernant : les circonstances, 
    les lieux, la date de l'accident, la nature des blessures, les noms et adresses des 
    témoins éventuels ou des tiers responsables de l'accident. Ces informations seront 
    également transmises au responsable de l’établissement d’enseignement.
</p>
<p>
    Les stages effectués à l’étranger sont signalés préalablement au départ du stagiaire 
    à la sécurité sociale lorsque celle-ci le demande.
</p>
<p>
    Pour les stages à l’étranger, les dispositions suivantes sont applicables sous réserve 
    de conformité avec la législation du pays d’accueil et de celle régissant le type 
    d’organisme d’accueil.
</p>
<h6>6.1 - Protection maladie du/de la stagiaire à l’étranger</h6>
<ol>
    <li>Protection issue du régime étudiant français
        <ul>
            <li>
                pour les stages au sein de l’Espace Economique Européen (EEE) effectués 
                par des ressortissants d’un état de l’union européenne ou de la Norvège, 
                de l’Islande, du Liechtenstein ou de la Suisse, ou encore de tout autre état 
                (dans ce dernier cas, cette disposition n’est pas applicable pour un stage 
                au Danemark, Norvège, Islande, Liechtenstein ou Suisse) l’étudiant doit 
                demander la carte européenne d’assurance maladie (CEAM)
            </li>
            <li>
                pour les stages effectués au Québec par les étudiant(e)s de nationalité 
                française, l’étudiant doit demander le formulaire SE401Q (104 pour les 
                stages en entreprises, 106 pour les stages en université) ;
            </li>
            <li>
                dans tous les autres cas les étudiants qui engagent des frais de santé 
                peuvent être remboursés auprès de la mutuelle qui leur tient lieu de 
                caisse de sécurité sociale étudiante, au retour et sur présentation des 
                justificatifs : le remboursement s’effectue alors sur la base des tarifs 
                de soins français. Des écarts importants peuvent exister entre les frais 
                engagés et les tarifs français base du remboursement. Il est donc fortement 
                conseillé aux étudiants de souscrire une assurance maladie complémentaire 
                spécifique valable pour le pays et la durée du stage, auprès de l’organisme 
                d’assurance de son choix (mutuelle étudiante, mutuelle des parents, compagnie 
                privée ad hoc…) ou éventuellement et après vérification de l’étendue des 
                garanties proposées, auprès de l’organisme d’accueil si celui-ci fournit au 
                stagiaire une couverture maladie en vertu du droit local (voir 2° ci-dessous)
            </li>
        </ul> 
    </li>
    <li>Protection sociale issue de l’organisme d’accueil
        <p>
            En cochant la case appropriée, l’organisme d’accueil indique ci-après s’il fournit 
            une protection maladie au stagiaire, en vertu du droit local :<br>
            <input type="checkbox"> OUI : cette protection s’ajoute au maintien, à l’étranger, 
            des droits issus du droit français<br>
            <input type="checkbox"> NON : la protection découle alors exclusivement du maintien, 
            à l’étranger, des droits issus du régime français étudiant).
        </p>
        <p>Si aucune case n’est cochée, le 6.3-1 s’applique.</p>
    </li>
</ol>
<h6>6.2 - Protection Accident du travail du stagiaire à l’étranger</h6>
<ol>
    <li>
        Pour pouvoir bénéficier de la législation française sur la couverture accident de 
        travail, le présent stage doit :
        <ul>
            <li>
                être d’une durée au plus égale à 6 mois, prolongations incluses ;
            </li>
            <li>
                ne donner lieu à aucune rémunération susceptible d’ouvrir des droits 
                à une protection accident de travail dans le pays d’accueil ; une indemnité 
                ou gratification est admise dans la limite de 13,75% du plafond horaire de 
                la sécurité sociale (cf point 5), et sous réserve de l’accord de la caisse 
                primaire d’assurance maladie  sur la demande de maintien de droit ;
            </li>
            <li>
                se dérouler exclusivement dans l’organisme signataire de la présente 
                convention ;
            </li>
            <li>
                se dérouler exclusivement dans le pays d’accueil étranger cité.
            </li>
        </ul>
        <p>
            Lorsque ces conditions ne sont pas remplies, l’organisme d’accueil s’engage à 
            cotiser pour la protection du stagiaire et à faire les déclarations nécessaires 
            en cas d’accident de travail.            
        </p>
    </li>
    <li>
        La déclaration des accidents de travail incombe à l’établissement d’enseignement 
        qui doit en être informé par l’organisme d’accueil par écrit dans un délai de 
        48 heures.
    </li>
    <li>
        La couverture concerne les accidents survenus :
        <ul>
            <li>
                dans l’enceinte du lieu de stage et aux heures du stage,
            </li>
            <li>
                sur le trajet aller-retour habituel entre la résidence du stagiaire sur 
                le territoire  étranger et le lieu de stage,
            </li>
            <li>
                dans le cadre d’une mission confiée par l’organisme  d’accueil du stagiaire 
                et obligatoirement par ordre de mission,
            </li>
            <li>
                lors du premier trajet pour se rendre depuis son domicile sur le lieu de 
                sa résidence durant le stage (déplacement à la date du début du stage),
            </li>
            <li>
                lors du dernier trajet de retour depuis sa résidence durant le stage à 
                son domicile personnel.
            </li>
        </ul> 
    </li>
    <li>
        Pour le cas où l’une seule des conditions prévues au point 6.4-1 n’est pas remplie, 
        l’organisme d’accueil s’engage à couvrir le/la stagiaire contre le risque d’accident 
        de travail, de trajet et les maladies professionnelles et à en assurer toutes les 
        déclarations nécessaires.
    </li>
    <li>
        Dans tous les cas :
        <ul>
            <li>
                si l’étudiant est victime d’un accident de travail durant le stage, 
                l’organisme d’accueil doit impérativement signaler immédiatement cet 
                accident à l’établissement d’enseignement ;
            </li>
            <li>
                si l’étudiant remplit des missions limitées en-dehors de l’organisme 
                d’accueil ou en-dehors du pays du stage, l’organisme d’accueil doit 
                prendre toutes les dispositions nécessaires pour lui fournir les 
                assurances appropriées.
            </li>
        </ul>
    </li>
</ol>
<h5>Article 7 - Responsabilité et assurance</h5>
<p>
    Le stagiaire, pendant la durée de son séjour dans l'organisme d’accueil, 
    demeure étudiant(e) au Lycée Henri Wallon mais sera placé sous la direction 
    du Chef d'organisme d’accueil ou de son délégué (le tuteur généralement).
</p>.
<p>
    L’organisme d’accueil déclare être garanti au titre de la responsabilité civile. 
    Le lycée Henri Wallon a contracté une assurance auprès de la MAIF 
    (N° de sociétaire : 7063028H) qui couvre la responsabilité civile du stagiaire.
</p>
<p>
    Il appartient au responsable de l’Organisme d’accueil ou à son représentant, 
    ou à toute autre personne ayant eu connaissance d'un accident survenu au 
    stagiaire participant à la réalisation de la présente convention, d'en informer 
    immédiate­ment, par tout moyen, le chef de l'établissement à l’adresse suivante :
</p>
<p>
    16 place de la république<br>
    59300 VALENCIENNES
</p>
<p>
    Pour les stages à l’étranger ou outremer, le stagiaire s’engage à souscrire un 
    contrat d’assistance  (rapatriement sanitaire, assistance juridique…) et un 
    contrat d’assurance individuel accident.
</p>
<p>
    Lorsque l’organisme d’accueil met un véhicule à la disposition du stagiaire, 
    il lui incombe de vérifier préalablement que la police d’assurance du véhicule 
    couvre son utilisation par un étudiant.
</p>
<p>
    Lorsque dans le cadre de son stage, l’étudiant utilise son propre véhicule ou un 
    véhicule prêté par un tiers, il déclare expressément à l’assureur dudit véhicule et, 
    le cas échéant, s’acquitte de la prime y afférente.
</p>
<h5>Article 8 - Discipline</h5>
<p>
    Le stagiaire est soumis à la discipline et aux clauses du règlement intérieur qui 
    lui sont applicables et qui sont portées à sa connaissance avant le début du stage, 
    notamment en ce qui concerne les horaires et les règles d’hygiène et de sécurité en 
    vigueur dans l’organisme d’accueil.
</p>
<p>
    Toute sanction disciplinaire ne peut être décidée que par l’établissement d’enseignement. 
    Dans ce cas, l’organisme d’accueil  informe l’enseignant  référent et l’établissement 
    des manquements et fournit éventuellement les éléments constitutifs.
</p>
<p>
    En cas de manquement particulièrement grave à la discipline, l’organisme d’accueil 
    se réserve le droit de mettre fin au stage tout en respectant les dispositions fixées 
    à l’article 9 de la présente convention.
</p>
<p>
    En cas de manquement à la discipline, le responsable de l’organisme d’accueil peut 
    mettre fin à l'action professionnelle du stagiaire après avoir prévenu le Proviseur 
    du Lycée Henri Wallon. La décision de rompre le contrat et l’exposé des faits devront 
    être formalisés par l’envoi d’un courrier ou d’un message électronique.
</p>
<p>
    Gestion des absences : le responsable de l’organisme d’accueil s’engage à informer le 
    Lycée Henri Wallon de l’absence programmée ou constatée du stagiaire. Réciproquement 
    le stagiaire s’engage à informer le Lycée Henri Wallon et son tuteur en  organisme 
    d’accueil en cas d’impossibilité de pouvoir respecter le calendrier et les horaires 
    détaillés dans l’annexe pédagogique de la présente convention.
</p>
<h5>Article 9 - Congés-Interruption du stage</h5>
<p>
    En France (sauf en cas de règles particulières  applicables  dans certaines collectivités 
    d’outre-mer françaises ou dans les organismes de droit public), en cas de grossesse, de 
    paternité ou d’adoption, le stagiaire bénéficie de congés et d’autorisations d’absence 
    d’une durée équivalente à celle prévues pour les salariés aux articles L1225-16 à L1225-28, 
    L1225-35, L1225-37, L1225-46 du code du travail.
</p>
<p>
    Pour les stages dont la durée est supérieure à deux mois et dans la limite de la durée 
    maximale de 6 mois, des congés ou autorisations d’absence sont possibles.
</p>
<p>
    Nombre de jours de congés autorisés/ ou modalités des congés et autorisations d’absence 
    durant le stage : <?= $congés_autorisés; ?>
</p>
<p>
    Pour toute autre interruption temporaire du stage (maladie, absence injustifiée…) 
    l’organisme d’accueil avertit l’établissement d’enseignement par courrier.
</p>
<p>
    Toute interruption du stage, est signalée aux autres parties à la convention et à 
    l’enseignant référent. Une modalité de validation est mise en place le cas échéant 
    par l’établissement. En cas d’accord des parties à la convention, un report de la 
    fin du stage est possible afin de permettre la réalisation de la durée totale du 
    stage prévue initialement. Ce report fera l’objet d’un avenant à la convention de stage.
</p>
<p>
    Un avenant à la convention pourra être établi en cas de prolongation du stage sur demande 
    conjointe de l’organisme  d’accueil et du stagiaire, dans le respect de la durée maximale 
    du stage fixée par la loi (6 mois).
</p>
<p>
    En cas de volonté d’une des trois parties (organisme d’accueil, stagiaire, établissement 
    d’enseignement) d’arrêter le stage, celle-ci doit immédiatement en informer les deux 
    autres parties par écrit. Les raisons invoquées seront examinées en étroite concertation. 
    La décision définitive d’arrêt du stage ne sera prise qu’à l’issue de cette phase de 
    concertation.
</p>
<h5>Article 10 - Devoir de réserve et confidentialité</h5>
<p>
    Le devoir de réserve est de rigueur absolue et apprécié par l’organisme d’accueil 
    compte-tenu de ses spécificités. Le stagiaire prend donc l’engagement de n’utiliser 
    en aucun cas les informations recueillies  ou obtenues par lui pour en faire publication, 
    communication à des tiers sans accord préalable de l’organisme d’accueil, y compris dans 
    un rapport de stage. Cet engagement vaut non seulement pour la durée du stage mais 
    également après son expiration. Le stagiaire s’engage à ne conserver, emporter, ou prendre 
    copie d’aucun document ou logiciel, de quelque nature que ce soit, appartenant à 
    l’organisme d’accueil, sauf accord de ce dernier.
</p>
<p>
    Dans le cadre de la confidentialité des informations contenues dans un rapport de stage, 
    l’organisme d’accueil peut demander une restriction de la diffusion du rapport, voire le 
    retrait de certains éléments confidentiels. Les personnes amenées à en connaître sont 
    contraintes par le secret professionnel à n’utiliser ni ne divulguer les informations du 
    rapport.
</p>
<h5>Article 11 - Propriété intellectuelle</h5>
<p>
    Conformément au code de la propriété intellectuelle, dans le cas où les activités du 
    stagiaire donnent lieu à la création d’une œuvre protégée par le droit d’auteur ou la 
    propriété industrielle (y compris un logiciel), si l’organisme d’accueil souhaite 
    l’utiliser et que le stagiaire en est d’accord , un contrat devra être signé entre le 
    stagiaire (auteur) et l’organisme d’accueil.
</p>
<p>
    Le contrat devra alors notamment préciser l’étendue des droits cédés, l’éventuelle 
    exclusivité, la destination, les supports utilisés et la durée de la cession, ainsi que, 
    le cas échéant, le montant de la rémunération due au stagiaire au titre de la cession. 
    Cette clause s’applique quel que soit le statut de l’organisme d’accueil.
</p>
<h5>Article 12 - Fin de stage-Rapport-Evaluation</h5>
<ol>
    <li>
        Attestation de stage : à l’issue du stage, l’organisme d’accueil délivre une 
        attestation dont le modèle figure dans la circulaire d’organisation annuelle du 
        diplôme préparé, mentionnant au minimum la durée effective du stage et, le cas 
        échéant, le montant de la gratification perçue. Le stagiaire devra produire cette 
        attestation à l’appui de sa demande éventuelle d’ouverture  de droits au régime 
        général d’assurance vieillesse  prévue à l’art. L351-17 du code de la sécurité 
        sociale ;
    </li>
    <li>
        Evaluation de l’activité du stagiaire : à l’issue du stage, l’organisme  d’accueil 
        renseigne une fiche d’évaluation de l’activité du stagiaire qu’il retourne à 
        l’enseignant référent. 
    </li>
    <li>
        Modalités d’évaluation pédagogiques :  le stagiaire devra réaliser tout travail 
        demandé par l’équipe pédagogique de son établissement d’enseignement.<br>
        Nombre d’ECTS  (le cas échéant) : <?= $ects ?>
    </li>
    <li>
        Le tuteur de l’organisme d’accueil ou tout membre de l’organisme d’accueil appelé 
        à se rendre dans l’établissement d’enseignement dans le cadre de la préparation, 
        du déroulement et de la validation du stage ne peut prétendre à une quelconque prise 
        en charge ou indemnisation de la part de l’établissement d’enseignement.
    </li>
</ol>
<h5>Article 13 - Droit applicable-Tribunaux compétents</h5>
<p>
    La présente convention est régie exclusivement par le droit français. Tout litige non 
    résolu par voie amiable sera soumis à la compétence de la juridiction française 
    compétente.
</p>
