<?php if($its_me): ?>
<h1>Mon profil</h1>
<?php else: ?>
<h1>Profil de <?= $utilisateur->prenom; ?> <?= $utilisateur->nom; ?></h1>
<?php endif; ?>
<form method="POST" class="needs-validation form-password-confirm" novalidate enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?= $utilisateur->id; ?>">
    <input type="hidden" name="type" value="<?= $utilisateur->typeUtilisateur->slug; ?>">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $max_file_size; ?>">
    <input type="hidden" name="active_before" value="<?= $utilisateur->active; ?>">

    <!-- Compte Utilisateur -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Compte Utilisateur</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <!-- Identifiant -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="identifiant">Identifiant</label> 
                        <input type="text" class="form-control" id="identifiant" name="identifiant" value="<?= $utilisateur->identifiant; ?>" readonly>
                    </div>

                    <!-- Mot de passe -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="motdepasse">Mot de passe</label>
                        <div class="col-12 p-0">
                            <button type="button" id="change_password" class="btn btn-outline-primary"><i class="far fa-edit"></i> Modifier</button>
                        </div>
                        <div id="password-block" class="col-12 p-0" style="display: none;">
                            <div class="input-group">
                                <input type="password" class="form-control mb-1" id="motdepasse" name="motdepasse" placeholder="mot de passe" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary mb-1" id="btn_show_hide_password" for="motdepasse" data-toggle="tooltip" data-placement="bottom" title="Afficher">
                                        <i class="fa fa-eye" style="width: 1.2rem;" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="input-group">
                            <input type="password" class="form-control" id="motdepasse2" name="motdepasse2" placeholder="vérification">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary" for="motdepasse2" id="btn_show_hide_password2" data-toggle="tooltip" data-placement="bottom" title="Afficher">
                                        <i class="fa fa-eye" style="width: 1.2rem;"  aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="invalid-tooltip">
                                Veuillez saisir un mot de passe d'au moins 8 caractères avec des minuscules, majuscules, nombres et caractères spéciaux.
                            </div>
                        </div>
                    </div>

                    <!-- Type -->
                    <div class="form-group col-12 col-md-4">
                        <label for="id_type_utilisateur">Type</label>
                        <select id="id_type_utilisateur" class="form-control" disabled>
                            <option value="<?= $utilisateur->typeUtilisateur->slug; ?>" selected><?= $utilisateur->typeUtilisateur->nom; ?></option>
                        </select>
                    </div>

<?php if(in_array('Administrateur(s)', $connected_user->groupes)): ?>
                    <!-- Section Administrateur -->

                    <!-- Groupes -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="groupes">Groupe(s)</label> 
                        <select class="form-control" id="groupes" size="3" disabled>
<?php foreach($utilisateur->groupes as $groupe): ?>
                            <option><?= $groupe ?></option>
<?php endforeach; ?>                            
                        </select>
                    </div>

<?php if($connected_user->typeUtilisateur->slug == 'administrateur' && (
            $utilisateur->typeUtilisateur->slug == "etablissement" || 
            $utilisateur->typeUtilisateur->slug == "secretaire" || 
            $utilisateur->typeUtilisateur->slug == "enseignant")): ?>
                    <!-- Promouvoir -->
                    <div class="form-group col-12 col-sm-6 col-md-8 col-lg-4">
                        <label for="promouvoir">Promouvoir</label>
                        <div>
<?php if(in_array("Administrateur(s)", $utilisateur->groupes)): ?>
                            <a class="btn btn-outline-danger" id="promouvoir" href="<?= $basePath ?>/utilisateur/admin/revoke/<?= $utilisateur->id ?>?callback=/utilisateur/<?= $utilisateur->id ?>&callback2=<?= $callback; ?>">Révoquer l'administrateur</a>
<?php else: ?>
                            <a class="btn btn-outline-danger" id="promouvoir" href="<?= $basePath ?>/utilisateur/admin/grant/<?= $utilisateur->id ?>?callback=/utilisateur/<?= $utilisateur->id ?>&callback2=<?= $callback; ?>">Nommer administrateur</a>
<?php endif; // bouton?>
                        </div>
                    </div>
<?php endif; // promouvoir ?>

                    <!-- Activation -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="nom">Activation</label> 
                        <div class="form-control custom-control custom-control-right custom-switch mb-1"<?= ($its_me)?" readonly":""; ?>>
                        <input type="hidden" id="hidden_active" name="active" value="<?= $utilisateur->active; ?>">
<?php if($its_me): ?>
                            <input type="checkbox" class="custom-control-input" <?= ($utilisateur->active)?" checked":""; ?> disabled>
                            <label class="custom-control-label" for="active" title="vous ne pouvez désactiver votre propre compte">Compte activé</label>
<?php else: ?>
                            <input type="checkbox" class="custom-control-input" id="active" name="active" value="<?= $utilisateur->active; ?>"<?= ($utilisateur->active)?" checked":""; ?>>
                            <label class="custom-control-label" for="active">Compte activé</label>
<?php endif; ?>
                        </div>
<?php if($utilisateur->jeton_activation): ?>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-link"></i></div>
                            </div>
                            <input type="text" class="form-control" id="jeton_activation" value="<?= "http://{$host}{$basePath}/utilisateur/link/{$utilisateur->identifiant}?jeton=" . $utilisateur->jeton_activation; ?>" readonly>
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-secondary" id="btn_copy" data-toggle="tooltip" data-placement="bottom" title="Copier le lien"><i class="far fa-copy"></i></button>
                                <button type="button" class="btn btn-outline-secondary" id="btn_email" data-toggle="tooltip" data-placement="bottom" title="Envoyer l'email d'activation"><i class="far fa-paper-plane"></i></button>
                            </div>
                        </div>
<?php else: ?>
<?php if(! $utilisateur->active): ?>
                        <a class="btn btn-outline-primary" href="<?= $basePath; ?>/utilisateur/link/new/<?= $utilisateur->id; ?>?callback=/utilisateur/<?= $utilisateur->id ?>&callback2=<?= $callback; ?>"><i class="fas fa-link"></i> Lien d'activation</a>
<?php endif; ?>
<?php endif; ?>
                    </div>

                    <!-- Connexion -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label>Connexion</label> 
                        <input type="text" class="form-control mb-1" name="ip" value="<?= $utilisateur->ip; ?>" placeholder="Adresse IP" title="Adresse IP" readonly>
                        <input type="datetime-local" class="form-control" name="connexion" value="<?= $utilisateur->connexion; ?>" placeholder="Dernière connexion" title="Dernière connexion" readonly>
                    </div>

                    <!-- Erreur(s) de Connexion -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label>Erreur(s) de Connexion</label> 
                        <input type="text" class="form-control mb-1" name="nb_erreurs" value="<?= $utilisateur->nb_erreurs; ?>" placeholder="Erreurs" title="Erreurs" readonly>
                        <input type="datetime-local" class="form-control mb-1" name="derniere_erreur" value="<?= $utilisateur->derniere_erreur; ?>" placeholder="Dernière erreur" title="Dernière erreur" readonly>
<?php if($utilisateur->nb_erreurs): ?>
                        <a class="btn btn-outline-warning" href="<?= $basePath; ?>/utilisateur/errors/reset/<?= $utilisateur->id; ?>?callback=/utilisateur/<?= $utilisateur->id ?>&callback2=<?= $callback; ?>" id="btn_raz_erreurs"><i class="fas fa-undo-alt"></i> Remettre à Zéro</a>
<?php endif; ?>
                    </div>

                    <!-- Suppression de compte -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label class="text-danger font-weight-bold">Suppression de compte</label> 
<?php if($its_me): ?>
                        <button type="button" class="btn btn-outline-danger" disabled title="vous ne pouvez supprimer votre propre compte"><i class="fas fa-user-times"></i> Supprimer le compte</button>
<?php else: ?>
                        <button type="button" class="btn btn-outline-danger" id="btn_delete"><i class="fas fa-user-times"></i> Supprimer le compte</button>
<?php endif; ?>
                    </div>

<?php endif; // section administrateur?>
                </div>
            </div>
        </div>
    </fieldset>

    <!-- Informations Personnelles -->
    <fieldset class="form-group">
        <div class="row">
            <legend class="col col-md-2 col-form-label pt-0 font-weight-bold">Informations Personnelles</legend>
            <div class="col-md-10 border rounded pt-3">
                <div class="form-row">
                    <!-- Nom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="nom">Nom</label> 
                        <input type="text" class="form-control" id="nom" name="nom" value="<?= $utilisateur->nom; ?>" placeholder="Ex: DUPONT">
                    </div>

                    <!-- Prénom -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="prenom">Prénom</label> 
                        <input type="text" class="form-control" id="prenom" name="prenom" value="<?= $utilisateur->prenom; ?>" placeholder="Ex: Bernard">
                    </div>

                    <!-- Fonction -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="fonction">Fonction</label> 
                        <input type="text" class="form-control" id="fonction" name="fonction" value="<?= $utilisateur->fonction; ?>" placeholder="Ex: directeur, employé, ..." maxlength="40">
                    </div>

                    <!-- Téléphone -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="telephone">Téléphone</label> 
                        <input type="tel" class="form-control" id="telephone" name="telephone" value="<?= $utilisateur->telephone; ?>" placeholder="Ex: 03 99 99 99 99 ou +33 (0)3 99 99 99 99" pattern="^((\+\d{1,3}((-)\d{3})?(-| )?\(?\d\)?(-| )?\d{1})|(\d{2}))(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}(-| )?\d{2}$">
                        <div class="invalid-tooltip">
                            Veuillez saisir un numéro de téléphone valide.
                        </div>
                    </div>

                    <!-- E-Mail -->
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <label for="email">E-Mail</label> 
                        <input type="email" class="form-control" id="email" name="email" value="<?= $utilisateur->email; ?>" placeholder="Ex: bernard.dupond@domain.com">
                        <div class="invalid-tooltip">
                            Veuillez saisir une adresse email valide.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
<?php
$sub_template = "utilisateur_details_{$utilisateur->typeUtilisateur->slug}.html.php";
if (file_exists("src/view/templates/{$sub_template}")) {
    require_once $sub_template;
} ?>
    <div class="row mb-3">
        <!-- Buttons -->
        <div class="col p-0 text-right">
<?php if($its_me): ?>
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer" name="btn_enregistrer" formaction="<?= $basePath ?>/utilisateur/update/me?callback=<?= $callback ?>">Enregistrer</button>
<?php else: ?>
            <button type="submit" class="btn btn-outline-success mr-3" id="btn_enregistrer" name="btn_enregistrer" formaction="<?= $basePath ?>/utilisateur/update?callback=<?= $callback ?>">Enregistrer</button>
<?php endif; ?>
            <a class="btn btn-outline-secondary" href="<?= $basePath . $callback ?>">Retour</a>
        </div>
    </div>
</form>
<?php require_once "utilisateur_details_modal_delete.html.php"; ?>
<?php require_once "utilisateur_details_modal_sendmail_welcome.html.php"; ?>
