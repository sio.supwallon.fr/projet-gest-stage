<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>

<?php if (Session::isGranted("/entreprises")):?>
    <h2 class="mb-3 user-select-none">Affichage de(s) (l') entreprise(s)</h2>

    <form method="post" class="need-validation user-select-none">

        <div class="form-row pt-20">

            <div class="form-group col-md-3 user-select-none">
                <select class="form-control" name='filtre_region' id="filtre_region">
                <option value="0" selected>&lt;Toutes les régions&gt;</option>
                    <?php foreach ($regions as $region): ?>
                            <option value="<?= $region->id; ?>"> <?= $region->nom; ?> </option>
                    <?php endforeach ?>                      
                </select>
            </div>

            <div class="form-group col-md-3 user-select-none">
                <select class="form-control" name='filtre_departement' id="filtre_departement">
                    <option value="0" selected>&lt;Tous les départements&gt;</option>          
                </select>
            </div>

            <div class="form-group col-md-2 user-select-none">
                <select class="form-control" name='filtre_ville' id="filtre_ville">
                    <option value="0"  selected>&lt;Toutes les villes&gt;</option>                 
                </select>
            </div>

            <div class="form-group col-md-2 user-select-none">
                <select class="form-control" name='filtre_section' id="filtre_section">
                <option value="0" selected>&lt;Toutes les sections&gt;</option>
                    <?php foreach ($sections as $section): ?>
                            <option value="<?= $section->id; ?>"> <?= $section->nom; ?> </option>
                    <?php endforeach ?>                      
                </select>
            </div>

            <div class="form-group col-md-2 user-select-none">
                <input type="text" id="filtre_naf" name="filtre_naf" class="form-control" placeholder="Entrer un code naf" />
            </div>
            
        </div>
        
        <table id="lst_entreprise" class="table table-striped user-select-none">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">raison social</th>
                    <th scope="col">ville</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody id="tabEntreprise">
            <?php foreach ($entreprises as $entreprise):
                $id_entreprise = $entreprise->id;
            ?> 
                <tr>
                    <th scope="row"><?= $entreprise->id; ?></th>
                    <td><?= $entreprise->raison_sociale; ?></td>
                    <td><?= $entreprise->ville->nom; ?></td>
                    <td><a class="btn btn-outline-secondary btn-block" href="<?=$basePath;?>/entreprise/<?=$id_entreprise; ?>?callback=<?= $current_uri; ?>" >Consulter</a></td>
                </tr>            
                            
            <?php endforeach ?>
            
            </tbody>
        </table>

        </textarea>

    </form>
<?php endif; ?>