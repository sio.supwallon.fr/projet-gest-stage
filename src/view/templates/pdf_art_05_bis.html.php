<strong style="font-size: 11pt;">Article&nbsp;5&nbsp;bis&nbsp;-&nbsp;Accès aux droits des salariés – Avantages</strong><br>
<i>Organisme de droit privé en France sauf en cas de règles particulières applicables dans certaines collectivités d’outre-mer  françaises</i><br>
Le stagiaire bénéficie des protections et droits mentionnés aux articles L1121-1, L1152-1  et 11 L1153-1 du code du travail, dans les mêmes conditions que les salariés.<br>
Le stagiaire a accès au restaurant d’entreprise ou aux titres-restaurants prévus à l’article L3262-1 du code du travail, dans les mêmes conditions que les salariés de l’organisme d’accueil. Il bénéficie également de la prise en charge des frais de transport prévue à l’article L3261-2 du même code.<br>
Le stagiaire accède aux activités sociales et culturelles mentionnées à l’article L2323-83 du code du travail dans les mêmes conditions que les salariés.<br>
<br>
Autres avantages accordés : 
<?php if($convention->avantage1): ?><?= $convention->avantage1; ?><br>
<?php else: ?><i>Aucun avantage indiqué.</i><br>
<?php endif; ?>