<strong style="font-size: 11pt;">Article&nbsp;5&nbsp;ter&nbsp;-&nbsp;Accès aux droits des agents - Avantages</strong><br>
<i>Organisme de droit public en France sauf en cas de règles particulières applicables dans certaines collectivités d’outre-mer françaises</i><br>
Les trajets effectués par le stagiaire d’un organisme de droit public entre leur domicile et leur lieu de stage sont pris en charge dans les conditions fixées par le décret n°2010-676 du 21 juin 2010 instituant une prise en charge partielle du prix des titres d’abonnement correspondant aux déplacements effectués par les agents publics entre leur résidence administrative le lieu de stage indiqué dans la présente convention.<br>
<br>
Autres avantages accordés : 
<?php if($convention->avantage2): ?><?= $convention->avantage2; ?><br>
<?php else: ?><i>Aucun avantage indiqué.</i><br>
<?php endif; ?>