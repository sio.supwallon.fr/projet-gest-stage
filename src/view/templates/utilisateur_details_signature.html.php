                    <!-- Signature -->
                    <div class="form-group col-12">
                        <label for="signature">Signature</label>
                        <div id="img-signature" class="img-signature border" style="background-image: url(<?= $basePath; ?>/src/view/images/users/<?= $utilisateur->id; ?>/<?= $utilisateur->signature; ?>);"></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="signature" name="signature" accept=".jpg, .png, .svg" lang="fr">
                            <label class="custom-file-label" for="signature">Aucun fichier choisi</label>
                        </div>
                    </div>
