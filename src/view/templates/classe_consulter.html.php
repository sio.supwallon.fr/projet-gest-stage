<?php
namespace GEST_STAGE\src\view\templates;

use GEST_STAGE\kernel\Session;

?>
<?php if (Session::isGranted("/classes")):?>
    <h2 class="mb-3 user-select-none">Affichage des Classes</h2>

    <form method="post" class="need-validation user-select-none">

         
        <table id="lst_entreprise" class="table table-striped user-select-none">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Section</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody id="tabClasse">
            <?php foreach ($classes as $classe):
                $id_classe = $classe->id;
            ?> 
                <tr>
                    <th scope="row"><?= $classe->id; ?></th>
                    <td><?= $classe->nom; ?></td>
                    <td><?= $classe->section->nom; ?></td>
                    <td><a class="btn btn-outline-secondary btn-block" href="<?=$basePath;?>/classe/<?=$id_classe; ?>?callback=<?= $current_uri; ?>" >Consulter</a></td>
                </tr>            
                            
            <?php endforeach ?>
            
            </tbody>
        </table>


    </form>
<?php endif; ?>