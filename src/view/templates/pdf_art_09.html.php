<strong style="font-size: 11pt;">Article&nbsp;9&nbsp;-&nbsp;Congés - Interruption du stage</strong><br>
En France (sauf en cas de règles particulières  applicables  dans certaines collectivités d’outre-mer françaises ou dans les organismes de droit public), en cas de grossesse, de paternité ou d’adoption, le stagiaire bénéficie de congés et d’autorisations d’absence d’une durée équivalente à celle prévues pour les salariés aux articles  L1225-16 à L1225-28, L1225-35, L1225-37, L1225-46 du code du travail.<br>
Pour les stages dont la durée est supérieure à deux mois et dans la limite de la durée maximale de 6 mois, des congés ou autorisations d’absence sont possibles.<br>
<br>
Nombre de jours de congés autorisés / ou modalités des congés et autorisations d’absence durant le stage&nbsp;:
<?php if($convention->jours_conge): ?><?= $convention->jours_conge; ?>.<br>
<?php else: ?><i>Aucune modalité de congé indiquée.<br></i>
<?php endif; ?>
<br>
Pour toute autre interruption temporaire du stage (maladie, absence injustifiée…) l’organisme d’accueil avertit l’établissement d’enseignement par courrier.<br>
Toute interruption du stage, est signalée aux autres parties à la convention et à l’enseignant référent. Une modalité de validation est mise en place le cas échéant par l’établissement. En cas d’accord des parties à la convention, un report de la fin du stage est possible afin de permettre la réalisation de la durée totale du stage prévue initialement. Ce report fera l’objet d’un avenant à la convention de stage.<br>
Un avenant à la convention pourra être établi en cas de prolongation du stage sur demande conjointe de l’organisme  d’accueil et du stagiaire, dans le respect de la durée maximale du stage fixée par la loi (6 mois).<br>
En cas de volonté d’une des trois parties (organisme d’accueil, stagiaire, établissement d’enseignement) d’arrêter le stage, celle-ci doit immédiatement en informer les deux autres parties par écrit. Les raisons invoquées seront examinées en étroite concertation. La décision définitive d’arrêt du stage ne sera prise qu’à l’issue de cette phase de concertation.<br>