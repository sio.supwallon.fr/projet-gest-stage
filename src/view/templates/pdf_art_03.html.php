<strong style="font-size: 11pt;">Article&nbsp;3&nbsp;-&nbsp;Modalités du stage</strong><br>
La durée hebdomadaire de présence du stagiaire dans l’organisme d’accueil sera de <?= $pdf['art_03']['nb_heures_hebdo']; ?> heures heures.<br>
Si le stagiaire doit être présent dans l’organisme d’accueil la nuit, le dimanche ou un jour férié, préciser les cas particuliers&nbsp;: 
<?php if($convention->jours_particuliers): ?><?= $convention->jours_particuliers; ?><br>
<?php else: ?><i>Aucun jour particulier.</i><br>
<?php endif; ?>
<br>
<table style="border: 1px solid lightgray;">
    <tr style="color: white; background-color: darkgray;">
        <th>DATES ou PÉRIODES</th>
        <th>LIEU D’ACCUEIL</th>
    </tr>
<?php foreach($convention->periodes as $index => $periode): ?>
    <tr style="<?= ($index % 2 == 1)?"background-color: lightgray;":""; ?>">
        <th style="width: 180px;">du&nbsp;: <?= $periode->debut_fr ?> au&nbsp;: <?= $periode->fin_fr ?></th>
        <td style="width: 350px;"><?= $periode->lieu ?></td>
    </tr>
<?php endforeach; ?>
    <tr>
        <th colspan="2">Soit une durée totale en jours ouvrables de : <?= $convention->nb_jours_ouvrables ?> jours.</th> 
    </tr>
</table>
<br>