<u>1&nbsp;-&nbsp;L’ÉTABLISSEMENT D’ENSEIGNEMENT</u><br>
<br>
<span style="font-weight: bold;"><?= $etablissement['nom']; ?></span><br>
<?= $etablissement['adresse']; ?><br>
<?= $etablissement['code_postal'] . " ". $etablissement['ville']; ?><br>
Tel.&nbsp;: <?= $etablissement['telephone']; ?><br>
<br>
Représenté par&nbsp;:
<strong><?= $etablissement['responsable']['nom']; ?></strong><br>
Fonction : <?= $etablissement['responsable']['fonction']; ?><br>
Tel.&nbsp;: <?= $etablissement['responsable']['telephone']; ?><br>
Mél&nbsp;: <?= $etablissement['responsable']['email']; ?><br>
Adresse (si différente de l’établissement)&nbsp;:
<?php if(!empty($etablissement['responsable']['adresse1']) 
      || !empty($etablissement['responsable']['adresse2'])): ?>
<?= $etablissement['responsable']['adresse1']; ?><br>
<?= $etablissement['responsable']['adresse2']; ?><br>
<?php endif; ?>