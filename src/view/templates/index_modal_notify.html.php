<div id="modal-notify" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-info">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-info-circle text-info"></i>
                    Notification
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
<?php foreach($modals['notify'] as $notification): ?>
                <p>
                    <?= $notification; ?>
                </p>
<?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Ok</button>
                </form>
            </div>
        </div>
    </div>
</div>