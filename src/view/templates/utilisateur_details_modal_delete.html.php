<div id="modal-delete" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <h5 class="modal-title font-weight-bold">
                    <i class="fas fa-exclamation-triangle text-danger"></i>
                    Confirmation
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold text-danger">
                    Supprimer un compte utilisateur est irréversible.
                    Êtes-vous sûr de vouloir supprimer le compte '<?= $utilisateur->identifiant; ?>'&nbsp;?
                </p>
                <p>
                    Pour éviter toute perte accidentelle de données, nous vous demandons de confirmer cette demande.<br>
                    Merci de saisir <code id="confirm-match"><?= $utilisateur->identifiant; ?></code> et de confirmer votre demande. Sinon vous pouvez fermer cette fenêtre pour annuler.
                </p>
                <input id="confirm-value" class="form-control">
            </div>
            <div class="modal-footer">
                <form class="form-inline" method="POST">
                    <button id="confirm-button" class="btn btn-outline-danger mr-3" formaction="<?= $basePath; ?>/utilisateur/delete/<?= $utilisateur->id; ?>?callback=<?= $callback; ?>" disabled>Confirmer</button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                </form>
            </div>
        </div>
    </div>
</div>