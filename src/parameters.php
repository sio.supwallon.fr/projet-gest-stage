<?php
namespace GEST_STAGE;

use DateTime;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;

/**
 * sendmail parameters
 */

ini_set('SMTP','smtp.gmail.com');
ini_set('smtp_port','587');
ini_set('sendmail_from', 'G-Stage<g.stage.hdf@gmail.com>');
ini_set('auth_username','g.stage.hdf@gmail.com');
ini_set('auth_password', 'g!St4g3-HdF-DR-bo-al');

/**
 * Common Parameters
 */

$common_vars = [];

/**
 * Template Variables
 * Loaded by the CommonController::bindCommonVariable
 */
$datetime = new DateTime();
$router = new Router();
$connected = Session::isConnected();
$year = $datetime->format('Y');
if ($datetime->format('m') < 9) {
    // avant septembre
    $annee2 = $datetime->format('Y');
    $annee1 = $annee2 - 1;
} else {
    // à partir de septembre
    $annee1 = $datetime->format('Y');
    $annee2 = $annee2 + 1;
}
$template_vars['application']['favicon']['href'] = "file-alt-solid.svg";
$template_vars['application']['favicon']['sizes'] = "any";
$template_vars['application']['favicon']['type'] = "image/svg+xml";
$template_vars['application']['navbar']['enabled'] = true;
$template_vars['application']['title'] = "G-Stage";
$template_vars['datetime'] = $datetime;
$template_vars['basePath'] = $router->getBasePath();
$template_vars['connected'] = $connected;
$template_vars['current_uri'] = $router->getUri();
$template_vars['host'] = $router->getHost();
$template_vars['modals'] = Session::getModals();
$template_vars['router'] = $router;
$template_vars['etablissement']['nom'] = "Lycée Henri Wallon";
$template_vars['etablissement']['adresse'] = "16 place de la République - B.P. 435";
$template_vars['etablissement']['code_postal'] = "59322";
$template_vars['etablissement']['ville'] = "VALENCIENNES CEDEX";
$template_vars['etablissement']['telephone'] = "03 27 19 30 40";
$template_vars['etablissement']['assurance'] = "MAIF (N° de sociétaire : 7063028H)";
$template_vars['etablissement']['responsable']['nom'] = "M. Geoffroy FONTAINE";
$template_vars['etablissement']['responsable']['fonction'] = "Proviseur";
$template_vars['etablissement']['responsable']['telephone'] = "03 27 19 30 40";
$template_vars['etablissement']['responsable']['email'] = "ce.0590221v@ac-lille.fr";
$template_vars['etablissement']['responsable']['adresse1'] = "";
$template_vars['etablissement']['responsable']['adresse2'] = "";
$template_vars['contact']['email'] =  "wallonctx@gmail.com";
$template_vars['contact']['nom'] = "Thierry Dejean";
$template_vars['contact']['suffixe'] = "Directeur Délégué aux Formations Professionnelles et Techonologiques";
$template_vars['contact']['telephone'] = "03 27 19 30 40";
$template_vars['pdf']['images'] = "src/view/images/pdf/";
$template_vars['pdf']['first_page'] = "premiere-page.jpeg";
$template_vars['pdf']['title'] = "Convention de stage";
$template_vars['pdf']['subtitle'] = "Lycée Wallon - VALENCIENNES";
$template_vars['pdf']['summary'] = "Stages étudiant";
$template_vars['pdf']['author'] = "DDFPT";
$template_vars['pdf']['date'] = "{$annee1}-{$annee2}";
$template_vars['pdf']['logo'] = "logoLHW.jpeg";
$template_vars['pdf']['art_03']['nb_heures_hebdo'] = 35;
$template_vars['pdf']['fait_a']['ville'] = "VALENCIENNES";
$template_vars['pdf']['jours_feries_statics'] = [
    (object)['nom' => 'Jour de l\'an', 'date' => "{$annee2}-01-01"],
    (object)['nom' => 'Fête du travail', 'date' => "{$annee2}-05-01"],
    (object)['nom' => 'Armistice 1945', 'date' => "{$annee2}-05-08"],
    (object)['nom' => 'Fête Nationale', 'date' => "{$annee2}-07-14"],
    (object)['nom' => 'Assomption', 'date' => "{$annee2}-08-15"],
    (object)['nom' => 'Armistice 1918', 'date' => "{$annee1}-11-11"],
    (object)['nom' => 'Noël', 'date' => "{$annee1}-12-25"],
];

// mettre en commentaire les jours non ouvrés
// ATTENTION : Laisser les jours en anglais !
// pour comparaison avec le DateTime::format('l')
$template_vars['pdf']['jours_ouvres'] = [
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 
//    'Saturday', 'Sunday',
];

if ($connected) {
    $template_vars['connected_user'] = Session::getUser();
}

// UPLOAD MAX FILE SIZE

$upload_max_filesize = ini_get('upload_max_filesize');
$length = strlen($upload_max_filesize);
$unit = $upload_max_filesize[$length - 1];

switch($unit) {
    case 'G':
        $multiple = 1024 * 1024 * 1024;
    break;
    case 'M':
        $multiple = 1024 * 1024;
    break;
    case 'K':
        $multiple = 1024;
    break;
    default:
        $multiple = 1;
    break;
}
$max_value = substr($upload_max_filesize,0, $length - 1);
$max_file_size = $max_value * $multiple;

$template_vars['max_file_size'] = $max_file_size;