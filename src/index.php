<?php
namespace GEST_STAGE;

use GEST_STAGE\controllers\CommonController;
use GEST_STAGE\kernel\Session;

Session::start();
Session::check();
//var_dump($_SESSION);

require_once "./src/parameters.php";

CommonController::bindTemplateVars($template_vars);

require_once "./src/route.php";
