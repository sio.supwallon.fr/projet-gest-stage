<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Classe;
use GEST_STAGE\model\classes\Section;
use GEST_STAGE\model\classes\Enseignant;
use GEST_STAGE\model\classes\Etudiant;
use GEST_STAGE\model\classes\Accompagnement;
use GEST_STAGE\kernel\View;

class ClasseController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/classes/by-section/{id_section}", ClasseController::class, "ajax_classes_by_section_action"));
        $router->addRoute(new Route("/classes", ClasseController::class, "consulter_classe"));
        $router->addRoute(new Route("/classe/ajouter", ClasseController::class, "ajouter_classe"));
        $router->addRoute(new Route("/classe/ajouter/submit", ClasseController::class, "submit_classe"));
        $router->addRoute(new Route("/classe/supprimer/{id}", ClasseController::class, "delete_classe"));
        $router->addRoute(new Route("/classe/modifier/submit", ClasseController::class, "update_classe"));
        $router->addRoute(new Route("/classe/{id}", ClasseController::class, "detail_classe"));
        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/classes/by-section/{id_section}",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_classes_by_section_action($id_section)
    {
        $array = Classe::getAllBySection($id_section);
        echo json_encode($array);
    }

    public static function consulter_classe(){

        View::setTemplate('classe_consulter');

        $classes = Classe::getAll();
        View::bindParam("classes", $classes);

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        View::bindParam("callback", $callback);

        View::display();

    }

    public static function ajouter_classe(){
        
        View::setTemplate('classe_ajouter');

        $sections = Section::getAll();
        View::bindParam("sections", $sections);

        $Accompagnements = Accompagnement::getAll();
        View::bindParam("Accompagnements", $Accompagnements);

        $Enseignants = Enseignant::getAll();
        View::bindParam("Enseignants", $Enseignants);

        $Etudiants = Etudiant::getAll();
        View::bindParam("Etudiants", $Etudiants);

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        View::bindParam("callback", $callback);

        View::display();
    }

    public static function submit_classe(){
        
        $NewNom = filter_input(INPUT_POST, 'New_nom_classe', FILTER_SANITIZE_STRING);
        $NewSlug = filter_input(INPUT_POST, 'New_slug_classe', FILTER_SANITIZE_STRING);
        $NewSection = filter_input(INPUT_POST, 'id_sect', FILTER_SANITIZE_NUMBER_INT);
        
        $classe = new Classe();

        $classe->nom = $NewNom;
        $classe->slug = $NewSlug;
        $classe->id_section = $NewSection;
        
        $classe->save();

        $classe_id = $classe->id;

        //recuration des donnée du multi select id_enseignant[]

        foreach($TabEnseignants as $enseignant){
            Classe::addEnseignant($classe_id, $enseignant->id);
        }

        //recuration des donnée du multi select id_accompagnement[]

        foreach($TabAccompagnements as $accompagnement){
            Classe::addAccompagnement($classe_id, $accompagnement->id);
        }


    }

    public static function delete_classe($id){
        
        Classe::delete($id);

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        $basePath = View::getParam('basePath');
        header("Location: {$basePath}{$callback}");
    }

    public static function update_classe(){
        
    }

    public static function detail_classe($id){
        
        View::setTemplate('classe_detail');

        $classe = Classe::get($id);
        View::bindParam("classe", $classe);

        $sections = Section::getAll();
        View::bindParam("sections", $sections);

        $Enseignants = Enseignant::getAllByClasse($id);
        View::bindParam("Enseignants", $Enseignants);

        $Etudiants = Etudiant::getAllByClasse($id);
        View::bindParam("Etudiants", $Etudiants);

        $Accompagnements = Accompagnement::getAllByClasse($id);
        View::bindParam("Accompagnements", $Accompagnements);

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        View::bindParam("callback", $callback);

        View::display();
    }
    
}
