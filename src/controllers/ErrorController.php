<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\View;

class ErrorController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/error/404", ErrorController::class, "error_404_action"));
        $router->addRoute(new Route("{*}", ErrorController::class, "error_404_action"));
        
        $route = $router->findRoute();
        View::setRoute($route);
        $route->execute();
    }

    public static function error_404_action()
    {
        View::setTemplate('error-404');
        View::display();
    }
}