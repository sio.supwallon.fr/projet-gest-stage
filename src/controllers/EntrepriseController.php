<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Entreprise;
use GEST_STAGE\model\classes\Region;
use GEST_STAGE\model\classes\Departement;
use GEST_STAGE\model\classes\Ville;
use GEST_STAGE\model\classes\Naf;
use GEST_STAGE\model\classes\Section;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\classes\Employe;
use GEST_STAGE\model\classes\TypeUtilisateur;
use GEST_STAGE\model\classes\Utilisateur;

class EntrepriseController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/entreprise/{id}", EntrepriseController::class, "ajax_entreprise_action"));
        $router->addRoute(new Route("/ajax/entreprises", EntrepriseController::class, "ajax_entreprises_filtre"));
        $router->addRoute(new Route("/ajax/entreprises/departement/{id_region}", EntrepriseController::class, "ajax_departement_filter"));
        $router->addRoute(new Route("/ajax/entreprises/ville/{id_departement}", EntrepriseController::class, "ajax_ville_filter"));
        $router->addRoute(new Route("/ajax/entreprise/naf/{naf}", EntrepriseController::class, "ajax_naf"));
        $router->addRoute(new Route("/entreprises", EntrepriseController::class, "entreprises_consulter"));
        $router->addRoute(new Route("/entreprise/update", EntrepriseController::class, "update_entreprise"));
        $router->addRoute(new Route("/entreprise/ajouter", EntrepriseController::class, "ajouter_entreprise"));
        $router->addRoute(new Route("/entreprise/new/submit", EntrepriseController::class, "submit_entreprise"));
        $router->addRoute(new Route("/entreprise/supprimer/{id}", EntrepriseController::class, "delete_entreprise"));
        $router->addRoute(new Route("/entreprise/{id_entreprise}", EntrepriseController::class, "entreprise_detail"));
        

        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/entreprise/{id}",
            "/ajax/entreprises",
            "/ajax/entreprises/departement/{id_region}",
            "/ajax/entreprises/ville/{id_departement}",
            "/ajax/entreprise/naf/{naf}"
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_entreprise_action($id)
    {
        // méthode AJAX
        // recherche de l'information
        $object = Entreprise::get($id);
        // retrait des informations sensibles
        unset($object->identifiant);
        unset($object->motdepasse);
        // conversion JSON + envoie
        echo json_encode($object);

    }

    public static function ajax_entreprises_filtre(){

        if(isset($_GET['id_region'])){
            $id_region = filter_input(INPUT_GET, 'id_region', FILTER_VALIDATE_INT);
        }else{
            $id_region = null;
        }
        if(isset($_GET['id_departement'])){
            $id_departement = filter_input(INPUT_GET, 'id_departement', FILTER_VALIDATE_INT);
        }else{
            $id_departement = null;
        }
        if(isset($_GET['id_ville'])){
            $id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_VALIDATE_INT);
        }else{
            $id_ville = null;
        }
        if(isset($_GET['code_naf'])){
            $code_naf = filter_input(INPUT_GET, 'code_naf', FILTER_SANITIZE_STRING);
        }else{
            $code_naf = null;
        }
        if(isset($_GET['section'])){
            $section = filter_input(INPUT_GET, 'section', FILTER_VALIDATE_INT);
        }else{
            $section = null;
        }
        
        if(isset($_GET['id_region']) || isset($_GET['id_departement']) || isset($_GET['id_ville']) || isset($_GET['code_naf']) || isset($_GET['section']) ){
            $array = Entreprise::getEntrepriseByFiltre($id_region, $id_departement, $id_ville, $code_naf, $section);
        }else{
            $array = Entreprise::getAll();
        }
        
        echo json_encode($array);
    }

    public static function ajax_departement_filter($id_region){

        if($id_region != 0){
            $array = Departement::getAllByRegion($id_region);
        }else{
            $array = 0;
        }
        
        echo json_encode($array);

    }

    public static function ajax_ville_filter($id_departement){

        if($id_departement != 0){
            $array = Ville::getAllByDepartement($id_departement);
        }else{
            $array = 0;
        }
        
        echo json_encode($array);

    }

    public static function ajax_naf($naf){
        $array = Naf::getAllByNom($naf);
        echo json_encode($array);
    }

    public static function entreprises_consulter(){

        View::setTemplate('entreprises_consulter');

        $entreprises = Entreprise::getAll();
        View::bindParam("entreprises", $entreprises);

        $regions = Region::getAll();
        View::bindParam("regions", $regions);

        $sections = Section::getAll();
        View::bindParam("sections", $sections);

        View::display();
    }

    public static function entreprise_detail($id_entreprise){

        View::setTemplate('entreprise_detail');

        $entreprise = Entreprise::get($id_entreprise);
        View::bindParam("entreprise", $entreprise);

        $lst_entreprise = Entreprise::getAllBySiege($id_entreprise);
        View::bindParam("lst_entreprise", $lst_entreprise);

        $sieges = Entreprise::getAllByEstSiege();
        View::bindParam("sieges", $sieges);

        $employes =  Employe::getAllByEntreprise($id_entreprise);
        View::bindParam("employes", $employes);

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        View::bindParam("callback", $callback);

        View::display();
    }

    public static function update_entreprise(){
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
    
        // recupération des infos génériques
        $id = filter_input(INPUT_POST, 'id_entreprise', FILTER_VALIDATE_INT);
        $raisonSocial = filter_input(INPUT_POST, 'raisonSocial', FILTER_SANITIZE_STRING);
        $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_STRING);
        $telephone = filter_input(INPUT_POST, 'telephone', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $id_naf = filter_input(INPUT_POST, 'id_naf', FILTER_SANITIZE_STRING);
        $id_ville = filter_input(INPUT_POST, 'id_ville', FILTER_SANITIZE_STRING);
        $id_resp = filter_input(INPUT_POST, 'id_resp', FILTER_SANITIZE_STRING);
        $est_siege = filter_input(INPUT_POST, 'siege', FILTER_SANITIZE_NUMBER_INT);
        if($est_siege == 1){
            $id_siege = null;
        }else{
            $id_sieges = filter_input(INPUT_POST, 'id_siege', FILTER_SANITIZE_NUMBER_INT);
            if($id_sieges == 0){
                $id_siege = null;
            }else{
                $id_siege = $id_sieges;
            }
        }
        $UrlGmap = filter_input(INPUT_POST, 'UrlGmap', FILTER_SANITIZE_URL);


        $entreprise = Entreprise::get($id);

        // mise à jour des autres informations
        $entreprise->id = $id;
        $entreprise->raison_sociale = $raisonSocial;
        $entreprise->adresse = $adresse;
        $entreprise->telephone = $telephone;
        $entreprise->email = $email;
        $entreprise->id_naf = $id_naf;
        $entreprise->id_ville = $id_ville;
        $entreprise->id_responsable = $id_resp;
        $entreprise->est_siege = $est_siege;
        $entreprise->id_siege = $id_siege;
        $entreprise->maps = $UrlGmap;

        $entreprise->save();

        if($entreprise->est_siege == 0){
            $lst_entreprise_lies = Entreprise::getAllBySiege($entreprise->id);
            foreach($lst_entreprise_lies as $entreprise_lies){
                $entrep = Entreprise::get($entreprise_lies->id);

                $entrep->id_siege = null;

                $entrep->save();
            }
        }

        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}");
        
    }

    public static function ajouter_entreprise(){

        View::setTemplate('entreprise_ajouter');

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        View::bindParam("callback", $callback);

        $sieges = Entreprise::getAllByEstSiege();
        View::bindParam("sieges", $sieges);

        View::display();

    }

    public static function submit_entreprise(){

        $NewRaisonSocial = filter_input(INPUT_POST, 'NewRaisonSocial', FILTER_SANITIZE_STRING);
        $NewTelephone = filter_input(INPUT_POST, 'NewTelephone', FILTER_SANITIZE_STRING);
        $NewEmail = filter_input(INPUT_POST, 'NewEmail', FILTER_SANITIZE_STRING);
        $Naf = filter_input(INPUT_POST, 'id_newNaf', FILTER_SANITIZE_STRING);
        $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_STRING);
        $id_ville = filter_input(INPUT_POST, 'id_ville', FILTER_SANITIZE_NUMBER_INT);
        $UrlGmap = filter_input(INPUT_POST, 'UrlGmap', FILTER_SANITIZE_URL);
        $est_siege = filter_input(INPUT_POST, 'siege', FILTER_SANITIZE_NUMBER_INT);
        if($est_siege == 1){
            $id_siege = null;
        }else{
            $id_siege = filter_input(INPUT_POST, 'id_siege', FILTER_SANITIZE_NUMBER_INT);
            if($id_siege == 0){
                $id_siege = null;
            }
        }
        
        $entreprise = new Entreprise();

        $entreprise->raison_sociale = $NewRaisonSocial;
        $entreprise->adresse = $adresse;
        $entreprise->telephone = $NewTelephone;
        $entreprise->email = $NewEmail;
        $entreprise->id_naf = $Naf;
        $entreprise->id_ville = $id_ville;
        $entreprise->maps = $UrlGmap;
        $entreprise->est_siege = $est_siege;
        $entreprise->id_siege = $id_siege;

        $entreprise->save();

        $identifiant = filter_input(INPUT_POST, 'identifiant', FILTER_SANITIZE_STRING);
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $type = "employe";

        $id_type_utilisateur = TypeUtilisateur::getBySlug($type);
        var_dump($id_type_utilisateur);
        $employe = new Employe();

        $employe->identifiant = $identifiant;
        $employe->nom = $nom;
        $employe->prenom = $prenom;
        $employe->email = $email;
        $employe->jeton_activation = self::generateToken();
        $employe->id_type_utilisateur = $id_type_utilisateur->id;
        $employe->id_entreprise = $entreprise->id;
        $employe->save();

        
        $updateEntreprise = Entreprise::get($entreprise->id);

        $updateEntreprise->id_responsable = $employe->id;

        $updateEntreprise->save();

        $basePath = View::getParam('basePath');
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);

        header("Location: {$basePath}{$callback}");

    }

    public static function generateToken()
    {
        $data = random_bytes(256);
        $base64 = base64_encode($data);
        $token = substr($base64, 0, 256);
        $token = str_replace('+', ' ', $token);
        $token = filter_var($token, FILTER_SANITIZE_URL);

        return $token;
    }

    public static function delete_entreprise($id){

        $entreprise = Entreprise::get($id);


        if($entreprise->est_siege == 1){
            $lst_entreprise_lies = Entreprise::getAllBySiege($entreprise->id);
            foreach($lst_entreprise_lies as $entreprise_lies){
                $entrep = Entreprise::get($entreprise_lies->id);
                $entrep->id_siege = null;
                $entrep->save();
            }
        }

        Entreprise::delete($id);

        // Redirection
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        $basePath = View::getParam('basePath');
        header("Location: {$basePath}{$callback}");
    }

}



