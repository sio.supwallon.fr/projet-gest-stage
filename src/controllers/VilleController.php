<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Ville;
use GEST_STAGE\kernel\View;

class VilleController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/ville/{id}", VilleController::class, "ajax_ville_action"));
        $router->addRoute(new Route("/ajax/ville/by-cp/{code_postal}", VilleController::class, "ajax_ville_by_cp_action"));
        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/ville/{id}",
            "/ajax/ville/by-cp/{code_postal}",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_ville_action($id)
    {
        $object = Ville::get($id);
        echo json_encode($object);
    }

    public static function ajax_ville_by_cp_action($cp)
    {
        $array = Ville::getAllByCodePosal($cp);
        echo json_encode($array);
    }
}