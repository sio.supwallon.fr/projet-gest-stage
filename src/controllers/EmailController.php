<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\kernel\Email;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\classes\Utilisateur;

class EmailController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/email/welcome", EmailController::class, "email_welcome_action"));
        $router->addRoute(new Route("/email/password/changed/by-me", EmailController::class, "email_password_changed_by_me_action"));
        $router->addRoute(new Route("/email/password/changed/by-another", EmailController::class, "email_password_changed_by_another_action"));
        $router->addRoute(new Route("/email/password/reset", EmailController::class, "email_password_reset_action"));
        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/email/update/my/password",
            "/email/password/changed/by-me",
            "/email/password/changed/by-another",
            "/email/password/reset",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        $route->execute();
    }

    public static function email_welcome_action()
    {
        $application = View::getParam('application');
        $receiver = Utilisateur::get(1);
        $password = "test";

        Email::setTo($receiver->email);
        Email::setSubject("[{$application['title']}] Vos identifiants");
        Email::setTemplate("email_welcome");
        // bindParams
        Email::bindParam("receiver", $receiver);
        Email::bindParam("password", $password);
        Email::render();
        $result = Email::send();
        var_dump($result);        
    }

    public static function email_password_changed_by_me_action()
    {
        $application = View::getParam('application');
        $receiver = Utilisateur::get(1);
        $password = "test";

        Email::setTo($receiver->email);
        Email::setSubject("[{$application['title']}] Mot de passe modifié");
        Email::setTemplate("email_password_changed_by_me");
        // bindParams
        Email::bindParam("receiver", $receiver);
        Email::bindParam("password", $password);
        Email::render();
        $result = Email::send();
        var_dump($result);        
    }

    public static function email_password_changed_by_another_action()
    {
        $application = View::getParam('application');
        $sender = Utilisateur::get(1);
        $receiver = Utilisateur::get(1);
        $password = "test";

        Email::setTo($receiver->email);
        Email::setSubject("[{$application['title']}] Mot de passe modifié");
        Email::setTemplate("email_password_changed_by_another");
        // bindParams
        Email::bindParam("sender", $sender);
        Email::bindParam("receiver", $receiver);
        Email::bindParam("password", $password);
        Email::render();
        $result = Email::send();
        var_dump($result);        
    }

    public static function email_password_reset_action()
    {
        $application = View::getParam('application');
        $receiver = Utilisateur::get(1);
        $receiver->jeton_activation = "test";

        Email::setTo($receiver->email);
        Email::setSubject("[{$application['title']}] Procédure de réinitialisation de mot de passe");
        Email::setTemplate("email_password_reset");
        // bindParams
        Email::bindParam("receiver", $receiver);
        Email::render();
        $result = Email::send();
        var_dump($result);        
    }
}