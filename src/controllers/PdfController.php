<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Pdf;
use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\View;

class PdfController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/pdf/test/me", PdfController::class, "pdf_test_me_action"));
        $router->addRoute(new Route("{*}", ErrorController::class, "error_404_action"));
        
        $route = $router->findRoute();
        View::setRoute($route);
        $route->execute();
    }

    public static function pdf_test_me_action()
    {        
        Pdf::bindParam('annee1',2019);
        Pdf::bindParam('annee2',2020);
        Pdf::addPage1();
        Pdf::addPage2('pdf_test_me');
        Pdf::output();
    }
}