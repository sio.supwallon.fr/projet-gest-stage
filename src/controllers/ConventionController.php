<?php
namespace GEST_STAGE\controllers;

use DateTime;
use GEST_STAGE\kernel\Pdf;
use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Entreprise;
use GEST_STAGE\model\classes\Classe;
use GEST_STAGE\model\classes\Convention;
use GEST_STAGE\model\classes\Periode;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\classes\Accompagnement;
use GEST_STAGE\model\classes\Etablissement;
use GEST_STAGE\model\classes\Utilisateur;

class ConventionController
{
    public static function route()
    {
    $router = new Router();
    $router->addRoute(new Route("/conventions", ConventionController::class, "liste_action"));
    $router->addRoute(new Route("/convention/creer", ConventionController::class, "creer_action"));
    $router->addRoute(new Route("/convention/{id}", ConventionController::class, "consulter_action"));
    $router->addRoute(new Route("/convention/creer/submit", ConventionController::class, "convention_submit_action"));
    $router->addRoute(new Route("/convention/modification/{id}", ConventionController::class, "convention_modif_action"));
    $router->addRoute(new Route("/convention/supprimer/{id}", ConventionController::class, "convention_supp_action"));
    $router->addRoute(new Route("/convention/unlock/{id}", ConventionController::class, "convention_unlock_action"));
    $router->addRoute(new Route("/convention/signer/{id}/{signature}", ConventionController::class, "convention_signer_action"));
    $router->addRoute(new Route("/convention/pdf/{id}", ConventionController::class, "convention_pdf_action"));
    
    
    $error404 = new Route("{*}", ErrorController::class, "error_404_action");
    $router->addRoute($error404);
    
    $route = $router->findRoute();

    $whitelist_routes = [];

    $requested_uri = $route->getUri();
    if (! in_array($requested_uri, $whitelist_routes)
        && ! Session::isGranted($requested_uri)) {
        $route = $error404;
    }

    View::setRoute($route);
    $route->execute();
    }

    public static function convention_supp_action($id)
    {

        Convention::deleteConvention($id);

        $router = new Router();
        $basepath = $router->getBasePath();

        header("Location: {$basepath}/conventions");
    }

    public static function convention_unlock_action($id)
    {

        Periode::unlockPeriode($id);
        Convention::unlockConvention($id);


        $router = new Router();
        $basepath = $router->getBasePath();

        header("Location: {$basepath}/conventions");
    }

    public static function convention_signer_action($id,$signature)
    {

        switch($signature):
            case("etudiant"):
                Convention::signer_etudiantConvention($id);
            break;
            case("enseignant"):
                Convention::signer_enseignantConvention($id);
            break;
            case("responsable"):
                Convention::signer_responsableConvention($id);
            break;
            case("tuteur"):
                Periode::signer_tuteurPeriode($id,Session::getUser()->id);
            break;
            case("etablissement"):
                Convention::signer_etablissementConvention($id);
            break;
        endswitch;

        $router = new Router();
        $basepath = $router->getBasePath();

        header("Location: {$basepath}/conventions");
    }

    public static function convention_submit_action()
    {
        // partie liste entreprise
        $lst_entreprise = filter_input(INPUT_POST, "lst_entreprise", FILTER_VALIDATE_INT);

        // parite saisie entreprise
        $entreprise_service_stage = filter_input(INPUT_POST, "entreprise_service_stage", FILTER_SANITIZE_STRING);
        $entreprise_lieu_stage = filter_input(INPUT_POST, "entreprise_lieu_stage", FILTER_SANITIZE_STRING);


        // partie liste stagiaire
        $lst_classe = filter_input(INPUT_POST, "lst_classe", FILTER_VALIDATE_INT);
        $lst_etudiant = filter_input(INPUT_POST, "lst_etudiant", FILTER_VALIDATE_INT);
        $CPAM = filter_input(INPUT_POST, "stagiaire_cpam", FILTER_SANITIZE_STRING);


        // parite saisie période de stage avec le tuteur
        $debut_arr = [];
        $fin_arr = [];
        $id_tuteur_arr = [];
        $periode_lieu_arr = [];

        foreach($_POST['periode_du'] as $debut){
            $filtered = filter_var($debut, FILTER_SANITIZE_STRING);
            if($filtered){
                $debut_arr[] = $filtered;
            }
        }
        foreach($_POST['periode_au'] as $fin){
            $filtered = filter_var($fin, FILTER_SANITIZE_STRING);
            if($filtered){
                $fin_arr[] = $filtered;
            }
        }
        foreach($_POST['lst_tuteur'] as $id_tuteur){
            $filtered = filter_var($id_tuteur, FILTER_VALIDATE_INT);
            if($filtered){
                $id_tuteur_arr[] = $filtered;
            }
        }
        foreach($_POST['periode_lieu'] as $periode_lieu){
            $filtered = filter_var($periode_lieu, FILTER_SANITIZE_STRING);
            if($filtered){
                $periode_lieu_arr[] = $filtered;
            }
        }

        // parite saisie information du stage
        $stage_activite = filter_input(INPUT_POST, "stage_activite", FILTER_SANITIZE_STRING);
        $stage_competence = filter_input(INPUT_POST, "stage_competence", FILTER_SANITIZE_STRING);
        $stage_presence = filter_input(INPUT_POST, "stage_presence", FILTER_SANITIZE_STRING);
        $stage_nb_jours = filter_input(INPUT_POST, "stage_nb_jours", FILTER_VALIDATE_INT);


        // partie saisie condition du stage
        $condition_avantage1 = filter_input(INPUT_POST, "condition_avantage1", FILTER_SANITIZE_STRING);
        $condition_avantage2 = filter_input(INPUT_POST, "condition_avantage2", FILTER_SANITIZE_STRING);
        $condition_maladie = filter_input(INPUT_POST, "condition_maladie");
        $condition_autorisation = filter_input(INPUT_POST, "condition_autorisation", FILTER_SANITIZE_STRING);
        $condition_cas = filter_input(INPUT_POST, "condition_cas", FILTER_SANITIZE_STRING);

        //creer la methode dao pour enregistrement dans la bdd
        if(count($debut_arr) == count($fin_arr) && count($id_tuteur_arr) == count($fin_arr)){
        Convention::insertConvention ($lst_entreprise, $entreprise_service_stage, $entreprise_lieu_stage, $lst_classe, $lst_etudiant, $CPAM, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours, $condition_avantage1, $condition_avantage2, $condition_maladie, $condition_autorisation, $condition_cas, $debut_arr, $fin_arr, $id_tuteur_arr, $periode_lieu_arr);
        }

        $router = new Router();
        $basepath = $router->getBasePath();

        header("Location: {$basepath}/conventions");
    }

    public static function convention_modif_action($id)
    {

        // partie liste entreprise
        $lst_entreprise = filter_input(INPUT_POST, "lst_entreprise", FILTER_VALIDATE_INT);

        // parite saisie entreprise
        $entreprise_service_stage = filter_input(INPUT_POST, "entreprise_service_stage", FILTER_SANITIZE_STRING);
        $entreprise_lieu_stage = filter_input(INPUT_POST, "entreprise_lieu_stage", FILTER_SANITIZE_STRING);

        //creer la methode dao pour la modifier dans la bdd
        Convention::updateEntreprise($id, $entreprise_service_stage, $entreprise_lieu_stage, $lst_entreprise);


        // partie liste stagiaire
        $lst_classe = filter_input(INPUT_POST, "lst_classe", FILTER_VALIDATE_INT);
        $lst_etudiant = filter_input(INPUT_POST, "lst_etudiant", FILTER_VALIDATE_INT);
        $CPAM = filter_input(INPUT_POST, "cpam", FILTER_SANITIZE_STRING);

        //creer la methode dao pour la modifier dans la bdd
        Convention::updateStagiaire($id, $lst_classe, $lst_etudiant, $CPAM);


        $debut_arr = [];
        $fin_arr = [];
        $id_tuteur_arr = [];
        $periode_lieu_arr = [];

        foreach($_POST['periode_du'] as $debut){
            $filtered = filter_var($debut, FILTER_SANITIZE_STRING);
            if($filtered){
                $debut_arr[] = $filtered;
            }
        }
        foreach($_POST['periode_au'] as $fin){
            $filtered = filter_var($fin, FILTER_SANITIZE_STRING);
            if($filtered){
                $fin_arr[] = $filtered;
            }
        }
        foreach($_POST['lst_tuteur'] as $id_tuteur){
            $filtered = filter_var($id_tuteur, FILTER_VALIDATE_INT);
            if($filtered){
                $id_tuteur_arr[] = $filtered;
            }
        }
        foreach($_POST['periode_lieu'] as $periode_lieu){
            $filtered = filter_var($periode_lieu, FILTER_SANITIZE_STRING);
            if($filtered){
                $periode_lieu_arr[] = $filtered;
            }
        }
        

        if(count($debut_arr) == count($fin_arr) && count($id_tuteur_arr) == count($fin_arr)){
            Periode::deleteByConvention($id);
            foreach($debut_arr as $key => $value){
                //creer la methode dao pour la modifier dans la bdd
                Periode::insert($debut_arr[$key], $fin_arr[$key], $id_tuteur_arr[$key], $periode_lieu_arr[$key], $id);
            }
        }


        // parite saisie information du stage
        $stage_activite = filter_input(INPUT_POST, "stage_activite", FILTER_SANITIZE_STRING);
        $stage_competence = filter_input(INPUT_POST, "stage_competence", FILTER_SANITIZE_STRING);
        $stage_presence = filter_input(INPUT_POST, "stage_presence", FILTER_SANITIZE_STRING);
        $stage_nb_jours = filter_input(INPUT_POST, "stage_nb_jours", FILTER_VALIDATE_INT);

        //creer la methode dao pour la modifier dans la bdd
        Convention::updateInformation($id, $stage_activite, $stage_competence, $stage_presence, $stage_nb_jours);


        // partie saisie condition du stage
        $condition_avantage1 = filter_input(INPUT_POST, "condition_avantage1", FILTER_SANITIZE_STRING);
        $condition_avantage2 = filter_input(INPUT_POST, "condition_avantage2", FILTER_SANITIZE_STRING);
        $condition_maladie = filter_input(INPUT_POST, "condition_maladie");
        $condition_autorisation = filter_input(INPUT_POST, "condition_autorisation", FILTER_SANITIZE_STRING);
        $condition_cas = filter_input(INPUT_POST, "condition_cas", FILTER_SANITIZE_STRING);

        //creer la methode dao pour la modifier dans la bdd
        Convention::updateCondition($id, $condition_avantage1, $condition_avantage2,$condition_maladie, $condition_autorisation, $condition_cas);


        $router = new Router();
        $basepath = $router->getBasePath();

        header("Location: {$basepath}/conventions");
    }

    public static function liste_action()
    {
        for($id_etat = 1; $id_etat < 6; $id_etat++) {
            $conventions[$id_etat] = Convention::getAllByEtat($id_etat);
        }
        
        View::setTemplate('convention_liste');
        View::bindParam("conventions", $conventions);

        $utilisateur = Utilisateur::get(Session::getUser()->id);
        View::bindParam("utilisateur", $utilisateur);

        View::display();
    }

    public static function consulter_action($id)
    {

        View::setTemplate('convention_consulter');

        $convention = Convention::get($id);
        View::bindParam("convention", $convention);


        $classes = Classe::getAll();
        View::bindParam("classes", $classes);


        $entreprises = Entreprise::getAll();
        View::bindParam("entreprises", $entreprises);

        $utilisateur = Utilisateur::get(Session::getUser()->id);
        View::bindParam("utilisateur", $utilisateur);

        View::display();
    }


    public static function creer_action()
    {
        $classes = Classe::getAll();
        View::bindParam("classes", $classes);

        $utilisateur = Session::getUser();
        if ($utilisateur->typeUtilisateur == 'etudiant') {
            View::bindParam("etudiant", $utilisateur);
        }

        

        $entreprises = Entreprise::getAll();
        View::bindParam("entreprises", $entreprises);

        View::setTemplate('convention_creer');
        View::display();
    }

    public static function convention_pdf_action($id)
    {
        // Convention
        $convention = Convention::get($id);

        // vérification de la sécurité
        // pour éditer un PDF il faut être :
        // - Admin
        // - Etablissement
        // - Secrétaire
        // - Enseignant : concerné en tant que référent de la classe
        // - Etudiant : concerné en tant que stagiaire
        // - Employé : concerné en tant que responsable
        // - Employé : concerné en tant que tuteur

        $autorise = false;

        if ($convention) {
            // si la convention existe ...

            $utilisateur = Utilisateur::get(Session::getUser()->id);
        
            switch($utilisateur->typeUtilisateur->slug) {
                case 'administrateur':
                case 'etablissement':
                case 'secretaire':
                    $autorise = true;
                break;
                case 'enseignant':
                    $autorise = $utilisateur->is_referent($convention);
                break;
                case 'etudiant':
                    $autorise = $utilisateur->id == $convention->id_etudiant;
                break;
                case 'employe':
                    // est responsable ?
                    $autorise = $utilisateur->is_responsable($convention);
                    if (! $autorise) {
                        // est tuteur ?
                        $autorise = $utilisateur->is_tuteur($convention);
                    }
                break;
                default:
                    $autorise = false;
                break;
            }
        }


        if (! $autorise) {
            $error404 = new Route("{*}", ErrorController::class, "error_404_action");
            $error404->execute();
            die();
        }

        // Préparation des données nécessaires
        $periodes = $convention->periodes;
        $id_tuteur_arr = [];
        $periode_arr = [];
        foreach($periodes as $periode) {
            if (! in_array($periode->id_tuteur, $id_tuteur_arr)) {
                $id_tuteur_arr[] = $periode->id_tuteur;
                $periode_arr[] = $periode;
            }
        }
        $nb_tuteurs = count($periode_arr);
        
        Pdf::bindParam('convention',$convention);
        Pdf::bindParam('nb_tuteurs',$nb_tuteurs);
        Pdf::bindParam('periode_arr',$periode_arr);

        // Année Universitaire
        $datetime = new DateTime();
        if ($datetime->format("mm") < "09") {
            // entre janvier et août
            $annee2 = $datetime->format("yy");
            $annee1 = $annee2 - 1;
        } else {
            // entre septembre et décembre
            $annee1 = $datetime->format("yy");
            $annee2 = $annee1 + 1;
        }

        Pdf::bindParam('annee1',$annee1);
        Pdf::bindParam('annee2',$annee2);

        $accompagnements = Accompagnement::getAllCheckedByClasse($convention->id_classe);
        Pdf::bindParam('accompagnements',$accompagnements);

        Pdf::addPage1();
        Pdf::addPage2();
        Pdf::addPage3();
        Pdf::addPage4();
        Pdf::addPage5();
        Pdf::addPage6();
        Pdf::addPage7();

        Pdf::output();
    }
}