<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\View;

class BaseController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/", BaseController::class, "accueil_action"));
        $router->addRoute(new Route("/accueil", BaseController::class, "accueil_action"));
        $router->addRoute(new Route("{*}", ErrorController::class, "error_404_action"));
        
        $route = $router->findRoute();
        View::setRoute($route);
        $route->execute();
    }

    public static function accueil_action()
    {
        View::setTemplate('accueil');
        View::display();
    }
}