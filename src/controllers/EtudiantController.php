<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Etudiant;
use GEST_STAGE\kernel\View;

class EtudiantController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/etudiants/{id_classe}", EtudiantController::class, "ajax_etudiants_action"));
        $router->addRoute(new Route("/ajax/etudiant/{id}", EtudiantController::class, "ajax_etudiant_action"));
        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/etudiants/{id_classe}",
            "/ajax/etudiant/{id}",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_etudiants_action($id_classe)
    {
        // méthode AJAX
        // recherche de l'information
        $array = Etudiant::getAllByClasse($id_classe);
        // conversion JSON + envoie
        echo json_encode($array);
    }

    public static function ajax_etudiant_action($id)
    {
        // méthode AJAX
        // recherche de l'information
        $object = Etudiant::get($id);
        // conversion JSON + envoie
        echo json_encode($object);
    }
}
