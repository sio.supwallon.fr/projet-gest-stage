<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Email;
use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Groupe;
use GEST_STAGE\model\classes\Section;
use GEST_STAGE\model\classes\TypeUtilisateur;
use GEST_STAGE\model\classes\Utilisateur;
use GEST_STAGE\kernel\View;
use GEST_STAGE\model\classes\Employe;

class UtilisateurController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/utilisateur/count-by-identifiant/{identifiant}", UtilisateurController::class, "ajax_count_by_identifiant_action"));
        $router->addRoute(new Route("/ajax/utilisateur/get-last-identifiant/{identifiant}", UtilisateurController::class, "ajax_get_last_identifiant_action"));
        $router->addRoute(new Route("/ajax/utilisateur/by-nom/{nom}", UtilisateurController::class, "ajax_utilisateur_by_nom"));
        $router->addRoute(new Route("/profil", UtilisateurController::class, "profil_action"));
        $router->addRoute(new Route("/utilisateur/admin/grant/{id}", UtilisateurController::class, "admin_grant_action"));
        $router->addRoute(new Route("/utilisateur/admin/revoke/{id}", UtilisateurController::class, "admin_revoke_action"));
        $router->addRoute(new Route("/utilisateur/delete/{id}", UtilisateurController::class, "delete_action"));
        $router->addRoute(new Route("/utilisateur/errors/reset/{id}", UtilisateurController::class, "erreurs_raz_action"));
        $router->addRoute(new Route("/utilisateur/link/{identifiant}", UtilisateurController::class, "link_action"));
        $router->addRoute(new Route("/utilisateur/link/new/{id}", UtilisateurController::class, "link_new_action"));
        $router->addRoute(new Route("/utilisateur/login", UtilisateurController::class, "login_action"));
        $router->addRoute(new Route("/utilisateur/logout", UtilisateurController::class, "logout_action"));
        $router->addRoute(new Route("/utilisateur/mot-de-passe-oublie", UtilisateurController::class, "mot_de_passe_oublie_action"));
        $router->addRoute(new Route("/utilisateur/mot-de-passe-oublie/submit", UtilisateurController::class, "mot_de_passe_oublie_submit_action"));
        $router->addRoute(new Route("/utilisateur/new", UtilisateurController::class, "new_action"));
        $router->addRoute(new Route("/utilisateur/new/submit", UtilisateurController::class, "new_submit_action"));
        $router->addRoute(new Route("/utilisateur/sendmail/welcome/{id}", UtilisateurController::class, "sendmail_welcome_action"));
        $router->addRoute(new Route("/utilisateur/update", UtilisateurController::class, "update_action"));
        $router->addRoute(new Route("/utilisateur/update/me", UtilisateurController::class, "update_action"));
        $router->addRoute(new Route("/utilisateur/update/my/password", UtilisateurController::class, "update_my_password_action"));
        $router->addRoute(new Route("/utilisateur/{id}", UtilisateurController::class, "details_action"));
        $router->addRoute(new Route("/utilisateurs", UtilisateurController::class, "list_action"));
        $router->addRoute(new Route("/utilisateurs/import", UtilisateurController::class, "utilisateurs_import_action"));
        $router->addRoute(new Route("/utilisateurs/import/submit", UtilisateurController::class, "utilisateurs_import_submit_action"));
        $router->addRoute(new Route("/utilisateurs/{lettre}", UtilisateurController::class, "list_lettre_action"));
        $router->addRoute(new Route("/utilisateurs/type/{type}", UtilisateurController::class, "list_type_action"));
        $router->addRoute(new Route("/utilisateurs/{type}/{lettre}", UtilisateurController::class, "list_type_lettre_action"));
        
        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/utilisateur/count-by-identifiant/{identifiant}",
            "/ajax/utilisateur/get-last-identifiant/{identifiant}",            "/ajax/utilisateur/by-nom/{nom}",
            "/utilisateur/login",
            "/utilisateur/logout",
            "/utilisateur/link/{identifiant}",
            "/utilisateur/mot-de-passe-oublie",
            "/utilisateur/mot-de-passe-oublie/submit",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_count_by_identifiant_action($identifiant)
    {
        $data = Utilisateur::countByIdentifiant($identifiant);
        echo json_encode($data);
    }

    public static function ajax_get_last_identifiant_action($identifiant)
    {
        $data = Utilisateur::getLastIdentifiant($identifiant);
        echo json_encode($data);
    }

    public static function ajax_utilisateur_by_nom($nom){
        $array = Utilisateur::getAllByNom($nom);
        echo json_encode($array);
    }

    public static function utilisateurs_import_action()
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);

        $sections = Section::getAll();
        $typeUtilisateur_arr['enseignant'] = TypeUtilisateur::getBySlug('enseignant');
        $typeUtilisateur_arr['etudiant'] = TypeUtilisateur::getBySlug('etudiant');

        View::setTemplate('utilisateurs_import');
        View::bindParam('callback', $callback);
        View::bindParam('sections', $sections);
        View::bindParam('typeUtilisateur_arr', $typeUtilisateur_arr);
        View::display();
    }

    public static function utilisateurs_import_submit_action()
    {
        $id_type_utilisateur = filter_input(INPUT_POST, 'id_type_utilisateur', FILTER_VALIDATE_INT);
        $active = filter_input(INPUT_POST, 'active', FILTER_VALIDATE_INT);
        if (! $active) {
            $send_link = filter_input(INPUT_POST, 'send_link', FILTER_VALIDATE_INT);
        } else {
            $send_link = 0;
        }

        $typeUtilisateur = TypeUtilisateur::get($id_type_utilisateur);
        if ($typeUtilisateur->slug == 'etudiant') {
            $id_classe = filter_input(INPUT_POST, 'id_classe', FILTER_VALIDATE_INT);
            if (! $id_classe) {
                $id_classe = null;
            }
        }

        if(isset(Utilisateur::SUBCLASSES[$typeUtilisateur->slug])) {
            $class = Utilisateur::SUBCLASSES[$typeUtilisateur->slug];
        } else {
            $class = Utilisateur::class;
        }

        // Upload CSV file
        // gestion de la signature
        if (isset($_FILES['csv_file']) && ! empty($_FILES['csv_file']['tmp_name'])) {
            $file = $_FILES['csv_file']['tmp_name'];
            $authorized_mime_types = [
                'text/csv',
                'text/plain',
            ];

            $mime = mime_content_type($file);

            if (in_array($mime, $authorized_mime_types)) {
                // true = le fichier est autorisé
                // récupération du contenu

                $csv = [];
                // parcours du fichier
                if (($handle = fopen($file, "r")) !== FALSE) {
                    $headers = fgetcsv($handle, 0, ";");
                    if (count($headers) == 6) {
                        $sendmail_result = true;
                        while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                            $o = (object)array_combine($headers, $data);
                            $utilisateur = new $class();
                            foreach($headers as $prop) {
                                $utilisateur->$prop = $o->$prop;
                            }
                            if (empty($utilisateur->identifiant)) {
                                // recherche d'un identifiant unique
                                $nom = strtolower($utilisateur->nom);
                                $prenom = strtolower($utilisateur->prenom);
                                $identifiant = "{$prenom}.{$nom}";
                                $count = Utilisateur::countByIdentifiant($identifiant);
                                if ($count == 0) {
                                    $utilisateur->identifiant = $identifiant;
                                } else {
                                    $last = Utilisateur::getLastIdentifiant($identifiant);
                                    if ($last == $identifiant) {
                                        $count = 1;
                                    } else {
                                        $end = substr($last, strlen($identifiant));
                                        if (is_numeric($end)) {
                                            $count = $end;
                                        } else {
                                            $count = 5554;
                                        }
                                    }
                                    $count++;
                                    $utilisateur->identifiant = $identifiant . $count;
                                }
                            }
                            $utilisateur->nom = strtoupper($utilisateur->nom);
                            $utilisateur->prenom = strtoupper($utilisateur->prenom[0]) . strtolower(substr($utilisateur->prenom, 1));
                            if (empty($utilisateur->motdepasse)) {
                                $utilisateur->motdepasse = null;
                            }
                            if ($send_link) {
                                $utilisateur->jeton_activation = self::generateToken();
                            }
                            if(isset($id_classe)) {
                                $utilisateur->id_classe = $id_classe;
                            }
                            $utilisateur->id_type_utilisateur = $id_type_utilisateur;

                            // enregistrement de l'utilisateur
                            $utilisateur->save();

                            if($send_link) {
                                // sendmail to user

                                if (empty($utilisateur->email)) {
                                    // no email
                                    Session::addError("Veuillez définir l'adresse email de l'utilisateur {$utilisateur->identifiant}.", __LINE__);
                                } else {

                                    // sendmail email_welcome
                                    $application = View::getParam('application');

                                    Email::setTo($utilisateur->email);
                                    $subject = "Bienvenue " 
                                    . (!empty($utilisateur->civilite)?$utilisateur->civilite." ":"")
                                    . (!empty($utilisateur->prenom)?$utilisateur->prenom." ":"")
                                    . (!empty($utilisateur->nom)?$utilisateur->nom." ":"");
                                    Email::setSubject("[{$application['title']}] {$subject}");
                                    Email::setTemplate("email_welcome");
                                    Email::bindParam("sender", Session::getUser());
                                    Email::bindParam("receiver", $utilisateur);
                                    if (! Email::send()) {
                                        $sendmail_result = false;
                                    }
                                }
                            }
                            $csv[] = $utilisateur;
                        }
                        fclose($handle);

                        if ($sendmail_result) {
                            $notifications = Session::getModal('notify');
                            $notifications[] = "Les liens d'activations ont bien été envoyés.";
                            Session::setModal('notify', $notifications);
                        } else {
                            Session::addError("Une erreur est survenue pendant l'envoie des emails d'activation", __LINE__);
                        }

                    } else {
                        // erreur de format de fichier
                        Session::addError("Une erreur est survenue pendant la lecture du fichier.", __LINE__);
                    }
                }
            } else {
                // false = fichier interdit
                Session::addError("Une erreur est survenue pendant la lecture du fichier.", __LINE__);
            }
        } else {
            // on n'a pas récupéré de fichier
        }

        // Redirection
        $basePath = View::getParam('basePath');
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);

        header("Location: {$basePath}{$callback}");
    }

    public static function mot_de_passe_oublie_action()
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);

        View::setTemplate('utilisateur_forgot_password');
        View::bindParam('callback', $callback);
        View::display();
    }

    public static function mot_de_passe_oublie_submit_action()
    {
        $identifiant = filter_input(INPUT_POST, 'identifiant', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

        $utilisateur = Utilisateur::getByIdentifiantEmail($identifiant, $email);

        if ($utilisateur) {
            // gen activation token
            $utilisateur->active = 0;
            $utilisateur->motdepasse = null;
            $utilisateur->jeton_activation = self::generateToken();
            $utilisateur->save();

            // sendmail

            $application = View::getParam('application');
    
            Email::setTo($utilisateur->email);
            Email::setSubject("[{$application['title']}] Réinitialisation de votre mot de passe");
            Email::setTemplate("email_password_reset");
            // bindParams
            Email::bindParam("receiver", $utilisateur);
            $result = Email::send();
            if ($result) {
                $notify = "Votre email de réinitialisation a été envoyé.";
                Session::addModal('notify', $notify);
            } else {
                Session::addError("Une erreur s'est produite à l'envoie de l'email de réinitialisation de mot de passe.", __LINE__);
            }
        } else {
            Session::addError("Erreur d'identifiant ou d'adresse email.", __LINE__);
        }

        // Redirection
        $basePath = View::getParam('basePath');
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);

        header("Location: {$basePath}/utilisateur/mot-de-passe-oublie?callback={$callback}");
    }


    public static function profil_action()
    {
        $utilisateur = Session::getUser();

        self::details_action($utilisateur->id);
    }

    public static function login_action()
    {
        $identifiant = filter_input(INPUT_POST, 'identifiant', FILTER_SANITIZE_STRING);
        $motdepasse = filter_input(INPUT_POST, 'motdepasse', FILTER_SANITIZE_STRING);
        
        $utilisateur = Utilisateur::getByIdentifiant($identifiant);

        if ($utilisateur) {
            // salage du mot de passe
            $motdepasse .= $utilisateur->sel;
            // hachage du mot de passe
            $motdepasse = sha1($motdepasse);
        }

        self::session_check($identifiant, $motdepasse);

        // Redirection
        $basePath = View::getParam('basePath');
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);

        header("Location: {$basePath}{$callback}");
    }

    public static function logout_action() 
    {
        Session::logout();
        $basePath = View::getParam('basePath');

        header("Location: {$basePath}");  
    }

    public static function new_action()
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);

        $types = TypeUtilisateur::getAll();

        View::setTemplate('utilisateur_new');
        View::bindParam("callback", $callback);
        View::bindParam("types", $types);

        View::display();
    }

    public static function new_submit_action()
    {
        $identifiant = filter_input(INPUT_POST, 'identifiant', FILTER_SANITIZE_STRING);
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
        $id_type_utilisateur = filter_input(INPUT_POST, 'id_type_utilisateur', FILTER_VALIDATE_INT);

        $type = TypeUtilisateur::get($id_type_utilisateur);
        if (isset(Utilisateur::SUBCLASSES[$type->slug])) {
            $class = Utilisateur::SUBCLASSES[$type->slug];
        } else {
            $class = Utilisateur::class;
        }
        

        $utilisateur = new $class();

        $utilisateur->identifiant = $identifiant;
        $utilisateur->nom = $nom;
        $utilisateur->prenom = $prenom;
        $utilisateur->email = $email;
        $utilisateur->jeton_activation = self::generateToken();
        $utilisateur->id_type_utilisateur = $id_type_utilisateur;
        
        
        $utilisateur->save();

        // Redirection
        $basePath = View::getParam('basePath');
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);

        header("Location: {$basePath}{$callback}");
    }

    public static function update_action()
    {
        // recupération des infos génériques
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $identifiant = filter_input(INPUT_POST, 'identifiant', FILTER_SANITIZE_STRING);
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_STRING);
        $fonction = filter_input(INPUT_POST, 'fonction', FILTER_SANITIZE_STRING);
        $telephone = filter_input(INPUT_POST, 'telephone', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

        $its_me = Session::getUser()->id == $id;
        $password_changed = false;

        if (isset($_POST['active'])) {
            $active = filter_input(INPUT_POST, 'active', FILTER_VALIDATE_INT);
        } else {
            $active = null;
        }

        // récupération de l'utilisateur
        $utilisateur = Utilisateur::get($id);
        $class = get_class($utilisateur);

        // mise à jour du mot de passe ?
        if(!empty($_POST['motdepasse']) && !empty($_POST['motdepasse2'])) {
            $motdepasse = filter_input(INPUT_POST, 'motdepasse', FILTER_SANITIZE_STRING);
            $motdepasse2 = filter_input(INPUT_POST, 'motdepasse2', FILTER_SANITIZE_STRING);

            if ($motdepasse == $motdepasse2) {
                // salage du mot de passe
                $salted_password = $motdepasse . $utilisateur->sel;
                $password_hash = sha1($salted_password);
                
                // màj utilisateur
                $utilisateur->motdepasse = $password_hash;
                $password_changed = true;

                // màj session
                if($its_me) {
                    Session::getUser()->motdepasse = $password_hash;
                }
            }
        }

        // mise à jour des autres informations
        $utilisateur->identifiant = $identifiant;
        $utilisateur->nom = $nom;
        $utilisateur->prenom = $prenom;
        $utilisateur->fonction = $fonction;
        $utilisateur->telephone = $telephone;
        $utilisateur->email = $email;
        $utilisateur->active = $active;

        // Mise à jour des informations spécifiques
        // sauf la signature qui est gérée après
        switch($utilisateur->typeUtilisateur) {
            case 'employe':
                $utilisateur->id_utilisateur = $id;
                $id_entreprise = filter_input(INPUT_POST, 'id_entreprise', FILTER_VALIDATE_INT);
                $utilisateur->id_entreprise = $id_entreprise;
            break;
            case 'enseignant':
                $utilisateur->id_utilisateur = $id;
            break;
            case 'etablissement':
                $utilisateur->id_utilisateur = $id;
            break;
            case 'etudiant':
                $utilisateur->id_utilisateur = $id;
                $sexe = filter_input(INPUT_POST, 'sexe', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^(F|M)$/']]);
                $utilisateur->sexe = $sexe;
                $num_secu = filter_input(INPUT_POST, 'num_secu', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\d{15}$/']]);
                $utilisateur->num_secu = $num_secu;
                $date_naissance = filter_input(INPUT_POST, 'date_naissance', FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => '/^\d{4}-\d{2}-\d{2}$/']]);
                $utilisateur->date_naissance = $date_naissance;
                $adresse = filter_input(INPUT_POST, 'adresse', FILTER_SANITIZE_STRING);
                $utilisateur->adresse = $adresse;
                $id_ville = filter_input(INPUT_POST, 'id_ville', FILTER_VALIDATE_INT);
                $utilisateur->id_ville = $id_ville;
                $id_ville_naissance = filter_input(INPUT_POST, 'id_ville_naissance', FILTER_VALIDATE_INT);
                $utilisateur->id_ville_naissance = $id_ville_naissance;
                $id_classe = filter_input(INPUT_POST, 'id_classe', FILTER_VALIDATE_INT);
                $utilisateur->id_classe = $id_classe;
            break;
        }

        // gestion de la signature
        if (isset($_FILES['signature']) && ! empty($_FILES['signature']['tmp_name'])) {
            //var_dump($_FILES);
            $tmp_name = $_FILES['signature']['tmp_name'];
            $authorized_mime_types = [
                'image/jpeg', 'image/png', 'image/svg+xml'
            ];
            $mime_types_extension = [
                'image/jpeg' => '.jpg', 
                'image/png' => '.png',
                'image/svg+xml' => '.svg',
            ];

            $mime = mime_content_type($tmp_name);
            if (in_array($mime, $authorized_mime_types)) {
                // true = le fichier est autorisé
                $extension = $mime_types_extension[$mime];
                $signature = sha1_file($tmp_name) . $extension;
                $destination = "src/view/images/users/{$id}/{$signature}";
                if (! file_exists("src/view/images/users/{$id}")) {
                    mkdir("src/view/images/users/{$id}");
                }
                move_uploaded_file($tmp_name, $destination);

                // suppression de l'ancienne signature
                $utilisateur = Utilisateur::get($id);
                if(! empty($utilisateur->signature)) {
                    $old_file = "src/view/images/users/{$id}/{$utilisateur->signature}";
                    unlink($old_file);
                }

                $utilisateur->signature = $signature;

            } else {
                // false = fichier interdit
            }
        } else {
            // on n'a pas récupéré de fichier
        }
        
        // enregistrement de l'utilisateur
        $utilisateur->save();

        if($password_changed) {
            $application = View::getParam('application');
            Email::setTo($utilisateur->email);
            Email::setSubject("[{$application['title']}] Mot de passe modifié");
            // Common bindParams
            Email::bindParam("receiver", $utilisateur);
            Email::bindParam("password", $motdepasse);

            if ($its_me) {
                // template : password changed by me
                Email::setTemplate("email_password_changed_by_me");
            } else {
                // template : password changed by another
                Email::setTemplate("email_password_changed_by_another");
                // Specific bindParams
                Email::bindParam("sender", Session::getUser());
            }
            // sendmail
            $result = Email::send();
        }

        // Redirection
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);
        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}");
    }

    public static function details_action($id)
    {
        $utilisateur = Utilisateur::get($id);
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);

        if ($utilisateur->typeUtilisateur->slug == "etudiant") {
            $sections = Section::getAll();
        }

        $its_me = Session::getUser()->id == $utilisateur->id;

        //var_dump($utilisateur);

        View::setTemplate('utilisateur_details');
        View::bindParam("utilisateur", $utilisateur);
        View::bindParam("its_me", $its_me);
        View::bindParam("callback", $callback);
        if ($utilisateur->typeUtilisateur->slug == "etudiant") {
            View::bindParam("sections", $sections);
        }

        View::display();
    }

    public static function link_action($identifiant)
    {
        $jeton = filter_input(INPUT_GET, 'jeton', FILTER_SANITIZE_URL);

        $utilisateur = Utilisateur::getByIdentifiantJeton($identifiant, $jeton);
        
        if ($utilisateur) {
            // activation du compte
            
            Utilisateur::active($utilisateur);

            // ouverture de session
            Session::login($utilisateur, $utilisateur->groupes, $utilisateur->droits);

            // préparation du template

            $application = View::getParam("application");
            $application['navbar']['enabled'] = false;
        
            View::setTemplate('utilisateur_activation');
            View::bindParam("application", $application);
            View::bindParam("utilisateur", $utilisateur);
    
        } else {
            // erreur d'activation

            View::setTemplate('utilisateur_activation_error');
        }
        View::display();
    }

    public static function delete_action($id)
    {
        // Action
        Utilisateur::delete($id);

        // Redirection
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        $basePath = View::getParam('basePath');
        header("Location: {$basePath}{$callback}");
    }

    public static function erreurs_raz_action($id)
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        $callback2 = filter_input(INPUT_GET, 'callback2', FILTER_SANITIZE_STRING);

        $basePath = View::getParam('basePath');

        Utilisateur::resetErrors($id);

        header("Location: {$basePath}{$callback}?callback={$callback2}");
    }

    public static function session_check($identifiant, $motdepasse)
    {
        if (isset($_POST['jeton'])){
            $jeton = filter_input(INPUT_POST, 'jeton', FILTER_SANITIZE_STRING);
            $utilisateur = Utilisateur::getByIdentifiantJeton($identifiant, $jeton);
        } else {
            $utilisateur = Utilisateur::getByIdentifiantMotdepasse($identifiant, $motdepasse);
        }
        
        if(isset($utilisateur)) {
            if ($utilisateur->active) {
                Session::login($utilisateur, $utilisateur->groupes, $utilisateur->droits);
                Utilisateur::login($utilisateur);
            } else {
                // erreur compte désactivé
                Session::logout();
                Session::addError("Compte désactivé. Veuillez contacter un administrateur.", __LINE__);
            }
        } else {
            // erreur d'identifiant / mot de passe
            Session::logout();
            
            $utilisateur = Utilisateur::getByIdentifiant($identifiant);
            if (isset($utilisateur)) {
                // enregistrement de l'erreur
                $utilisateur->nb_erreurs++;
                Utilisateur::loginError($utilisateur);
                if ($utilisateur->nb_erreurs < 3) {
                    Session::addError("Identifiant ou mot de passe incorrect", __LINE__);
                } else {
                    Session::addError(
                        "Identifiant ou mot de passe incorrect<br>
                         Veuillez patienter 3 minutes avant de vous reconnecter.", __LINE__);
                }
            } else {
                Session::addError("Identifiant ou mot de passe incorrect", __LINE__);
            }
        }
    }

    public static function list_action()
    {
        $lettre = Utilisateur::getPremiereLettre();
        self::list_lettre_action($lettre);
    }

    public static function list_type_action($type)
    {
        $lettre = Utilisateur::getPremiereLettreByType($type);
        self::list_type_lettre_action($type, $lettre);
    }

    public static function list_lettre_action($lettre)
    {
        self::list_type_lettre_action("", $lettre);
    }

    public static function list_type_lettre_action($type, $lettre)
    {
        if (empty($type)) {
            $utilisateurs = Utilisateur::getAllByLettre($lettre);
            $lettres_array = Utilisateur::getLettres();
            $premiere_lettre = Utilisateur::getPremiereLettre();
        } else {            
            $utilisateurs = Utilisateur::getAllByTypeLettre($type, $lettre);
            $lettres_array = Utilisateur::getLettresByType($type);
            $premiere_lettre = Utilisateur::getPremiereLettreByType($type);
        }

        
        $lettres_enabled = [];
        foreach($lettres_array as $row)
        {
            $lettres_enabled[] = $row['lettre'];
        }

        if (empty($type)) {
            $selected_type = null;
        } else {
            $selected_type = TypeUtilisateur::getBySlug($type);
        }
        
        $types = TypeUtilisateur::getAll();
        
        $lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $uri = View::getParam('current_uri');
        if ($uri == '/utilisateurs') {
            $uri = "/utilisateurs/{$premiere_lettre}";
        } else if ($uri == "/utilisateurs/type/{$type}") {
            $uri = "/utilisateurs/{$type}/{$premiere_lettre}";
        }

        View::setTemplate('utilisateur_list');
        View::bindParam("utilisateurs", $utilisateurs);
        View::bindParam("utilisateurs", $utilisateurs);
        View::bindParam("lettres_enabled", $lettres_enabled);
        View::bindParam("lettres", $lettres);
        View::bindParam("lettre", $lettre);
        View::bindParam("premiere_lettre", $premiere_lettre);
        View::bindParam("types", $types);
        View::bindParam("selected_type", $selected_type);
        View::bindParam("uri", $uri);
        View::display();
    }

    public static function admin_grant_action($id)
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);
        $callback2 = filter_input(INPUT_GET, 'callback2', FILTER_SANITIZE_URL);

        //addGroup
        $groupe_admin = Groupe::getByNom("Administrateur(s)");
        Utilisateur::addGroupe($id, $groupe_admin->id);

        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}?callback={$callback2}");
    }

    public static function admin_revoke_action($id)
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);
        $callback2 = filter_input(INPUT_GET, 'callback2', FILTER_SANITIZE_URL);

        //removeGroup
        $groupe_admin = Groupe::getByNom("Administrateur(s)");
        Utilisateur::removeGroupe($id, $groupe_admin->id);

        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}?callback={$callback2}");
    }

    public static function link_new_action($id)
    {
        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_URL);
        $callback2 = filter_input(INPUT_GET, 'callback2', FILTER_SANITIZE_URL);

        $jeton = self::generateToken();

        Utilisateur::setJetonActivation($id, $jeton);

        Session::setModal('notify-link', true);

        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}?callback={$callback2}");
    }

    public static function update_my_password_action()
    {
        // recupération des infos génériques
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $motdepasse = filter_input(INPUT_POST, 'motdepasse', FILTER_SANITIZE_STRING);
        $motdepasse2 = filter_input(INPUT_POST, 'motdepasse2', FILTER_SANITIZE_STRING);

        if ($motdepasse == $motdepasse2) {
            // récupération de l'utilisation
            $utilisateur = Session::getUser();

            // salage du mot de passe

            $salted_password = $motdepasse . $utilisateur->sel;
            $password_hash = sha1($salted_password);
            
            // màj session
            $utilisateur->motdepasse = $password_hash;

            // màj mot de passe + ràz jeton
            Utilisateur::update_my_password($id, $password_hash);

            // sendmail email_account_activated
            $application = View::getParam('application');

            Email::setTo($utilisateur->email);
            Email::setSubject("[{$application['title']}] Activation de votre compte");
            Email::setTemplate("email_account_activated");
            // bindParams
            Email::bindParam("receiver", $utilisateur);
            Email::bindParam("password", $motdepasse);
            $result = Email::send();
        }

        // Redirection
        $basePath = View::getParam('basePath');

        header("Location: {$basePath}");
    }

    public static function generateToken()
    {
        $data = random_bytes(256);
        $base64 = base64_encode($data);
        $token = substr($base64, 0, 256);
        $token = str_replace('+', ' ', $token);
        $token = filter_var($token, FILTER_SANITIZE_URL);

        return $token;
    }

    public static function sendmail_welcome_action($id)
    {
        $application = View::getParam('application');
        $sender = Session::getUser();
        $receiver = Utilisateur::get($id);

        if (empty($receiver->email)) {
            // no email
            Session::addError("Veuillez définir l'adresse email de l'utilisateur.", __LINE__);
        } else {
            // sendmail email_welcome
            Email::setTo($receiver->email);
            $subject = "Bienvenue " 
            . (!empty($receiver->civilite)?$receiver->civilite." ":"")
            . (!empty($receiver->prenom)?$receiver->prenom." ":"")
            . (!empty($receiver->nom)?$receiver->nom." ":"");
            Email::setSubject("[{$application['title']}] {$subject}");
            Email::setTemplate("email_welcome");
            Email::bindParam("sender", $sender);
            Email::bindParam("receiver", $receiver);
            $result = Email::send();
            if ($result) {
                $notifications = Session::getModal('notify');
                $notifications[] = "L'email a bien été envoyé.";
                Session::setModal('notify', $notifications);
            } else {
                Session::addError("Une erreur est survenue pendant l'envoie de l'email", __LINE__);
            }
        }

        $callback = filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING);
        $callback2 = filter_input(INPUT_GET, 'callback2', FILTER_SANITIZE_STRING);
        $basePath = View::getParam('basePath');

        header("Location: {$basePath}{$callback}?callback={$callback2}");
    }
}
