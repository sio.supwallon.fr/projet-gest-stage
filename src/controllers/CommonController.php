<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\kernel\View;

final class CommonController
{
    public static function bindTemplateVars($template_vars)
    {
        foreach($template_vars as $var => $value) {
            View::bindParam($var, $value);
        }
    }
}