<?php
namespace GEST_STAGE\controllers;

use GEST_STAGE\kernel\Route;
use GEST_STAGE\kernel\Router;
use GEST_STAGE\kernel\Session;
use GEST_STAGE\model\classes\Employe;
use GEST_STAGE\kernel\View;

class EmployeController
{
    public static function route()
    {
        $router = new Router();
        $router->addRoute(new Route("/ajax/tuteurs/{id}", EmployeController::class, "ajax_tuteur"));
        $router->addRoute(new Route("/ajax/responsable/{id}", EmployeController::class, "ajax_responsable_action"));

        
        $error404 = new Route("{*}", ErrorController::class, "error_404_action");
        $router->addRoute($error404);
        
        $route = $router->findRoute();

        $whitelist_routes = [
            "/ajax/tuteurs/{id}",
            "/ajax/responsable/{id}",
        ];

        $requested_uri = $route->getUri();
        if (! in_array($requested_uri, $whitelist_routes)
            && ! Session::isGranted($requested_uri)) {
            $route = $error404;
        }

        View::setRoute($route);
        $route->execute();
    }

    public static function ajax_tuteur($id_entreprise)
    {
        $array = Employe::getAllByEntreprise($id_entreprise);
        echo json_encode($array);
    }

    public static function ajax_responsable_action($id)
    {
        // méthode AJAX
        // recherche de l'information
        $object = Employe::get($id);
        // conversion JSON + envoie
        echo json_encode($object);
    }
}
