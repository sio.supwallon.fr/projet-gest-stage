<?php
namespace GEST_STAGE\kernel;

use DateInterval;
use DatePeriod;
use DateTime;

abstract class Date
{
    public static function toFr($date_string)
    {
        $date = new DateTime($date_string);
        return $date->format('d/m/Y');
    }

    public static function getPeriod($from, $to)
    {
        $begin = new DateTime($from);

        $interval = new DateInterval('P1D');

        $end = new DateTime($to);
        // pour que le dernier jour soit inclus !
        $end->setTime(0, 0, 1);

        return new DatePeriod($begin, $interval, $end);
    }
}


