<?php
namespace GEST_STAGE\kernel;

use Exception;

class Email extends AbstractView
{
    private static $_to;
    private static $_subject;
    private static $_message;

    public static function setTo($value) { self::$_to = $value; }
    public static function setSubject($value) { self::$_subject = $value; }

    public static function send()
    {
        if(! isset(self::$_to)) {
            throw new Exception("Email Exception: Receiver is not set");
        }
        if(! isset(self::$_subject)) {
            throw new Exception("Email Exception: Subject is not set");
        }
        if(! isset(self::$template)) {
            throw new Exception("Email Exception: Template is not set");
        }

        self::$_message = self::loadTemplate();

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= "From: g.stage.hdf@gmail.com" . "\r\n";

        return mail(
            self::$_to,
            self::$_subject,
            self::$_message,
            $headers
        );
    }

    public static function render()
    {
        foreach(self::$parameters as $name => $value)
        {
            $$name = $value;
        }

        $template = self::$template . ".html.php";
        
        require "src/view/templates/email_index.html.php";
    }

    private static function loadTemplate()
    {
        foreach(self::$parameters as $name => $value)
        {
            $$name = $value;
        }

        $template = self::$template . ".html.php";

        ob_start();
        require "src/view/templates/email_index.html.php";
        $message = ob_get_clean();
        if (ob_get_contents()) ob_end_flush();

        return $message;
    }
}


