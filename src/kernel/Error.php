<?php
namespace GEST_STAGE\kernel;

class Error
{
    public $code;
    public $message;

    public function __construct($message, $code = null)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function __toString()
    {
        $str = $this->message;
        if (isset($this->code)) {
            $str .= " (code: {$this->code})";
        } 
        return $str;
    }
}