<?php
namespace GEST_STAGE\kernel;

class View extends AbstractView
{
    protected static $_route;
    public static function setRoute($value) { self::$_route = $value; }

    public static function display()
    {
        foreach(self::$parameters as $name => $value)
        {
            $$name = $value;
        }

        $template = self::$template . ".html.php";
        $script = self::$template . ".js";
        $style = self::$template . ".css";

        $route = self::$_route;

        require_once "src/view/templates/index.html.php";
    }
}


