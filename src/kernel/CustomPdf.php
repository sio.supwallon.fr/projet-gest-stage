<?php
namespace GEST_STAGE\kernel;

use TCPDF;

class CustomPdf extends TCPDF {
    protected $print_header_first_page = true;
    public function setPrintHeaderOnFirstPage($val = true) { $this->print_header_first_page = $val; }
    protected $print_footer_first_page = true;
    public function setPrintFooterOnFirstPage($val = true) { $this->print_footer_first_page = $val; }
    protected $parameters = [];
    public function setParameters($values = []) { $this->parameters = $values; }

    //Page header
    public function Header() {
        if ($this->page == 1) {
            // First page config

            // get the current page break margin
            $bMargin = $this->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $this->AutoPageBreak;

            // disable auto-page-break
            $this->SetAutoPageBreak(false, 0);
            // set bacground image
            $img_file = $this->parameters['pdf']['images'] . $this->parameters['pdf']['first_page'];
            $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

            // restore auto-page-break status
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $this->setPageMark();
        }
    }

    // Page footer
    public function Footer() {
        if ($this->page > 1 || $this->print_footer_first_page) {
            // custom footer config

            // Set font
            $this->SetFont('helvetica', '', 10);
            // Page number
            $page = $this->getAliasNumPage();
            $pages = $this->getAliasNbPages();
            $txt = "{$page} / {$pages}";
            $this->SetY(285);
            $this->Cell(210, 10, $txt, false, false, 'R', 0, '', 0, false, 'T', 'M');
        }
    }
}