<?php
namespace GEST_STAGE\kernel;

use TCPDF;

class Pdf extends AbstractView {
    private static $_pdf;
    private static $_pages;
    private static $_html;

    private static function init()
    {
        // init $_pages;

        self::$_pages = 0;

        // create new PDF document
        self::$_pdf = new CustomPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set pdf custom parameters
        self::$_pdf->setParameters(self::$parameters);

        // set document information
        self::$_pdf->SetCreator(PDF_CREATOR);
        self::$_pdf->SetAuthor('G-Stage');
        self::$_pdf->SetTitle('Convention de Stage');
        self::$_pdf->SetSubject('Convention de Stage');
        self::$_pdf->SetKeywords('G-Stage','Convention', 'Stage');

        // remove default header/footer on first page
        self::$_pdf->setPrintHeaderOnFirstPage(false);
        self::$_pdf->setPrintFooterOnFirstPage(false);

        // remove default header/footer
        //self::$_pdf->setPrintHeader(false);
        //self::$_pdf->setPrintFooter(false);

        // set default header data
        //self::$_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Convention de stage', 'Prénom NOM Classe');
        //self::$_pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        self::$_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        self::$_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        self::$_pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        self::$_pdf->SetMargins(10, 0, 10);
        self::$_pdf->SetHeaderMargin(0);
        self::$_pdf->SetFooterMargin(10);

        // set cell padding
        self::$_pdf->setCellPaddings(2, 2, 1, 0);

        // set cell margins
        self::$_pdf->setCellMargins(0, 0, 0, 0);

        // set auto page breaks
        //self::$_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        self::$_pdf->SetAutoPageBreak(true, 20);

        // set image scale factor
        //self::$_pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            self::$_pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        self::$_pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        self::$_pdf->SetFont('dejavusans', '', 10, '', true);

    }

    public static function addPage($template)
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();
        self::$_pages++;
        self::$_html[self::$_pages] = self::loadTemplate($template);
        //self::$_pdf->writeHTML(self::$_html[self::$_pages]);
        self::$_pdf->writeHTMLCell(0, 0, '', '', self::$_html[self::$_pages], 0, 1, 0, true, '', true);
    }

    public static function addPage1()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        $x = 20;

        // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        $html = self::loadTemplate('pdf_p01_title');
        self::$_pdf->writeHTMLCell('', '', $x, 220, $html, 0, 1, 0, true, '', false);

        $html = self::loadTemplate('pdf_p01_subtitle');
        self::$_pdf->writeHTMLCell('', '', $x, 235, $html, 0, 1, 0, true, '', false);

        $html = self::loadTemplate('pdf_p01_summary');
        self::$_pdf->writeHTMLCell('', '', $x, 247, $html, 0, 1, 0, true, '', false);

        $html = self::loadTemplate('pdf_p01_author_date');
        self::$_pdf->writeHTMLCell('', '', $x, 270, $html, 0, 1, 0, true, '', false);

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage2()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        $html = self::loadTemplate('pdf_p02_annee_universitaire');
        self::$_pdf->SetXY(0, 22);
        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
        self::$_pdf->writeHTMLCell(130, '', '', '', $html, 0, 0, 0, true, '', true);

        $html = self::loadTemplate('pdf_p02_titre');
        self::$_pdf->SetXY(0, 30);
        self::$_pdf->writeHTMLCell(130, '', '', '', $html, 0, 0, 0, true, '', true);

        $image_file = self::$parameters['pdf']['images'] . self::$parameters['pdf']['logo'];
        self::$_pdf->SetXY(140, 10);
        //           Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        self::$_pdf->Image($image_file, '', '', 60, '', '', '', '', false, 300, '', false, false, 0, false, false, false);

        $html = self::loadTemplate('pdf_p02_zone_01_etablissement');
        self::$_pdf->SetXY(5, 65);
        self::$_pdf->writeHTMLCell(100, 70, '', '', $html, 1, 1, 0, true, '', true);

        $html = self::loadTemplate('pdf_p02_zone_02_entreprise');
        self::$_pdf->SetXY(105, 65);
        self::$_pdf->writeHTMLCell(100, 70, '', '', $html, 1, 1, 0, true, '', true);

        self::$_pdf->SetXY(5, 135);
        self::$_pdf->writeHTMLCell(200, 50, '', '', '', 1, 1, 0, true, '', true);
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

        self::$_pdf->SetXY(5, 135);
        $html = self::loadTemplate('pdf_p02_zone_03_1_stagiaire');
        self::$_pdf->MultiCell(200, 10, $html, 0, 'C', 0, 1, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p02_zone_03_2_c1_stagiaire');
        self::$_pdf->MultiCell(80, 25, $html, 0, 'L', 0, 0, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p02_zone_03_2_c2_stagiaire');
        self::$_pdf->MultiCell(70, 25, $html, 0, 'L', 0, 0, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p02_zone_03_2_c3_stagiaire');
        self::$_pdf->MultiCell(50, 25, $html, 0, 'L', 0, 1, '', '', true, 0, true);
        
        $html = self::loadTemplate('pdf_p02_zone_03_3_stagiaire');
        self::$_pdf->MultiCell(200, 10, $html, 0, 'L', 0, 1, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p02_zone_04_referent');
        self::$_pdf->SetXY(5, 185);
        self::$_pdf->writeHTMLCell(100, 40, '', '', $html, 1, 1, 0, true, '', true);

        $html = self::loadTemplate('pdf_p02_zone_05_tuteur');
        self::$_pdf->SetXY(105, 185);
        self::$_pdf->writeHTMLCell(100, 40, '', '', $html, 1, 1, 0, true, '', true);

        self::$_pdf->SetXY(5, 225);
        $html = self::loadTemplate('pdf_p02_zone_06_periodes');
        self::$_pdf->writeHTMLCell(200, 40, '', '', $html, 1, 1, 0, true, '', true);

        self::$_pdf->SetXY(5, 265);
        $html = self::loadTemplate('pdf_p02_zone_07_cpam');
        self::$_pdf->writeHTMLCell(200, 20, '', '', $html, 1, 1, 0, true, '', true);

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage3()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->SetFont('dejavusans', '', 8.5, '', true);
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        self::$_pdf->SetY(10);
        $html = self::loadTemplate('pdf_art_01');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_02');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_03');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_04');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_05');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage4()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        self::$_pdf->SetY(10);
        $html = self::loadTemplate('pdf_art_05_bis');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_05_ter');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_06');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_06.1');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage5()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        self::$_pdf->SetY(10);
        $html = self::loadTemplate('pdf_art_06.2');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_07');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_07_part_02');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'C');

        $html = self::loadTemplate('pdf_art_07_part_03');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_08');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage6()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        self::$_pdf->SetY(10);
        $html = self::loadTemplate('pdf_art_09');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_10');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_11');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_12');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        $html = self::loadTemplate('pdf_art_13');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function addPage7()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }
        self::$_pdf->AddPage();

        // get the current page break margin
        $bMargin = self::$_pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = self::$_pdf->getAutoPageBreak();

        // disable auto-page-break
        self::$_pdf->SetAutoPageBreak(false, 0);

        //           writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        self::$_pdf->SetY(10);

        $html = self::loadTemplate('pdf_p07_fait_a');
        self::$_pdf->writeHTMLCell(190, '', '', '', $html, 0, 1, 0, true, 'J');

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(0, 0, 30, 10);

        // set color for background
        //TCPDF::SetFillColor($col1=0, $col2=-1, $col3=-1, $col4=-1, $ret=false, $name='');
        self::$_pdf->SetFillColor(255*(1-.20)); // Gray

        // Récupération de la convention
        $convention = self::getParam('convention');
        $nb_tuteurs = self::getParam('nb_tuteurs');

        $html = self::loadTemplate('pdf_p07_zone_01_etablissement');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 0, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p07_zone_02_responsable');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 1, '', '', true, 0, true);

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(55, 0, 0, 10);

        $html = self::loadTemplate('pdf_p07_zone_03_etudiant');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 1, '', '', true, 0, true);

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(0, 0, 30, 0);

        $html = self::loadTemplate('pdf_p07_zone_04_enseignant');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 0, '', '', true, 0, true);

        if ($nb_tuteurs == 1) {
            $html = self::loadTemplate('pdf_p07_zone_05_tuteur');
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 1, '', '', true, 0, true);
        } else {
            $html = self::loadTemplate('pdf_p07_zone_05_tuteurs');
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            self::$_pdf->MultiCell(80, 50, $html, 1, 'L', true, 1, '', '', true, 0, true);
        }

        self::$_pdf->SetY(33);

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(5, 0, 35, 25);

        //TCPDF::SetFillColor($col1=0, $col2=-1, $col3=-1, $col4=-1, $ret=false, $name='');
        self::$_pdf->SetFillColor(255*(1-.0)); // Gray (0% = White)

        $html = self::loadTemplate('pdf_p07_sign_01_etablissement');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(70, 35, $html, 1, 'C', true, 0, '', '', true, 0, true);

        $html = self::loadTemplate('pdf_p07_sign_02_responsable');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(70, 35, $html, 1, 'C', true, 1, '', '', true, 0, true);

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(60, 0, 0, 18);

        $html = self::loadTemplate('pdf_p07_sign_03_etudiant');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(70, 35, $html, 1, 'C', true, 1, '', '', true, 0, true);

        // setCellMargins(left='', top='', right='', bottom='');
        self::$_pdf->setCellMargins(5, 7, 35, 0);

        $html = self::loadTemplate('pdf_p07_sign_04_enseignant');
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        self::$_pdf->MultiCell(70, 35, $html, 1, 'C', true, 0, '', '', true, 0, true);

        if ($nb_tuteurs == 1) {
            $html = self::loadTemplate('pdf_p07_sign_05_tuteur');
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            self::$_pdf->MultiCell(70, 35, $html, 1, 'C', true, 1, '', '', true, 0, true);
        } else {
            // SetCellMargins(left='', top='', right='', bottom='');
            self::$_pdf->SetCellMargins(2, 0, 0, 0);

            // SetCellPaddings($left='', $top='', $right='', $bottom='');
            self::$_pdf->SetCellPaddings(0,0,0,0);

            $html = self::loadTemplate('pdf_p07_zone_05_tuteurs_liste');
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            self::$_pdf->MultiCell(37, '', $html, 0, 'L', false, 0, '', '', true, 0, true);

            // SetCellMargins(left='', top='', right='', bottom='');
            self::$_pdf->SetCellMargins(0, 0, 0, 0);
            
            $html = self::loadTemplate('pdf_p07_sign_05_tuteurs');
            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            self::$_pdf->MultiCell(37, 35, $html, 1, 'C', true, 0, '', '', true, 0, true);
        }

        // restore auto-page-break status
        self::$_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    }

    public static function output()
    {
        if (!isset(self::$_pdf)) {
            self::init();
        }

        self::$_pdf->Output('convention.pdf', 'I');
    }

    public static function render()
    {
        foreach(self::$parameters as $name => $value)
        {
            $$name = $value;
        }

        $template = self::$template . ".html.php";
        
        require "src/view/templates/pdf_index.html.php";
    }

    private static function loadTemplate($template)
    {
        foreach(self::$parameters as $name => $value)
        {
            $$name = $value;
        }

        $template = $template . ".html.php";

        ob_start();
        require "src/view/templates/$template";
        $html = ob_get_clean();
        //ob_end_clean();

        return $html;
    }
}