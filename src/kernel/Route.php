<?php
namespace GEST_STAGE\kernel;
 
final class Route
{
    private $uri;
    private $controller;
    private $action;
    private $name;
    private $requirements;
    private $uri_items;
    private $uri_parameters;

    /**
     * __construct
     *
     * @param string $uri          uri
     * @param string $controller   controller
     * @param string $action       action
     * @param string $name         name (default:"")
     * @param string $requirements requirements(default:[])
     */
    public function __construct($uri, $controller, $action="route", $name="", $requirements = [])
    {
        $this->uri = $uri;
        $this->controller = $controller;
        $this->action = $action;
        $this->name = $name;
        $this->requirements = $requirements;
        $this->initParameters();
    }

    /**
     * Function isParameter
     *
     * @param string $item item
     *
     * @return bool true if an item is a well formed parameter
     */
    private function isParameter($item)
    {
        return substr($item, 0, 1) == '{' && substr($item, -1, 1) == '}';
    }

    /**
     * Function getParameterName
     *
     * @param string $item item
     *
     * @return string the parameter name
     */
    private function getParameterName($item)
    {
        return substr($item, 1, strlen($item) -2);
    }

    /**
     * Procedure initParameters
     * - explode items from uri
     * - check items
     * - set parameters to null
     *
     * @return void
     */
    private function initParameters()
    {
        $this->uri_items = array();
        $this->uri_parameters = array();
        $items = explode('/', $this->uri);
        foreach ($items as $item) {
            // check if item is not an empty string
            if (trim($item) != '') {
                // add the item in the items list
                array_push($this->uri_items, $item);
                // check if it's a paramater
                if ($this->isParameter($item)) {
                    // store the parameter name as a key
                    $name = $this->getParameterName($item);
                    $this->uri_parameters[$name] = null;
                }
            }
        }
    }

    /**
     * Function getParameters
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->uri_parameters;
    }

    /**
     * Function getUri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Function getController
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Function getAction
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Function getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Function getRequirements
     *
     * @return array
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Function match
     *
     * @param string $uri uri
     *
     * @return bool true is uri match route
     */
    public function match($uri)
    {
        // if there is no parameter in this route
        if (!count($this->uri_parameters)) {
            // there is no parameter
            // match the whole uri route
            $match = $uri == $this->uri;
        } else {
            $match = true;
            
            // match each item from uri and store parameters
            
            $uri_items = explode('/', $uri);
            if ($uri_items[0] == '') {
                array_shift($uri_items);
            }

            $count_this_uri_items = count($this->uri_items);
            $count_uri_items = count($uri_items);

            $last_index = $count_this_uri_items - 1;
            $last_item = $this->uri_items[$last_index];
            if ($this->isJocker($last_index)) {

                // last item is a Jocker : matching the n-1 first items
                // the end of route is ignored

                // check count uri items
                if ($count_uri_items >= $count_this_uri_items) {
                    // pop the last parameter
                    array_pop($this->uri_items);
                    // check the n-1 first items
                    foreach ($this->uri_items as $index => $item) {
                        $uri_item = array_shift($uri_items);
                        $match = $this->matchUriItem($uri_item, $index);
                        if (!$match) {
                            break;
                        }
                    }
                    if ($match) {
                        // store the last parameter without matching
                        $name = $this->getParameterName($last_item);
                        $value = implode('/', $uri_items);
                        $match = $this->storeParameter($name, $value);
                    }
                } else {
                    $match = false;
                }
            } else {
                if (count($uri_items) == count($this->uri_items)) {
                    foreach ($uri_items as $index => $uri_item) {
                        $item = $this->uri_items[$index];
                        // check if the uri item is a parameter value
                        $match = $this->matchUriItem($uri_item, $index);
                        if (!$match) {
                            break;
                        }
                    }
                } else {
                    $match = false;
                }
            }
        }

        return $match;
    }

    private function isJocker($index)
    {
        return $this->uri_items[$index] == "{*}";
    }

    /**
     * Function matchUriItem
     *
     * @param string $uri_item item in uri at index position
     * @param int    $index    index
     * @return bool true if uri item at index match route
     */
    public function matchUriItem($uri_item, $index)
    {
        $match = true;
        // check if the uri item is a parameter value
        if ($this->isParameter($this->uri_items[$index])) {
            // get parameter name
            $name = $this->getParameterName($this->uri_items[$index]);
            $value = $uri_item;
            $match = $this->storeParameter($name, $value);
        } else {
            // check if uri item is matching route item
            if ($uri_item != $this->uri_items[$index]) {
                // uri item doesn't match route item
                $match = false;
            }
        }
        return $match;
    }

    /**
     * Function storeParameter
     *
     * @param string $name  parameter name
     * @param mixed  $value parameter value
     * @return true if value match requirements
     */
    public function storeParameter($name, $value)
    {
        $match = true;
        // check if there are specific requirements for the parameter
        if (isset($this->requirements[$name])) {
            // check the requirements
            if (!preg_match($this->requirements[$name], $value)) {
                // doesn't match requirements
                $match = false;
            }
        }
        if ($match) {
            // store parameter value
            $this->uri_parameters[$name] = $value;
        }
        return $match;
    }

    /**
     * Procedure execute
     * call the controller
     *
     * @return void
     */
    public function execute()
    {
        call_user_func_array([$this->controller, $this->action], $this->uri_parameters);
    }

    /**
     * Function __toString
     *
     * @return string
     */
    public function __toString()
    {
        return $this->uri;
    }
}
