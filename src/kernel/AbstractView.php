<?php
namespace GEST_STAGE\kernel;

abstract class AbstractView
{
    protected static $template;
    protected static $parameters = [];

    public static function bindParam($name, $value)
    {
        self::$parameters[$name] = $value;
    }

    public static function getParam($name)
    {
        return self::$parameters[$name];
    }

    public static function setTemplate($template)
    {
        self::$template = $template;
    }
}


