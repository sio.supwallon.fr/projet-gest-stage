<?php
namespace GEST_STAGE\kernel;

use GEST_STAGE\controllers\UtilisateurController;

final class Session
{
    public static function start()
    {
        session_start();
        if(empty($_SESSION))
        {
            self::init();
        }
    }

    private static function init()
    {
        $_SESSION['connected'] = false;
        $_SESSION['user'] = null;
        $_SESSION['groups'] = [];
        $_SESSION['privileges'] = [];
        $_SESSION['modals'] = [];
    }

    public static function login($user, $groups, $privileges)
    {
        $_SESSION['connected'] = true;
        $_SESSION['user'] = $user;
        $_SESSION['groups'] = $groups;
        $_SESSION['privileges'] = $privileges;
    }

    public static function check()
    {
        if(Session::getUser() !== null) {
            $utilisateur = Session::getUser();
            UtilisateurController::session_check($utilisateur->identifiant, $utilisateur->motdepasse);
        }
    }

    public static function logout()
    {
        session_unset();
        session_destroy();
        self::start();
    }

    public static function isConnected()
    {
        return $_SESSION['connected'];
    }

    public static function getUser()
    {
        return $_SESSION['user'];
    }

    public static function getGroups()
    {
        return $_SESSION['groups'];
    }

    public static function getPrivileges()
    {
        return $_SESSION['privileges'];
    }

    public static function addError($message, $code=null)
    {
        $_SESSION['modals']['error'][] = new Error($message, $code);
    }

    public static function getModals()
    {
        return $_SESSION['modals'];
    }

    public static function getModal($name)
    {
        if (isset($_SESSION['modals'][$name])) {
            $result = $_SESSION['modals'][$name];
        } else {
            $result = null;
        }
        return $result;
    }

    public static function setModal($name, $value)
    {
        $_SESSION['modals'][$name] = $value;
    }

    public static function clearModal($name)
    {
        unset($_SESSION['modals'][$name]);
    }

    public static function addModal($name, $value)
    {
        $_SESSION['modals'][$name][] = $value;
    }

    public static function isGranted($privilege)
    {
        return in_array($privilege, $_SESSION['privileges']);
    }

    public static function isAffected($group)
    {
        return in_array($group, $_SESSION['groups']);
    }
}
