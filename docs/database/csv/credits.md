# Credits

Les fichiers ```cities.csv``` , ```departments.csv``` et ```regions.csv``` proviennent de la base de données fournie par : Stanislas Poisson <contact@stanislas-poisson.fr>, sous Licence MIT https://github.com/Stanislas-Poisson/French-zip-code/tree/master/Exports/csv

Les données datent du 05 Juillet 2018 
