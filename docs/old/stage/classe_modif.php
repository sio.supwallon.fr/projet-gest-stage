<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
session_start();
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> MODIFICATIONclasses </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <h1>Modification de la Classe</h1>
        <form action="classe_modif.php?id=<?php echo $id; ?>" method="post">
            <table>
                <?php
                if (isset($_SESSION['type']) && $_SESSION['type'] = 'A') {

                    $select = ' SELECT * FROM classe WHERE classe.id =:id;';
                    $rs = $dbs->prepare($select);
                    $rs->bindParam(':id', $id);
                    $result = $rs->execute();

                    if ($result == true) {
                        $tab = $rs->fetch();
                        while ($tab) {

                            echo '<tr>';
                            echo '<th>Intitulé de la Classe :</th>';
                            echo '<td><input type="text" value="' . $tab['intitule'] . '" name="modif_int"/></td>';

                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Description de la Classe :</th>';
                            echo '<td><textarea name="modif_des" cols="33,8" rows="2">' . $tab['description'] . '</textarea></td>';
                            echo '</tr>';

                            $tab = $rs->fetch();
                        }
                    } else {
                        echo $select;
                        print_r($dbs->errorInfo());
                    }
                } elseif ($_SESSION['type'] != "A") {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous n\'avez pas les droits pour cette manipulation';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
                <tr><td><p><button class="bouton" type="submit" name="enregistrer">enregister les Modifications</button></p></td>
                    <td><p><a href="classes.php">Retour</a></p></td></tr>



            </table>
        </form>
        <?php
        if (isset($_POST['enregistrer'])) {

            $des = $_POST['modif_des'];
            $int = $_POST['modif_int'];
            $req = ' UPDATE `classe` SET intitule=:intitule, description=:des WHERE id=:id;';
            $res = $dbs->prepare($req);
            $res->bindParam(':intitule', $int);
            $res->bindParam(':des', $des);
            $res->bindParam(':id', $id);

            $result = $res->execute();

            if ($result == false) {
                echo 'erreur d\'insertion';
            } else {
                header('location:classes.php');
            }
        }
        ?>

    </body>
</html>
