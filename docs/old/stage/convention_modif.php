<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();


//Si le bouton enregistrer a été utilisé
if (isset($_POST['enregistrer'])) {
    //initialisation des variables
    $req = true;

    //vérification du remplissage des champs de l'élève
    if (empty($_POST['periode1_debut'])) {
        $req = false;
        echo 'le champs début de la 1ère période est vide';
    }

    if (empty($_POST['periode1_fin'])) {
        $req = false;
        echo 'le champs fin de la 1ère période est vide';
    }

    if (empty($_POST['duree_sem'])) {
        $req = false;
        echo 'le champs durée en semaines est vide';
    }

    if (empty($_POST['duree_jour'])) {
        $req = false;
        echo 'le champs durée en jours est vide';
    }

    //insertion des valeurs du stagiaires dans la table stagiaire
    if ($req != false) {
        try {
            //déclaration des variables des infos sur le stagiaire
            $type_conv = $_POST['type_conv'];
            $service_sta = $_POST['service'];
            $lieu_sta = $_POST['lieu'];
            $activites_sta = $_POST['activites'];
            $competences_sta = $_POST['competences'];
            $jour_particulier_sta = $_POST['jours_particuliers'];
            $periode1_deb_sta = $_POST['periode1_debut'];
            $periode1_fin_sta = $_POST['periode1_fin'];
            $periode2_deb_sta = $_POST['periode2_debut'];
            $periode2_fin_sta = $_POST['periode2_fin'];
            $periode3_deb_sta = $_POST['periode3_debut'];
            $periode3_fin_sta = $_POST['periode3_fin'];
            $duree_sem_sta = $_POST['duree_sem'];
            $duree_jour_sta = $_POST['duree_jour'];
            $avantages1_sta = $_POST['avantages1'];
            $avantages2_sta = $_POST['avantages2'];
            $conge_sta = $_POST['conge'];
            $etc_sta = $_POST['etc'];
            $id_sta = $_POST['etudiant_id'];
            $id_ent = $_POST['entreprise'];
            $id_tut = $_POST['tuteur'];
            $id_resp = $_POST['responsable'];
            $id_prof = $_POST['enseignant'];
            $id_classe = $_POST['classe'];

            $req_sta = 'UPDATE convention'
                    . ' SET '
                    . ' `type`=:type_conv,'
                    . ' `service`=:service_sta,'
                    . ' `lieu`=:lieu_sta,'
                    . ' `activites`=:activites_sta,'
                    . ' `competences`=:competences_sta,'
                    . ' `jours_particuliers`=:jours_particuliers_sta,'
                    . ' `periode1_debut`=:periode1_deb_sta,'
                    . ' `periode1_fin`=:periode1_fin_sta,'
                    . ' `periode2_debut`=:periode2_deb_sta,'
                    . ' `periode2_fin`=:periode2_fin_sta,'
                    . ' `periode3_debut`=:periode3_deb_sta,'
                    . ' `periode3_fin`=:periode3_fin_sta,'
                    . ' `duree_semaines`=:duree_semaines_sta,'
                    . ' `duree_jours`=:duree_jours_sta,'
                    . ' `avantage1`=:avantage1_sta,'
                    . ' `avantage2`=:avantage2_sta,'
                    . ' `jours_conge`=:jours_conge_sta,'
                    . ' `nbre_etc`=:nbre_etc_sta,'
                    . ' `id_sta`=:id_sta_sta,'
                    . ' `id_ent`=:id_ent_sta,'
                    . ' `id_tut`=:id_tut_sta,'
                    . ' `id_resp`=:id_resp_sta,'
                    . ' `id_prof`=:id_prof_sta,'
                    . ' `id_classe`=:id_classe_sta,'
                    . ' `validation`=:validation'
                    . '  WHERE `id` = :id;';
            //préparation de la requête
            $sta = $dbs->prepare($req_sta);

            //envoyent des paramètres
            $sta->bindParam(':type_conv', $type_conv);
            $sta->bindParam(':service_sta', $service_sta);
            $sta->bindParam(':lieu_sta', $lieu_sta);
            $sta->bindParam(':activites_sta', $activites_sta);
            $sta->bindParam(':competences_sta', $competences_sta);
            $sta->bindParam(':jours_particuliers_sta', $jour_particulier_sta);
            $sta->bindParam(':periode1_deb_sta', $periode1_deb_sta);
            $sta->bindParam(':periode1_fin_sta', $periode1_fin_sta);
            $sta->bindParam(':periode2_deb_sta', $periode2_deb_sta);
            $sta->bindParam(':periode2_fin_sta', $periode2_fin_sta);
            $sta->bindParam(':periode3_deb_sta', $periode3_deb_sta);
            $sta->bindParam(':periode3_fin_sta', $periode3_deb_sta);
            $sta->bindParam(':duree_semaines_sta', $duree_sem_sta);
            $sta->bindParam(':duree_jours_sta', $duree_jour_sta);
            $sta->bindParam(':avantage1_sta', $avantages1_sta);
            $sta->bindParam(':avantage2_sta', $avantages2_sta);
            $sta->bindParam(':jours_conge_sta', $conge_sta);
            $sta->bindParam(':nbre_etc_sta', $etc_sta);
            $sta->bindParam(':id_sta_sta', $id_sta);
            $sta->bindParam(':id_ent_sta', $id_ent);
            $sta->bindParam(':id_tut_sta', $id_tut);
            $sta->bindParam(':id_resp_sta', $id_resp);
            $sta->bindParam(':id_prof_sta', $id_prof);
            $sta->bindParam(':id_classe_sta', $id_classe);
            $sta->bindParam(':id', $id);
            $sta->bindValue(':validation', 0);
            //execution de la requête		
            $resultat_sta = $sta->execute();

            if ($resultat_sta == true) {
                echo '<p>données enregistrées</p>';
            } else {
                echo '<p>erreur de requête</p>';
//                echo '<pre>';
//                var_dump($resultat_sta);
//                print_r($dbs->errorInfo());
//                var_dump($req_sta);
//                print_r($sta);
//
//
//                var_dump($type_conv);
//                var_dump($service_sta);
//                var_dump($lieu_sta);
//                var_dump($activites_sta);
//                var_dump($competences_sta);
//                var_dump($jour_particulier_sta);
//                var_dump($periode1_deb_sta);
//                var_dump($periode1_fin_sta);
//                var_dump($periode2_deb_sta);
//                var_dump($periode2_fin_sta);
//                var_dump($duree_sem_sta);
//                var_dump($duree_jour_sta);
//                var_dump($avantages1_sta);
//                var_dump($avantages2_sta);
//                var_dump($conge_sta);
//                var_dump($etc_sta);
//                var_dump($id_sta);
//                var_dump($id_ent);
//                var_dump($id_tut);
//                var_dump($id_resp);
//                var_dump($id_prof);
//                var_dump($id_classe);
//
//                var_dump($id);
//                var_dump($id_classe);
//                echo '</pre>';
            }
        } catch (exception $e) {
            echo 'erreur';
        }
    }
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des conventions </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="convention_modif.php?id=' . $id . '" method="post">';
        ?>
        <table>
            <tr>
                <?php
                if (isset($_SESSION['type']) && isset($_GET['id'])) {
                    //faire la requête
                    $req_conv = ('SELECT `type`,`service`,`lieu`,`activites`,`competences`,`jours_particuliers`,`periode1_debut`,`periode1_fin`,`periode2_debut`,`periode2_fin`,`periode3_debut`,`periode3_fin`,`duree_semaines`,`duree_jours`,`avantage1`,`avantage2`,`jours_conge`,`nbre_etc`, `id_sta`, `id_ent`, `id_tut`, `id_resp`, `id_prof`, `id_classe` FROM `convention` WHERE `id` =:id;');
                    $sth_conv = $dbs->prepare($req_conv);
                    $sth_conv->bindParam(':id', $id);
                    $sth_conv->execute();
                    // parcourir ces résultats
                    $tab_r_conv = $sth_conv->fetchAll();
                    // parcourir le tableau avec les résultats
                    foreach ($tab_r_conv as $r_conv) {
                        ?>
                        <th>Classe :</th>
                        <td>
                            <?php
                            //Remplir la liste des classes
                            ?>
                            <select name="classe" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req = ('SELECT `id`,`intitule` FROM `classe`;');
                                //récupérer les résultats de la requête
                                $result = $dbs->query($req);
                                // parcourir ces résultats
                                $tab_r = $result->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si la classe a été choisie
                                    if (isset($_POST['classe']) && $_POST['classe'] != "") {
                                        $classe = $_POST['classe'];
                                        //Si l'id de l'objet en cours est égal à l'id de la classe choisie précédemment
                                        if ($r['id'] == $_POST['classe'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['intitule'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                    }
                                    else {
                                        $classe = $r_conv['id_classe'];
                                        if ($r['id'] == $r_conv['id_classe'])
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['intitule'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Stagiaire :</th>
                        <td>
                            <?php
                            //Récupère et affiche l'étudiant de la convention
                            //faire la requête
                            $req = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id =:id;');
                            $sth = $dbs->prepare($req);
                            $sth->bindParam(':id', $r_conv['id_sta']);
                            $sth->execute();
                            // parcourir ces résultats
                            $tab_r = $sth->fetchAll();
                            // parcourir le tableau avec les résultats
                            foreach ($tab_r as $r) {
                                echo '<input readonly type="hidden" name="etudiant_id" value ="' . $r['id'] . '" required="required"/>';
                                echo '<input readonly type="text" name="etudiant" value ="' . $r['nom_sta'] . ' ' . $r['prenom_sta'] . '" required/>';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Entreprise :</th>
                        <td>
                            <select name="entreprise" onchange="this.form.submit()">
                                <?php
                                echo '<option value="">Faites votre choix</option>';
                                //Récupère et affiche les entreprises
                                //faire la requête
                                $req = ('SELECT `entreprise`.`id`,`nom_ent`,`code_postal`,`nom_ville` FROM `entreprise` INNER JOIN `ville` ON `id_ville` = `ville`.`id`;');
                                //récupérer les résultats de la requête
                                $result = $dbs->query($req);
                                // parcourir ces résultats
                                $tab_r = $result->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si l'entreprise a été choisie
                                    if (isset($_POST['entreprise']) && $_POST['entreprise'] != "") {
                                        $entreprise = $_POST['entreprise'];
                                        //Si l'id de l'objet en cours est égal à l'id de l'entreprise choisie précédemment
                                        if ($r['id'] == $_POST['entreprise'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                    }
                                    else {
                                        $entreprise = $r_conv['id_ent'];
                                        if ($r['id'] == $r_conv['id_ent'])
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Responsable :</th>
                        <td>
                            <select name="responsable" onchange="this.form.submit()">
                                <?php
                                echo '<option value="">Faites votre choix</option>';
                                //Récupère et affiche les responsables
                                //faire la requête
                                $req = ('SELECT `id`,`nom_resp`,`prenom_resp`,`fonction_resp` FROM `responsable` WHERE `id_ent` = :entreprise ;');
                                $sth_r = $dbs->prepare($req);
                                $sth_r->bindParam(':entreprise', $entreprise);
                                $sth_r->execute();
                                // parcourir ces résultats
                                $tab_r = $sth_r->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si le responsable a été choisi
                                    if (isset($_POST['responsable']) && $_POST['responsable'] != "") {
                                        //Si l'id de l'objet en cours est égal à l'id du responsable choisi précédemment
                                        if ($r['id'] == $_POST['responsable'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                    }
                                    else {
                                        if ($r['id'] == $r_conv['id_tut'])
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Tuteur :</th>
                        <td>
                            <select name="tuteur" onchange="this.form.submit()">
                                <?php
                                echo '<option value="">Faites votre choix</option>';
                                //Récupère et affiche les tuteurs
                                //faire la requête
                                $req = ('SELECT `id`,`nom_tut`,`prenom_tut`,`fonction_tut` FROM `tuteur` WHERE `id_entreprise` = :entreprise;');
                                $sth_r = $dbs->prepare($req);
                                $sth_r->bindParam(':entreprise', $entreprise);
                                $sth_r->execute();
                                // parcourir ces résultats
                                $tab_r = $sth_r->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si le tuteur a été choisi
                                    if (isset($_POST['tuteur']) && $_POST['tuteur'] != "") {
                                        //Si l'id de l'objet en cours est égal à l'id du tuteur choisi précédemment
                                        if ($r['id'] == $_POST['tuteur'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                    }
                                    else {
                                        if ($r['id'] == $r_conv['id_tut'])
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Enseignant :</th>
                        <td>
                            <select name="enseignant" onchange="this.form.submit()">
                                <?php
                                echo '<option value="">Faites votre choix</option>';
                                //Récupère et affiche les tuteurs
                                //faire la requête
                                $req = ('SELECT `id`,`nom_prof`,`prenom_prof`,`discipline_prof` FROM `professeur` INNER JOIN `suivre` ON `id` = `id_professeur` WHERE `id_classe` = :classe;');
                                $sth_r = $dbs->prepare($req);
                                $sth_r->bindParam(':classe', $classe);
                                $sth_r->execute();
                                // parcourir ces résultats
                                $tab_r = $sth_r->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si le tuteur a été choisi
                                    if (isset($_POST['enseignant']) && $_POST['enseignant'] != "") {
                                        //Si l'id de l'objet en cours est égal à l'id du tuteur choisi précédemment
                                        if ($r['id'] == $_POST['enseignant'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                    }
                                    else {
                                        if ($r['id'] == $r_conv['id_prof'])
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                    }
                                }
                                ?>	 </select>
                        </td>
                    </tr>
                    <tr>

                        <!--affichage des zones de saisie complémentaires!-->
                        <th>Type :</th>
                        <td>
                            <select name="type_conv" >
                                <?php
                                echo '<option value="">Faites votre choix</option>';
                                if ($r_conv['type'] == "convention") {
                                    echo '<option value="convention" selected>convention</option>';
                                } else {
                                    echo '<option value="convention">convention</option>';
                                }
                                if ($r_conv['type'] == "avenant") {
                                    echo '<option value="avenant" selected>avenant</option>';
                                } else {
                                    echo '<option value="avenant">avenant</option>';
                                }
                                ?>	 
                            </select>
                        </td>

                        <?php
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Service :</th>';
                        echo '<td>';
                        if (isset($_POST['service']))
                            echo '<input type="text" name="service" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['service'] . '" required/>';
                        else
                            echo '<input type="text" name="service" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['service'] . '" required/>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Lieu :</th>';
                        echo '<td>';
                        if (isset($_POST['lieu']))
                            echo '<input type="text" name="lieu" placeholder="Entrer le lieu effectif du travail du stagiaire" value ="' . $_POST['lieu'] . '" required/>';
                        else
                            echo '<input type="text" name="lieu" placeholder="Entrer le lieu effectif du travail du stagiaire" value ="' . $r_conv['lieu'] . '" required/>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Activites :</th>';
                        echo '<td>';
                        if (isset($_POST['activites']))
                            echo '<textarea name="activites" cols="33,8" rows="2" placeholder="Entrer les activités confiées au stagiaire">' . trim($_POST['activites']) . '</textarea>';
                        else
                            echo '<textarea name="activites" cols="33,8" rows="2" placeholder="Entrer les activités confiées au stagiaire">' . trim($r_conv['activites']) . '</textarea>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Compétences :</th>';
                        echo '<td>';
                        if (isset($_POST['competences']))
                            echo '<textarea name="competences" cols="33,8" rows="2" placeholder="Entrer les compétences qui seront validées par le stagiaire durant son stage">' . trim($_POST['competences']) . '</textarea>';
                        else
                            echo '<textarea name="competences" cols="33,8" rows="2" placeholder="Entrer les compétences qui seront validées par le stagiaire durant son stage">' . trim($r_conv['competences']) . '</textarea>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Jours particuliers :</th>';
                        echo '<td>';
                        if (isset($_POST['jours_particuliers']))
                            echo '<textarea name="jours_particuliers" cols="33,8" rows="2" placeholder="Entrer les périodes de nuit ou les week ends ou jours fériés travaillés par le stagiaire">' . trim($_POST['jours_particuliers']) . '</textarea>';
                        else
                            echo '<textarea name="jours_particuliers" cols="33,8" rows="2" placeholder="Entrer les périodes de nuit ou les week ends ou jours fériés travaillés par le stagiaire">' . trim($r_conv['jours_particuliers']) . '</textarea>';
                        echo '</td>';
                        echo '</tr>';
                        //`periode1`,`periode2`,`duree_semaines`,`duree_jours`,`nbre_etc
                        echo '<tr>';
                        echo '<th>1ère période :</th>';
                        echo '<td>';
                        echo 'DU :';
                        if (isset($_POST['periode1_debut']))
                            echo '<input type="date" name="periode1_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode1_debut'] . '" />';
                        else
                            echo '<input type="date" name="periode1_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode1_debut'] . '" />';
                        //                  echo '<input type="date" name="periode1_debut">';
                        echo '</td>';
                        echo '<td>';
                        echo 'AU :';
                        if (isset($_POST['periode1_fin']))
                            echo '<input type="date" name="periode1_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode1_fin'] . '" />';
                        else
                            echo '<input type="date" name="periode1_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode1_fin'] . '" />';
                        //                  echo '<input type="date" name="periode1_fin">';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>2nde période :</th>';
                        echo '<td>';
                        echo 'DU :';
                        if (isset($_POST['periode2_debut']))
                            echo '<input type="date" name="periode2_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode2_debut'] . '" />';
                        else
                            echo '<input type="date" name="periode2_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode2_debut'] . '" />';
                        //                  echo '<input type="date" name="periode2_debut">';
                        echo '</td>';
                        echo '<td>';
                        echo 'AU :';
                        if (isset($_POST['periode2_fin']))
                            echo '<input type="date" name="periode2_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode2_fin'] . '" />';
                        else
                            echo '<input type="date" name="periode2_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode2_fin'] . '" />';
                        //                  echo '<input type="date" name="periode2_fin">';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>3eme période :</th>';
                        echo '<td>';
                        echo 'DU :';
                        if (isset($_POST['periode3_debut']))
                            echo '<input type="date" name="periode3_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode3_debut'] . '" />';
                        else
                            echo '<input type="date" name="periode3_debut" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode3_debut'] . '" />';
                        //                  echo '<input type="date" name="periode2_debut">';
                        echo '</td>';
                        echo '<td>';
                        echo 'AU :';
                        if (isset($_POST['periode3_fin']))
                            echo '<input type="date" name="periode3_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['periode3_fin'] . '" />';
                        else
                            echo '<input type="date" name="periode3_fin" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $r_conv['periode3_fin'] . '" />';
                        //                  echo '<input type="date" name="periode2_fin">';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Durée en semaines :</th>';
                        echo '<td>';
                        if (isset($_POST['duree_sem']))
                            echo '<input type="text" name="duree_sem" placeholder="Entrer le nombre de semaines du stage" value ="' . $_POST['duree_sem'] . '" required/>';
                        else
                            echo '<input type="text" name="duree_sem" placeholder="Entrer le nombre de semaines du stage" value ="' . $r_conv['duree_semaines'] . '" required/>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Durée en jours :</th>';
                        echo '<td>';
                        if (isset($_POST['duree_jour']))
                            echo '<input type="text" name="duree_jour" placeholder="Entrer le nombre de jours ouvrables du stage" value ="' . $_POST['duree_jour'] . '" required/>';
                        else
                            echo '<input type="text" name="duree_jour" placeholder="Entrer le nombre de jours ouvrables du stage" value ="' . $r_conv['duree_jours'] . '" required/>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Avantages :</th>';
                        echo '<td>';
                        if (isset($_POST['avantages1']))
                            echo '<textarea name="avantages1" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . trim($_POST['avantages1']) . '</textarea>';
                        else
                            echo '<textarea name="avantages1" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . trim($r_conv['avantage1']) . '</textarea>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Autres avantages :</th>';
                        echo '<td>';
                        if (isset($_POST['avantages2']))
                            echo '<textarea name="avantages2" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . trim($_POST['avantages2']) . '</textarea>';
                        else
                            echo '<textarea name="avantages2" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . trim($r_conv['avantage2']) . '</textarea>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Jours de congé :</th>';
                        echo '<td>';
                        if (isset($_POST['conge']))
                            echo '<input type="number" name="conge" placeholder="Entrer le nombre de jours de congé exceptionnels accordés au stagiaire" value ="' . $_POST['conge'] . '" />';
                        else
                            echo '<input type="number" name="conge" placeholder="Entrer le nombre de jours de congé exceptionnels accordés au stagiaire" value ="' . $r_conv['jours_conge'] . '" />';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<th>Nombre d\'ECTS :</th>';
                        echo '<td>';
                        if (isset($_POST['etc']))
                            echo '<input type="number" name="etc" placeholder="Entrer le nombre d\'ECTS obtenus par le stagiaire" value ="' . $_POST['etc'] . '" />';
                        else
                            echo '<input type="number" name="etc" placeholder="Entrer le nombre d\'ECTSobtenus par le stagiaire" value ="' . $r_conv['nbre_etc'] . '" />';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>';
                        echo '</td>';
                        echo '<td>';
                        echo '<a href="etudiants.php?action=liste&id=' . $id_classe . '">retour</a>';
                    }
                }
                else {
                    if (!isset($_SESSION['type'])) {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez vous connecter';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="connexion.php">connexion</a>';
                    } else {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez fournir la convention souhaitée';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="etudiants.php">Retour</a>';
                    }
                }
                ?>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
