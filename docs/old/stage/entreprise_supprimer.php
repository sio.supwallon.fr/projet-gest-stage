<!DOCTYPE html>
<?php
require 'scripts/constante.php';
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);

session_start();
//ouverture de la connexion
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer une entreprise</title>
    </head>
    <body>

        <form action="entreprise_supprimer.php?id_ville=<?php echo $id_ville; ?>&amp;id=<?php echo $id; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P" )) {
                echo '<p>êtes vous sûr de vouloir suprimer l\'entreprise ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';

                if (isset($_GET['id']) && isset($_POST['btn_oui'])) {
                    //faire la requête de suppression
                    $req_ent = ' DELETE FROM `entreprise` WHERE entreprise.id=:id ;';

                    //préparation de la requête
                    $ent = $dbs->prepare($req_ent);
                    $ent->bindParam(':id', $id);
                    //execution de la requête
                    $resultat_sta = $ent->execute();
                    if ($resultat_sta == true) {
                        header("location:entreprises.php?action=liste&id_ville=".$id_ville);
                    } else {
                        print_r($dbs->errorInfo());
                    }
                } elseif (isset($_GET['id']) && isset($_POST['btn_non'])) {
                    header("location:entreprises.php?action=liste&id_ville=".$id_ville);
                } elseif (!isset($_GET['id'])) {
                    echo'<p>veuillez sélectionner une entreprise</p>';
                }
            } else {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            }
            ?>



        </form>
    </body>
</html>
