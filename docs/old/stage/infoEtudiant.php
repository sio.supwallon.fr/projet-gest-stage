<!DOCTYPE html>
<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
$id_sta = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
?>
<html>
    <head>
        <title> INFOetudiant </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>

        <form action="etudiants.php" method="post">
            <h1>information sur l'etudiant</h1>
            <?php
            //ouverture de la connexion
            ?>
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {
                        $req = ' SELECT nom_sta, prenom_sta, sexe_sta, num_ss_sta, date_naissance_sta, lieu_naissance_sta, adresse_sta, telephone_sta, email_sta, caisse_sta, nom_ville FROM stagiaire'
                                . ' INNER JOIN ville ON id_ville=ville.id'
                                . ' WHERE stagiaire.id = :id;';
                        
                        $sth = $dbs->prepare($req);
                        $sth->bindParam(':id', $id_sta);
                        $sth->execute();
                        $tab = $sth->fetch();
                        echo '<table>';
                        while ($tab != null) {
                            echo '<tr>';
                            echo '<th><p>Nom :</p></th>';
                            echo '<td>' . $tab['nom_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Prenom :</p></th>';
                            echo '<td>' . $tab['prenom_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Sexe :</p></th>';
                            if ($tab['sexe_sta'] = 'M') {
                                echo '<td>Masculin</td><th></th><td></td>';
                            } else {
                                echo '<td>Feminin</td><th></th><td></td>';
                            }
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Né(e) le :</p></th>';
                            $date = $tab['date_naissance_sta'];
                            $tab_date = explode('-', $date);
                            $date_naiss = $tab_date[2].'/'.$tab_date[1].'/'.$tab_date[0];
                            echo '<td>' . $date_naiss . '</td>';
                            echo '<th><p>à :</p></th>';
                            echo '<td>' . $tab['lieu_naissance_sta'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Vie à :</p></th>';
                            echo '<td>' . $tab['nom_ville'] . '</td>';
                            echo '<th><p>à l\'adresse :</p></th>';
                            echo '<td>' . $tab['adresse_sta'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Adresse email :</p></th>';
                            echo '<td>' . $tab['email_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Telephone :</p></th>';
                            echo '<td>' . $tab['telephone_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Numéro de Sécurité Sociale :</p></th>';
                            echo '<td>' . $tab['num_ss_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Caisse d\'assurance maladie :</p></th>';
                            echo '<td>' . $tab['caisse_sta'] . '</td><th></th><td></td>';
                            echo '</tr>';
                             $tab = $sth->fetch();
                        }
                        ?>
                <p><a href="etudiants.php?action=liste&amp;id=<?php echo $id_classe; ?>">retour</a></p>
                    <?php
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
            </table>

        </form>
    </body>
</html>
