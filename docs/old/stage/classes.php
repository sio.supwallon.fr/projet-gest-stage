<!DOCTYPE html>
<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
session_start();

?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des classes </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="classe_modif.php" method="GET">
            <table>
                <?php
                if (isset($_SESSION['type']) && $_SESSION['type'] = 'A') {
                    ?>
                    <tr>
                        <th><p>Classes :</p></th>
                        <td><a href="classe_ajout.php"><p>Ajouter</p><p><p>une classe</p></a></td>
                    </tr>
                    <?php
                    $select = ' SELECT * FROM classe ORDER BY intitule;';
                    $res = $dbs->query($select);
                    $tab = $res->fetchAll();
                    foreach ($tab as $s) {
                        echo '<tr>';
                        echo '<td >';
                        echo '<p>' . $s['intitule'] . '</p>';
                        echo '<p>' . $s['description'] . '</p>';
                        echo '</td>';
                        echo '<td>';
                        //faire un tableau dans un tableau
                        echo '<table>';
                        echo '<tr>';
                        echo '<td>';
                        //envoie l'id dans l'url pour identifier la suppression
                        echo '<p><a href=classe_supprimer.php?id=' . $s['id'] . '>Supprimer</a></p>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        //envoie l'id et l'intitule dans l'url de classe_modif pour pourvoir reconnaitre la classe
                        echo '<p><a href=classe_modif.php?id=' . $s['id'] . '>Modifier</a></p>';
                        echo '</td>';
                        echo '</tr>';
                        echo '</table>';
                        echo '</td>';
                        echo '</tr>';
                    }
                    ?><p><a href="index.php">retour</a></p><?php
                } elseif ($_SESSION['type'] != "A") {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous n\'avez pas les droits pour cette manipulation';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
            </table>
        </form>
    </body>
</html>



