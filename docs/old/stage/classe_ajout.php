<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
session_start();
if (isset($_POST['enregistrer'])) {
    $insert = ' INSERT INTO classe(intitule, description) VALUES (:int,:des);';
    $res = $dbs->prepare($insert);
    $res->bindParam(':int', $_POST['add_intitule']);
    $res->bindParam(':des', $_POST['add_des']);
    $resultat = $res->execute();
    header('location:classes.php');
    /* echo '<pre>';
      var_dump($resultat);
      print_r($dbs->errorInfo());
      var_dump($insert);

      echo '</pre>';
     */
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> AJOUTclasses </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="classe_ajout.php" method="post">
            <h1>Ajouter une Classe</h1>
            <table>
                <?php
                if (isset($_SESSION['type']) && $_SESSION['type'] = 'A') {
                    ?>


                    <tr>
                        <th>intitulé :</th>
                        <td><input type='text' name="add_intitule" required="required"></td>
                    </tr>
                    <tr>
                        <th>description :</th>
                        <td><textarea type='text' name="add_des"></textarea></td>
                    </tr>
                    <tr>
                        <td><p><button type="submit" name="enregistrer">Ajouter</button></p></td>
                        <td><p><a href="classes.php">Retour</a></p></td>
                    </tr>

                    <?php
                } elseif ($_SESSION['type'] != 'A') {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous n\'avez pas les droits d\'ajouter une classe';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
            </table>
        </form>



    </body>
</html>
