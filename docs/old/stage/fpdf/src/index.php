<?php

require "../vendor/autoload.php";

define("TEMPLATES_DIRECTORY", "templates/");
define("TUTORIALS_DIRECTORY", "tutorials/");
define("IMAGES_DIRECTORY", "images/");

if(isset($_GET['page']))
{
    $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
}
else
{
    $page = 0;
}

switch($page)
{
    case 0:
        $template = "home.html";
        break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
        $template = "tutorial.html";
        break;
    default:
        $template = "unknown.html";
        break;
}

include TEMPLATES_DIRECTORY . $template ;