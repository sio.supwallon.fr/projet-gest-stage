<?php

require "../vendor/autoload.php";

$info['titre'] = "Bienvenue";
$info['nom'] = "RIEHL";
$info['prenom'] = "David";



$pdf = new FPDF();

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);

$pdf->Cell(40,10,$info['titre']);
$pdf->Ln();

$pdf->SetFont('Arial','B',11);

$pdf->Cell(20,5,utf8_decode("Nom : "));
$pdf->Cell(40,5,utf8_decode($info['nom']));
$pdf->Ln();

$pdf->Cell(20,5,utf8_decode("Prénom : "));
$pdf->Cell(40,5,utf8_decode($info['prenom']));
$pdf->Ln();

//print_r($pdf);

$pdf->Output();
