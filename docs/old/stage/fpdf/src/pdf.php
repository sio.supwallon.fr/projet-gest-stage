<?php

define("TUTORIALS_DIRECTORY", "tutorials/");
define("IMAGES_DIRECTORY", "images/");
define("FILES_DIRECTORY", "files/");

if(isset($_GET['id']))
{
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
    $file = "tutorial" . $id . ".php";
    include TUTORIALS_DIRECTORY . $file ;
}