<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();



if (isset($_POST['enregistrer'])) {

    if (isset($_POST['mdp_ad'])) {
        $mdp = filter_input(INPUT_POST, 'mdp_ad', FILTER_SANITIZE_STRING);
        $mdp = md5($mdp);
    }
    $nom = filter_input(INPUT_POST, 'nom_ad', FILTER_SANITIZE_STRING);
    $login = filter_input(INPUT_POST, 'login_ad', FILTER_SANITIZE_STRING);

    $req = ' UPDATE `administratif` SET login_ad=:login, nom_ad=:nom ';
    if (isset($_POST['mdp_ad'])) {
        $req = $req . ',mdp_ad=:mdp;';
    } else {
        $req = $req . ';';
    }

    $res = $dbs->prepare($req);
    $res->bindParam(':login', $login);
    $res->bindParam(':nom', $nom);
    if (isset($_POST['mdp_ad'])) {
        $res->bindParam(':mdp', $mdp);
    }

    $result = $res->execute();
    if ($result == true) {
        echo '<p>données enregistrées</p>';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion de l'administrateur </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>

        <form action="administrateur.php" method="post">

            <?php
            if (isset($_SESSION['type']) && $_SESSION['type'] == "A") {

                $req_ad = ' SELECT nom_ad AS nom, login_ad AS login FROM administratif;';
                $res = $dbs->query($req_ad);

                $result = $res->fetch();
                ?>
                <h1>Information sur l'administrateur</h1>
                <table>
                    <tr>
                        <th>Nom :</th>
                        <td><input type="text" name="nom_ad" value="<?php echo $result['nom']; ?>"></td>
                    </tr>
                    <tr>

                        <th>Login :</th>
                        <td><input type="text" name="login_ad" value="<?php echo $result['login']; ?>"></td>
                    </tr>
                    <tr>
                        <?php
                        if (isset($_POST['enregistrer'])) {
                            echo '<th>Mot de passe :</th>';
                            echo '<td><input type="password" name="mdp_ad" value="' . $_POST['mdp_ad'] . '"></td>';
                        } else {
                            ?>
                            <th>Mot de passe :</th>
                            <td><input type="password" name="mdp_ad" value=""></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td><input type="submit" name="enregistrer" value="enregistrer"></td>
                        <td><a href="index.php">retour</a></td>
                    </tr>

                </table>
                <?php
            } else {
                echo '<p>il faut d\'abord se connecter</p>';
                echo '<p><a href="connexion.php>connexion</a></p>';
            }
            ?>
        </form>
    </body>
</html>


