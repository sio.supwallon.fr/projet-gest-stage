<!DOCTYPE html>
<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
$id_conv = filter_input(INPUT_GET, 'id_conv', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> validation des conventions </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />


</html>
</head>
<body>
    <form action="validation.php?id_classe=<?php echo $id_classe; ?>&amp;id_conv=<?php echo $id_conv; ?>" method="post">
        <table>
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P" )) {
                echo '<p>êtes vous sûr de vouloir valider la convention ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
            } else {
                echo'<p>vous devez vous connecter avec un compte abilité</p>';
                echo '<p><a href="connexion.php">connexion</a></p>';
            }
            ?>
        </table>
    </form>
    <?php
    if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P" ) && isset($_POST['btn_oui'])) {
//faire la requête de modification
        $req_conv = ('UPDATE `convention` SET `validation`=1 WHERE `id` =:id;');

//préparation de la requête
        $sth = $dbs->prepare($req_conv);
        $sth->bindParam(':id', $id_conv);

//execution de la requête	
        $result_conv = $sth->execute();


        if ($result_conv == true) {
            header('location:etudiants.php?action=liste&id=' . $id_classe);
        } else {
            print_r($dbs);
        }
    } elseif (isset($_SESSION['type']) && ($_SESSION['type'] == "A") && isset($_POST['btn_non'])) {
        header('location:etudiants.php?action=liste&id=' . $id_classe);
    }
    ?>
</body>
</html>
