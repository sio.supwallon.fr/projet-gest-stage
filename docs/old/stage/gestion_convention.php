<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

session_start();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des convention </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <table>
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
                ?>
                <tr>
                    <td>
                        <a href="list_conv_valide.php">liste des conventions validées</a>
                    </td>
                    <td>
                        <a href="conventions.php">éditer une conventions</a>
                    </td>
                </tr>
                <?php
            } else {
                ?>
                <tr>
                    <td>
                        <p>vous devez vous connecter avec compte abilité</p>
                    </td>
                    <td>
                        <a href="connexion.php">connexion</a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <p><img width="30%" src="image/logowallon.jpg" alt="Image non chargée"/></p>
        <p><a href="index.php">retour</a></p>
        <?php
        // put your code here
        ?>
    </body>
</html>
