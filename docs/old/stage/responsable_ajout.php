<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();


$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($_POST['enregistrer'])) {
    //initialisation des variables
    $req2 = true;

    //vérification du remplissage des champs du reponsable
    if (empty($_POST['nom_resp'])) {
        $req2 = false;
        echo 'le champs nom du responsable est vide';
    }
    if (empty($_POST['telephone_resp'])) {
        $req2 = false;
        echo 'le champs telephone du responsable est vide';
    }
    if (empty($_POST['email_resp'])) {
        $req2 = false;
        echo 'le champs email du responsable est vide';
    }



    if ($req2 != false) {
        //déclaration des variables des infos sur le responsable'
        $nom_resp = filter_input(INPUT_POST, 'nom_resp', FILTER_SANITIZE_SPECIAL_CHARS);
        $nom_resp = strtoupper($nom_resp);
        $prenom_resp = filter_input(INPUT_POST, 'prenom_resp', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom_resp = strtolower($prenom_resp);
        $prenom_resp = ucwords($prenom_resp);
        $pos = strpos($prenom_resp, "-");
        if($pos !== false)
        {
          $prenom_resp = substr($prenom_resp,0,$pos+1) . strtoupper(substr($prenom_resp,$pos+1,1)) . substr($prenom_resp,$pos+2);
        }
        $fonction_resp = filter_input(INPUT_POST, 'fonction_resp', FILTER_SANITIZE_SPECIAL_CHARS);
        $telephone_resp = filter_input(INPUT_POST, 'telephone_resp', FILTER_SANITIZE_SPECIAL_CHARS);
        $email_resp = filter_input(INPUT_POST, 'email_resp', FILTER_SANITIZE_SPECIAL_CHARS);


        $req_resp = 'INSERT INTO `responsable` (`nom_resp`,`prenom_resp`,`fonction_resp`, telephone_resp, email_resp, id_ent) Values (:nom_resp, :prenom_resp, :fonction_resp, :tel_resp, :email_resp, :id_ent);';

        //préparation de la requête
        $resp = $dbs->prepare($req_resp);

        //envoyent des paramètres
        $resp->bindParam(':nom_resp', $nom_resp);
        $resp->bindParam(':prenom_resp', $prenom_resp);
        $resp->bindParam(':fonction_resp', $fonction_resp);

        $resp->bindParam(':tel_resp', $telephone_resp);
        $resp->bindParam(':email_resp', $email_resp);
        $resp->bindParam(':id_ent', $id_ent);

        //execution de la requête		
        $resultat_resp = $resp->execute();

        if ($resultat_resp == true) {
            echo '<p>les données sont enregistrées</p>';
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> Ajout d'un responsable </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />

    </head>
    <body>
        <h1>Ajout d'un responsable</h1>
        <?php
        echo '<form action="responsable_ajout.php?id_ville=' . $id_ville . '&amp;id_ent=' . $id_ent . '" method="post">';
        ?>
        <table>
            <?php
            if (isset($_SESSION['type'])) {
                ?>

                <tr>
                    <th>Nom :</th>
                    <?php
                    echo '<td><input type="text" name="nom_resp" placeholder="Nom responsable" value ="" required/></td>';
                    ?>
                </tr>
                <tr>
                    <th>Prenom :</th>
                    <?php
                    echo '<td><input type="text" name="prenom_resp" placeholder="prenom responsable" value ="" required/></td>';
                    echo "</tr>";
                    ?>
                <tr>
                <tr>
                    <th>fonction :</th>
                    <?php
                    echo '<td><input type="text" name="fonction_resp" placeholder="fonction du responsable" value ="" required/></td>';
                    echo "</tr>";
                    ?>
                <tr>
                    <th>N°téléphone du responsable :</th>
                    <?php echo'<td><input type="tel"  size="20" minlength="10" maxlength="10" name="telephone_resp" placeholder="N° Téléphone" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" required/></td>'; ?>
                </tr>
                <tr>
                    <th>Adresse email du responsable :</th>
                    <?php echo'<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_resp" placeholder="Adresse email" required/></td>'; ?>
                </tr>


                <tr>
                    <td>
                        <button class="bouton" type="submit" name="enregistrer">Enregistrer le responsable</button>
                    </td>
                    <td>
                        <a href="responsable.php?action=liste&amp;id=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>">retour</a>
                    </td>
                </tr>
                <?php
            } elseif (!isset($_SESSION['type'])) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            } elseif (!isset($_GET['id_ent']) && (isset($_SESSION['type']))) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez choisir une entreprise';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="entreprises.php">retour</a>';
                echo '</td>';
            }
            ?>		
        </table>
    </form>

</body>
</html>

