<!DOCTYPE html>
<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();
//ouverture de la connexion
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
$dep_ent = filter_input(INPUT_POST, 'dep_ent', FILTER_SANITIZE_SPECIAL_CHARS);

switch ($action) {
    case 'liste':
        $req = ' SELECT departement FROM ville WHERE id=:id_ville ;';
        $res = $dbs->prepare($req);
        $res->bindParam(':id_ville', $id_ville);
        $res->execute();
        $tab_r = $res->fetch();
        while ($tab_r != null) {
            $_POST['dep_ent'] = $tab_r['departement'];
            $_POST['ville_ent'] = $id_ville;
            $dep_ent = $tab_r['departement'];
            $tab_r = $res->fetch();
        }
        break;
    case null:
        break;

    default :
        echo 'erreur';
        break;
}
?>
<html>
    <head>
        <title> entreprises </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>

        <form action="entreprises.php" method="post">
            <h1>Gestion des entreprises</h1>

            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {
                        ?>
                    <tr>



                    <p><a href="index.php">retour</a></p><th>Département :</th>
                    <td>
                        <!--liste déroulante du département -->
                        <select name = "dep_ent" onchange = "this.form.submit()">
                            <option>Choisir</option> 
                            <?php
                            //faire la requête
                            $req = ('SELECT DISTINCT `departement` FROM `ville`;');
                            //récupérer les résultats de la requête
                            $result = $dbs->query($req);
                            // parcourir ces résultats
                            $tab_r = $result->fetchAll();
                            // parcourir le tableau avec les résultats

                            foreach ($tab_r as $r) {

                                if ($r['departement'] == $dep_ent) {

                                    echo '<option value="' . $r['departement'] . '"  selected>' . $r['departement'] . '</option>';
                                    $dep = $r['departement'];
                                } else {
                                    echo '<option value="' . $r['departement'] . '">' . $r['departement'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                    </tr>
            </form>



            <form action="entreprises.php" method="post">
                <?php
                if (isset($_POST['dep_ent']) && $dep_ent != "Choisir") {
                    ?>
                    <tr>
                        <th>Ville :</th>
                        <td>
                            <input type="hidden" name="dep_ent" value="<?= $dep_ent; ?>" />
                            <!--liste déroulante de la ville-->
                            <select name="ville_ent" onchange="this.form.submit()">  
                                <option>Choisir</option>
                                <?php
                                //faire la requête
                                $req = ('SELECT `id`,`code_postal`,`nom_ville` FROM ville WHERE departement = :dep_ent ORDER BY `nom_ville`;');
                                //récupérer les résultats de la requête
                                $sth = $dbs->prepare($req);
                                $sth->bindParam(':dep_ent', $dep_ent);
                                $sth->execute();
                                // parcourir ces résultats
                                $tab_r = $sth->fetchAll();
                                // parcourir le tableau avec les résultats

                                foreach ($tab_r as $r) {

                                    if (isset($_POST['ville_ent']) && $r['id'] == $_POST['ville_ent'])
                                        echo '<option value="' . $r['id'] . '" selected>' . $r['nom_ville'] . '-' . $r['code_postal'] . '</option>';
                                    else
                                        echo '<option value="' . $r['id'] . '" >' . $r['nom_ville'] . '-' . $r['code_postal'] . '</option>';
                                }
                                ?>
                            </select>

                        </td>
                    </tr>
                </table>
                <table>

                    <?php
                    if (isset($_POST['ville_ent']) && $_POST['ville_ent'] != "Choisir") {
                        ?>

                        <tr>
                            <th colspan="2">les entreprises</th> 
                        <tr>
                            <?php
                            $idville = $_POST['ville_ent'];
                            //faire la requête
                            $req_ent = ('SELECT `id`,`nom_ent`, `adresse_ent`, `id_ville` FROM `entreprise` WHERE `id_ville` =:idville ORDER BY `nom_ent`;');
                            //récupérer les résultats de la requête
                            $res = $dbs->prepare($req_ent);
                            $res->bindValue(':idville', $idville);
                            $res->execute();
                            // parcourir ces résultats
                            $tab_r = $res->fetchAll();
                            // parcourir le tableau avec les résultats
                            foreach ($tab_r as $r) {
                                $nom = $r['nom_ent'];
                                $adresse = $r['adresse_ent'];
                                $ville = $r['id_ville'];

                                echo '<tr>';
                                echo '<td colspan = "2">';
                                echo '<p>Raison sociale : <a href="infoEntreprise.php?id=' . $r['id'] . '&amp;id_ville=' . $r['id_ville'] . '">' . $nom . ' ' . $adresse . '</a></p>';
                                echo '<tr>';
                                echo '<td>';
                                echo '<p><a href = "tuteur.php?id_ville=' . $ville . '&amp;id=' . $r['id'] . '">voir les tuteurs de l\'entreprise</a><p>';
                                echo '</td>';
                                echo '<td>';
                                echo '<p><a href = "responsable.php?id_ville=' . $ville . '&amp;id=' . $r['id'] . '">voir le responsable de l\'entreprise</a><p>';
                                echo '</td>';
                                echo '</tr>';
                                echo '</td>';


                                if ($_SESSION['type'] != 'S') {
                                    echo '<td>';
                                    echo '<a href="entreprise_supprimer.php?id=' . $r['id'] . '&amp;id_ville=' . $r['id_ville'] . '">suppression de l\'entreprise</a>';
                                    echo '</td>';
                                }

                                if ($_SESSION['type'] != 'S') {
                                    echo '<td>';
                                    echo '<a href="entreprise_modif.php?id=' . $r['id'] . '&amp;id_ville=' . $r['id_ville'] . '">modification de l\'entreprise</a>';
                                    echo '</td>';
                                }
                                echo '</tr>';
                            }
                            ?>
                    <tr>
                        <?php
                        if (!isset($nom) && isset($_POST['ville_ent'])) {
                            echo "<p>Il n'y a pas d'entreprise enregistré dans cette ville. Veuillez l'";
                            echo '<a href="entreprise_ajout.php?id_ville=' . $_POST['ville_ent'] . '">Ajouter</a></p>';
                        } elseif (isset($_POST['ville_ent'])) {
                            echo '<p>Pour Ajouter une Entreprise à la ville cliquez ';
                            echo '<a href="entreprise_ajout.php?id_ville=' . $_POST['ville_ent'] . '">ICI</a>.</p>';
                        }
                        ?>
                    </tr>
                            <?php
                        }
                        ?>

                    
                    <?php
                }
                ?>
            </table>

            <?php
        } else {
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
        }
        ?>


    </form>
</body>
</html>
