<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_tut = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
if (isset($_GET['id'])) {
    //Récupère l'id de la classe sélectionnée dans la page "etudiants.php"
    $num = $id_tut;

    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {
        $nom_tut = filter_input(INPUT_POST, 'nom_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $nom_tut = strtoupper($nom_tut);
        $prenom_tut = filter_input(INPUT_POST, 'prenom_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom_tut = strtolower($prenom_tut);
        $prenom_tut = ucwords($prenom_tut);
        $pos = strpos($prenom_tut, "-");
        if($pos !== false)
        {
          $prenom_tut = substr($prenom_tut,0,$pos+1) . strtoupper(substr($prenom_tut,$pos+1,1)) . substr($prenom_tut,$pos+2);
        }
        $fonction_tut = filter_input(INPUT_POST, 'fonction_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $telephone_tut = filter_input(INPUT_POST, 'telephone_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $email_tut = filter_input(INPUT_POST, 'email_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $email_tut = strtolower($email_tut);
        $ent_tut = filter_input(INPUT_POST, 'entreprise_tut', FILTER_SANITIZE_SPECIAL_CHARS);

        //faire la requête de modification
        $req_tut = ('UPDATE tuteur SET nom_tut= :nom_tut, prenom_tut=:prenom_tut,fonction_tut=:fonction_tut, telephone_tut=:telephone_tut,email_tut=:email_tut,id_entreprise=:id_ent WHERE id =:id;'); //Récupère le tuteur choisi précédemment
        //préparation de la requête
        $tut = $dbs->prepare($req_tut);
        //récupère l'adresse sans les espaces               
        //envoyent des paramètres
        $tut->bindParam(':nom_tut', $nom_tut);
        $tut->bindParam(':prenom_tut', $prenom_tut);
        $tut->bindParam(':fonction_tut', $fonction_tut);
        $tut->bindParam(':telephone_tut', $telephone_tut);
        $tut->bindParam(':email_tut', $email_tut);
        $tut->bindParam(':id_ent', $ent_tut);
        $tut->bindParam(':id', $id_tut);

        //execution de la requête		
        $resultat_tut = $tut->execute();
        if ($resultat_tut == true) {
            echo '<p>données enregistrées</p>';
        } else {
            print_r($dbs->errorInfo()[2]);
        }
    }
} else
    $num = 0;
?>





<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Modification d'un tuteur </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="tuteur_modif.php?action=liste&amp;id_ent=' . $id_ent . '&amp;id_ville=' . $id_ville . '&amp;id=' . $num . '" method="post">';
        ?>
        <table>

            <?php
            if ((isset($_SESSION['type'])) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {

                //faire la requête 
                $req = ('SELECT `nom_tut`,`prenom_tut`,`fonction_tut` ,`telephone_tut`,`email_tut`,`nom_ent` FROM tuteur INNER JOIN entreprise ON entreprise.id = tuteur.id_entreprise WHERE tuteur.id=:id;');
                //récupérer les résultats de la requête

                $sth = $dbs->prepare($req);
                $sth->bindParam(':id', $num);
                $res = $sth->execute();
                if ($res != True) {
                    echo '<pre>';
                    var_dump($res);
                    print_r($dbs->errorInfo());
                    var_dump($req);
                    print_r($res);
                    var_dump($num);
                    echo '</pre>';
                } else {
                    // parcourir ces résultats
                    $tab_r = $sth->fetch();
                    // parcourir le tableau avec les résultats
                    while ($tab_r != null) {
                        //Affiche les informations de l'étudiant récupérées
                        ?>

                        <tr>
                            <th>Nom :</th>
                            <?php
                            echo '<td><input type="text" name="nom_tut" value ="' . $tab_r['nom_tut'] . '" ></td>';
                            ?>
                        </tr>

                        <tr>
                            <th>Prenom :</th>

                            <td><input type="text" name="prenom_tut" value ="<?php echo $tab_r['prenom_tut']; ?> " ></td>


                        </tr>
                        <tr>
                            <th>fonction :</th>
                            <td><input type="text" name="fonction_tut" value ="<?php echo $tab_r['fonction_tut']; ?> " ></td>
                        </tr>
                        <tr>
                            <th>Téléphone :</th>
                            <?php
                            echo '<td><input type="tel" name="telephone_tut" size="20" minlength="10" maxlength="10"  value ="' . $tab_r['telephone_tut'] . '">';
                            ?>
                            </td>

                        </tr>
                        <tr>
                            <th>Email :</th>
                            <?php
                            echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_tut" placeholder="pauldurand@gmail.com"  value ="' . $tab_r['email_tut'] . '"></td>';
                            ?>

                        </tr>
                        <tr>
                            <th>entreprise :</th>
                            <td>
                                <select name="entreprise_tut">
                                    <option value=''>Faites votre choix</option>
                                    <?php
                                    //faire la requête
                                    $req_v = ('SELECT * FROM `entreprise` ORDER BY `nom_ent`;');
                                    //récupérer les résultats de la requête
                                    $result_v = $dbs->query($req_v);
                                    // parcourir ces résultats
                                    $tab_r_v = $result_v->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    //Rempli la liste des villes en sélectionnant celle de l'étudiant récupéré
                                    foreach ($tab_r_v as $r_v) {
                                        if ($r_v['id'] == $id_ent)
                                            echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ent'] . '</option>';
                                        else
                                            echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ent'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        $tab_r = $sth->fetch();
                    }
                    ?>
                    <tr>
                        <td>
                            <button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>
                        </td>
                        <td>
                            <a href="tuteur.php?action=liste&amp;id=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>">retour</a>
                        </td>
                        <?php
                    }
                }elseif (($_SESSION['type'] != "A" || $_SESSION['type'] != "P")) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter avec un compte abilité';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } elseif (!isset($_SESSION['type'])) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez choisir une entreprise';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="entreprises.php">Retour</a>';
                    echo '</td>';
                }
                ?>
            </tr>
        </table>

    </form>
</body>
</html>