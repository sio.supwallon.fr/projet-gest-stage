<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();
//Récupère l'id de la classe choisie dans la page précédente
$num_prof = filter_input(INPUT_GET, 'id_prof', FILTER_SANITIZE_SPECIAL_CHARS);
if (isset($_POST['enregistrer'])) {
    //vérification du choix de la classe
    if (empty($_POST['classe_professeur'])) {
        $req1 = false;
        echo 'la classe doit être sélectionnée';
    } else {
        $req_sui = 'INSERT INTO `suivre` (`id_professeur`, `id_classe`) Values (:id_prof, :id_cla);';

        //préparation de la requête
        $sui = $dbs->prepare($req_sui);
        $id_classe = filter_input(INPUT_POST, 'classe_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        //envoyent des paramètres
        $sui->bindParam(':id_prof', $num_prof);
        $sui->bindParam(':id_cla', $id_classe);

        //execution de la requête		
        $resultat_sui = $sui->execute();
    }
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Ajout d'une classe pour l'enseignant </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="professeur_ajout_classe.php?id_prof=' . $num_prof . '" method="post">';
//http://localhost/Stage_Jeremy3/
        echo '<table>';
        if (isset($_SESSION['type']) && $_SESSION['type'] == "A" && isset($_GET['id_prof'])) {
            //faire la requête
            $req_c = ('SELECT `nom_prof`, `prenom_prof` FROM `professeur` WHERE `id` = :id;');
            //récupérer les résultats de la requête
            $sth = $dbs->prepare($req_c);
            $sth->bindParam(':id', $num_prof);
            $sth->execute();
            // parcourir ces résultats
            $tab_r_c = $sth->fetchAll();
            // parcourir le tableau avec les résultats
            foreach ($tab_r_c as $r_c) {
                //Affiche les informations de l'enseignant récupérées
                echo '<tr>';
                echo '<th>enseignant :</th>';
                echo '<td>';
                echo $r_c['nom_prof'] . " " . $r_c['prenom_prof'];
                echo '</td>';
                echo '</tr>';
            }
            echo '<tr>';
            echo '<th>Classe :</th>';
            echo '<td>';
            echo '<select name="classe_professeur">';
            echo '<option value="">Faites votre choix</option>';
            //faire la requête
            $req_c = ('SELECT `id`,`intitule` FROM `classe` WHERE `id` NOT IN (SELECT `id_classe` FROM `suivre` WHERE `id_professeur` =:id);');
            //récupérer les résultats de la requête
            $sth = $dbs->prepare($req_c);
            $sth->bindParam(':id', $num_prof);
            $sth->execute();
            // parcourir ces résultats
            $tab_r_c = $sth->fetchAll();
            // parcourir le tableau avec les résultats
            foreach ($tab_r_c as $r_c) {
                echo '<option value="' . $r_c['id'] . '">' . $r_c['intitule'] . '</option>';
            }
            ?>
        </select>
    </td>
    </tr>
    <tr>
        <td>
            <button class="bouton" type="submit" name="enregistrer">Enregistrer l'ajout</button>
        </td>
        <td>
            <a href="professeurs.php">retour</a>
        </td>
    </tr>
    <?php
} elseif (!isset($_GET['id_prof'])) {
    echo '<tr>';
    echo '<td>';
    echo 'Vous devez vous choisir un enseignant';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="professeurs.php">Retour</a>';
    echo '</td>';
} elseif (!isset($_SESSION['type'])) {
    echo '<tr>';
    echo '<td>';
    echo 'Vous devez vous connecter';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="connexion.php">connexion</a>';
    echo '</td>';
} else {
    echo '<tr>';
    echo '<td>';
    echo 'Vous n\'avez pas le droit d\'ajouter une classe à un enseignant';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="connexion.php">connexion</a>';
    echo '</td>';
}
?>
</table>
</body>
</html>