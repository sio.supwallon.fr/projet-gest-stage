<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();
//ouvrir la connexion


if (isset($_SESSION['type']) && $_SESSION['type'] == "S") {
    //Requête de récupération du stagiaire connecté
    $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta`, `id_classe` FROM `stagiaire` WHERE `id` =:session;'); //Récupère l'étudiant choisi précédemment
    //récupérer les résultats de la requête
    $sth_e = $dbs->prepare($req_e);
    $sth_e->bindParam('session', $_SESSION['id']);
    $result_e = $sth_e->execute();
    // parcourir ces résultats
    $r_e = $sth_e->fetch();
    $classe = $r_e['id_classe'];
    //Requête de sélection de la classe du stagiaire connecté
    $req_c = ('SELECT `id`,`intitule` FROM `classe` WHERE `id` =' . $classe . ';');
    //faire la requête
    $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =:classe AND `id` =:session;'); //Récupère l'étudiant choisi précédemment
    $sth_e = $dbs->prepare($req_e);
    $sth_e->bindParam('classe', $classe);
    $sth_e->bindParam('session', $_SESSION['id']);
} elseif (isset($_SESSION['type']) && $_SESSION['type'] == "P") {
    //Requête de sélection des classes de l'enseignant connecté
    $req_c = ('SELECT `id`,`intitule` FROM `classe` INNER JOIN `suivre` ON `id` = `id_classe` WHERE `id_professeur` =' . $_SESSION['id'] . ';');
    if (isset($_POST['classe']) && $_POST['classe'] != "") { //|| isset($classe))
        $classe = filter_input(INPUT_POST, 'classe', FILTER_SANITIZE_NUMBER_INT);
        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =:classe;'); //Récupère l'étudiant choisi précédemment
        $sth_e = $dbs->prepare($req_e);
        $sth_e->bindParam('classe', $classe);
    } else {
        //$req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire`;'); //Récupère l'étudiant choisi précédemment
    }
} else {
    //faire la requête
    $req_c = ('SELECT `id`,`intitule` FROM `classe` ORDER BY intitule;');
    //Si la classe a été choisie
    if (isset($_POST['classe']) && $_POST['classe'] != "") { //|| isset($classe))
        $classe = filter_input(INPUT_POST, 'classe', FILTER_SANITIZE_NUMBER_INT);
        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =:classe;'); //Récupère l'étudiant choisi précédemment
        $sth_e = $dbs->prepare($req_e);
        $sth_e->bindParam('classe', $classe); //Récupère l'étudiant choisi précédemment
    } else {
        // $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire`;'); //Récupère l'étudiant choisi précédemment
    }
}

//Si le bouton enregistrer a été utilisé
if (isset($_POST['enregistrer'])) {
    //initialisation des variables
    $req = true;

    //vérification du remplissage des champs de l'élève
    if (empty($_POST['classe'])) {
        $req = false;
        echo 'le champs classe est vide';
    }

    if (empty($_POST['etudiant'])) {
        $req = false;
        echo 'le champs étudiant est vide';
    }

    if (empty($_POST['entreprise'])) {
        $req = false;
        echo 'le champs entreprise est vide';
    }

    if (empty($_POST['tuteur'])) {
        $req = false;
        echo 'le champs tuteur est vide';
    }

    if (empty($_POST['responsable'])) {
        $req = false;
        echo 'le champs responsable est vide';
    }

    if (empty($_POST['enseignant'])) {
        $req = false;
        echo 'le champs enseignant est vide';
    }

    if (empty($_POST['periode1_debut'])) {
        $req = false;
        echo 'le champs début de la 1ère période est vide';
    }

    if (empty($_POST['periode1_fin'])) {
        $req = false;
        echo 'le champs fin de la 1ère période est vide';
    }

    if (empty($_POST['duree_sem'])) {
        $req = false;
        echo '<p>le champs durée en semaines est vide</p>';
    }
//    $req2 = true;
//    if (!empty($_POST['periode1_fin']) && !empty($_POST['periode1_debut']) && !empty($_POST['etudiant'])) {
//        $id_sta = filter_input(INPUT_POST, 'etudiant', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_debut = filter_input(INPUT_POST, 'periode1_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_fin = filter_input(INPUT_POST, 'periode1_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//        if (!empty($_POST['periode2_fin']) && !empty($_POST['periode2_debut'])) {
//            $id_periode2_debut = filter_input(INPUT_POST, 'periode2_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//            $id_periode2_fin = filter_input(INPUT_POST, 'periode2_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//        }
//        if ($id_periode1_fin > $id_periode1_debut) {
//            $req_periode = ' SELECT `periode1_fin`, `periode1_debut`, `periode2_fin`, `periode2_debut`, `periode3_fin`, `periode3_debut` FROM convention WHERE id_sta=:id_sta;';
//            $sth_p = $dbs->prepare($req_periode);
//            $sth_p->bindParam(':id_sta', $id_sta);
//            $sth_p->execute();
//            $tab_p = $sth_p->fetchAll();
//            foreach ($tab_p as $p) {
//                if ($id_periode1_fin < $p['periode1_debut'] && $id_periode1_fin != $p['periode1_debut'] && $id_periode1_fin != $id_periode1_debut) {
//                    $req2 = true;
//                } elseif ($id_periode1_debut > $p['periode1_fin'] && $id_periode1_debut != $p['periode1_fin'] && $id_periode1_debut != $id_periode1_fin) {
//                    $req2 = true;
//                } else {
//                    $req2 = false;
//                }
//                if (!empty($_POST['periode2_fin']) && !empty($_POST['periode2_debut'])) {
//                    if ($id_periode1_fin < $p['periode2_debut'] && $id_periode1_fin != $p['periode2_debut'] && $id_periode1_fin != $id_periode2_debut) {
//                        $req2 = true;
//                    } elseif ($id_periode1_debut > $p['periode2_fin'] && $id_periode1_debut != $p['periode2_fin'] && $id_periode1_debut != $id_periode2_fin) {
//                        $req2 = true;
//                    } else {
//                        $req2 = false;
//                    }
//                    if (!empty($_POST['periode3_fin']) && !empty($_POST['periode3_debut']) && !empty($p['periode3_fin']) && !empty($p['periode3_debut'])) {
//                        if ($id_periode1_fin < $p['periode3_debut'] && $id_periode1_fin != $p['periode3_debut'] && $id_periode1_fin != $id_periode3_debut) {
//                            $req2 = true;
//                        } elseif ($id_periode1_debut > $p['periode3_fin'] && $id_periode1_debut != $p['periode3_fin'] && $id_periode1_debut != $id_periode3_fin) {
//                            $req2 = true;
//                        } else {
//                            $req2 = false;
//                        }
//                    }
//                }
//            }
//            if ($req2 == false) {
//                echo '<p>l\'éléve  a déjà un stage dans cette première période</p>';
//            }
//        } elseif ($id_periode1_fin < $id_periode1_debut) {
//            $req2 = false;
//            echo '<p>la date de fin de 1er période doit être supérieur à la date de début de 1er période</p>';
//        } elseif (!empty($_POST['periode2_fin']) && !empty($_POST['periode2_debut'])) {
//            if ($id_periode1_fin > $id_periode2_debut) {
//                $req2 = false;
//                echo '<p>la date de fin de 1er période doit être inférieur à la date de début de 2eme période</p>';
//            }
//        }
//    }
//    $req3 = true;
//    if (!empty($_POST['periode2_fin']) && !empty($_POST['periode2_debut']) && !empty($_POST['etudiant'])) {
//
//        $id_sta = filter_input(INPUT_POST, 'etudiant', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_debut = filter_input(INPUT_POST, 'periode1_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_fin = filter_input(INPUT_POST, 'periode1_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode2_debut = filter_input(INPUT_POST, 'periode2_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode2_fin = filter_input(INPUT_POST, 'periode2_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//
//        if ($id_periode2_fin > $id_periode2_debut) {
//            $req_periode = ' SELECT `periode1_fin`, `periode1_debut`, `periode2_fin`, `periode2_debut`, `periode3_fin`, `periode3_debut` FROM convention WHERE id_sta=:id_sta;';
//            $sth_p = $dbs->prepare($req_periode);
//            $sth_p->bindParam(':id_sta', $id_sta);
//            $sth_p->execute();
//            $tab_p = $sth_p->fetchAll();
//            foreach ($tab_p as $p) {
//                if ($id_periode2_fin < $p['periode2_debut'] && $id_periode2_fin != $p['periode2_debut'] && $id_periode2_fin != $id_periode2_debut) {
//                    $req3 = true;
//                } elseif ($id_periode2_debut >= $p['periode2_fin'] && $id_periode2_debut != $p['periode2_fin'] && $id_periode2_fin != $id_periode2_debut) {
//                    $req3 = true;
//                } else {
//                    $req3 = false;
//                }
//                if ($id_periode2_fin < $p['periode1_debut'] && $id_periode2_fin != $p['periode1_debut'] && $id_periode2_fin != $id_periode1_debut) {
//                    $req3 = true;
//                } elseif ($id_periode2_debut > $p['periode1_fin'] && $id_periode2_debut != $p['periode1_fin'] && $id_periode2_debut != $id_periode1_fin) {
//                    $req3 = true;
//                } else {
//                    $req3 = false;
//                }
//                if (!empty($_POST['periode3_fin']) && !empty($_POST['periode3_debut']) && !empty($p['periode3_fin']) && !empty($p['periode3_debut'])) {
//                    if ($id_periode2_fin < $p['periode3_debut'] && $id_periode2_fin != $p['periode3_debut'] && $id_periode2_fin != $id_periode3_debut) {
//                        $req3 = true;
//                    } elseif ($id_periode2_debut > $p['periode3_fin'] && $id_periode2_debut != $p['periode3_fin'] && $id_periode2_debut != $id_periode3_fin) {
//                        $req3 = true;
//                    } else {
//                        $req3 = false;
//                    }
//                }
//            }
//            if ($req3 == false) {
//                echo '<p>l\'éléve  a déjà un stage dans cette deuxième période</p>';
//            }
//        } else {
//            $req3 = false;
//            echo '<p>la date de fin de 2eme période doit être supérieur à la date de début de 2eme période</p>';
//        }
//    }
//    $req4 = true;
//    if (!empty($_POST['periode3_fin']) && !empty($_POST['periode3_debut']) && !empty($_POST['etudiant']) && !empty($p['periode3_fin']) && !empty($p['periode3_debut'])) {
//
//        $id_sta = filter_input(INPUT_POST, 'etudiant', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_debut = filter_input(INPUT_POST, 'periode1_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode1_fin = filter_input(INPUT_POST, 'periode1_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode2_debut = filter_input(INPUT_POST, 'periode2_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode2_fin = filter_input(INPUT_POST, 'periode2_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode3_debut = filter_input(INPUT_POST, 'periode3_debut', FILTER_SANITIZE_SPECIAL_CHARS);
//        $id_periode3_fin = filter_input(INPUT_POST, 'periode3_fin', FILTER_SANITIZE_SPECIAL_CHARS);
//
//        if ($id_periode3_fin > $id_periode3_debut) {
//            $req_periode = ' SELECT `periode1_fin`, `periode1_debut`, `periode2_fin`, `periode2_debut`, `periode3_fin`, `periode3_debut` FROM convention WHERE id_sta=:id_sta;';
//            $sth_p = $dbs->prepare($req_periode);
//            $sth_p->bindParam(':id_sta', $id_sta);
//            $sth_p->execute();
//            $tab_p = $sth_p->fetchAll();
//            foreach ($tab_p as $p) {
//                if ($id_periode3_fin < $p['periode3_debut'] && $id_periode3_fin != $p['periode3_debut'] && $id_periode3_fin != $id_periode3_debut) {
//                    $req4 = true;
//                } elseif ($id_periode3_debut >= $p['periode3_fin'] && $id_periode3_debut != $p['periode3_fin'] && $id_periode3_fin != $id_periode3_debut) {
//                    $req4 = true;
//                } else {
//                    $req4 = false;
//                }
//                if ($id_periode3_fin < $p['periode1_debut'] && $id_periode3_fin != $p['periode1_debut'] && $id_periode3_fin != $id_periode1_debut) {
//                    $req4 = true;
//                } elseif ($id_periode3_debut > $p['periode1_fin'] && $id_periode3_debut != $p['periode1_fin'] && $id_periode3_debut != $id_periode1_fin) {
//                    $req4 = true;
//                } else {
//                    $req4 = false;
//                }
//                if ($id_periode3_fin < $p['periode2_debut'] && $id_periode3_fin != $p['periode2_debut'] && $id_periode3_fin != $id_periode2_debut) {
//                    $req4 = true;
//                } elseif ($id_periode3_debut > $p['periode2_fin'] && $id_periode3_debut != $p['periode2_fin'] && $id_periode3_debut != $id_periode2_fin) {
//                    $req4 = true;
//                } else {
//                    $req4 = false;
//                }
//            }
//            if ($req4 == false) {
//                echo '<p>l\'éléve  a déjà un stage dans cette deuxième période</p>';
//            }
//        } else {
//            $req4 = false;
//            echo '<p>la date de fin de 2eme période doit être supérieur à la date de début de 2eme période</p>';
//        }
//    }
//    if ($req2 == false || $req3 == false || $req4 == false) {
//        $req = false;
//        echo '<p>période non valide</p>';
//    }
    $id_sta = filter_input(INPUT_POST, 'etudiant', FILTER_SANITIZE_NUMBER_INT);
    $req = 'SELECT EXISTS 
            (
                SELECT *
                FROM convention
                WHERE id_sta = :id_sta AND (
                ( periode1_debut <= :pfin AND periode1_fin >= :pdebut)
                OR ( periode2_debut <= :pfin AND periode2_fin >= :pdebut)
                OR ( periode3_debut <= :pfin AND periode3_fin >= :pdebut)
                )
            ) AS result;';
    $sth = $dbs->prepare($req);
    $sth->bindParam(':id_sta', $id_sta);

    $tab[0]['debut'] = filter_input(INPUT_POST, 'periode1_debut', FILTER_SANITIZE_STRING);
    $tab[0]['fin'] = filter_input(INPUT_POST, 'periode1_fin', FILTER_SANITIZE_STRING);

    if ($tab[0]['debut'] >= $tab[0]['fin']) {
        $req = false;
        echo '<p>La date de fin de la période 1 doit être obligatoirement superieur la date de debut de la période 1</p>';
    }


    if (isset($_POST['periode2_debut']) && $_POST['periode2_debut'] != "" && isset($_POST['periode2_fin']) && $_POST['periode2_fin'] != "") {
        $tab[1]['debut'] = filter_input(INPUT_POST, 'periode2_debut', FILTER_SANITIZE_STRING);
        $tab[1]['fin'] = filter_input(INPUT_POST, 'periode2_fin', FILTER_SANITIZE_STRING);
        if ($tab[1]['debut'] >= $tab[1]['fin']) {
            $req = false;
            echo '<p>La date de fin de la période 2 doit être obligatoirement superieur la date de debut de la période 2</p>';
        }
    }
    if (isset($_POST['periode3_debut']) && $_POST['periode3_debut'] != "" && isset($_POST['periode3_fin']) && $_POST['periode3_fin'] != "") {
        $tab[2]['debut'] = filter_input(INPUT_POST, 'periode3_debut', FILTER_SANITIZE_STRING);
        $tab[2]['fin'] = filter_input(INPUT_POST, 'periode3_fin', FILTER_SANITIZE_STRING);
        if ($tab[2]['debut'] >= $tab[2]['fin']) {
            $req = false;
            echo '<p>La date de fin de la période 3 doit être obligatoirement superieur la date de debut de la période 3</p>';
        }
    }
    $nb = count($tab);
    //var_dump($nb);
    for ($i = 0; $i < $nb; $i++) {
        $sth->bindParam(':pdebut', $tab[$i]['debut']);
        $sth->bindParam(':pfin', $tab[$i]['fin']);
        $res = $sth->execute();
        $resultat = $sth->fetch()['result'];

        if ($resultat == true) {
            $req = false;
            /*
              echo "<pre>";
              var_dump($id_sta);
              var_dump($i);
              var_dump($tab[$i]['debut']);
              var_dump($tab[$i]['fin']);
              var_dump($sth);
              echo "</pre>";
             */
        }
    }

    if ($req == false) {
        echo '<p>les dates sont en conflis avec un des stages déjà enregistré du stagiaire</p>';
    }





    if (empty($_POST['duree_jour'])) {
        $req = false;
        echo '<p>le champs durée en jours est vide</p>';
    }
    if (!is_numeric($_POST['duree_sem'])) {
        $req = false;
        echo '<p>le nombre de semaine doit être un nombre</p>';
    }
    if (!is_numeric($_POST['duree_jour'])) {
        $req = false;
        echo '<p>la durée doit être un nombre</p>';
    }
    if (!is_numeric($_POST['conge'])) {
        $req = false;
        echo '<p>le nombre de congé doit être un nombre</p>';
    }
    if (!empty($_POST['etc']) && !is_numeric($_POST['etc'])) {
        $req = false;
        echo '<p>le nombre d\'ETC doit être un nombre</p>';
    }

    //insertion des valeurs du stagiaires dans la table stagiaire
    if ($req != false) {
        try {
            //déclaration des variables des infos sur le stagiaire
            $type_conv = $_POST['type_conv'];
            $service_sta = $_POST['service'];
            $lieu_sta = $_POST['lieu'];
            $activites_sta = $_POST['activites'];
            $competences_sta = $_POST['competences'];
            $jour_particulier_sta = $_POST['jours_particuliers'];
            $periode1_deb_sta = $_POST['periode1_debut'];
            $periode1_fin_sta = $_POST['periode1_fin'];
            if ($_POST['periode2_debut'] == null) {
                $periode2_deb_sta = null;
            } else {
                $periode2_deb_sta = $_POST['periode2_debut'];
            }
            if ($_POST['periode2_fin'] == null) {
                $periode2_fin_sta = null;
            } else {
                $periode2_fin_sta = $_POST['periode2_fin'];
            }
            if ($_POST['periode3_debut'] == null) {
                $periode3_deb_sta = null;
            } else {
                $periode3_deb_sta = $_POST['periode3_debut'];
            }
            if ($_POST['periode3_fin'] == null) {
                $periode3_fin_sta = null;
            } else {
                $periode3_fin_sta = $_POST['periode3_fin'];
            }

            $duree_sem_sta = $_POST['duree_sem'];
            $duree_jour_sta = $_POST['duree_jour'];
            $avantages1_sta = $_POST['avantages1'];
            $avantages2_sta = $_POST['avantages2'];
            $conge_sta = $_POST['conge'];
            if (!empty($_POST['etc'])) {
                $etc_sta = $_POST['etc'];
            }

            $id_sta = $_POST['etudiant'];
            $id_ent = $_POST['entreprise'];
            $id_tut = $_POST['tuteur'];
            $id_resp = $_POST['responsable'];
            $id_prof = $_POST['enseignant'];
            $id_classe = $_POST['classe'];

            if (!empty($_POST['etc'])) {
                $req_sta = ' INSERT INTO convention (`type` ,`service`, `lieu`, `activites`, `competences`,`jours_particuliers`, `periode1_debut`,`periode1_fin`, `periode2_debut`,`periode2_fin`, `periode3_debut`,`periode3_fin`,`duree_semaines`,`duree_jours`,`avantage1`,`avantage2`,`jours_conge`,`nbre_etc`, `id_sta`, `id_ent`, `id_tut`, `id_resp`, `id_prof`, `id_classe`, `validation`) Values (:type ,:service_sta, :lieu_sta, :activites_sta, :competences_sta, :jours_particuliers_sta, :periode1_deb_sta, :periode1_fin_sta, :periode2_deb_sta, :periode2_fin_sta, :periode3_deb_sta, :periode3_fin_sta, :duree_semaines_sta,:duree_jours_sta, :avantage1_sta, :avantage2_sta, :jours_conge_sta, :nbre_etc_sta, :id_sta_sta, :id_ent_sta, :id_tut_sta, :id_resp_sta, :id_prof_sta, :id_classe_sta, :validation);';
            } else {
                $req_sta = ' INSERT INTO convention (`type` ,`service`, `lieu`, `activites`, `competences`,`jours_particuliers`, `periode1_debut`,`periode1_fin`, `periode2_debut`,`periode2_fin`, `periode3_debut`,`periode3_fin`,`duree_semaines`,`duree_jours`,`avantage1`,`avantage2`,`jours_conge`, `id_sta`, `id_ent`, `id_tut`, `id_resp`, `id_prof`, `id_classe`, `validation`) Values (:type ,:service_sta, :lieu_sta, :activites_sta, :competences_sta, :jours_particuliers_sta, :periode1_deb_sta, :periode1_fin_sta, :periode2_deb_sta, :periode2_fin_sta, :periode3_deb_sta, :periode3_fin_sta, :duree_semaines_sta,:duree_jours_sta, :avantage1_sta, :avantage2_sta, :jours_conge_sta, :id_sta_sta, :id_ent_sta, :id_tut_sta, :id_resp_sta, :id_prof_sta, :id_classe_sta, :validation);';
            }


            //préparation de la requête
            $sta = $dbs->prepare($req_sta);

            //envoyent des paramètres
            $sta->bindParam(':type', $type_conv);
            $sta->bindParam(':service_sta', $service_sta);
            $sta->bindParam(':lieu_sta', $lieu_sta);
            $sta->bindParam(':activites_sta', $activites_sta);
            $sta->bindParam(':competences_sta', $competences_sta);
            $sta->bindParam(':jours_particuliers_sta', $jour_particulier_sta);
            $sta->bindParam(':periode1_deb_sta', $periode1_deb_sta);
            $sta->bindParam(':periode1_fin_sta', $periode1_fin_sta);
            $sta->bindParam(':periode2_deb_sta', $periode2_deb_sta);
            $sta->bindParam(':periode2_fin_sta', $periode2_fin_sta);
            $sta->bindParam(':periode3_deb_sta', $periode3_deb_sta);
            $sta->bindParam(':periode3_fin_sta', $periode3_fin_sta);
            $sta->bindParam(':duree_semaines_sta', $duree_sem_sta);
            $sta->bindParam(':duree_jours_sta', $duree_jour_sta);
            $sta->bindParam(':avantage1_sta', $avantages1_sta);
            $sta->bindParam(':avantage2_sta', $avantages2_sta);
            $sta->bindParam(':jours_conge_sta', $conge_sta);
            if (!empty($_POST['etc'])) {
                $sta->bindParam(':nbre_etc_sta', $etc_sta);
            }
            $sta->bindParam(':id_sta_sta', $id_sta);
            $sta->bindParam(':id_ent_sta', $id_ent);
            $sta->bindParam(':id_tut_sta', $id_tut);
            $sta->bindParam(':id_resp_sta', $id_resp);
            $sta->bindParam(':id_prof_sta', $id_prof);
            $sta->bindParam(':id_classe_sta', $id_classe);
            $sta->bindValue(':validation', 0);

            //execution de la requête		
            $resultat_sta = $sta->execute();
            if ($resultat_sta == true) {
                echo'<p>données entregistrées</p>';
            } else {
                echo '<pre>';
                var_dump($resultat_sta);
                print_r($dbs->errorInfo());
                var_dump($req_sta);
                print_r($sta);
                var_dump($type_conv);

                var_dump($type_conv);
                var_dump($service_sta);
                var_dump($lieu_sta);
                var_dump($activites_sta);
                var_dump($competences_sta);
                var_dump($jour_particulier_sta);
                var_dump($periode1_deb_sta);
                var_dump($periode1_fin_sta);
                var_dump($periode2_deb_sta);
                var_dump($periode2_fin_sta);
                var_dump($periode3_deb_sta);
                var_dump($periode3_fin_sta);
                var_dump($duree_sem_sta);
                var_dump($duree_jour_sta);
                var_dump($avantages1_sta);
                var_dump($avantages2_sta);
                var_dump($conge_sta);
                var_dump($etc_sta);
                var_dump($id_sta);
                var_dump($id_ent);
                var_dump($id_tut);
                var_dump($id_resp);
                var_dump($id_prof);
                var_dump($id_classe);


                echo '</pre>';
            }
        } catch (exception $e) {
            echo 'erreur';
        }
    }
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des conventions </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="conventions.php" method="post">
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {
                        ?>
                        <th>Classe :</th>
                        <td>
                            <?php
                            //Remplir la liste des classes
                            ?>
                            <select name="classe" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
//            $req = ('SELECT `id`,`intitule` FROM `classe`;');
                                //récupérer les résultats de la requête
                                $result = $dbs->query($req_c);
                                // parcourir ces résultats
                                $tab_r = $result->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si la classe a été choisie
                                    if (isset($_POST['classe']) && $_POST['classe'] != "") {
                                        //Si l'id de l'objet en cours est égal à l'id de la classe choisie précédemment
                                        if ($r['id'] == $_POST['classe'])
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['intitule'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                    }
                                    elseif (isset($_SESSION['type']) && $_SESSION['type'] == "S")
                                        echo '<option value="' . $r['id'] . '" selected>' . $r['intitule'] . '</option>';
                                    else
                                        echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Stagiaire :</th>
                        <td>
                            <select name="etudiant" onchange="this.form.submit()">
                                <?php
                                //Si la classe a été choisie
                                if ((isset($_POST['classe']) && $_POST['classe'] != "") || (isset($_SESSION['type']) && $_SESSION['type'] == "S")) {
                                    echo '<option value="">Faites votre choix</option>';
                                    //Récupère et affiche les étudiants de la classe sélectionnée
                                    //faire la requête
                                    //$req = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe ='.$_POST['classe'].';');
                                    //récupérer les résultats de la requête
                                    $result = $sth_e->execute();
                                    // parcourir ces résultats
                                    $tab_r = $sth_e->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    foreach ($tab_r as $r) {
                                        //Si le stagiaire a été choisi
                                        if (isset($_POST['etudiant']) && $_POST['etudiant'] != "") {
                                            //Si l'id de l'objet en cours est égal à l'id de l'étudiant choisi précédemment
                                            if ($r['id'] == $_POST['etudiant'])
                                            //sélectionner la valeur
                                                echo '<option value="' . $r['id'] . '" selected>' . $r['nom_sta'] . ' ' . $r['prenom_sta'] . '</option>';
                                            else
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_sta'] . ' ' . $r['prenom_sta'] . '</option>';
                                        }
                                        elseif (isset($_SESSION['type']) && $_SESSION['type'] == "S")
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['nom_sta'] . ' ' . $r['prenom_sta'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_sta'] . ' ' . $r['prenom_sta'] . '</option>';
                                    }
                                }
                                else {
                                    echo '<option value="">Choisir une classe</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Entreprise :</th>
                        <td>
                            <select name="entreprise" onchange="this.form.submit()">
                                <?php
                                //Si le stagiaire a été choisi
                                if ((isset($_POST['etudiant']) && $_POST['etudiant'] != "") || (isset($_SESSION['type']) && $_SESSION['type'] == "S")) {
                                    echo '<option value="">Faites votre choix</option>';
                                    //Récupère et affiche les entreprises
                                    //faire la requête
                                    $req = (' SELECT `entreprise`.`id`,`nom_ent`,`code_postal`,`nom_ville` FROM `entreprise` INNER JOIN `ville` ON `id_ville` = `ville`.`id`;');
                                    //récupérer les résultats de la requête
                                    $result = $dbs->query($req);
                                    // parcourir ces résultats
                                    $tab_r = $result->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    foreach ($tab_r as $r) {
                                        //Si l'entreprise a été choisie
                                        if (isset($_POST['entreprise']) && $_POST['entreprise'] != "") {
                                            //Si l'id de l'objet en cours est égal à l'id de l'entreprise choisie précédemment
                                            if ($r['id'] == $_POST['entreprise'])
                                            //sélectionner la valeur
                                                echo '<option value="' . $r['id'] . '" selected>' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                            elseif (isset($_SESSION['type']) && $_SESSION['type'] == "S")
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                            else
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                        } else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_ent'] . ' ' . $r['code_postal'] . ' ' . $r['nom_ville'] . '</option>';
                                    }
                                }
                                else {
                                    echo '<option value="">Choisir un stagiaire</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Responsable :</th>
                        <td>
                            <select name="responsable" onchange="this.form.submit()">
                                <?php
                                //Si l'entreprise a été choisie
                                if (isset($_POST['entreprise']) && $_POST['entreprise'] != "") {
                                    echo '<option value="">Faites votre choix</option>';
                                    //Récupère et affiche les responsables
                                    //faire la requête
                                    $req = (' SELECT `id`,`nom_resp`,`prenom_resp`,`fonction_resp` FROM `responsable` WHERE `id_ent` = ' . $_POST['entreprise'] . ';');
                                    //récupérer les résultats de la requête
                                    $result = $dbs->query($req);
                                    // parcourir ces résultats
                                    $tab_r = $result->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    foreach ($tab_r as $r) {
                                        //Si le responsable a été choisi
                                        if (isset($_POST['responsable']) && $_POST['responsable'] != "") {
                                            //Si l'id de l'objet en cours est égal à l'id du responsable choisi précédemment
                                            if ($r['id'] == $_POST['responsable'])
                                            //sélectionner la valeur
                                                echo '<option value="' . $r['id'] . '" selected>' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                            else
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                        } else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_resp'] . ' ' . $r['prenom_resp'] . ' ' . $r['fonction_resp'] . '</option>';
                                    }
                                }
                                else {
                                    echo '<option value="">Choisir une entreprise</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Tuteur :</th>
                        <td>
                            <select name="tuteur" onchange="this.form.submit()">
                                <?php
                                //Si le responsable a été choisi
                                if (isset($_POST['responsable']) && $_POST['responsable'] != "") {
                                    echo '<option value="">Faites votre choix</option>';
                                    //Récupère et affiche les tuteurs
                                    //faire la requête
                                    $req = ('SELECT `id`,`nom_tut`,`prenom_tut`,`fonction_tut` FROM `tuteur` WHERE `id_entreprise` = ' . $_POST['entreprise'] . ';');
                                    //récupérer les résultats de la requête
                                    $result = $dbs->query($req);
                                    // parcourir ces résultats
                                    $tab_r = $result->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    foreach ($tab_r as $r) {
                                        //Si le tuteur a été choisi
                                        if (isset($_POST['tuteur']) && $_POST['tuteur'] != "") {
                                            //Si l'id de l'objet en cours est égal à l'id du tuteur choisi précédemment
                                            if ($r['id'] == $_POST['tuteur'])
                                            //sélectionner la valeur
                                                echo '<option value="' . $r['id'] . '" selected>' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                            else
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                        } else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_tut'] . ' ' . $r['prenom_tut'] . ' ' . $r['fonction_tut'] . '</option>';
                                    }
                                }
                                else {
                                    echo '<option value="">Choisir un responsable</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Enseignant :</th>
                        <td>
                            <select name="enseignant" onchange="this.form.submit()">
                                <?php
                                //Si le tuteur a été choisi
                                if (isset($_POST['tuteur']) && $_POST['tuteur'] != "") {
                                    echo '<option value="">Faites votre choix</option>';
                                    //Récupère et affiche les tuteurs
                                    //faire la requête
                                    $req = (' SELECT `id`,`nom_prof`,`prenom_prof`,`discipline_prof` FROM `professeur` INNER JOIN `suivre` ON `id` = `id_professeur` WHERE `id_classe` = ' . $_POST['classe'] . ';');
                                    //récupérer les résultats de la requête
                                    $result = $dbs->query($req);
                                    // parcourir ces résultats
                                    $tab_r = $result->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    foreach ($tab_r as $r) {
                                        //Si le tuteur a été choisi
                                        if (isset($_POST['enseignant']) && $_POST['enseignant'] != "") {
                                            //Si l'id de l'objet en cours est égal à l'id du tuteur choisi précédemment
                                            if ($r['id'] == $_POST['enseignant'])
                                            //sélectionner la valeur
                                                echo '<option value="' . $r['id'] . '" selected>' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                            else
                                                echo '<option value="' . $r['id'] . '">' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                        } else
                                            echo '<option value="' . $r['id'] . '">' . $r['nom_prof'] . ' ' . $r['prenom_prof'] . ' ' . $r['discipline_prof'] . '</option>';
                                    }
                                }
                                else {
                                    echo '<option value="">Choisir un tuteur</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <?php
                        //Si l'enseignant a été choisi
                        if (isset($_POST['enseignant']) && $_POST['enseignant'] != "") {
                            //affichage des zones de saisie complémentaires
                            ?>
                            <th>Type de convention :</th>
                            <td>
                                <select name="type_conv" >
                                    <?php
                                    if (isset($_POST['type_conv'])) {
                                        if ($_POST['type_conv'] == "convention") {
                                            echo '<option value="">Faites votre choix</option>';
                                            echo '<option value="convention" selected>convention</option>';
                                            echo '<option value="avenant">avenant</option>';
                                        } elseif ($_POST['type_conv'] == "avenant") {
                                            echo '<option value="">Faites votre choix</option>';
                                            echo '<option value="convention">convention</option>';
                                            echo '<option value="avenant" selected>avenant</option>';
                                        }
                                    } else {
                                        echo '<option value="">Faites votre choix</option>';
                                        echo '<option value="convention">convention</option>';
                                        echo '<option value="avenant">avenant</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <th>Service :</th>
                        <td>
                            <?php
                            if (isset($_POST['service']))
                                echo '<input type="text" name="service" placeholder="Entrer le service d\'accueil du stagiaire" value ="' . $_POST['service'] . '" />';
                            else
                                echo '<input type="text" name="service" placeholder="Entrer le service d\'accueil du stagiaire" value =""/>';
                            ?>
                        </td>
                        </tr>
                        <tr>
                            <th>Lieu :</th>
                            <td>
                                <?php
                                if (isset($_POST['lieu']))
                                    echo '<input type="text" name="lieu" placeholder="Entrer le lieu effectif du travail du stagiaire" value ="' . $_POST['lieu'] . '" />';
                                else
                                    echo '<input type="text" name="lieu" placeholder="Entrer le lieu effectif du travail du stagiaire" value ="" />';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Activites :</th>
                            <td>
                                <?php
                                if (isset($_POST['activites']))
                                    echo '<textarea name="activites" cols="33,8" rows="2" placeholder="Entrer les activités confiées au stagiaire">' . $_POST['activites'] . '</textarea>';
                                else
                                    echo '<textarea name="activites" cols="33,8" rows="2" placeholder="Entrer les activités confiées au stagiaire"></textarea>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Compétences :</th>
                            <td>
                                <?php
                                if (isset($_POST['competences']))
                                    echo '<textarea name="competences" cols="33,8" rows="2" placeholder="Entrer les compétences qui seront validées par le stagiaire durant son stage">' . $_POST['competences'] . '</textarea>';
                                else
                                    echo '<textarea name="competences" cols="33,8" rows="2" placeholder="Entrer les compétences qui seront validées par le stagiaire durant son stage"></textarea>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Jours particuliers :</th>
                            <td>
                                <?php
                                if (isset($_POST['jours_particuliers']))
                                    echo '<textarea name="jours_particuliers" cols="33,8" rows="2" placeholder="Entrer les périodes de nuit ou les week ends ou jours fériés travaillés par le stagiaire">' . $_POST['jours_particuliers'] . '</textarea>';
                                else
                                    echo '<textarea name="jours_particuliers" cols="33,8" rows="2" placeholder="Entrer les périodes de nuit ou les week ends ou jours fériés travaillés par le stagiaire"></textarea>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>1ère période :</th>
                            <td>
                                DU :
                                <?php
                                if (isset($_POST['periode1_debut']))
                                    echo '<input type="date" name="periode1_debut" value = "' . $_POST['periode1_debut'] . '"/>';
                                else
                                    echo '<input type="date" name="periode1_debut" value = ""/>';
                                ?>
                            </td>
                            <td>
                                AU :
                                <?php
                                if (isset($_POST['periode1_fin']))
                                    echo '<input type="date" name="periode1_fin" value = "' . $_POST['periode1_fin'] . '"/>';
                                else
                                    echo '<input type="date" name="periode1_fin" value = ""/>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>2nde période :</th>
                            <td>
                                DU :
                                <?php
                                if (isset($_POST['periode2_debut']))
                                    echo '<input type="date" name="periode2_debut" value = "' . $_POST['periode2_debut'] . '"/>';
                                else
                                    echo '<input type="date" name="periode2_debut" value = ""/>';
                                ?>
                            </td>
                            <td>
                                AU :
                                <?php
                                if (isset($_POST['periode2_fin']))
                                    echo '<input type="date" name="periode2_fin" value = "' . $_POST['periode2_fin'] . '"/>';
                                else
                                    echo '<input type="date" name="periode2_fin" value = ""/>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>3eme période :</th>
                            <td>
                                DU :
                                <?php
                                if (isset($_POST['periode3_debut']))
                                    echo '<input type="date" name="periode3_debut" value = "' . $_POST['periode3_debut'] . '"/>';
                                else
                                    echo '<input type="date" name="periode3_debut" value = ""/>';
                                ?>
                            </td>
                            <td>
                                AU :
                                <?php
                                if (isset($_POST['periode3_fin']))
                                    echo '<input type="date" name="periode3_fin" value = "' . $_POST['periode3_fin'] . '"/>';
                                else
                                    echo '<input type="date" name="periode3_fin" value = ""/>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Durée en semaines :</th>
                            <td>
                                <?php
                                if (isset($_POST['duree_sem'])) {
                                    echo '<input type="nomber" name="duree_sem" placeholder="Entrer le nombre de semaines du stage" value ="' . $_POST['duree_sem'] . '" />';
                                } else {
                                    echo '<input type="nomber" name="duree_sem" placeholder="Entrer le nombre de semaines du stage" value ="" />';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Durée en jours :</th>
                            <td>
                                <?php
                                if (isset($_POST['duree_jour']))
                                    echo '<input type="nomber" name="duree_jour" placeholder="Entrer le nombre de jours ouvrables du stage" value ="' . $_POST['duree_jour'] . '" />';
                                else
                                    echo '<input type="nomber" name="duree_jour" placeholder="Entrer le nombre de jours ouvrables du stage" value ="" />';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Avantages :</th>
                            <td>
                                <?php
                                if (isset($_POST['avantages1']))
                                    echo '<textarea name="avantages1" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . $_POST['avantages1'] . '</textarea>';
                                else
                                    echo '<textarea name="avantages1" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire"></textarea>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Autres avantages :</th>
                            <td>
                                <?php
                                if (isset($_POST['avantages2']))
                                    echo '<textarea name="avantages2" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire">' . $_POST['avantages2'] . '</textarea>';
                                else
                                    echo '<textarea name="avantages2" cols="33,8" rows="2" placeholder="Entrer les avantages reçues par le stagiaire"></textarea>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Jours de congé :</th>
                            <td>
                                <?php
                                if (isset($_POST['conge']))
                                    echo '<input type="number" name="conge"  placeholder="Entrer les jours de congé exeptionnels accordés au stagiaire" value="' . $_POST['conge'] . '">';
                                else
                                    echo '<input type="number" name="conge"  placeholder="Entrer les jours de congé exeptionnels accordés au stagiaire">';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Nombre d'ECTS :</th>
                            <td>
                                <?php
                                if (isset($_POST['etc']))
                                    echo '<input type="number" name="etc" placeholder="Entrer le nombre d\'ECTS obtenus par le stagiaire" value ="' . $_POST['etc'] . '" />';
                                else
                                    echo '<input type="number" name="etc" placeholder="Entrer le nombre d\'ECTS obtenus par le stagiaire" value ="" />';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="bouton" type="submit" name="enregistrer">Enregistrer la convention</button>
                            </td>

                            <?php
                        }
                        if ($_SESSION['type'] == "S") {
                            ?>
                            <td>
                                <a href="index.php">retour</a>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td>
                                <a href="gestion_convention.php">retour</a>
                            </td>
                            <?php
                        }
                    } else {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez vous connecter';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="connexion.php">connexion</a>';
                    }
                    ?>

                </tr>
            </table>
        </form>
    </body>
</html>
