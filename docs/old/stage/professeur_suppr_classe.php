<!DOCTYPE html>
<?php
require 'scripts/constante.php';
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
$id_prof = filter_input(INPUT_GET, 'id_prof', FILTER_SANITIZE_SPECIAL_CHARS);

session_start();
//ouverture de la connexion
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer un professeur</title>
    </head>
    <body>

        <form action="professeur_suppr_classe.php?id_prof=<?php echo $id_prof; ?>&amp;id_classe=<?php echo $id_classe; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" )) {
                echo '<p>êtes vous sûr de vouloir suprimer la classe à cet enseignant ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>
            </form>
            <?php
            if (isset($_GET['id_prof']) && isset($_GET['id_classe']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_ent = ' DELETE FROM suivre '
                        . ' WHERE `id_professeur`=:id_prof '
                        . ' AND `id_classe`=:id_classe;';

                //préparation de la requête
                $ent = $dbs->prepare($req_ent);
                $ent->bindParam(':id_prof', $id_prof);
                $ent->bindParam(':id_classe', $id_classe);
                //execution de la requête
                $resultat_sta = $ent->execute();
                /*
                  echo '<pre>';
                  var_dump($resultat_sta);
                  print_r($dbs->errorInfo());
                  var_dump($req_ent);
                  print_r($ent);
                  var_dump($id);
                  echo '</pre>';
                  die();
                 */
                if ($resultat_sta == true) {
                    header("location:professeurs.php");
                } else {
                    print_r($dbs->errorInfo());
                }
            } elseif (isset($_GET['id_prof']) && isset($_GET['id_classe']) && isset($_POST['btn_non'])) {
                header("location:professeurs.php");
            }
        } elseif (!isset($_GET['id_prof'])) {
            echo'<p>veuillez sélectionner un professeur</p>';
        } elseif (!isset($_GET['id_classe'])) {
            echo'<p>veuillez sélectionner une classe</p>';
        } elseif ($_SESSION['type'] != "A") {
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour cette manipulation';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
        } else {
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
        }
        ?>




    </body>
</html>
