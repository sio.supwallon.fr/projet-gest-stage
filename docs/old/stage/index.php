<?php

// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();
if (isset($_POST['deconnexion'])) {
    $_SESSION = array();
    session_destroy();

    //session_stop();

    header('Location: connexion.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des conventions </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>

        <form action="index.php" method="post">

            <?php
            if (isset($_SESSION['type'])) {
                ?>

                <table>
                    <tr>

                        <?php
                        if ($_SESSION['type'] != "E") {
                            echo '<td>';
                            echo '<a href="etudiants.php">Gestion des étudiants</a>';
                            echo '</td>';
                        }
                        ?>

                        <td>
                            <a href="entreprises.php">Gestion des entreprises</a>
                        </td>


                        <?php
                        if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                            echo '<td>';
                            echo '<a href="professeurs.php">Gestion des professeurs</a>';
                            echo '</td>';
                        }
                        if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                            ?>
                            <td>
                                <a href="gestion_convention.php">Gestion des conventions</a>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td>
                                <a href="conventions.php">Gestion des conventions</a>
                            </td>
                            <?php
                        }
                        if ($_SESSION['type'] == "A") {
                            echo '<td>';
                            echo '<a href="classes.php">Gestion des classes</a>';
                            echo '</td>';
                            echo '<td>';
                            echo '<a href="administrateur.php">Gestion de l\'administrateur</a>';
                            echo '</td>';
                        }
                        ?>
                        <td>
                            <button type="submit" name="deconnexion">Déconnexion</button>
                        </td>
                    </tr>
                    
                </table>



                <?php
            } else {
                echo '<table>';
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
                echo '</tr>';
                echo '</table>';
            }
            ?>


            <p><img width=30% hight=30% src="image/logowallon.jpg" alt="Image non chargée"/></p>
        </form>
    </body>
</html>
