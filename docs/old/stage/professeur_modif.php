<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$num = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
if (isset($_GET['id'])) {
    //Récupère l'id de l'enseignant sélectionné dans la page "professeurs.php"

    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {
        try {
            $num = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
            $nom_prof = filter_input(INPUT_POST, 'nom_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
            $prenom_prof = filter_input(INPUT_POST, 'prenom_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
            $discipline_professeur = filter_input(INPUT_POST, 'discipline_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
            $telephone_professeur = filter_input(INPUT_POST, 'telephone_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
            $email_professeur = filter_input(INPUT_POST, 'email_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
            if (isset($_POST['mdp_professeur']) && ($_POST['mdp_professeur'] != "")) {

                //ouvrir la connexion
                $mdp = filter_input(INPUT_POST, 'mdp_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
                $mdp = md5($mdp);
                //faire la requête de modification (il faudra ajouter la date de naissance)
                $req_prof = ('UPDATE `professeur` SET `nom_prof`=:nom_prof, `prenom_prof`=:prenom_prof, `discipline_prof`=:discipline_prof, `telephone_prof`=:telephone_prof, `email_prof`=:email_prof, `mdp_prof`=:mdp_prof WHERE `id` =:id;'); //Récupère l'étudiant choisi précédemment
                //préparation de la requête
                $prof = $dbs->prepare($req_prof);
                //envoyent des paramètres
                $prof->bindParam(':nom_prof', $nom_prof);
                $prof->bindParam(':prenom_prof', $prenom_prof);
                $prof->bindParam(':discipline_prof', $discipline_professeur);
                $prof->bindParam(':telephone_prof', $telephone_professeur);
                $prof->bindParam(':email_prof', $email_professeur);
                $prof->bindParam(':mdp_prof', $mdp);
                $prof->bindParam(':id', $num);

                //execution de la requête		
                $resultat_prof = $prof->execute();
                echo '<p>données enregistrées</p>';
            } elseif (($_POST['mdp_professeur'] == null)) {

                $req_prof = ('UPDATE `professeur` SET `nom_prof`=:nom_prof, `prenom_prof`=:prenom_prof, `discipline_prof`=:discipline_prof, `telephone_prof`=:telephone_prof, `email_prof`=:email_prof WHERE `id` =:id;'); //Récupère l'étudiant choisi précédemment
                //préparation de la requête
                $prof = $dbs->prepare($req_prof);
                //envoyent des paramètres
                $prof->bindParam(':nom_prof', $nom_prof);
                $prof->bindParam(':prenom_prof', $prenom_prof);
                $prof->bindParam(':discipline_prof', $discipline_professeur);
                $prof->bindParam(':telephone_prof', $telephone_professeur);
                $prof->bindParam(':email_prof', $email_professeur);
                $prof->bindParam(':id', $num);

                $resultat_prof = $prof->execute();
                echo '<p>données enregistrées</p>';
            }
        } catch (exception $e) {
            echo 'erreur';
        }
    }
} else
    $num = 0;
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Modification d'un enseignant </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="professeur_modif.php?id=' . $num . '" method="post">';
//http://localhost/Stage_Jeremy3/
        echo '<table>';
        if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") && isset($_GET['id'])) {
            //faire la requête de sélection de l'étudiant choisi dans la page précédente
            $req = ('SELECT `id`,`nom_prof`,`prenom_prof`,`discipline_prof`,`telephone_prof`,`email_prof` FROM `professeur` WHERE `id` =:id;');
            //récupérer les résultats de la requête
            $sth = $dbs->prepare($req);
            $sth->bindParam(':id', $num);
            $sth->execute();
            // parcourir ces résultats
            $tab_r = $sth->fetchAll();
            // parcourir le tableau avec les résultats
            foreach ($tab_r as $r) {
                //Affiche les informations de l'enseignant récupérées
                ?>
            <tr>
                <th>Nom :</th>
                <?php
                echo '<td><input type="text" name="nom_professeur" value ="' . $r['nom_prof'] . '" /></td>';
                ?>
            </tr>
            <tr>
                <th>Prénom :</th>
                <?php
                echo '<td><input type="text" name="prenom_professeur" value ="' . $r['prenom_prof'] . '" /></td>';
                ?>
            </tr>
            <tr>
                <th>discipline :</th>
                <td>
                    <?php
                    echo '<input type="text" name="discipline_professeur" value ="' . $r['discipline_prof'] . '"/>';
                    ?>
                </td>
            </tr>
            <tr>
                <th>Téléphone :</th>
                <?php
                echo '<td><input type="tel" name="telephone_professeur" maxlength="10"  value ="' . $r['telephone_prof'] . '"/>';
                ?>
            </td>                                                                                                                                                           
        </tr>
        <tr>
            <th>Email :</th>
            <?php
            echo '<td><input type="email" name="email_professeur" placeholder="pauldurand@gmail.com"  value ="' . $r['email_prof'] . '"/></td>';
            ?>
        </tr>
        <tr>
            <th>Mot de passe :</th>
            <td>
                <?php
                echo '<input type="password" name="mdp_professeur"  />';
                ?>
            </td> 
        </tr>
        <?php
    }
    ?>
    <tr>
        <td>
            <button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>
        </td>
        <td>
            <p><input type="submit" value="retour" name="retour" /></p>
        </td>
    </tr>
    <?php
    if (isset($_POST['retour'])) {
        header('location:professeurs.php');
    }
} elseif (!isset($_GET['id'])) {
    echo '<tr>';
    echo '<td>';
    echo 'Vous devez vous choisir un enseignant';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="professeurs.php">Retour</a>';
    echo '</td>';
} elseif (!isset($_SESSION['type'])) {
    echo '<tr>';
    echo '<td>';
    echo 'Vous devez vous connecter';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="connexion.php">connexion</a>';
    echo '</td>';
} else {
    echo '<tr>';
    echo '<td>';
    echo 'Vous n\'avez pas le droit de modifier un enseignant';
    echo '</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td>';
    echo '<a href="connexion.php">connexion</a>';
    echo '</td>';
}
?>      
</table>

</form>
</body>
</html>
