<!DOCTYPE html>
<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();
$id_ent = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css">
        <title>CREATIONtuteur</title>
    </head>
    <body>
        <h1>les tuteurs de l'entreprise</h1>
        <table>
            <form action="tuteur.php?id_ent=<?php echo $id_ent; ?>&amp;id_ville = <?php echo $id_ville; ?>" method="post"



                  <?php
                  if (isset($_SESSION['type'])) {
                      $req = ' SELECT * FROM tuteur WHERE id_entreprise=:id;';
                      $sth = $dbs->prepare($req);
                      $sth->bindParam(':id', $id_ent);
                      $res = $sth->execute();
                      $tab = $sth->fetchAll();
                      ?>

                      <tr>
                        <th>Nom </th><th>Prenom </th><th>Fonction </th><th>telephone </th><th>email </th>
                    </tr>
                    <?php
                    foreach ($tab as $r) {
                        ?>



                        <tr>
                            <?php echo '<td>' . $r['nom_tut'] . '</td>'; ?>
                            <?php echo '<td>' . $r['prenom_tut'] . '</td>'; ?>
                            <?php echo '<td>' . $r['fonction_tut'] . '</td>'; ?>
                            <?php echo '<td>' . $r['telephone_tut'] . '</td>'; ?>
                            <?php echo '<td>' . $r['email_tut'] . '</td>'; ?>
                            <?php if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") { ?>
                                <?php echo '<td><a href ="tuteur_modif.php?id_ville=' . $id_ville . '&amp;id_ent=' . $id_ent . '&amp;id=' . $r['id'] . '">modification</a></td>'; ?>
                                <?php echo '<td><a href ="tuteur_suppr.php?id_ville=' . $id_ville . '&amp;id_ent=' . $id_ent . '&amp;id=' . $r['id'] . '">suppression</a></td>'; ?>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <?php echo '<td><a href="tuteur_ajout.php?id_ent=' . $id_ent . '&amp;id_ville=' . $id_ville . '">ajout</a></td>'; ?>
                    </tr>


                    <p><a href="entreprises.php?action=liste&amp;id_ville=<?php echo $id_ville; ?>">retour</a></p>
                    <?php
                } else {

                    echo '<tr>';
                    echo '<td>';
                    echo '<p>vous devez vous connecter</p>';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<p><a href="connexion.php">connexion</a></p>';
                    echo '</td>';
                    echo '</tr>';
                }
                ?>

            </form>
        </table>
    </body>
</html>