
<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text; charset=UTF-8');
$id_conv = filter_input(INPUT_GET, 'id_conv', FILTER_SANITIZE_NUMBER_INT);
session_start();
//ouvrir la connexion

if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
    require "vendor/autoload.php";
    require "classes/PDF.php";


    $req = ' SELECT `type`,`service`,`lieu`,`activites`,`competences`,`jours_particuliers`,`periode1_debut`, `periode3_debut`,`periode2_debut`,'
            . ' `periode1_fin`,`periode2_fin`,`periode3_fin`,`duree_semaines`,`duree_jours`,`avantage1`,`avantage2`,`jours_conge`,`nbre_etc`,'
            . ' `caisse_sta`,`lieu_naissance_sta`,`nom_sta`,`num_ss_sta`,`prenom_sta`,`date_naissance_sta`,`sexe_sta`,`adresse_sta`,'
            . ' `telephone_sta`,`email_sta`,`adresse_ent`,entreprise.`email` AS email_ent,`nom_ent`,entreprise.`telephone`,`fonction_tut`,`telephone_tut`,'
            . ' `email_tut`,`nom_tut`,`prenom_tut`,`nom_resp`,`prenom_resp`,`fonction_resp`,`nom_prof`,`telephone_prof`,`email_prof`,`prenom_prof`,'
            . ' `discipline_prof`,`intitule`, classe.`description` '
            . ' FROM convention '
            . ' INNER JOIN stagiaire ON `id_sta` = stagiaire.`id` '
            . ' INNER JOIN entreprise ON `id_ent` = entreprise.`id` '
            . ' INNER JOIN tuteur ON `id_tut` = tuteur.`id` '
            . ' INNER JOIN responsable ON `id_resp` = responsable.`id` '
            . ' INNER JOIN professeur ON `id_prof` = professeur.`id` '
            . ' INNER JOIN classe ON convention.`id_classe` = classe.`id` '
            . ' WHERE convention.`id` = :id;';

    $sth = $dbs->prepare($req);
    $sth->bindParam(':id', $id_conv);
    $res = $sth->execute();



    if ($res == true) {
        $header = array('');
        $tab = $sth->fetch();
        while ($tab == true) {
            //en tête
            $pdf = new PDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 20);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->Cell(10, 230, '', 0, 0, 'R', 1);
            $pdf->Image('image/imageconv.jpg', 00, 10, -100);
            $pdf->Image('image/imageconvs.jpg', 10, 200, -50);

            $pdf->SetTextColor(54, 95, 145);
            $pdf->Text(20, 240, utf8_decode('CONVENTION DE STAGE'));

            $pdf->SetFont('Arial', '', 12);
            $pdf->SetTextColor(74, 68, 42);
            $pdf->Text(20, 248, utf8_decode('Lycée Wallon - VALENCIENNES'));

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(20, 265, utf8_decode('Stages étudiant'));
            $pdf->Text(20, 270, 'type de convention : ' . $tab['type']);

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Text(20, 280, utf8_decode('DDFPT'));
            $pdf->SetFont('Arial', '', 10);
            $date = date('d-m-Y');
            $pdf->Text(20, 285, utf8_decode($date));

            //page 1
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetTextColor(76, 155, 211);
            $date = date('Y');
            $date2 = $date - 1;
            $pdf->Cell(40, 10, '', 0, 0, 'R', 1);
            $pdf->Text(40, 30, utf8_decode('Année universitaire : ' . $date2 . '/' . $date));
            $pdf->Ln();
            $pdf->SetFont('Arial', 'B', 20);
            $pdf->SetTextColor(204, 0, 102);
            $pdf->Text(30, 40, utf8_decode('CONVENTION de STAGE'));
            $pdf->Text(60, 50, utf8_decode('entre'));
            $pdf->Image('image/logowallon.jpg', 120, 0, -400);

            //cadre Lycée

            $pdf->BasicTable(60, 10, 90, 70, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Cell(30, 10, '', 0, 0, 'R', 1);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(12, 65, utf8_decode('1 - L\'ÉTABLISSEMENT D\'ENSEIGNEMENT'));
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Text(12, 72, utf8_decode('LYCÉE HENRI WALLON'));
            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(12, 77, utf8_decode('16 Place de la République BP 435'));
            $pdf->Text(12, 82, utf8_decode('59322 Valenciennes Cedex'));
            $pdf->Text(12, 87, utf8_decode('tel : 03.27.19.30.40'));
            $pdf->Text(12, 92, utf8_decode('Représenté par : M. Thierry Claisse'));
            $pdf->Text(12, 97, utf8_decode('Qualité du représentant : Proviseur'));
            $pdf->Text(12, 102, utf8_decode('Composante :'));
            $pdf->Text(15, 107, utf8_decode('............................................................................'));
            $pdf->Text(12, 112, utf8_decode('Mèl :'));
            $req_info_wallon = ' SELECT email FROM entreprise WHERE entreprise.nom_ent = "Lycee Henri WALLON";';
            $sth_inf_wal = $dbs->query($req_info_wallon);
            $tab_inf_wallon = $sth_inf_wal->fetch();
            $pdf->Text(15, 117, utf8_decode($tab_inf_wallon['email']));
            $pdf->Text(12, 122, utf8_decode(' Adresse (si différente de l\'établissement):'));
            $pdf->Text(15, 127, utf8_decode('............................................................................'));

            //cadre entreprise
            //req pour récupérer le cp et la ville de l'entreprise
            $req_ent = ' SELECT id_ent FROM convention WHERE id = :id_conv;';
            $sth_ent = $dbs->prepare($req_ent);
            $sth_ent->bindParam(':id_conv', $id_conv);
            $sth_ent->execute();
            $tab_ent = $sth_ent->fetch();
            while ($tab_ent == true) {
                $req_vil = ' SELECT ville.`nom_ville`, ville.`code_postal` FROM entreprise INNER JOIN ville ON `id_ville` = ville.`id` WHERE entreprise.`id` = :id;';
                $sth_ville = $dbs->prepare($req_vil);
                $sth_ville->bindParam(':id', $tab_ent['id_ent']);
                $sth_ville->execute();
                $tab_ent = $sth_ent->fetch();
            }

            $tab_ville = $sth_ville->fetch();
            while ($tab_ville == true) {

                $pdf->BasicTable(60, 110, 90, 70, $header);
                $pdf->SetFont('Arial', 'BU', 10);
                $pdf->Cell(110, 10, '', 0, 0, 'R', 1);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Text(112, 65, utf8_decode('2 - L\'ORGANISME D\'ACCUEIL'));
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Text(112, 72, utf8_decode('Nom : ' . $tab['nom_ent']));
                $pdf->SetFont('Arial', '', 10);
                $pdf->Text(112, 77, utf8_decode('Adresse : ' . $tab['adresse_ent']));
                $pdf->Text(112, 82, utf8_decode('CP : ' . $tab_ville['code_postal'] . ' Ville : ' . $tab_ville['nom_ville']));
                $pdf->Text(112, 87, utf8_decode('Représenté par : ' . $tab['prenom_resp'] . " " . $tab['nom_resp']));
                $pdf->Text(112, 92, utf8_decode('Fonction : ' . $tab['fonction_resp']));
                $pdf->Text(112, 97, utf8_decode('Service dans lequel le stage sera effectué  : '));
                $pdf->Text(115, 102, utf8_decode($tab['service']));
                $pdf->Text(112, 107, utf8_decode('tel : ' . $tab['telephone']));
                $pdf->Text(112, 112, utf8_decode('Mèl : ' . $tab['email_ent']));
                $pdf->Text(112, 117, utf8_decode('Lieu du stage (1) :'));
                $pdf->Text(115, 122, utf8_decode($tab['lieu']));
                $pdf->SetFont('Arial', 'I', 8);
                $pdf->Text(105, 133, utf8_decode('(1) En cas de stage à l\'étranger, une fiche concernant les droits et obligations'));
                $pdf->Text(110, 136, utf8_decode(' du stagiaire dans le pays d\'accueil sera annexée à la convention'));

                $tab_ville = $sth_ville->fetch();
            }

            //cadre info stagiaire
            $pdf->BasicTable(140, 10, 190, 50, $header);

            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(90, 144, utf8_decode('3 - LE STAGIAIRE'));
            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(15, 150, utf8_decode('Nom : ' . $tab['nom_sta']));
            $pdf->Text(50, 150, utf8_decode('Prenom : ' . $tab['prenom_sta']));
            $pdf->Text(90, 150, utf8_decode('Sexe : ' . $tab['sexe_sta']));
            $pdf->Text(120, 150, utf8_decode('N°Sécurité Social : ' . $tab['num_ss_sta']));

            //date de naissance ne Fr
            $date_naiss = explode('-', $tab['date_naissance_sta']);
            $date = $date_naiss[2] . '/' . $date_naiss[1] . '/' . $date_naiss[0];

            $pdf->Text(15, 155, utf8_decode('date de naissance : ' . $date));
            $pdf->Text(90, 155, utf8_decode('à : ' . $tab['lieu_naissance_sta']));
            //requete pour récupérer la ville du stagiaire
            $req_sta = ' SELECT id_sta FROM convention WHERE id = :id_conv;';
            $sth_sta = $dbs->prepare($req_sta);
            $sth_sta->bindParam(':id_conv', $id_conv);
            $sth_sta->execute();
            $tab_sta = $sth_sta->fetch();
            while ($tab_sta == true) {
                $req_ville_sta = ' SELECT ville.`nom_ville`, ville.`code_postal` FROM stagiaire INNER JOIN ville ON `id_ville` = ville.`id` WHERE `stagiaire`.id = :id;';
                $sth_ville_sta = $dbs->prepare($req_ville_sta);
                $sth_ville_sta->bindParam(':id', $tab_sta['id_sta']);
                $sth_ville_sta->execute();
                $tab_sta = $sth_sta->fetch();
            }

            $tab_ville_sta = $sth_ville_sta->fetch();
            while ($tab_ville_sta == true) {

                $pdf->Text(90, 165, utf8_decode('CP : ' . $tab_ville_sta['code_postal'] . " " . ' Ville : ' . $tab_ville_sta['nom_ville']));
                $tab_ville_sta = $sth_ville_sta->fetch();
            }
            $pdf->Text(120, 155, utf8_decode('classe : ' . $tab['intitule']));
            $pdf->Text(15, 165, utf8_decode('adresse : ' . $tab['adresse_sta']));
            $pdf->Text(15, 170, utf8_decode('tel : ' . $tab['telephone_sta']));
            $pdf->Text(50, 170, utf8_decode('email : ' . $tab['email_sta']));
            $pdf->Text(40, 185, utf8_decode('INTITULÉ DE LA FORMATION SUIVIE : ' . $tab['description']));

            //ENCADREMENT DU STAGIAIRE PAR L’ÉTABLISSEMENT D’ENSEIGNEMENT
            $pdf->BasicTable(190, 10, 95, 40, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(12, 194, utf8_decode('ENCADREMENT DU STAGIAIRE PAR'));
            $pdf->Text(12, 198, utf8_decode('L\'ÉTABLISSEMENT D\'ENSEIGNEMENT'));

            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(12, 203, utf8_decode('Nom et prénom de l\'enseignant référent :'));
            $pdf->Text(12, 208, utf8_decode($tab['nom_prof'] . ' ' . $tab['prenom_prof']));
            $pdf->Text(12, 213, utf8_decode('Fonction (ou discipline) : ' . $tab['discipline_prof']));
            $pdf->Text(12, 218, utf8_decode('tel : ' . $tab['telephone_prof']));
            $pdf->Text(12, 223, utf8_decode('email : ' . $tab['email_prof']));

            //ENCADREMENT DU STAGIAIRE PAR L’ORGANISME D’ACCUEIL
            $pdf->BasicTable(190, 105, 95, 40, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(112, 194, utf8_decode('ENCADREMENT DU STAGIAIRE PAR'));
            $pdf->Text(112, 198, utf8_decode('L\'ORGANISME D\'ACCUEIL'));

            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(112, 203, utf8_decode('Nom et prénom du tuteur de stage :'));
            $pdf->Text(112, 208, utf8_decode($tab['nom_tut'] . ' ' . $tab['prenom_tut']));
            $pdf->Text(112, 213, utf8_decode('Fonction : ' . $tab['fonction_tut']));
            $pdf->Text(112, 218, utf8_decode('tel : ' . $tab['telephone_tut']));
            $pdf->Text(112, 223, utf8_decode('email : ' . $tab['email_tut']));

            //PERIODE(S) DE STAGE 
            $pdf->BasicTable(230, 10, 190, 30, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(12, 235, utf8_decode('PERIODE(S) DE STAGE'));


            //date en fr 

            $periode1_debut = explode('-', $tab['periode1_debut']);
            $periode1_fin = explode('-', $tab['periode1_fin']);
            $periode1_debut = $periode1_debut[2] . '/' . $periode1_debut[1] . '/' . $periode1_debut[0];
            $periode1_fin = $periode1_fin[2] . '/' . $periode1_fin[1] . '/' . $periode1_fin[0];


            if (isset($tab['periode2_debut']) && isset($tab['periode2_fin'])) {
                $periode2_debut = explode('-', $tab['periode2_debut']);
                $periode2_fin = explode('-', $tab['periode2_fin']);
                $periode2_debut = $periode2_debut[2] . '/' . $periode2_debut[1] . '/' . $periode2_debut[0];
                $periode2_fin = $periode2_fin[2] . '/' . $periode2_fin[1] . '/' . $periode2_fin[0];
            }
            if (isset($tab['periode3_debut']) && isset($tab['periode3_fin'])) {
                $periode3_debut = explode('-', $tab['periode3_debut']);
                $periode3_fin = explode('-', $tab['periode3_fin']);
                $periode3_debut = $periode3_debut[2] . '/' . $periode3_debut[1] . '/' . $periode3_debut[0];
                $periode3_fin = $periode3_fin[2] . '/' . $periode3_fin[1] . '/' . $periode3_fin[0];
            }

            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(12, 239, utf8_decode('Dates : '));
            $pdf->Text(12, 243, utf8_decode('du :' . $periode1_debut . ' au : ' . $periode1_fin));
            if (isset($tab['periode2_debut']) && isset($tab['periode2_fin'])) {
                $pdf->Text(12, 248, utf8_decode('du :' . $periode2_debut . ' au : ' . $periode2_fin));
            }
            if (isset($tab['periode3_debut']) && isset($tab['periode3_fin'])) {
                $pdf->Text(70, 243, utf8_decode('du :' . $periode3_debut . ' au : ' . $periode3_fin));
            }

            $pdf->Text(12, 253, utf8_decode('Représentant une durée totale de' . ' ' . $tab['duree_semaines'] . ' ' . 'semaines'));
            $pdf->SetFont('Arial', '', 8);
            $pdf->Text(12, 258, utf8_decode('Commentaire : l’équipe pédagogique et le tuteur définissent en concertation les activités confiées au stagiaire en fonction des objectifs de formation'));

            //caisse d'assurance maladie 
            $pdf->BasicTable(260, 10, 190, 15, $header);
            $pdf->BasicTable(230, 10, 190, 30, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Text(12, 263, 'Caisse primaire d\'assurance maladie à contacter en cas d\'accident');
            $pdf->Text(12, 268, '(lieu du domicile du stagiaire sauf exception): ');
            $pdf->SetFont('Arial', '', 10);
            $pdf->Text(12, 273, $tab['caisse_sta']);

            //article de lois 

            $pdf->AddPage();

            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', 'B', 10);
            //mettre les heures dans l'article 3
            $pdf->Text(118, 88.5, 35 * ($tab['duree_semaines']));

            //requête
            $req_article = ' SELECT * FROM article_de_loi;';

            $resultat_art = $dbs->query($req_article);

            $tab_article = $resultat_art->fetchAll();

            foreach ($tab_article as $value) {
                //si on se situe à l'article 2 , afficher les compétences et l'activité du stagiaire
                if ($value['intitule'] == "Article 2 - Objectif du stage") {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Ln();
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->write(5, utf8_decode('Activités confiées :'));
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $tab['activites']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->write(5, utf8_decode('Compétences à acquérir ou à développer  :'));
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $tab['competences']);
                } else if ($value['intitule'] == utf8_decode("Article 3 - Modalités du stage")) {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Ln();
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->SetFont('Arial', 'BU', 8);
                    $pdf->Ln();
                    $pdf->write(5, ' ' . $tab['jours_particuliers']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    //tableau des périodes
                    $header = array('DATES OU PERIODES', 'LIEU D\'ACCUEIL');

                    if (isset($tab['periode3_debut']) && isset($tab['periode3_fin'])) {
                        $pdf->BasicTable3($header, 'du ' . $periode1_debut . ' au ' . $periode1_fin, $tab['lieu'], 'du  ' . $periode2_debut . ' au ' . $periode2_fin, $tab['lieu'], 'du  ' . $periode3_debut . ' au ' . $periode3_fin, $tab['lieu'], $tab['duree_jours']);
                    } elseif (isset($tab['periode2_debut']) && isset($tab['periode2_fin'])) {
                        $pdf->BasicTable2($header, 'du ' . $periode1_debut . ' au ' . $periode1_fin, $tab['lieu'], 'du  ' . $periode2_debut . ' au ' . $periode2_fin, $tab['lieu'], $tab['duree_jours']);
                    } else {
                        $pdf->BasicTable1($header, 'du ' . $periode1_debut . ' au ' . $periode1_fin, $tab['lieu'], $tab['duree_jours']);
                    }
                } else if ($value['intitule'] == "Article 4 - Accueil et encadrement du stagiaire") {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->Ln();
                    $pdf->Ln();
                    $pdf->Ln();
                    $pdf->Ln();
                    $pdf->Image('image/modalte.jpg', 8, 165, -108);
                } else if ($value['id'] == 7) { //article 5bis
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Ln();
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->write(5, utf8_decode('Autres avantages accordés :'));
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, utf8_decode($tab['avantage1']));
                    $pdf->Ln();
                } else if ($value['id'] == 8) { //article 5ter
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->write(5, utf8_decode('Autres avantages accordés :'));
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, utf8_decode($tab['avantage2']));
                    $pdf->Ln();
                } else if ($value['intitule'] == utf8_decode("Article 7 - Responsabilité et assurance")) {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Ln();
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->SetFont('Arial', 'B', 10);

                    //requête pour avoir l'actuel responssable de wallon
                    $req_wallon = ' SELECT nom_resp, prenom_resp FROM responsable INNER JOIN entreprise ON id_ent = entreprise.id WHERE entreprise.nom_ent = "Lycee Henri WALLON";';
                    $sth_wal = $dbs->query($req_wallon);
                    $tab_wallon = $sth_wal->fetch();
                    while ($tab_wallon == true) {
                        $pdf->Text(30, 133, utf8_decode('Monsieur ' . $tab_wallon['prenom_resp'] . ' ' . $tab_wallon['nom_resp'] . ' Proviseur du Lycée Henri WALLON'));
                        $pdf->Text(25, 138, utf8_decode('16, place de la République - B.P. 435 - 59322 VALENCIENNES CEDEX'));
                        $tab_wallon = $sth_wal->fetch();
                        $pdf->Ln();
                    }
                } else if ($value['intitule'] == utf8_decode("Article 9 - Congés-Interruption du stage")) {
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Ln();
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Text(150, 13.5, utf8_decode($tab['jours_conge']));
                } else if ($value['id'] == 17) {     //article 12
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Text(56, 198.5, utf8_decode($tab['nbre_etc']));
                } else {

                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Write(5, $value['intitule']);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->write(5, $value['description']);
                }

               
            }

            //les signatures 
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'BU', 8);
            $date = date('d-m-Y');
            $pdf->Write(5, 'fait le : ' . utf8_decode($date));

            $header = array('');

            //haut gauche
            $pdf->BasicTable(20, 20, 50, 50, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Text(22, 23, utf8_decode('POUR L\'ÉTABLISSEMENT'));
            $pdf->Text(22, 27, utf8_decode('D\'ENSEIGNEMENT'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->Text(22, 31, utf8_decode('Nom et signature du représentant'));
            $pdf->Text(22, 34, utf8_decode('de l\'établissement'));


            //haut droite
            $pdf->BasicTable(140, 20, 50, 50, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Text(142, 24, utf8_decode('POUR L\'ORGANISME'));
            $pdf->Text(142, 28, utf8_decode('D\'ACCUEIL'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->Text(142, 32, utf8_decode('Nom et signature du représentant'));
            $pdf->Text(142, 36, utf8_decode('L\'organisme d’accueil'));

            //bas droite
            $pdf->BasicTable(140, 140, 50, 50, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Text(142, 144, utf8_decode('Le tuteur de stage'));
            $pdf->Text(142, 148, utf8_decode('de l\'organisme d\'accueil'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->Text(142, 152, utf8_decode('Nom et signature'));


            //bas gauche
            $pdf->BasicTable(20, 140, 50, 50, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Text(22, 143, utf8_decode('L\'enseignant référent'));
            $pdf->Text(22, 147, utf8_decode('du stagiaire'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->Text(22, 151, utf8_decode('Nom et signature'));

            //centre
            $pdf->BasicTable(80, 80, 50, 50, $header);
            $pdf->SetFont('Arial', 'BU', 10);
            $pdf->Text(81, 83, utf8_decode('STAGIAIRE'));
            $pdf->Text(81, 87, utf8_decode('(et son représentant légal'));
            $pdf->Text(81, 91, utf8_decode('dans le cas échéant)'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->Text(82, 95, utf8_decode('Nom et signature'));


            $pdf->Output();
            $tab = $sth->fetch();
        }
    } else {
        echo '<p>erreur</p>';
//        echo '<pre>';
//        var_dump($res);
//        print_r($dbs->errorInfo());
//        var_dump($req);
//        echo '</pre>';
//        die();
    }
} else {
    echo '<p>la page pdf n\'est pas accessible si vous ne vous connectez pas avec un compte qui a l\'autorisation</p>';
}