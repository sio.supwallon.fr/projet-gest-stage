<!DOCTYPE html>
<?php
require 'scripts/constante.php';
session_start();
//ouverture de la connexion
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer une classe</title>
    </head>
    <body>

        <form action="classe_supprimer.php?id=<?php echo $id; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A")) {
                echo '<p>êtes vous sûr de vouloir suprimer la classe ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>

            </form>

            <?php
            if (isset($_GET['id']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_ent = ' DELETE FROM `classe` WHERE id=:id ;';

                //préparation de la requête
                $ent = $dbs->prepare($req_ent);
                $ent->bindParam(':id', $id);
                //execution de la requête
                $resultat_sta = $ent->execute();
                //echo '<pre>';
                //var_dump($resultat_sta);
                //print_r($dbs->errorInfo());
                //var_dump($req_ent);
                // print_r($ent);
                // var_dump($id);
                //echo '</pre>';
                //die();
                if ($resultat_sta == true) {
                    header("location:classes.php");
                } else {
                    print_r($dbs->errorInfo());
                }
            } elseif (isset($_GET['id']) && isset($_POST['btn_non'])) {
                header("location:classes.php");
            } elseif (!isset($_GET['id'])) {
                echo'<p>veuillez sélectionner une classe</p>';
            }
        } elseif ($_SESSION['type'] != "A") {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour cette manipulation';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        } else {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        }
        ?>




    </body>
</html>
