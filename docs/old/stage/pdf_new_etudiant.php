<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$lesvaleurs = $_POST['lesvaleurs']; //filter_input(INPUT_POST, 'lesvaleurs', FILTER_SANITIZE_STRING);
$tableau_val = json_decode($lesvaleurs);
//echo "<pre>";
//echo "debug\n";
//var_dump($lesvaleurs);
//var_dump($tableau_val);
//print_r($tableau_val);
//echo "</pre>";
//die();

session_start();
//ouvrir la connexion

if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
    require "vendor/autoload.php";
    require "classes/PDF.php";

//en tête
    $pdf = new PDF();
//$header = array('nom', 'prenom', 'mot de passe');
    $pdf->AddPage();

    $pdf->SetFont('Arial', 'BU', 10);
    $pdf->SetTextColor(0, 0, 0);

    $req_classe = '     SELECT intitule AS nom FROM classe WHERE id = :id;';
    $sth = $dbs->prepare($req_classe);
    $sth->bindParam(':id', $id_classe);
    $req = $sth->execute();
    $nom = $sth->fetch()['nom'];
    $date = date('d/m/Y');
    $pdf->SetTextColor(255, 0, 0);
    $pdf->Write(20, 'nouveaux comptes pour la classe ' . $nom . ' ' . $date);

    $pdf->Ln();

    $pdf->SetFont('Arial', '', 10);
    $pdf->SetFillColor(255, 255, 255);


    $header = array('nom', 'prenom', 'login', 'mot de passe');

    $pdf->SetTextColor(0, 0, 0);

    foreach ($header as $col) {
        $pdf->Cell(40, 8, $col, 1);
    }
    $pdf->Ln();

    foreach ($tableau_val as $row) {
        foreach ($row as $key => $col) {
            $pdf->Cell(40, 6, $col, 1);
            if ($key == "mdp") {
                $pdf->Ln();
            }
        }
    }

//$pdf->Text(20, 20);

    $pdf->Output();
} else {
    echo '<p>la page pdf n\'est pas accessible si vous ne vous connectez pas avec un compte qui a l\'autorisation</p>';
}