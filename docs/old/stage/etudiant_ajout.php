<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
if (isset($_GET['id_classe'])) {
    session_start();
  
    //Récupère l'id de la classe choisie dans la page précédente
    $num_classe = $id_classe;

    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {
        //Récupération du code permettant d'ajouter un étudiant dans la base de données
        //initialisation des variables
        $req = true;

        //vérification du remplissage des champs de l'élève
        if (empty($_POST['nom_stagiaire'])) {
            $req = false;
            echo 'le champs nom de l\'élève est vide </br>';
        }

        if (empty($_POST['prenom_stagiaire'])) {
            $req = false;
            echo 'le champs prénom de l\'élève est vide </br>';
        }
        $email = filter_input(INPUT_POST, 'email_stagiaire', FILTER_SANITIZE_STRING);
// Remove all illegal characters from email
        $email_checked = filter_var($email, FILTER_SANITIZE_EMAIL);

        // Validate e-mail
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = $email_checked;
        } else {
            $req = false;
            echo("$email n'est pas une adresse email valide </br>");
        }

        //insertion des valeurs du stagiaires dans la table stagiaire
        if ($req != false) {
            //déclaration des variables des infos sur le stagiaire
            $gauche = 'INSERT INTO `stagiaire` (`login_sta`, `nom_sta`, `prenom_sta`,`mdp_sta`, `id_classe`';
            $droite = ') Values (:login_sta, :nom_sta, :prenom_sta, :mdp_sta, :classe_sta';

            if (!empty($_POST['sexe_stagiaire'])) {
                $gauche = $gauche . ', `sexe_sta`';
                $droite = $droite . ', :sexe_sta';
            }

            if (!empty($_POST['num_ss_stagiaire'])) {
                $gauche = $gauche . ', `num_ss_sta`';
                $droite = $droite . ', :num_ss_sta';
            }

            if (!empty($_POST['date_naissance_stagiaire'])) {
                $gauche = $gauche . ',`date_naissance_sta`';
                $droite = $droite . ', :date_naissance_sta';
            }

            if (!empty($_POST['lieu_naissance_stagiaire'])) {
                $gauche = $gauche . ', `lieu_naissance_sta`';
                $droite = $droite . ', :lieu_naissance_sta';
            }

            if (!empty($_POST['adresse_stagiaire'])) {
                $gauche = $gauche . ',`adresse_sta`';
                $droite = $droite . ', :adresse_sta';
            }

            if (!empty($_POST['telephone_stagiaire'])) {
                $gauche = $gauche . ',`telephone_sta`';
                $droite = $droite . ', :telephone_sta';
            }

            if (!empty($_POST['email_stagiaire'])) {

                $gauche = $gauche . ',`email_sta`';
                $droite = $droite . ', :email_sta';
            }

            if (!empty($_POST['caisse_stagiaire'])) {
                $gauche = $gauche . ',`caisse_sta`';
                $droite = $droite . ', :caisse_sta';
            }

            if (!empty($_POST['ville_stagiaire'])) {
                $gauche = $gauche . ', `id_ville`';
                $droite = $droite . ', :ville_sta';
            }

            $req_sta = $gauche . $droite . ');';

            //préparation de la requête
            $sta = $dbs->prepare($req_sta);
            $nom_stagiaire = filter_input(INPUT_POST, 'nom_stagiaire', FILTER_SANITIZE_STRING);
            $nom_stagiaire = strtoupper($nom_stagiaire);
            $prenom_stagiaire = filter_input(INPUT_POST, 'prenom_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $prenom_stagiaire = strtolower($prenom_stagiaire);
            $prenom_stagiaire = ucwords($prenom_stagiaire);
            $pos = strpos($prenom_stagiaire, "-");
            if($pos !== false)
            {
              $prenom_stagiaire = substr($prenom_stagiaire,0,$pos+1) . strtoupper(substr($prenom_stagiaire,$pos+1,1)) . substr($prenom_stagiaire,$pos+2);
            }
            $sexe_stagiaire = filter_input(INPUT_POST, 'sexe_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $num_ss_stagiaire = filter_input(INPUT_POST, 'num_ss_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $date_naissance_stagiaire = filter_input(INPUT_POST, 'date_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $lieu_naissance_stagiaire = filter_input(INPUT_POST, 'lieu_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $telephone_stagiairee = filter_input(INPUT_POST, 'telephone_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $ville_stagiaire = filter_input(INPUT_POST, 'ville_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $adresse_stagiaire = filter_input(INPUT_POST, 'adresse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
            $caisse_stagiaire = filter_input(INPUT_POST, 'caisse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);

            $req_login = 'SELECT COUNT(*) AS nb FROM stagiaire WHERE nom_sta=:nom AND prenom_sta=:prenom;';
            $sth_log = $dbs->prepare($req_login);

            $sth_log->bindParam(':nom', $nom_stagiaire);
            $sth_log->bindParam(':prenom', $prenom_stagiaire);
            
            $result_verif = $sth_log->execute();

            if ($result_verif == true) {
                $result = $sth_log->fetchAll();
                foreach ($result as $r) {
                  $res = $r['nb'];
                }
                if ($res != 0) {
                    $res++;
                    $log = strtolower($prenom_stagiaire . '.' . $nom_stagiaire) . $res;
                } else {
                    $log = strtolower($prenom_stagiaire . '.' . $nom_stagiaire);
                }
            }

            //envoyent des paramètres
            $sta->bindParam(':login_sta', $log);
            $sta->bindParam(':nom_sta', $nom_stagiaire);
            $sta->bindParam(':prenom_sta', $prenom_stagiaire);

            if (!empty($_POST['sexe_stagiaire']))
                $sta->bindParam(':sexe_sta', $sexe_stagiaire);

            if (!empty($_POST['num_ss_stagiaire']))
                $sta->bindParam(':num_ss_sta', $num_ss_stagiaire);

            if (!empty($_POST['date_naissance_stagiaire']))
                $sta->bindParam(':date_naissance_sta', $date_naissance_stagiaire);

            if (!empty($_POST['lieu_naissance_stagiaire']))
                $sta->bindParam(':lieu_naissance_sta', $lieu_naissance_stagiaire);

            if (!empty($_POST['adresse_stagiaire']))
                $sta->bindParam(':adresse_sta', $adresse_stagiaire);

            if (!empty($_POST['telephone_stagiaire']))
                $sta->bindParam(':telephone_sta', $telephone_stagiairee);

            if (!empty($_POST['email_stagiaire']))
                $sta->bindParam(':email_sta', $email);

            if (!empty($_POST['caisse_stagiaire']))
                $sta->bindParam(':caisse_sta', $caisse_stagiaire);


            $mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING);
            $mdp_sta = md5($mdp);

            $sta->bindParam(':mdp_sta', $mdp_sta);

            if (!empty($_POST['ville_stagiaire']))
                $sta->bindParam(':ville_sta', $ville_stagiaire);

            $sta->bindParam(':classe_sta', $num_classe);

            //execution de la requête		
            $resultat_sta = $sta->execute();
            if ($resultat_sta == true) {
                echo '<p>données enregistrées</p>';
            } else {
                echo '<p>erreur</p>';
//                echo '<pre>';
//                var_dump($resultat_sta);
//                print_r($sta->errorInfo());
//                var_dump($req_sta);
//                print_r($sta);
//                var_dump($nom_stagiaire);
//
//                var_dump($prenom_stagiaire);
//                var_dump($sexe_stagiaire);
//                var_dump($num_ss_stagiaire);
//                var_dump($date_naissance_stagiaire);
//                var_dump($lieu_naissance_stagiaire);
//                var_dump($adresse_stagiaire);
//                var_dump($telephone_stagiairee);
//                var_dump($caisse_stagiaire);
//                var_dump($mdp_sta);
//                var_dump($ville_stagiaire);
//                var_dump($num_classe);
//
//
//                echo '</pre>';
            }
        }
    }
} else
    $num_classe = 0;
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Ajout d'un étudiant </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="etudiant_ajout.php?id_classe=' . $num_classe . '" method="post">';
//http://localhost/Stage_Jeremy3/
        ?>
        <table>
            <tr>
                <?php
                if (isset($_SESSION['type']) && isset($_GET['id_classe']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
                    ?>
                    <th>classe :</th>
                    <td>
                        <?php
                        //faire la requête
                        $req_c = ('SELECT `intitule` FROM `classe` WHERE `id` = :id;');
                        //récupérer les résultats de la requête
                        $sth = $dbs->prepare($req_c);
                        $sth->bindParam(':id', $num_classe);
                        $sth->execute();
                        // parcourir ces résultats
                        $tab_r_c = $sth->fetchAll();
                        // parcourir le tableau avec les résultats
                        foreach ($tab_r_c as $r_c) {
                            echo $r_c['intitule'];
                        }
                        ?>
                    </td>
                </tr>
                <?php
                //Si le département a été sélectionnée
                if (isset($_POST['departement_stagiaire'])) {
                    //Affiche les valeurs précédemment affichées dans les différentes zones
                    ?>
                    <tr>
                        <th>Nom :</th>
                        <?php
                        echo '<td><input type="text" name="nom_stagiaire" placeholder="Entrer le nom du stagiaire" value ="' . $_POST['nom_stagiaire'] . '" /></td>';
                        ?>
                    </tr>
                    <tr>
                        <th>Prénom :</th>
                        <?php
                        echo '<td><input type="text" name="prenom_stagiaire" placeholder="Entrer le prénom du stagiaire" value ="' . $_POST['prenom_stagiaire'] . '" /></td>';
                        ?>
                    </tr>
                    <tr>
                        <th>Sexe :</th>
                        <td>
                            <?php
                            //Si le sexe avait été choisi
                            if (isset($_POST['sexe_stagiaire'])) {
                                //Sélectionne le sexe choisi précédemment
                                if ($_POST['sexe_stagiaire'] == 'M') {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" checked="checked" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" /></label>';
                                } else {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" checked="checked" /></label>';
                                }
                            } else {  //Sinon pas de sexe sélectionné
                                echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" /></label>';
                                echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" /></label>';
                            }
                            ?>	
                        </td>
                    </tr>
                    <tr>
                        <th>Numéro de sécurité sociale :</th>
                        <td>
                            <?php
                            echo '<input type="text" name="num_ss_stagiaire" maxlength="15" placeholder="Entrer le numéro de sécurité sociale du stagiaire"  value ="' . $_POST['num_ss_stagiaire'] . '"/>';
                            ?>

                        </td>
                    </tr>
                    <tr>
                        <th>Date de naissance :</th>
                        <?php
                        echo '<td><input type="date" name="date_naissance_stagiaire" value = "' . $_POST['date_naissance_stagiaire'] . '"/></td>';
                        ?>    				
                    </tr>
                    <tr>
                        <th>Lieu de naissance :</th>
                        <?php
                        echo '<td><input type="text" name="lieu_naissance_stagiaire" placeholder="Entrer le lieu de naissance du stagiaire" value ="' . $_POST['lieu_naissance_stagiaire'] . '" /></td>';
                        ?>

                    </tr>
                    <tr>
                        <th>Adresse :</th>
                        <td>
                            <textarea name="adresse_stagiaire" cols="33,8" rows="2" placeholder="Entrer l'adresse du stagiaire"> <?php echo trim($_POST['adresse_stagiaire']) ?></textarea>
                        </td>

                    </tr>
                    <tr>
                        <th>Département :</th>
                        <td>
                            <select name="departement_stagiaire" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req_d = ('SELECT distinct `departement` FROM `ville`;');
                                //récupérer les résultats de la requête
                                $result_d = $dbs->query($req_d);
                                // parcourir ces résultats
                                $tab_r_d = $result_d->fetchAll();
                                // parcourir le tableau avec les résultats
                                //Remplir la liste des départements en sélectionnant le département sélectionné précédemment
                                foreach ($tab_r_d as $r_d) {
                                    if ($r_d['departement'] == $_POST['departement_stagiaire'])
                                        echo '<option value="' . $r_d['departement'] . '" selected>' . $r_d['departement'] . '</option>';
                                    else
                                        echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Ville :</th>
                        <td>
                            <select name="ville_stagiaire" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $departement_stagiaire = filter_input(INPUT_POST, 'departement_stagiaire', FILTER_SANITIZE_NUMBER_INT);
                                $req_v = ' SELECT `id`,`code_postal`,`nom_ville` FROM `ville` WHERE `departement` = :departement;';
                                //récupérer les résultats de la requête
                                $sth = $dbs->prepare($req_v);
                                $sth->bindParam(':departement', $departement_stagiaire);
                                $sth->execute();
                                // parcourir ces résultats
                                $tab_r_v = $sth->fetchAll();
                                // parcourir le tableau avec les résultats
                                //Rempli la liste des villes pour le département sélectionné
                                foreach ($tab_r_v as $r_v) {
                                    if ($r_v['id'] == $_POST['ville_stagiaire'])
                                        echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ville'] . "-" . $r_v['code_postal'] . '</option>';
                                    else
                                        echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ville'] . "-" . $r_v['code_postal'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <th>Téléphone :</th>
                        <?php
                        echo '<td><input type="tel" name="telephone_stagiaire" maxlength="10"  placeholder="Entrer le téléphone du stagiaire" value ="' . $_POST['telephone_stagiaire'] . '"/>';
                        ?>
                        </td>

                    </tr>
                    <tr>
                        <th>Email :</th>
                        <?php
                        echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_stagiaire" placeholder="pauldurand@gmail.com" value ="' . $_POST['email_stagiaire'] . '"/></td>';
                        ?>

                    </tr>
                    <tr>
                        <th>Caisse primaire d'assurance maladie :</th>
                        <?php
                        echo '<td><input type="text" name="caisse_stagiaire" placeholder="Entrer la caisse primaire maladie du stagiaire" value ="' . $_POST['caisse_stagiaire'] . '"/></td>';
                        ?>

                    </tr>
                    <tr>
                        <th>Mot de passe :</th>
                        <?php
                        echo '<td>' . $_POST['mdp'] . '<input type="hidden" name="mdp" value="' . $_POST['mdp'] . '"></td>';
                        ?>
                    </tr>
                    <?php
                }
                else {
                    //Prépare les zones de saisie vide
                    ?>
                    <tr>
                        <th>Nom :</th>
                        <td><input type="text" name="nom_stagiaire" placeholder="Entrer le nom du stagiaire" value ="" /></td>

                    </tr>
                    <tr>
                        <th>Prénom :</th>
                        <td><input type="text" name="prenom_stagiaire" placeholder="Entrer le prénom du stagiaire" value ="" /></td>

                    </tr>
                    <tr>
                        <th>Sexe :</th>
                        <td>
                            <label>Masculin<input type="radio" name="sexe_stagiaire" value="M" /></label>
                            <label>Feminin<input type="radio" name="sexe_stagiaire" value="F" /></label>

                        </td>
                    </tr>
                    <tr>
                        <th>Numéro de sécurité sociale :</th>
                        <td>
                            <input type="text" name="num_ss_stagiaire" maxlength="15" placeholder="Entrer le numéro de sécurité sociale du stagiaire" value ="" />

                        </td>
                    </tr>
                    <tr>
                        <th>Date de naissance :</th>
                        <td>
                            <input type="date" name="date_naissance_stagiaire">
                        </td>

                    </tr>
                    <tr>
                        <th>Lieu de naissance :</th>
                        <td><input type="text" name="lieu_naissance_stagiaire" placeholder="Entrer le lieu de naissance du stagiaire" value ="" /></td>

                    </tr>
                    <tr>
                        <th>Adresse :</th>
                        <td>
                            <textarea name="adresse_stagiaire" cols="33,8" rows="2" placeholder="Entrer l'adresse du stagiaire" value =""></textarea>
                        </td>

                    </tr>
                    <tr>
                        <th>Département :</th>
                        <td>
                            <select name="departement_stagiaire" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req_d = ('SELECT distinct `departement` FROM `ville`;');
                                //récupérer les résultats de la requête
                                $result_d = $dbs->query($req_d);
                                // parcourir ces résultats
                                $tab_r_d = $result_d->fetchAll();
                                // parcourir le tableau avec les résultats
                                //Rempli la liste des départements
                                foreach ($tab_r_d as $r_d) {
                                    echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Ville :</th>
                        <td>
                            <select name="ville_stagiaire" onchange="this.form.submit()">
                                <option value=''>Choisir le département</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Téléphone :</th>
                        <td><input type="tel" name="telephone_stagiaire" maxlength="10" placeholder="Entrer le numéro de téléphone du stagiaire" value ="" />
                        </td>

                    </tr>
                    <tr>
                        <th>Email :</th>
                        <td><input type="text" name="email_stagiaire" placeholder="pauldurand@gmail.com" /></td>

                    </tr>
                    <tr>

                        <th>Caisse primaire d'assurance maladie :</th>
                        <td><input type="text" name="caisse_stagiaire" placeholder="Entrer la caisse primaire maladie du stagiaire" value =""/></td>
                    </tr>
                    <tr>

                        <th>Mot de passe :</th>

                        <?php
                        $mdp = uniqid();
                        echo '<td>' . $mdp . '<input type="hidden" name="mdp" value="' . $mdp . '"></td>';
                        ?>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td>
                        <button class="bouton" type="submit" name="enregistrer">Enregistrer l'étudiant</button>
                    </td>
                    <td>
                        <p><a href="etudiants.php?action=liste&amp;id=<?php echo $id_classe; ?>">retour</a></p>
                    </td>
                    <?php
                } elseif (!isset($_GET['id_classe'])) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous choisir une classe';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="etudiants.php">Retour</a>';
                    echo '</td>';
                } elseif (!isset($_SESSION['type'])) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous n\'avez pas le droit d\'ajouter un stagiaire, connectez vous sur un compte habilité';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
            </tr>
        </table>
    </form>
</body>
</html>
