<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
session_start();
//http://localhost/Stage_Jeremy3/
//ouvrir la connexion
?>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des entreprises </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="convention.php?id=<?php echo $id_classe; ?>" method="post">
            <h1>Information sur convention</h1>
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type']) && isset($_GET['id'])) {
                        //faire la requête
                        $req = ('SELECT `type`,`service`,`lieu`,`activites`,`competences`,`jours_particuliers`,`periode1_debut`,`periode2_debut`, `periode3_debut`,`periode1_fin`,`periode2_fin`, `periode3_fin`, `duree_semaines`,`duree_jours`,`avantage1`,`avantage2`,`jours_conge`,`nbre_etc`,`nom_sta`,`prenom_sta`,`nom_ent`,`nom_tut`,`prenom_tut`,`nom_resp`,`prenom_resp`,`nom_prof`,`prenom_prof`,`intitule` FROM `convention` INNER JOIN `stagiaire` ON `id_sta` = `stagiaire`.`id` INNER JOIN `entreprise` ON `id_ent` = `entreprise`.`id` INNER JOIN `tuteur` ON `id_tut` = `tuteur`.`id` INNER JOIN `responsable` ON `id_resp` = `responsable`.`id` INNER JOIN `professeur` ON `id_prof` = `professeur`.`id` INNER JOIN `classe` ON `convention`.`id_classe` = `classe`.`id` WHERE `convention`.`id` = :id;');
                        //récupérer les résultats de la requête
                        $sth = $dbs->prepare($req);
                        $sth->bindParam(':id', $id);
                        $result = $sth->execute();

                        // parcourir ces résultats
                        if ($result == FALSE) {
                            echo '<pre>';
                            var_dump($sth);
                            print_r($dbs->errorInfo());
                            var_dump($req);

                            echo '</pre>';
                            die();
                        }
                        $tab_r = $sth->fetchAll();
                        // parcourir le tableau avec les résultats
                        foreach ($tab_r as $r) {
                            echo '<tr>';
                            echo '<th>Stagiaire : </th>';
                            echo '<td>';
                            echo $r['nom_sta'] . " " . $r['prenom_sta'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Classe : </th>';
                            echo '<td>';
                            echo $r['intitule'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Enseignant : </th>';
                            echo '<td>';
                            echo $r['nom_prof'] . " " . $r['prenom_prof'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Entreprise : </th>';
                            echo '<td>';
                            echo $r['nom_ent'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Responsable : </th>';
                            echo '<td>';
                            echo $r['nom_resp'] . " " . $r['prenom_resp'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Tuteur : </th>';
                            echo '<td>';
                            echo $r['nom_tut'] . " " . $r['prenom_tut'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Type : </th>';
                            echo '<td>' . $r['type'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Service d\'accueil : </th>';
                            echo '<td>';
                            echo $r['service'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Lieu d\'accueil : </th>';
                            echo '<td>';
                            echo $r['lieu'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Activités confiées : </th>';
                            echo '<td>';
                            echo $r['activites'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Compétences vues : </th>';
                            echo '<td>';
                            echo $r['competences'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Jours particuliers de travail : </th>';
                            echo '<td>';
                            echo $r['jours_particuliers'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';

                            if (isset($r['periode3_debut']) && isset($r['periode3_fin'])) {
                                echo '<th rowspan="3">Périodes du stage : </th>';
                            } elseif (isset($r['periode2_debut']) && isset($r['periode2_fin'])) {
                                echo '<th rowspan="2">Périodes du stage : </th>';
                            } else {
                                echo '<th>Périodes du stage : </th>';
                            }

                            echo '<td>';
                            //date en fr
                            $P1_deb = $r['periode1_debut'];
                            $tab_P1_deb = explode('-', $P1_deb);
                            $P1_deb = $tab_P1_deb[2] . '/' . $tab_P1_deb[1] . '/' . $tab_P1_deb[0];
                            $P1_fin = $r['periode1_fin'];
                            $tab_P1_fin = explode('-', $P1_fin);
                            $P1_fin = $tab_P1_fin[2] . '/' . $tab_P1_fin[1] . '/' . $tab_P1_fin[0];
                            echo '<p style="font-weight: bold">Période 1 :</p> du : ' . $P1_deb . ' au : ' . $P1_fin;
                            echo '</td>';
                            echo '</tr>';

                            echo '</tr>';
                            //date en fr
                            if (isset($r['periode2_debut']) && isset($r['periode2_fin'])) {
                                echo '<td>';
                                $P2_deb = $r['periode2_debut'];
                                $tab_P2_deb = explode('-', $P2_deb);
                                $P2_deb = $tab_P2_deb[2] . '/' . $tab_P2_deb[1] . '/' . $tab_P2_deb[0];
                                $P2_fin = $r['periode2_fin'];
                                $tab_P2_fin = explode('-', $P2_fin);
                                $P2_fin = $tab_P2_fin[2] . '/' . $tab_P2_fin[1] . '/' . $tab_P2_fin[0];
                                echo '<p style="font-weight: bold">Période 2 :</p> du : ' . $P2_deb . ' au : ' . $P2_fin;
                                echo '</td>';
                            }
                            echo '</tr>';
                            echo '<tr>';
                            //date en fr
                            if (isset($r['periode3_debut']) && isset($r['periode3_fin'])) {
                                echo '<td>';
                                $P3_deb = $r['periode3_debut'];
                                $tab_P3_deb = explode('-', $P3_deb);
                                $P3_deb = $tab_P3_deb[2] . '/' . $tab_P3_deb[1] . '/' . $tab_P3_deb[0];
                                $P3_fin = $r['periode3_fin'];
                                $tab_P3_fin = explode('-', $P3_fin);
                                $P3_fin = $tab_P3_fin[2] . '/' . $tab_P3_fin[1] . '/' . $tab_P3_fin[0];
                                echo '<p style="font-weight: bold">Période 3 :</p> du : ' . $P3_deb . ' au : ' . $P3_fin;
                                echo '</td>';
                            }

                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Durée du stage en semaines : </th>';
                            echo '<td>';
                            echo $r['duree_semaines'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Durée du stage en jours ouvrables : </th>';
                            echo '<td>';
                            echo $r['duree_jours'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Avantage : </th>';
                            echo '<td>';
                            echo $r['avantage1'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Avantage : </th>';
                            echo '<td>';
                            echo $r['avantage2'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Jours de congé exceptionnels : </th>';
                            echo '<td>';
                            echo $r['jours_conge'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th>Nombre d\'ETC validée : </th>';
                            echo '<td>';
                            echo $r['nbre_etc'];
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<td colspan ="2">';
                            if ($action == 'liste') {
                                echo '<a href="list_conv_valide.php">retour</a>';
                            } else {
                                echo '<a href="etudiants.php?action=liste&amp;id=' . $id_classe . '">retour</a>';
                            }

                            echo '</td>';
                        }
                    } else {
                        if (!isset($_SESSION['type'])) {
                            echo '<tr>';
                            echo '<td>';
                            echo 'Vous devez vous connecter';
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<td>';
                            echo '<a href="connexion.php">connexion</a>';
                            echo '</td>';
                        } else {
                            echo '<tr>';
                            echo '<td>';
                            echo 'Vous devez fournir la convention souhaitée';
                            echo '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<td>';
                            echo '<a href="etudiants.php">Retour</a>';
                            echo '</td>';
                        }
                    }
                    ?>
                </tr>
            </table>
        </form>
    </body>
</html>