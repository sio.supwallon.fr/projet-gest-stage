<!DOCTYPE html>
<?php
require 'scripts/constante.php';
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);

session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer un étudiant</title>
    </head>
    <body>

        <form action="etudiant_supprimer.php?action=liste&amp;id=<?php echo $id; ?>&amp;id_classe=<?php echo $id_classe; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P" )) {
                echo '<p>êtes vous sûr de vouloir suprimer l\'étudiant ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>
            </form>
            <?php
            if (isset($_GET['id']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_ent = ' DELETE FROM `stagiaire` WHERE stagiaire.id=:id ;';

                //préparation de la requête
                $ent = $dbs->prepare($req_ent);
                $ent->bindParam(':id', $id);
                //execution de la requête
                $resultat_sta = $ent->execute();
                if ($resultat_sta == true) {
                    header("location:etudiants.php?action=liste&id=" . $id_classe);
                } else {
                    print_r($dbs->errorInfo());
                }
            } elseif (isset($_GET['id']) && isset($_POST['btn_non'])) {
                header("location:etudiants.php?action=liste&id=" . $id_classe);
            } elseif (!isset($_GET['id'])) {
                echo'<p>veuillez sélectionner un etudiant</p>';
            }
        } elseif ($_SESSION['type'] != "A" || $_SESSION['type'] != "P") {
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour cette manipulation';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
        } else {
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
        }
        ?>



    </body>
</html>
