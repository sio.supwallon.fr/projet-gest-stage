<!DOCTYPE html>
<?php
require 'scripts/constante.php';
$id = filter_input(INPUT_GET,'id', FILTER_SANITIZE_SPECIAL_CHARS);
header('Content-type: text/html; charset=UTF-8');
session_start();
?>
<html>
    <head>
        <title> INFOprofesseur </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>

        <form action="professeurs.php" method="post">
            <h1>information sur le professeur</h1>
            <?php
 
            ?>
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {
                        $req = ' SELECT login_prof, nom_prof, prenom_prof, discipline_prof, email_prof, telephone_prof FROM professeur WHERE professeur.id = :id;';
                        $sth = $dbs->prepare($req);
                        $sth->bindParam(':id', $id);
                        $sth->execute();
                        $tab = $sth->fetch();
                        echo '<table>';
                        while ($tab != null) {
                            echo '<tr>';
                            echo '<th><p>Login :</p></th>';
                            echo '<td>' . $tab['login_prof'] . '</td>';
                            echo '<tr>';
                            echo '<th><p>Nom :</p></th>';
                            echo '<td>' . $tab['nom_prof'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Prenom :</p></th>';
                            echo '<td>' . $tab['prenom_prof'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>discipline :</p></th>';
                            echo '<td>' . $tab['discipline_prof'] . '</td>';
                            echo '</tr>';
                            echo '<th><p>Telephone :</p></th>';
                            echo '<td>' . $tab['telephone_prof'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Adresse email :</p></th>';
                            echo '<td>' . $tab['email_prof'] . '</td>';
                            echo '</tr>';
                            $tab = $sth->fetch();
                        }
                        ?>
                    <p><input type="submit" value="retour" name="retour" /></p>
                    <?php
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                }
                ?>
            </table>

        </form>
    </body>
</html>

