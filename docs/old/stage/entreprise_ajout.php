<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();



$num_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);


if (isset($_POST['enregistrer'])) {
    //initialisation des variables
    $req2 = true;

    //vérification du remplissage des champs de l'entreprise
    if (empty($_POST['nom_entreprise'])) {
        $req2 = false;
        echo 'le champs nom de l\'entreprise est vide';
    }
    $adresse_ent = filter_input(INPUT_POST, 'adresse_entreprise', FILTER_SANITIZE_SPECIAL_CHARS);
    $adresse_ent = trim($adresse_ent);
    if (empty($adresse_ent)) {
        $req2 = false;
        echo 'le champs adresse de l\'entreprise est vide';
    }
    // Variable à verifier 
    $email = filter_input(INPUT_POST, 'email_entreprise', FILTER_SANITIZE_EMAIL);
// Remove all illegal characters from email
    $email_checked = filter_var($email, FILTER_SANITIZE_EMAIL);
    // Validate e-mail
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $email = strtolower($email_checked);
    } else {
        $req2 = false;
        echo("$email n'est pas une adresse email valide");
    }


    //Ajout de la ville
    //insertion des valeurs de l'entreprise dans la table entreprise
    if ($req2 != false) {
        //déclaration des variables des infos sur l'entreprise'
        $nom_ent = filter_input(INPUT_POST, 'nom_entreprise', FILTER_SANITIZE_STRING);
        $tel = filter_input(INPUT_POST, 'tel_entreprise', FILTER_SANITIZE_SPECIAL_CHARS);
        $naf_entreprise = filter_input(INPUT_POST, 'naf_entreprise', FILTER_SANITIZE_STRING);

        $req_ent = 'INSERT INTO `entreprise` (`nom_ent`,`adresse_ent`,`id_ville`, telephone, email, NAF) Values (:nom_ent, :adresse_ent, :ville, :tel, :email, :NAF);';

        //préparation de la requête
        $ent = $dbs->prepare($req_ent);

        //envoyent des paramètres
        $ent->bindParam(':nom_ent', $nom_ent);
        $ent->bindParam(':adresse_ent', $adresse_ent);
        $ent->bindParam(':ville', $num_ville);

        $ent->bindParam(':tel', $tel);
        $ent->bindParam(':email', $email);
        $ent->bindParam(':NAF', $naf_entreprise);

        //execution de la requête		
        $resultat_ent = $ent->execute();

        if ($resultat_ent == true) {
            echo '<p>les données sont enregistrées</p>';
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> Ajout d'une entreprise </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />

    </head>
    <body>
        <?php
        echo '<form action="entreprise_ajout.php?id_ville=' . $num_ville . '" method="post">';
//http://localhost/Stage_Jeremy3/
        ?>
        <table>
            <?php
            if (isset($_SESSION['type'])) {
                ?>
                <tr>
                    <th>Departement :</th>
                    <td>
                        <?php
                        //faire la requête
                        $req_c = ('SELECT `departement` FROM `ville` WHERE `id` = :num_ville;');
                        $sth_c = $dbs->prepare($req_c);
                        //envoyent des paramètres
                        $sth_c->bindParam(':num_ville', $num_ville);
                        //récupérer les résultats de la requête
                        $sth_c->execute();

                        // parcourir ces résultats
                        $tab_r_c = $sth_c->fetch();

                        // parcourir le tableau avec les résultats
                        while ($tab_r_c != null) {
                            echo $tab_r_c['departement'];
                            $tab_r_c = $sth_c->fetch();
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Ville:</th>
                    <td>
                        <?php
                        //faire la requête
                        $req_c = ('SELECT `nom_ville` FROM `ville` WHERE `id` = :num_ville;');
                        $sth_c = $dbs->prepare($req_c);
                        //envoyent des paramètres
                        $sth_c->bindParam(':num_ville', $num_ville);
                        //récupérer les résultats de la requête
                        $sth_c->execute();
                        // parcourir ces résultats
                        $tab_r_c = $sth_c->fetch();

                        // parcourir le tableau avec les résultats
                        while ($tab_r_c != null) {
                            echo $tab_r_c['nom_ville'];
                            $tab_r_c = $sth_c->fetch();
                        }
                        ?>
                    </td>
                </tr>
                <?php
                ?>
                <tr>
                    <th>Raison Sociale de entreprise :</th>

                    <?php
                    echo '<td><input type="text" name="nom_entreprise" placeholder="Raison Social" value ="" required/></td>';
                    ?>
                </tr>
                <tr>
                    <th>Adresse de l'entreprise :</th>
                    <td>
                      <textarea name="adresse_entreprise" cols="33,8" rows="2" required></textarea>
                    </td>
                    
                    <?php
  
                    //echo '<td><input type="text" name="adresse_entreprise" placeholder="Adresse" value ="" required/></td>';
                    echo "</tr>";
                    ?>

                <tr>
                    <th>Numéro de téléphone de l'entreprise :</th>
                    <?php echo'<td><input type="tel"  size="20" minlength="10" maxlength="10" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" name="tel_entreprise" placeholder="N°Téléphone" required/></td>'; ?>
                </tr>
                <tr>
                    <th>Adresse email de l'entreprise :</th>
                    <?php echo'<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_entreprise" placeholder="Adresse email" required/></td>'; ?>
                </tr>
                <tr>
                    <th>Code NAF/APE de l'entreprise :</th>
                    <?php echo'<td><input type="text" size ="5"  maxlength="5" name="naf_entreprise" placeholder="Code NAF" required/></td>'; ?>
                </tr>

                <tr>
                    <td>
                        <button class="bouton" type="submit" name="enregistrer">Enregistrer l'entreprise</button>
                    </td>
                    <td>
                        <a href="entreprises.php?action=liste&amp;id_ville=<?php echo $num_ville; ?>">retour</a>
                    </td>
                </tr>
                <?php
            } elseif (!isset($_SESSION['type'])) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            } elseif (!isset($_GET['id_ville']) && isset($_SESSION['type'])) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez choisir une ville';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="entreprises.php">Retour</a>';
                echo '</td>';
            }
            ?>		
        </table>
    </form>

</body>
</html>
