<?php
require 'scripts/constante.php';

// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();
//http://localhost/Stage_Jeremy3/
//ouvrir la connexion
if (isset($_SESSION['type']) && $_SESSION['type'] == "P") {
    //Requête de récupération du professeur connecté

    $req = ('SELECT `id`,`nom_prof`,`prenom_prof` FROM `professeur` WHERE `id` =:id;');
    $sth = $dbs->prepare($req);
    $sth->bindParam(':id', $_SESSION['id']);
    $sth->execute();
    $tab_r = $sth->fetchAll();
} else {
    //faire la requête
    $req = ('SELECT `id`,`nom_prof`,`prenom_prof` FROM `professeur`;');
    $result = $dbs->query($req);
    $tab_r = $result->fetchAll();
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des professeurs </title>
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="professeurs.php" method="post">



            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
                if ($_SESSION['type'] != "A") {
                    echo '<h1>enseignant :</h1>';
                } else {
                    ?>
                    <h1>Liste des enseignants :</h1>
                    <?php
                }
                echo '<p><a href="index.php">retour</a></p>';

                //Récupère et affiche les enseignants ainsi qu'un lien pour modifier l'enseignant
                //récupérer les résultats de la requête
                // parcourir ces résultats
                // parcourir le tableau avec les résultats
                foreach ($tab_r as $r) {
                    echo '<table class="prof">';
                    echo '<tr>';
                    echo '<td> ';
                    echo $r['nom_prof'] . ' ' . $r['prenom_prof'];
                    echo '</td>';
                    echo '<td>';
                    echo '<table>';
                    if ($_SESSION['type'] == "A") {
                        echo '<tr>';
                        echo '<td>';
                        echo '<p><a href="professeur_ajout_classe.php?id_prof=' . $r['id'] . '">Ajouter une classe</a></p>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="infoProfesseur.php?id=' . $r['id'] . '">informations</a>';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="professeur_modif.php?id=' . $r['id'] . '">modification</a>';
                        echo '</td>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="prof_supprimer.php?id=' . $r['id'] . '">supprimer</a>';
                        echo '</td>';
                        echo '</tr>';
                        echo '</tr>';
                        echo '</table>';
                        echo '</td>';
                        echo '</tr>';
                    } else {
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="professeur_modif.php?id=' . $r['id'] . '">modification</a>';
                        echo '</td>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="infoProfesseur.php?id=' . $r['id'] . '">informations</a>';
                        echo '</td>';
                        echo '</tr>';
                        echo '</tr>';
                        echo '</table>';
                    }


                    //faire la requête
                    $req_c = ('SELECT `id`,`intitule` FROM `classe` INNER JOIN `suivre` ON `id` = `id_classe` WHERE `id_professeur` = :id_prof;'); //Récupère l'étudiant choisi précédemment
                    //récupérer les résultats de la requête
                    $sth = $dbs->prepare($req_c);
                    $sth->bindParam(':id_prof', $r['id']);
                    $sth->execute();

                    // parcourir ces résultats
                    $tab_r_c = $sth->fetchAll();
                    // parcourir le tableau avec les résultats
                    echo '<tr>';
                    echo '<td>';
                    echo '<p class="italic">enseigne dans les classes:</p>';
                    echo '</td>';
                    echo '<td></td>';
                    echo '</tr>';
                    foreach ($tab_r_c as $r_c) {
                        if ($_SESSION['type'] == "A") {
                            echo '<tr>';
                            echo '<td>';
                            echo $r_c['intitule'];
                            echo '</td><td><a href="professeur_suppr_classe.php?id_prof=' . $r['id'] . '&amp;id_classe=' . $r_c['id'] . '">retirer la classe</a></td>';
                            echo '</tr>';
                        } else {
                            echo '<tr>';
                            echo '<td>';
                            echo $r_c['intitule'];
                            echo '</td><td></td>';
                            echo '</tr>';
                        }
                    }
                    echo '</table>';
                }
                if ($_SESSION['type'] == "A") {
                    ?>
                    <p><a href="professeur_ajout.php">Ajouter un enseignant</a></p>
                    <?php
                }
            } elseif (!isset($_SESSION['type'])) {
                echo '<table>';
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
                echo '</table>';
            } else {
                echo '<table>';
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez n\'avez pas le droit de gérer les enseignants';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
                echo '</table>';
            }
            ?>


    </body>
</html>