<!DOCTYPE html>
<?php
require 'scripts/constante.php';
session_start();

$id_resp = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer un responsable</title>
    </head>
    <body>

        <form action="responsable_suppr.php?id=<?php echo $id_resp; ?>&amp;id_ent=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || ($_SESSION['type'] == "P" ))) {
                echo '<p>êtes vous sûr de vouloir suprimer le responsable ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>

            </form>

            <?php
            if (isset($_GET['id']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_resp = ' DELETE FROM `responsable` WHERE id=:id ;';

                //préparation de la requête
                $resp = $dbs->prepare($req_resp);
                $resp->bindParam(':id', $id_resp);
                //execution de la requête
                $resultat_resp = $resp->execute();

                if ($resultat_resp == true) {
                    header('location:responsable.php?id=' . $id_ent . '&id_ville=' . $id_ville);
                } else {
                    echo '<pre>';
                    var_dump($resultat_resp);
                    print_r($dbs->errorInfo());
                    var_dump($req_resp);
                    print_r($resp);
                    var_dump($id_resp);
                    echo '</pre>';
                    die();
                }
            } elseif (isset($_GET['id']) && isset($_POST['btn_non'])) {
                header('location:responsable.php?id_ville=' . $id_ville . '&id=' . $id_ent);
            } elseif (!isset($_GET['id'])) {
                echo'<p>veuillez sélectionner un responsable</p>';
            }
        } elseif ($_SESSION['type'] != "A" || ($_SESSION['type'] != "P" )) {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour cette manipulation';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        } else {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        }
        ?>




    </body>
</html>
