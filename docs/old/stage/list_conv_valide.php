<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
session_start();
//ouvrir la connexion
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css"> 
        <title>liste des conventions valide</title>
    </head>
    <body>
        <h1>Liste des conventions valider</h1>
        <table>
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {
                $req = ' SELECT convention.`id`,`type`,`periode1_debut`,`periode1_fin`,`periode2_debut`,`periode2_fin`,`periode3_debut`,`periode3_fin`,`nom_ent`,convention.`id_classe`, `id_prof`, `nom_sta`, `prenom_sta` '
                        . ' FROM convention '
                        . ' INNER JOIN entreprise ON `id_ent` = entreprise.`id` '
                        . ' INNER JOIN stagiaire ON `id_sta` = stagiaire.`id` '
                        . ' WHERE `validation`=1;';

                $res = $dbs->query($req);
                if ($res == true) {
                    $tab = $res->fetch();
                    if ($tab == null) {
                        echo '<p>il n\'y a pas encore de convention validées</p>';
                    } else {
                        while ($tab == true) {
                            ?>
                            <tr>
                                <td>
                                    <?php
                                    echo $tab['nom_sta'] . " " . $tab['prenom_sta'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $P1_deb = $tab['periode1_debut'];
                                    $tab_P1_deb = explode('-', $P1_deb);
                                    $P1_deb = $tab_P1_deb[2] . '/' . $tab_P1_deb[1] . '/' . $tab_P1_deb[0];
                                    $P1_fin = $tab['periode1_fin'];
                                    $tab_P1_fin = explode('-', $P1_fin);
                                    $P1_fin = $tab_P1_fin[2] . '/' . $tab_P1_fin[1] . '/' . $tab_P1_fin[0];

                                    if ($tab['periode2_debut'] != "") {
                                        $et = " et du ";
                                        $flash = " au ";
                                        $P2_deb = $tab['periode2_debut'];
                                        $tab_P2_deb = explode('-', $P2_deb);
                                        $P2_deb = $tab_P2_deb[2] . '/' . $tab_P2_deb[1] . '/' . $tab_P2_deb[0];
                                        $P2_fin = $tab['periode2_fin'];
                                        $tab_P2_fin = explode('-', $P2_fin);
                                        $P2_fin = $tab_P2_fin[2] . '/' . $tab_P2_fin[1] . '/' . $tab_P2_fin[0];
                                    } else {
                                        $et = "";
                                        $flash = "";
                                    }
                                    if ($tab['periode3_debut'] != "") {
                                        $et2 = " et du ";
                                        $flash2 = " au ";
                                        $P3_deb = $tab['periode3_debut'];
                                        $tab_P3_deb = explode('-', $P3_deb);
                                        $P3_deb = $tab_P3_deb[2] . '/' . $tab_P3_deb[1] . '/' . $tab_P3_deb[0];
                                        $P3_fin = $tab['periode3_fin'];
                                        $tab_P3_fin = explode('-', $P3_fin);
                                        $P3_fin = $tab_P3_fin[2] . '/' . $tab_P3_fin[1] . '/' . $tab_P3_fin[0];
                                    } else {
                                        $et2 = "";
                                        $flash2 = "";
                                    }
                                    if ($tab['periode3_debut'] != "") {
                                        echo '<a href="infoConvention.php?action=liste&amp;id_classe=' . $tab['id_classe'] . '&amp;id=' . $tab['id'] . '">' . $tab['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . ' ' . $et . ' ' . $P2_deb . $flash . $P2_fin . ' ' . $et2 . ' ' . $P3_deb . $flash2 . $P3_fin . '</a>';
                                    } elseif ($tab['periode2_debut'] != "") {
                                        echo '<a href="infoConvention.php?action=liste&amp;id_classe=' . $tab['id_classe'] . '&amp;id=' . $tab['id'] . '">' . $tab['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . ' ' . $et . ' ' . $P2_deb . $flash . $P2_fin . '</a>';
                                    } else {
                                        echo '<a href="infoConvention.php?action=liste&amp;id_classe=' . $tab['id_classe'] . '&amp;id=' . $tab['id'] . '">' . $tab['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . '</a>';
                                    }
                                    ?>
                                </td>
                                <?php if ($_SESSION['type'] == "A") { ?>
                                    <td>
                                        <a href="pdf.php?id_conv=<?php echo $tab['id']; ?>" target="_blank">éditer un pdf</a>

                                    </td>
                                <?php } ?>
                            </tr>
                            <?php
                            $tab = $res->fetch();
                        }
                    }
                } else {
                    echo '<p>erreur de requête</p>';
                    echo '<pre>';
                    var_dump($res);
                    print_r($dbs->errorInfo());
                    var_dump($req);
                    echo '</pre>';
                    die();
                }
                echo '<p><a href="gestion_convention.php">retour</a></p>';
            } else {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter avec un compte abilité';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            }
            // put your code here
            ?>
        </table>
    </body>
</html>
