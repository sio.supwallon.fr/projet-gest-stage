<!DOCTYPE html>
<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();
?>
<html>
    <head>
        <title> INFOentreprises </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>

        <form action="entreprises.php" method="post">
            <h1>information sur l'entreprise</h1>
            <?php
            //ouverture de la connexion
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
            $id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
            ?>
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {

                        echo '<p><a href="entreprises.php?action=liste&amp;id_ville=' . $id_ville . '">retour</a></p>';

                        $req = ' SELECT nom_ent, adresse_ent, telephone, email, NAF FROM entreprise '
                                . ' WHERE entreprise.id =:id;';
                        $sth = $dbs->prepare($req);
                        $sth->bindParam(':id', $id);
                        $sth->execute();
                        $tab = $sth->fetch();
                        echo '<table>';
                        while ($tab != null) {
                            echo '<tr>';
                            echo '<th><p>Raison Sociale :</p></th>';
                            echo '<td>' . $tab['nom_ent'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Adresse :</p></th>';
                            echo '<td>' . $tab['adresse_ent'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Telephone :</p></th>';
                            echo '<td>' . $tab['telephone'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Adresse Email :</p></th>';
                            echo '<td>' . $tab['email'] . '</td>';
                            echo '</tr>';
                            echo '<tr>';
                            echo '<th><p>Code NAF :</p></th>';
                            echo '<td>' . $tab['NAF'] . '</td>';
                            echo '</tr>';
                            $tab = $sth->fetch();
                        }
                        echo '</table>';
                    } else {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez vous connecter';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="connexion.php">connexion</a>';
                        echo '</td>';
                    }
                    ?>


                    </form>
                    </body>
                    </html>
