<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_ent = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$id_ville_ent = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_NUMBER_INT);
session_start();
if (isset($_GET['id'])) {
    //Récupère l'id de la classe sélectionnée dans la page "etudiants.php"
    $num = $id_ent;

    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {
        $er = false;
        if (trim($_POST['adresse_ent']) == "") //(empty($_POST['adresse_ent']))
        {
          $er = true;
          echo 'l\'adresse n\'est pas remplie </br>';
        }

        if (empty($_POST['departement_ent']))
        {
          $er = true;
          echo 'Le département n\'est pas rempli </br>';
        } 

        if (empty($_POST['ville_ent']))
        {
          $er = true;
          echo 'La ville n\'est pas remplie </br>';
        } 

        // Variable à verifier 
        $email_ent = filter_input(INPUT_POST, 'email_ent', FILTER_SANITIZE_EMAIL);
        // Remove all illegal characters from email
        $email_checked = filter_var($email_ent, FILTER_SANITIZE_EMAIL);
        // Validate e-mail
        if (filter_var($email_ent, FILTER_VALIDATE_EMAIL))
        {
            $email_ent = strtolower($email_checked);
        }
        else
        {
            $er = false;
            echo("$email_ent n'est pas une adresse email valide");
        }

        if($er == false)
        {
          //faire la requête de modification
          $req_sta = 'UPDATE `entreprise` '
                  . 'SET `nom_ent` = :nom_ent, '
                  . '`adresse_ent` = :adresse_ent, '
                  . '`id_ville` = :ville_ent, '
                  . '`telephone` = :telephone_ent, '
                  . '`email` = :email_ent, '
                  . '`NAF` = :naf '
                  . 'WHERE `id` = :id;'; //Récupère l'étudiant choisi précédemment
          
          //préparation de la requête
          $sta = $dbs->prepare($req_sta);
          //récupère l'adresse sans les espaces  
          $ad = filter_input(INPUT_POST, 'adresse_ent', FILTER_SANITIZE_SPECIAL_CHARS);
          $adresse = trim($ad);
          $sta->bindParam(':adresse_ent', $adresse);
          
          //envoyent des paramètres
          $nom_ent = filter_input(INPUT_POST, 'nom_ent', FILTER_SANITIZE_STRING);
          $sta->bindParam(':nom_ent', $nom_ent);
  
          $telephone_ent = filter_input(INPUT_POST, 'telephone_ent', FILTER_SANITIZE_SPECIAL_CHARS);
          $sta->bindParam(':telephone_ent', $telephone_ent);
  
          $sta->bindParam(':email_ent', $email_ent);
  
          $NAF = filter_input(INPUT_POST, 'NAF', FILTER_SANITIZE_SPECIAL_CHARS);
          $sta->bindParam(':naf', $NAF);
  
          $ville_ent = filter_input(INPUT_POST, 'ville_ent', FILTER_SANITIZE_SPECIAL_CHARS);
          $sta->bindParam(':ville_ent', $ville_ent);
  
          $sta->bindParam(':id', $id_ent);
  
          //execution de la requête		
          $resultat_ent = $sta->execute();
          if ($resultat_ent == true) {
              echo '<p>données enregistrées</p>';
          } else {
              echo "<pre>";
              echo "Erreur : ";
              print_r($sta->errorInfo()[2]);
              echo "</pre>";
          }
        }
    }
} else
{
    $num = 0;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> Modification d'une entreprise </title>
        <meta charset=utf-8" />
        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>
        <?php //http-equiv="Content-Type" content="text/html;  type="text/css" 
        echo '<form action="entreprise_modif.php?action=liste&amp;id_ville=' . $id_ville_ent . '&amp;id=' . $num . '" method="post">';
        ?>
        <table>
            <h1>modifier l'entreprise</h1>
            <?php
            if (isset($_SESSION['type']) && isset($_GET['id'])) {
                //Si le département a été choisi 
                if (isset($_POST['departement_ent'])) {

                    //Affiche les valeurs mises dans les zones précédemment (celles récupérées ou celles modifiées)
                    ?>
                    <tr>
                        <th>Raison Sociale :</th>
                        <?php
                        echo '<td><input type="text" name="nom_ent" value ="' . $_POST['nom_ent'] . '" required/></td>';
                        ?>
                    </tr>


                    <tr>
                        <th>Adresse :</th>
                        <td>
                            <textarea name="adresse_ent" cols="33,8" rows="2" required><?php echo trim($_POST['adresse_ent']) ?></textarea>
                        </td>

                    </tr>
                    <tr>
                        <th>Département :</th>
                        <td>
                            <select name="departement_ent" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req_d = ('SELECT distinct `departement` FROM `ville`;');
                                //récupérer les résultats de la requête
                                $result_d = $dbs->query($req_d);
                                // parcourir ces résultats
                                $tab_r_d = $result_d->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r_d as $r_d) {
                                    //Si le département en cours est égal au département choisi précédemment
                                    if ($r_d['departement'] == $_POST['departement_ent'])
                                    //Sélectionne le département en cours
                                        echo '<option value="' . $r_d['departement'] . '" selected>' . $r_d['departement'] . '</option>';
                                    else
                                        echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Ville :</th>
                        <td>
                            <select name="ville_ent" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête de sélection des villes du département choisi
                                $dep = filter_input(INPUT_POST, 'departement_ent', FILTER_SANITIZE_SPECIAL_CHARS);
                                $req_v = ('SELECT `id`,`code_postal`,`nom_ville` FROM `ville` WHERE departement = :dep ORDER BY `nom_ville`;');
                                $sth_v = $dbs->prepare($req_v);
                                $sth_v->bindParam(':dep', $dep);
                                $resultat = $sth_v->execute();
                                
                                // parcourir ces résultats
                                $tab_r_v = $sth_v->fetchAll();
                                
//                                echo "</select>";
//                                echo "<pre>";
//                                var_dump($dep);
//                                var_dump($sth_v);
//                                var_dump($resultat);
//                                var_dump($tab_r_v);
//                                echo "</pre>";
//                                die();
                                
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r_v as $r_v) {
                                    //Si l'id de la ville en cours est égal à l'id de la ville sélectionnée précédemment
                                    if ($r_v['id'] == $_POST['ville_ent'])
                                    //Sélectionne la ville en cours
                                        echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                    else
                                        echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Téléphone :</th>
                        <?php
                        echo'<td><input type="tel"  size="20" minlength="10" maxlength="10" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" name="telephone_ent" placeholder="N° Téléphone" value ="' . $_POST['telephone_ent'] . '" required/></td>';
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Email :</th>
                        <?php
                        echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_ent" placeholder="pauldurand@gmail.com"  value ="' . $_POST['email_ent'] . '" required/></td>';
                        ?>
                    </tr>

                    <tr>
                        <th>Code NAF/APE de l'entreprise :</th>
                        <?php echo'<td><input type="text" size ="5"  maxlength="5" name="NAF" value ="' . $_POST['NAF'] . '" placeholder="Code NAF" required/></td>'; ?>
                    </tr>

                    <?php
                }
                else {
                    //faire la requête de sélection de l'étudiant choisi dans la page précédente
                    $req = ('SELECT `entreprise`.`id`,`nom_ent`,`adresse_ent`,`telephone`,`email`,`entreprise`.`id_ville`,`departement`, `NAF` FROM `entreprise` INNER JOIN `ville` ON `entreprise`.`id_ville` = `ville`.`id` WHERE `entreprise`.`id` =:num;');
                    $sth = $dbs->prepare($req);
                    $sth->bindParam(':num', $num);
                    $sth->execute();
                    // parcourir ces résultats
                    $tab_r = $sth->fetchAll();
                    // parcourir le tableau avec les résultats
                    foreach ($tab_r as $r) {
                        //Affiche les informations de l'étudiant récupérées
                        ?>

                        <tr>
                            <th>Raison Sociale :</th>
                            <?php
                            echo '<td><input type="text" name="nom_ent" value ="' . $r['nom_ent'] . '" required/></td>';
                            ?>
                        </tr>

                        <tr>
                            <th>Adresse :</th>
                            <td>
                                <textarea name="adresse_ent" cols="33,8" rows="2"> <?php echo trim($r['adresse_ent']) ?></textarea>
                            </td>

                        </tr>
                        <tr>
                            <th>Département :</th>
                            <td>
                                <select name="departement_ent" onchange="this.form.submit()">
                                    <option value=''>Faites votre choix</option>
                                    <?php
                                    //faire la requête
                                    $req_d = ('SELECT distinct `departement` FROM `ville`;');
                                    //récupérer les résultats de la requête
                                    $result_d = $dbs->query($req_d);
                                    // parcourir ces résultats
                                    $tab_r_d = $result_d->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    //rempli la liste des départements en sélectionnant celui de l'étudiant récupéré
                                    foreach ($tab_r_d as $r_d) {
                                        if ($r_d['departement'] == $r['departement'])
                                            echo '<option value="' . $r_d['departement'] . '" selected>' . $r_d['departement'] . '</option>';
                                        else
                                            echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Ville :</th>
                            <td>
                                <select name="ville_ent" onchange="this.form.submit()">
                                    <option value=''>Faites votre choix</option>
                                    <?php
                                    //faire la requête
                                    $req_v = ('SELECT `id`,`code_postal`,`nom_ville` FROM `ville` WHERE departement = :dep ORDER BY `nom_ville`;');
                                    $sth_v = $dbs->prepare($req_v);
                                    $sth_v->bindParam(':dep', $r['departement']);
                                    $sth_v->execute();
                                    // parcourir ces résultats
                                    $tab_r_v = $sth_v->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    //Rempli la liste des villes en sélectionnant celle de l'étudiant récupéré
                                    foreach ($tab_r_v as $r_v) {
                                        if ($r_v['id'] == $r['id_ville'])
                                            echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                        else
                                            echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Téléphone :</th>
                            <?php
                            echo '<td><input type="tel" name="telephone_ent" maxlength="10"  value ="' . $r['telephone'] . '"required/>';
                            ?>
                            </td>

                        </tr>
                        <tr>
                            <th>Email :</th>
                            <?php
                            echo '<td><input type="email" name="email_ent" placeholder="pauldurand@gmail.com"  value ="' . $r['email'] . '" required/></td>';
                            ?>

                        </tr>

                        <th>NAF :</th>
                        <td>

                            <input type="text" name="NAF" maxlength="5" value="<?php echo $r['NAF']; ?>" required/>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td>
                    <button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>
                </td>
                <td>
                    <a href="entreprises.php?action=liste&amp;id_ville=<?php echo $id_ville_ent; ?>">retour</a>
                </td>
                <?php
            }
            elseif (!isset($_SESSION['type'])) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            } else {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez choisir une entreprise';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="entreprises.php">Retour</a>';
                echo '</td>';
            }
            ?>
        </tr>
    </table>

</form>
</body>
</html>
