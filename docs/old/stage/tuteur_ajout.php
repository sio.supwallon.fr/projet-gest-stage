<?php
require 'scripts/constante.php';
header('Content-type: text/html; charset=UTF-8');
session_start();

$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($_POST['enregistrer'])) {
    //initialisation des variables
    $req2 = true;

    //vérification du remplissage des champs de l'entreprise
    if (empty($_POST['nom_tut'])) {
        $req2 = false;
        echo 'le champs nom du tuteur est vide';
    }
    if (empty($_POST['telephone_tut'])) {
        $req2 = false;
        echo 'le champs telephone du tuteur est vide';
    }
    if (empty($_POST['email_tut'])) {
        $req2 = false;
        echo 'le champs email du tuteur est vide';
    }


    //Ajout de la ville
    //insertion des valeurs de l'entreprise dans la table entreprise
    if ($req2 != false) {
        //déclaration des variables des infos sur l'entreprise'
        $nom_tut = filter_input(INPUT_POST, 'nom_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $nom_tut = strtoupper($nom_tut);
        $prenom_tut = filter_input(INPUT_POST, 'prenom_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom_tut = strtolower($prenom_tut);
        $prenom_tut = ucwords($prenom_tut);
        $pos = strpos($prenom_tut, "-");
        if($pos !== false)
        {
          $prenom_tut = substr($prenom_tut,0,$pos+1) . strtoupper(substr($prenom_tut,$pos+1,1)) . substr($prenom_tut,$pos+2);
        }
        $fonction_tut = filter_input(INPUT_POST, 'fonction_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $tel = filter_input(INPUT_POST, 'telephone_tut', FILTER_SANITIZE_SPECIAL_CHARS);
        $email = filter_input(INPUT_POST, 'email_tut', FILTER_SANITIZE_SPECIAL_CHARS);

        $req_tut = 'INSERT INTO `tuteur` (`nom_tut`,`prenom_tut`,`fonction_tut`, telephone_tut, email_tut, id_entreprise) Values (:nom_tut, :prenom_tut, :fonction_tut, :tel_tut, :email_tut, :id_ent);';

        //préparation de la requête
        $tut = $dbs->prepare($req_tut);

        //envoyent des paramètres
        $tut->bindParam(':nom_tut', strtoupper($nom_tut));
        $tut->bindParam(':prenom_tut', ucwords($prenom_tut));
        $tut->bindParam(':fonction_tut', $fonction_tut);

        $tut->bindParam(':tel_tut', $tel);
        $tut->bindParam(':email_tut', $email);
        $tut->bindParam(':id_ent', $id_ent);

        //execution de la requête		
        $resultat_tut = $tut->execute();

        if ($resultat_tut == true) {
            echo '<p>les données sont enregistrées</p>';
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> Ajout d'un tuteur </title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="scripts/style.css" />

    </head>
    <body>
        <h1>Ajout d'un tuteur</h1>
        <?php
        echo '<form action="tuteur_ajout.php?id_ville=' . $id_ville . '&amp;id_ent=' . $id_ent . '" method="post">';
//http://localhost/Stage_Jeremy3/
        ?>
        <table>
            <?php
            if (isset($_SESSION['type'])) {
                ?>

                <tr>
                    <th>Nom :</th>
                    <?php
                    echo '<td><input type="text" name="nom_tut" placeholder="Nom tuteur" value ="" required/></td>';
                    ?>
                </tr>
                <tr>
                    <th>Prenom :</th>
                    <?php
                    echo '<td><input type="text" name="prenom_tut" placeholder="prenom tuteur" value ="" required/></td>';
                    echo "</tr>";
                    ?>
                <tr>
                <tr>
                    <th>fonction :</th>
                    <?php
                    echo '<td><input type="text" name="fonction_tut" placeholder="fonction du tuteur" value ="" required/></td>';
                    echo "</tr>";
                    ?>
                <tr>
                    <th>N°téléphone du tuteur :</th>
                    <?php echo'<td><input type="tel" size="20" minlength="10" maxlength="10" name="telephone_tut" placeholder="N° Téléphone" required/></td>'; ?>
                </tr>
                <tr>
                    <th>Adresse email du tuteur :</th>
                    <?php echo'<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_tut" placeholder="Adresse email" required/></td>'; ?>
                </tr>


                <tr>
                    <td>
                        <button class="bouton" type="submit" name="enregistrer">Enregistrer le tuteur</button>
                    </td>
                    <td>
                        <a href="tuteur.php?action=liste&amp;id=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>">retour</a>
                    </td>
                </tr>
                <?php
            } elseif (!isset($_SESSION['type'])) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez vous connecter';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="connexion.php">connexion</a>';
                echo '</td>';
            } elseif (!isset($_GET['id_ent']) && (isset($_SESSION['type']))) {
                echo '<tr>';
                echo '<td>';
                echo 'Vous devez choisir une entreprise';
                echo '</td>';
                echo '</tr>';
                echo '<tr>';
                echo '<td>';
                echo '<a href="entreprises.php">retour</a>';
                echo '</td>';
            }
            ?>		
        </table>
    </form>

</body>
</html>
