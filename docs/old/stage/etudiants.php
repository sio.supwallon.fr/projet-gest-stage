<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_SPECIAL_CHARS);
switch ($action) {
    case 'liste':
        $_POST['classe'] = $id_classe;
        break;
    case null:
        break;

    default :
        echo 'erreur';
        break;
}

session_start();
//http://localhost/Stage_Jeremy3/
//ouvrir la connexion

if (isset($_POST['classe']) && $_POST['classe'] != "") {
    $classe = $_POST['classe'];
}
if ((isset($_SESSION['type'])) && ($_SESSION['type'] == "S")) {
    //Requête de récupération du stagiaire connecté
    $req_e = (' SELECT `nom_sta`,`prenom_sta`, `id_classe` FROM `stagiaire` WHERE `id` =' . $_SESSION['id'] . ';'); //Récupère l'étudiant choisi précédemment
    //récupérer les résultats de la requête
    $result_e = $dbs->query($req_e);
    // parcourir ces résultats
    $tab_r_e = $result_e->fetch();
    // parcourir le tableau avec les résultats
    while ($tab_r_e != null) {
        $classe = $tab_r_e['id_classe'];
        $tab_r_e = $result_e->fetch();
    }
    //Requête de sélection de la classe du stagiaire connecté
    $req_c = ('SELECT `id`,`intitule` FROM `classe` WHERE `id` =' . $classe . ' ORDER BY intitule;');
    //faire la requête
    $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =' . $classe . ' AND `id` =' . $_SESSION['id'] . ';'); //Récupère l'étudiant choisi précédemment
} elseif (isset($_SESSION['type']) && $_SESSION['type'] == "P") {
    //Requête de sélection des classes de l'enseignant connecté
    $req_c = ('SELECT `id`,`intitule` FROM `classe` INNER JOIN `suivre` ON `id` = `id_classe` WHERE `id_professeur` =' . $_SESSION['id'] . ';');
    if ((isset($_POST['classe']) && $_POST['classe'] != "") || isset($classe))
        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =' . $classe . ';'); //Récupère l'étudiant choisi précédemment
    else
        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire`;'); //Récupère l'étudiant choisi précédemment  }
}
else {
    //faire la requête
    $req_c = ('SELECT `id`,`intitule` FROM `classe` ORDER BY intitule;');
    //Si la classe a été choisie
    if ((isset($_POST['classe']) && $_POST['classe'] != "") || isset($classe))
        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =' . $classe . ';');
    else {
        //$req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire`;'); //Récupère l'étudiant choisi précédemment
    }
}

//if (isset($_GET['id_conv_modif'])) {
//    //Requête
//    $req = 'SELECT `convention`.`id` id_conv, `activites`, `competences`, `periode1`, `duree_semaines`, `duree_jours`, `nom_sta`, `prenom_sta`, `sexe_sta`, '
//            . '`num_ss_sta`, `date_naissance_sta`, `lieu_naissance_sta`, `adresse_sta`, `telephone_sta`, `email_sta`, `caisse_sta`, `v1`.`nom_ville` ville_sta, '
//            . '`intitule`, `description`, `nom_ent`, `adresse_ent`, `v2`.`nom_ville` ville_ent, `nom_tut`, `prenom_tut`, `fonction_tut`, `telephone_tut`, '
//            . '`email_tut`, `nom_resp`, `prenom_resp`, `fonction_resp`, `telephone_resp`, `email_resp`, `nom_prof`, `prenom_prof`, `discipline_prof`, '
//            . '`telephone_prof`, `email_prof`, `classe`.`id` classe_id '
//            . 'FROM `convention` INNER JOIN `stagiaire` ON `id_sta`=`stagiaire`.`id` '
//            . 'INNER JOIN `ville` v1 ON `stagiaire`.`id_ville`=`v1`.`id` '
//            . 'INNER JOIN `classe` ON `convention`.`id_classe`=`classe`.`id` '
//            . 'INNER JOIN `entreprise` ON `convention`.`id_ent`=`entreprise`.`id` '
//            . 'INNER JOIN `ville` v2 ON `entreprise`.`id_ville`=`v2`.`id` '
//            . 'INNER JOIN `tuteur` ON `id_tut`=`tuteur`.`id` '
//            . 'INNER JOIN `responsable` ON `id_resp`=`responsable`.`id` '
//            . 'INNER JOIN `professeur` ON `id_prof`=`professeur`.`id` '
//            . 'WHERE `convention`.`id` = ' . $_GET['id_conv_modif'] . ';';
//    //récupérer les résultats de la requête
//    $result = $dbs->query($req);
//    // parcourir ces résultats
//    $tab_r = $result->fetchAll();
//    // parcourir le tableau avec les résultats
//    foreach ($tab_r as $r) {
//        $erreur = false;
//        if (empty($r['activites']) || empty($r['competences']) || empty($r['periode1']) || empty($r['duree_semaines']) || empty($r['duree_jours'])) {
//            echo 'la convention n\'est pas complète<br>';
//            $erreur = true;
//        }
//        if (empty($r['nom_sta']) || empty($r['prenom_sta']) || empty($r['sexe_sta']) || empty($r['num_ss_sta']) || empty($r['date_naissance_sta']) || empty($r['lieu_naissance_sta']) || empty($r['adresse_sta']) || empty($r['telephone_sta']) || empty($r['email_sta']) || empty($r['caisse_sta']) || empty($r['ville_sta'])) {
//            echo 'l\'étudiant n\'a pas fourni tous ses renseignements<br>';
//            $erreur = true;
//        }
//        if (empty($r['intitule']) || empty($r['description'])) {
//            echo 'la classe n\'est pas renseignée correctement<br>';
//            $erreur = true;
//        }
//        if (empty($r['nom_ent']) || empty($r['adresse_ent']) || empty($r['ville_ent'])) {
//            echo 'l\'entreprise n\'est pas renseignée correctement<br>';
//            $erreur = true;
//        }
//        if (empty($r['nom_tut']) || empty($r['prenom_tut']) || empty($r['fonction_tut']) || empty($r['nom_resp']) || empty($r['prenom_resp']) || empty($r['fonction_resp']) || (empty($r['telephone_resp']) && empty($r['telephone_tut'])) || (empty($r['email_tut']) && empty($r['email_resp']))) {
//            echo 'le tuteur et le responsable de l\'entreprise ne sont pas renseignés correctement<br>';
//            $erreur = true;
//        }
//        if (empty($r['nom_prof']) || empty($r['prenom_prof']) || empty($r['discipline_prof']) || empty($r['telephone_prof']) || empty($r['email_prof'])) {
//            echo 'la classe n\'est pas renseignée correctement<br>';
//            $erreur = true;
//        }
//        if (!$erreur) {
//            //faire la requête de modification
//            $req_conv = ('UPDATE `convention` SET `validation`=1 WHERE `id` =' . $r['id_conv'] . ';');
//
//            //préparation de la requête
//            $conv = $dbs->prepare($req_conv);
//
//            //execution de la requête		
//            $resultat_conv = $conv->execute();
//        }
//        $classe = $r['classe_id'];
//        $req_e = ('SELECT `id`,`nom_sta`,`prenom_sta` FROM `stagiaire` WHERE id_classe =' . $classe . ' ORDER BY nom_sta;');
//    }
//}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des étudiants </title>

        <link rel="stylesheet" href="scripts/style.css" />
    </head>
    <body>
        <form classe="forme" action="etudiants.php" method="post">
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type'])) {
                        ?>
                        <th>Classe :</th>
                        <td>
                            <?php
                            //Remplir la liste des classes
                            ?>
                            <select name="classe" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //récupérer les résultats de la requête
                                $result = $dbs->query($req_c);
                                // parcourir ces résultats
                                $tab_r = $result->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r as $r) {
                                    //Si la classe a été choisie
                                    if ((isset($_POST['classe']) && $_POST['classe'] != "") || isset($classe)) {
                                        //Si l'id de l'objet en cours est égal à l'id de la classe choisie précédemment
                                        if ($r['id'] == $classe)
                                        //sélectionner la valeur
                                            echo '<option value="' . $r['id'] . '" selected>' . $r['intitule'] . '</option>';
                                        else
                                            echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                    } else
                                        echo '<option value="' . $r['id'] . '">' . $r['intitule'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    //Si la classe a été choisie
                    if (isset($classe)) {
                        //Récupère et affiche les étudiants (en lien) de la classe sélectionnée ainsi qu'un lien pour modifier l'étudiant
                        //récupérer les résultats de la requête
                        $result = $dbs->query($req_e);
                        // parcourir ces résultats
                        $tab_r = $result->fetchAll();


//                        echo "<pre>";
//                        var_dump($req_e);
//                        var_dump($result);
//                        print_r($tab_r);
//                        echo "</pre>";
                        // parcourir le tableau avec les résultats
                        foreach ($tab_r as $r) {
                            //faire la requête
                            $req_conv = 'SELECT `convention`.`id`,`periode1_debut`,`periode2_debut`, `periode3_debut`,`periode3_fin`,`periode1_fin`,`periode2_fin`,`nom_ent`, `validation`, `id_prof` FROM `convention` INNER JOIN `entreprise` ON `id_ent` = `entreprise`.`id` WHERE id_classe =' . $classe . ' AND id_sta = ' . $r['id'] . ';';    //.$_SESSION['id'].';');
                            //récupérer les résultats de la requête
                            $result_conv = $dbs->query($req_conv);
                            // parcourir ces résultats
                            $tab_r_conv = $result_conv->fetchAll();
                            $num = count($tab_r_conv);
                            echo '<tr>';
                            echo '<td rowspawn = ' . $num . '>';
                            echo $r['nom_sta'] . ' ' . $r['prenom_sta'];
                            //echo '<a href="index.php?id='.$r['id'].'">'.$r['nom_sta'].' '.$r['prenom_sta'].'</a>';
                            echo '</td>';
                            echo '<td>';
                            //page info etudiant
                            echo '<p><a href="infoEtudiant.php?action=liste&amp;id=' . $r['id'] . '&amp;id_classe=' . $classe . '">informations</a></p>';
                            ;
                            echo '</td>';
                            echo '<td>';
                            echo '<a href="etudiant_modif.php?action=liste&amp;id=' . $r['id'] . '&amp;id_classe=' . $classe . '">modification</a>';
                            echo '</td>';
                            if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                                echo '<td>';
                                echo '<a href="etudiant_supprimer.php?action=liste&amp;id=' . $r['id'] . '&amp;id_classe=' . $classe . '">suppression</a>';
                                echo '</td>';
                            }


                            // parcourir le tableau avec les résultats
                            $nombre = 0;
                            foreach ($tab_r_conv as $r_conv) {
                                $nombre += 1;
                                if ($nombre != 1) {
                                    echo '</tr>';
                                    echo '<tr>';
                                    echo '<td></td><td></td><td></td>';
                                    if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                                        echo '<td></td>';
                                    }
                                }
                                echo '<td>';
                                echo '<p>Stage ' . $nombre . ' :</p>';
                                echo '</td>';
                                echo '<td>';


                                $P1_deb = $r_conv['periode1_debut'];
                                $tab_P1_deb = explode('-', $P1_deb);
                                $P1_deb = $tab_P1_deb[2] . '/' . $tab_P1_deb[1] . '/' . $tab_P1_deb[0];
                                $P1_fin = $r_conv['periode1_fin'];
                                $tab_P1_fin = explode('-', $P1_fin);
                                $P1_fin = $tab_P1_fin[2] . '/' . $tab_P1_fin[1] . '/' . $tab_P1_fin[0];
                                if (!empty($r_conv['periode2_debut'])) {
                                    $P2_deb = $r_conv['periode2_debut'];
                                    $tab_P2_deb = explode('-', $P2_deb);
                                    $P2_deb = $tab_P2_deb[2] . '/' . $tab_P2_deb[1] . '/' . $tab_P2_deb[0];
                                    $et = "et";
                                }
                                if (!empty($r_conv['periode2_fin'])) {
                                    $P2_fin = $r_conv['periode2_fin'];
                                    $tab_P2_fin = explode('-', $P2_fin);
                                    $P2_fin = $tab_P2_fin[2] . '/' . $tab_P2_fin[1] . '/' . $tab_P2_fin[0];
                                }
                                if (!empty($r_conv['periode3_debut'])) {
                                    $P3_deb = $r_conv['periode3_debut'];
                                    $tab_P3_deb = explode('-', $P3_deb);
                                    $P3_deb = $tab_P3_deb[2] . '/' . $tab_P3_deb[1] . '/' . $tab_P3_deb[0];
                                    $et2 = "et";
                                }
                                if (!empty($r_conv['periode3_fin'])) {
                                    $P3_fin = $r_conv['periode3_fin'];
                                    $tab_P3_fin = explode('-', $P3_fin);
                                    $P3_fin = $tab_P3_fin[2] . '/' . $tab_P3_fin[1] . '/' . $tab_P3_fin[0];
                                }


                                if (!empty($r_conv['periode3_debut']) && !empty($r_conv['periode3_fin'])) {
                                    echo '<a href="infoConvention.php?id_classe=' . $classe . '&amp;id=' . $r_conv['id'] . '">' . $r_conv['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . ' ' . $et . ' du ' . $P2_deb . ' au ' . $P2_fin . ' ' . $et2 . ' du ' . $P3_deb . ' au ' . $P3_fin . '</a>';
                                } elseif (!empty($r_conv['periode2_debut']) && !empty($r_conv['periode2_fin'])) {
                                    echo '<a href="infoConvention.php?id_classe=' . $classe . '&amp;id=' . $r_conv['id'] . '">' . $r_conv['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . ' ' . $et . ' du ' . $P2_deb . ' au ' . $P2_fin . '</a>';
                                } else {
                                    echo '<a href="infoConvention.php?id_classe=' . $classe . '&amp;id=' . $r_conv['id'] . '">' . $r_conv['nom_ent'] . ' du ' . $P1_deb . ' au ' . $P1_fin . '</a>';
                                }

                                echo '</td>';
                                echo '<td>';

                                if ($r_conv['validation'] == true) {
                                    echo '<input type="checkbox" name="validation" value="1" checked disabled="disabled"/>validé';
                                } else {
                                    echo '<input type="checkbox" name="validation" value="1" disabled="disabled"/>validé';
                                }
                                echo '</td>';
                                //si la convention est validé on ne peut plus la supprimer ou la modifier sinon on peut 
                                if (!$r_conv['validation']) {
                                    echo '<td>';
                                    echo '<a href="convention_modif.php?action=liste&amp;id=' . $r_conv['id'] . '&amp;id_classe=' . $classe . '">modification</a>';
                                    echo '</td>';
                                    if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                                        echo '<td>';
                                        echo '<a href="convention_supprimer.php?action=liste&amp;id_conv=' . $r_conv['id'] . '&amp;id_classe=' . $classe . '">suppression</a>';
                                        echo '</td>';
                                    }
                                } else {
                                    echo '<td>';
                                    echo '<p>modification</p>';
                                    ?>
                                    <p style="font-style: italic; color: red; font-size: x-small;" class="val">(impossible de modifier si la convocation est valider)</p>
                                    <?php
                                    echo '</td>';
                                    if ($_SESSION['type'] == "A" || $_SESSION['type'] == "P") {
                                        echo '<td>';
                                        echo '<p>suppression</p>';
                                        ?>
                                        <p style="font-style: italic; color: red; font-size: x-small;" class="val">(impossible de supprimer si la convocation est valider)</p>
                                        <?php
                                        echo '</td>';
                                    }
                                }

                                if ((($_SESSION['type'] == "P" && $_SESSION['id'] == $r_conv['id_prof']) || $_SESSION['type'] == "A" ) && !$r_conv['validation']) {
                                    echo '<td>';
                                    echo '<a href="validation.php?id_classe=' . $classe . '&amp;action=liste&amp;id_conv=' . $r_conv['id'] . '">Valider</a>';
                                    echo '</td>';
                                }
                                if ($_SESSION['type'] == "A" && $r_conv['validation']) {
                                    echo '<td>';
                                    echo '<a href="invalidation.php?id_classe=' . $classe . '&amp;action=liste&amp;id_conv=' . $r_conv['id'] . '">Invalider</a>';
                                    echo '</td>';
                                }
                            }
                            if ($nombre != 1) {
                                echo '</tr>';
                            }
                        }
                        echo '</tr>';
                    }

                    //javascript:history.back()
                    ?>
                    <tr>

                        <?php
                        //Si la classe a été choisie, affichage du bouton ajouter pour ajouter un étudiant dans la classe
                        if (($_SESSION['type'] == "A" || $_SESSION['type'] == "P") && (isset($classe) || isset($_GET['id_classe']))) {
                            echo '<td>';
                            echo '<a href="etudiant_ajout.php?action=liste&amp;id_classe=' . $classe . '">Ajouter</a>';
                            echo '</td>';
                            echo '<td>';
                            echo '<a href="importation_etudiant.php?id_classe=' . $classe . '">importer un csv listant les étudiants</a>';
                            echo '</td>';
                        }
                        ?>


                    <p><a href="index.php">retour</a></p>

                    <?php
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">Retour</a>';
                    echo '</td>';
                }
                ?>
                </tr>
            </table>
        </form>
    </body>
</html>













