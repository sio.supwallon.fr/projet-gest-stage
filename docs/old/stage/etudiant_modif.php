<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);

$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
if (isset($_GET['id'])) {
    //Récupère l'id du stagiaire sélectionné dans la page "etudiants.php"
    $num = $_GET['id'];


    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {
        try {
            if (isset($_POST['mdp_stagiaire']) && ($_POST['mdp_stagiaire'] != "")) {
                          
              //déclaration des variables des infos sur le stagiaire
              $req_sta = 'UPDATE `stagiaire` SET `nom_sta`=:nom_sta, `prenom_sta`=:prenom_sta';
  
              if (!empty($_POST['sexe_stagiaire'])) {
                  $req_sta = $req_sta . ', `sexe_sta`=:sexe_sta';
              }
  
              if (!empty($_POST['num_ss_stagiaire'])) {
                  $req_sta = $req_sta . ', `num_ss_sta`=:num_ss_sta';
              }
  
              if (!empty($_POST['date_naissance_stagiaire'])) {
                  $req_sta = $req_sta . ', `date_naissance_sta`=:date_naissance_sta';
              }
  
              if (!empty($_POST['lieu_naissance_stagiaire'])) {
                  $req_sta = $req_sta . ', `lieu_naissance_sta`=:lieu_naissance_sta';
              }
  
              if (!empty($_POST['adresse_stagiaire'])) {
                  $req_sta = $req_sta . ', `adresse_sta`=:adresse_sta';
              }
  
              if (!empty($_POST['telephone_stagiaire'])) {
                  $req_sta = $req_sta . ',`telephone_sta`=:telephone_sta';
              }
  
              if (!empty($_POST['email_stagiaire'])) {
                  $req_sta = $req_sta . ', `email_sta`=:email_sta';
              }

              if (!empty($_POST['caisse_stagiaire'])) { 
                  $req_sta = $req_sta . ', `caisse_sta`= :caisse_sta';
              }
  
              if (!empty($_POST['ville_stagiaire'])) {
                  $req_sta = $req_sta . ', `id_ville`=:ville_sta';
              }
                                                                                                                             
              $req_sta = $req_sta . ', `id_classe`= :classe_sta, `mdp_sta`= :mdp_sta WHERE `id` =:id;';
                
                $mdp = filter_input(INPUT_POST, 'mdp_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $mdp = md5($mdp);
                $nom_sta = filter_input(INPUT_POST, 'nom_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $nom_sta = strtoupper($nom_sta);
                $prenom_sta = filter_input(INPUT_POST, 'prenom_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $prenom_sta = strtolower($prenom_sta);
                $prenom_sta = ucwords($prenom_sta);
                $pos = strpos($prenom_sta, "-");
                if($pos !== false)
                {
                  $prenom_sta = substr($prenom_sta,0,$pos+1) . strtoupper(substr($prenom_sta,$pos+1,1)) . substr($prenom_sta,$pos+2);
                }
                $sexe_stagiaire = filter_input(INPUT_POST, 'sexe_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $num_ss_stagiaire = filter_input(INPUT_POST, 'num_ss_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $date_naissance_stagiaire = filter_input(INPUT_POST, 'date_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $lieu_naissance_stagiaire = filter_input(INPUT_POST, 'lieu_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $telephone_stagiairee = filter_input(INPUT_POST, 'telephone_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $email = filter_input(INPUT_POST, 'email_stagiaire', FILTER_SANITIZE_STRING);
                // Remove all illegal characters from email
                $email_checked = filter_var($email, FILTER_SANITIZE_EMAIL);
                if (filter_var($email_checked, FILTER_VALIDATE_EMAIL)) {
                    $email = strtolower($email_checked);
                } else {
                    $req = false;
                    if ($email != "")
                        echo('<p>' . $email . 'n\'est pas une adresse email valide</p>');
                    else
                        echo('<p>adresse email invalide</p>');
                }
                $ville_stagiaire = filter_input(INPUT_POST, 'ville_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $classe_stagiaire = filter_input(INPUT_POST, 'classe_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $caisse_stagiaire = filter_input(INPUT_POST, 'caisse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                //faire la requête de modification
//                $req_sta = ('UPDATE `stagiaire` SET `nom_sta`=:nom_sta, `prenom_sta`=:prenom_sta, `sexe_sta`=:sexe_sta, `num_ss_sta`=:num_ss_sta, `date_naissance_sta`=:date_naissance_sta, `lieu_naissance_sta`=:lieu_naissance_sta, `adresse_sta`=:adresse_sta,`telephone_sta`=:telephone_sta, `email_sta`=:email_sta, `id_ville`=:ville_sta, `id_classe`= :classe_sta, `caisse_sta`= :caisse_sta, `mdp_sta`= :mdp_sta WHERE `id` =:id;'); //Récupère l'étudiant choisi précédemment
                //préparation de la requête
                $sta = $dbs->prepare($req_sta);
                //récupère l'adresse sans les espaces
                $adresse = filter_input(INPUT_POST, 'adresse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $adresse = trim($adresse);
                //envoyent des paramètres
                $sta->bindParam(':nom_sta', $nom_sta);
                $sta->bindParam(':prenom_sta', $prenom_sta);

            if (!empty($_POST['sexe_stagiaire']))
                $sta->bindParam(':sexe_sta', $sexe_stagiaire);   

            if (!empty($_POST['num_ss_stagiaire']))
                $sta->bindParam(':num_ss_sta', $num_ss_stagiaire);

            if (!empty($_POST['date_naissance_stagiaire']))
                $sta->bindParam(':date_naissance_sta', $date_naissance_stagiaire);

            if (!empty($_POST['lieu_naissance_stagiaire']))
                $sta->bindParam(':lieu_naissance_sta', $lieu_naissance_stagiaire);

            if (!empty($_POST['adresse_stagiaire']))
                $sta->bindParam(':adresse_sta', $adresse);  //_stagiaire);

            if (!empty($_POST['telephone_stagiaire']))
                $sta->bindParam(':telephone_sta', $telephone_stagiairee);

            if (!empty($_POST['email_stagiaire']))
                $sta->bindParam(':email_sta', $email);

            if (!empty($_POST['caisse_stagiaire']))
                $sta->bindParam(':caisse_sta', $caisse_stagiaire);


//            $mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING);
//            $mdp_sta = md5($mdp);

            $sta->bindParam(':mdp_sta', $mdp);  //_sta);

            if (!empty($_POST['ville_stagiaire']))
                $sta->bindParam(':ville_sta', $ville_stagiaire);

            $sta->bindParam(':classe_sta', $id_classe);

            $sta->bindParam(':id', $id);

            //execution de la requête		
            $resultat_sta = $sta->execute();
            if ($resultat_sta == TRUE) {
                echo '<p>données enregistrées</p>';
            } else {
                echo '<p>erreur d\'enregistrement</p>';
            }
            } elseif (($_POST['mdp_stagiaire'] == null)) {
              //déclaration des variables des infos sur le stagiaire
              $req_sta = 'UPDATE `stagiaire` SET `nom_sta`=:nom_sta, `prenom_sta`=:prenom_sta';
              if (!empty($_POST['sexe_stagiaire'])) {
                  $req_sta = $req_sta . ', `sexe_sta`=:sexe_sta';
              }
  
              if (!empty($_POST['num_ss_stagiaire'])) {
                  $req_sta = $req_sta . ', `num_ss_sta`=:num_ss_sta';
              }
  
              if (!empty($_POST['date_naissance_stagiaire'])) {
                  $req_sta = $req_sta . ', `date_naissance_sta`=:date_naissance_sta';
              }
  
              if (!empty($_POST['lieu_naissance_stagiaire'])) {
                  $req_sta = $req_sta . ', `lieu_naissance_sta`=:lieu_naissance_sta';
              }
  
              if (!empty($_POST['adresse_stagiaire'])) {
                  $req_sta = $req_sta . ', `adresse_sta`=:adresse_sta';
              }
  
              if (!empty($_POST['telephone_stagiaire'])) {
                  $req_sta = $req_sta . ',`telephone_sta`=:telephone_sta';
              }
  
              if (!empty($_POST['email_stagiaire'])) {
                  $req_sta = $req_sta . ', `email_sta`=:email_sta';
              }

              if (!empty($_POST['caisse_stagiaire'])) {
                  $req_sta = $req_sta . ', `caisse_sta`= :caisse_sta';
              }
  
              if (!empty($_POST['ville_stagiaire'])) {
                  $req_sta = $req_sta . ', `id_ville`=:ville_sta';
              }
                                                                                                                             
              $req_sta = $req_sta . ', `id_classe`= :classe_sta WHERE `id` =:id;';
              
              
                $nom_sta = filter_input(INPUT_POST, 'nom_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $nom_sta = strtoupper($nom_sta);
                $prenom_sta = filter_input(INPUT_POST, 'prenom_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $prenom_sta = strtolower($prenom_sta);
                $prenom_sta = ucwords($prenom_sta);
                $pos = strpos($prenom_sta, "-");
                if($pos !== false)
                {
                  $prenom_sta = substr($prenom_sta,0,$pos+1) . strtoupper(substr($prenom_sta,$pos+1,1)) . substr($prenom_sta,$pos+2);
                }
                $sexe_stagiaire = filter_input(INPUT_POST, 'sexe_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $num_ss_stagiaire = filter_input(INPUT_POST, 'num_ss_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $date_naissance_stagiaire = filter_input(INPUT_POST, 'date_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $lieu_naissance_stagiaire = filter_input(INPUT_POST, 'lieu_naissance_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $telephone_stagiairee = filter_input(INPUT_POST, 'telephone_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $email = filter_input(INPUT_POST, 'email_stagiaire', FILTER_SANITIZE_STRING);
                // Remove all illegal characters from email
                $email_checked = filter_var($email, FILTER_SANITIZE_EMAIL);
                if (filter_var($email_checked, FILTER_VALIDATE_EMAIL)) {
                    $email = strtolower($email_checked);
                } else {
                    $req = false;
                    if ($email != "")
                        echo('<p>' . $email . 'n\'est pas une adresse email valide</p>');
                    else
                        echo('<p>adresse email invalide</p>');
                }
                $ville_stagiaire = filter_input(INPUT_POST, 'ville_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $classe_stagiaire = filter_input(INPUT_POST, 'classe_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $caisse_stagiaire = filter_input(INPUT_POST, 'caisse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                //faire la requête de modification
//                $req_sta = ('UPDATE stagiaire SET `nom_sta`=:nom_sta, `prenom_sta`=:prenom_sta, `sexe_sta`=:sexe_sta, `num_ss_sta`=:num_ss_sta, `date_naissance_sta`=:date_naissance_sta, `lieu_naissance_sta`=:lieu_naissance_sta, `adresse_sta`=:adresse_sta,`telephone_sta`=:telephone_sta, `email_sta`=:email_sta, `id_ville`=:ville_sta, `id_classe`= :classe_sta, `caisse_sta`= :caisse_sta  WHERE `id` =:id;'); //Récupère l'étudiant choisi précédemment
                //préparation de la requête
                $sta = $dbs->prepare($req_sta);
                //récupère l'adresse sans les espaces               
                $adresse = filter_input(INPUT_POST, 'adresse_stagiaire', FILTER_SANITIZE_SPECIAL_CHARS);
                $adresse = trim($adresse);
                //envoyent des paramètres
                $sta->bindParam(':nom_sta', $nom_sta);
                $sta->bindParam(':prenom_sta', $prenom_sta);

            if (!empty($_POST['sexe_stagiaire']))
                $sta->bindParam(':sexe_sta', $sexe_stagiaire);

            if (!empty($_POST['num_ss_stagiaire']))
                $sta->bindParam(':num_ss_sta', $num_ss_stagiaire);

            if (!empty($_POST['date_naissance_stagiaire']))
                $sta->bindParam(':date_naissance_sta', $date_naissance_stagiaire);

            if (!empty($_POST['lieu_naissance_stagiaire']))
                $sta->bindParam(':lieu_naissance_sta', $lieu_naissance_stagiaire);

            if (!empty($_POST['adresse_stagiaire']))
                $sta->bindParam(':adresse_sta', $adresse);  //_stagiaire);

            if (!empty($_POST['telephone_stagiaire']))
                $sta->bindParam(':telephone_sta', $telephone_stagiairee);

            if (!empty($_POST['email_stagiaire']))
                $sta->bindParam(':email_sta', $email);

            if (!empty($_POST['caisse_stagiaire']))
                $sta->bindParam(':caisse_sta', $caisse_stagiaire);


/*            $mdp = filter_input(INPUT_POST, 'mdp', FILTER_SANITIZE_STRING);
            $mdp_sta = md5($mdp);

            $sta->bindParam(':mdp_sta', $mdp);  //_sta);
*/
            if (!empty($_POST['ville_stagiaire']))
                $sta->bindParam(':ville_sta', $ville_stagiaire);

                $sta->bindParam(':classe_sta', $classe_stagiaire);

                $sta->bindParam(':id', $id);

                //execution de la requête		
                $resultat_sta = $sta->execute();
                if ($resultat_sta == TRUE) {
                    echo '<p>données enregistrées</p>';
                } else {            
                    echo '<p>erreur d\'enregistrement</p>';
//                    echo '<pre>';
//                    var_dump($resultat_sta);
//                    print_r($dbs->errorInfo());
//                    var_dump($req_sta);
//                    print_r($sta);
//
//
//                    var_dump($nom_sta);
//                    var_dump($prenom_sta);
//                    var_dump($sexe_stagiaire);
//                    var_dump($num_ss_stagiaire);
//                    var_dump($date_naissance_stagiaire);
//                    var_dump($lieu_naissance_stagiaire);
//                    var_dump($adresse);
//                    var_dump($telephone_stagiairee);
//                    var_dump($email_stagiaire);
//                    var_dump($ville_stagiaire);
//                    var_dump($classe_stagiaire);
//                    var_dump($caisse_stagiaire);
//                    var_dump($id);
//
//
//
//                    echo '</pre>';
                }
            }
        } catch (exception $e) {
            echo 'erreur';
        }
    }
} else
    $num = 0;
?>





<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Modification d'un étudiant </title>
        <link rel="stylesheet" href="mis_en_page_formulaire.css" />
        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="etudiant_modif.php?id_classe=' . $id_classe . '&amp;id=' . $num . '" method="post">';
        ?>
        <table>
            <tr>
                <?php
                if (isset($_SESSION['type']) && isset($_GET['id'])) {
                    //faire la requête de sélection de l'étudiant choisi dans la page précédente
                    $req = ('SELECT `stagiaire`.`id`,`nom_sta`,`prenom_sta`,`sexe_sta`,`num_ss_sta`,`date_naissance_sta`,`lieu_naissance_sta`,`adresse_sta`,`telephone_sta`,`email_sta`,`stagiaire`.`id_ville`,`id_classe`, `caisse_sta`,`departement`, `mdp_sta` FROM `stagiaire` INNER JOIN `ville` ON `stagiaire`.`id_ville` = `ville`.`id` WHERE `stagiaire`.`id` =:id;');
                    //récupérer les résultats de la requête
                    $sta = $dbs->prepare($req);
                    $sta->bindParam(':id', $num);
                    //execution de la requête		
                    $resultat_sta = $sta->execute();
                    if ($resultat_sta == false) {
                        echo '<pre>';
                        var_dump($resultat_sta);
                        print_r($dbs->errorInfo());
                        var_dump($req);
                        print_r($sta);
                        print_r($num);
                        echo '</pre>';
                    }

                    $tab_r = $sta->fetchAll();
                    if ($tab_r == false) {
                        $req = ('SELECT * FROM `stagiaire` WHERE `stagiaire`.`id` =:id;');
                        $sta = $dbs->prepare($req);
                        $sta->bindParam(':id', $num);
                        $resultat_sta = $sta->execute();
                        $tab_r = $sta->fetchAll();
                        $new_etudiant = true;
                    } else {
                        $new_etudiant = false;
                    }
                    // parcourir le tableau avec les résultats
                    foreach ($tab_r as $r) {


                        //Affiche les informations de l'étudiant récupérées
                        ?>
                        <th>Classe :</th>
                        <td>
                            <select name="classe_stagiaire">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //Rempli la liste des classes en sélectionnant la classe de l'étudiant sélectionné
                                //faire la requête
                                $req_c = ('SELECT `id`,`intitule` FROM `classe`;');
                                //récupérer les résultats de la requête
                                $result_c = $dbs->query($req_c);
                                // parcourir ces résultats
                                $tab_r_c = $result_c->fetchAll();
                                // parcourir le tableau avec les résultats
                                foreach ($tab_r_c as $r_c) {
                                    //on testera à chaque fois le si le departement est sélectionné pour éviter que le rafréchissement supprime les valeur
                                    if (isset($_POST['departement_stagiaire'])) {
                                        if ($r_c['id'] == $_POST['classe_stagiaire']) {
                                            echo '<option value="' . $r_c['id'] . '" selected>' . $r_c['intitule'] . '</option>';
                                        } else {
                                            echo '<option value="' . $r_c['id'] . '">' . $r_c['intitule'] . '</option>';
                                        }
                                    } else {
                                        if ($r_c['id'] == $r['id_classe'])
                                            echo '<option value="' . $r_c['id'] . '" selected>' . $r_c['intitule'] . '</option>';
                                        else
                                            echo '<option value="' . $r_c['id'] . '">' . $r_c['intitule'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Nom :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="text" name="nom_stagiaire" value ="' . $_POST['nom_stagiaire'] . '" /></td>';
                        } else {
                            echo '<td><input type="text" name="nom_stagiaire" value ="' . $r['nom_sta'] . '" /></td>';
                        }
                        ?>
                    </tr>
                    <tr>
                        <th>Prénom :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="text" name="prenom_stagiaire" value ="' . $_POST['prenom_stagiaire'] . '" /></td>';
                        } else {
                            echo '<td><input type="text" name="prenom_stagiaire" value ="' . $r['prenom_sta'] . '" /></td>';
                        }
                        ?>
                    </tr>
                    <tr>
                        <th>Sexe :</th>
                        <td>
                            <?php
                            //sélectionne le sexe de l'étudiant récupéré
                            if (isset($_POST['departement_stagiaire'])) {
                                if ($_POST['sexe_stagiaire'] == 'M') {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" checked="checked" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" /></label>';
                                } else {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" checked="checked" /></label>';
                                }
                            } else {
                                if ($r['sexe_sta'] == 'M') {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" checked="checked" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" /></label>';
                                } else {
                                    echo '<label>Masculin<input type="radio" name="sexe_stagiaire" value="M" /></label>';
                                    echo '<label>Feminin<input type="radio" name="sexe_stagiaire" value="F" checked="checked" /></label>';
                                }
                            }
                            ?>	
                        </td>
                    </tr>
                    <tr>
                        <th>Numéro de sécurité sociale :</th>
                        <td>
                            <?php
                            if (isset($_POST['departement_stagiaire'])) {
                                echo '<input type="text" name="num_ss_stagiaire" maxlength="15"  value ="' . $_POST['num_ss_stagiaire'] . '"/>';
                            } else {
                                echo '<input type="text" name="num_ss_stagiaire" maxlength="15"  value ="' . $r['num_ss_sta'] . '"/>';
                            }
                            ?>

                        </td>
                    </tr>
                    <tr>
                        <th>Date de naissance :</th>
                        <td>
                            <?php
                            if (isset($_POST['departement_stagiaire'])) {
                                echo '<input type="date" name="date_naissance_stagiaire" value="' . $_POST['date_naissance_stagiaire'] . '"/>';
                            } else {
                                echo '<input type="date" name="date_naissance_stagiaire" value="' . $r['date_naissance_sta'] . '"/>';
                            }
                            ?>    				
                        </td>
                    </tr>
                    <tr>
                        <th>Lieu de naissance :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="text" name="lieu_naissance_stagiaire" value ="' . $_POST['lieu_naissance_stagiaire'] . '" /></td>';
                        } else {
                            echo '<td><input type="text" name="lieu_naissance_stagiaire" value ="' . $r['lieu_naissance_sta'] . '" /></td>';
                        }
                        ?>

                    </tr>
                    <tr>
                        <th>Adresse :</th>
                        <td>
                            <?php
                            if (isset($_POST['departement_stagiaire'])) {
                                echo '<textarea name="adresse_stagiaire" cols="33,8" rows="2">' . trim($_POST['adresse_stagiaire']) . '</textarea>';
                            } else {
                                ?>
                                <textarea name="adresse_stagiaire" cols="33,8" rows="2"> <?php echo trim($r['adresse_sta']) ?></textarea>
                                <?php
                            }
                            ?>

                        </td>

                    </tr>
                    <tr>
                        <th>Département :</th>
                        <td>
                            <select name="departement_stagiaire" onchange="this.form.submit()">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req_d = ('SELECT distinct `departement` FROM `ville`;');
                                //récupérer les résultats de la requête
                                $result_d = $dbs->query($req_d);
                                // parcourir ces résultats
                                $tab_r_d = $result_d->fetchAll();
                                // parcourir le tableau avec les résultats
                                //rempli la liste des départements en sélectionnant celui de l'étudiant récupéré
                                foreach ($tab_r_d as $r_d) {
                                    if (!isset($_POST['departement_stagiaire']) && $new_etudiant == false) {
                                        if ($r_d['departement'] == $r['departement']) {
                                            echo '<option value="' . $r_d['departement'] . '" selected>' . $r_d['departement'] . '</option>';
                                        } else {
                                            echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                        }
                                    } else {
                                        if ($r_d['departement'] == $_POST['departement_stagiaire']) {
                                            echo '<option value="' . $r_d['departement'] . '" selected>' . $r_d['departement'] . '</option>';
                                        } else
                                            echo '<option value="' . $r_d['departement'] . '">' . $r_d['departement'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Ville :</th>
                        <td>
                            <select name="ville_stagiaire">
                                <option value=''>Faites votre choix</option>
                                <?php
                                //faire la requête
                                $req_v = ('SELECT `id`,`code_postal`,`nom_ville` FROM `ville` WHERE departement = :departement ORDER BY `nom_ville`;');
                                //récupérer les résultats de la requête
                                $sta = $dbs->prepare($req_v);
                                if ($new_etudiant == false) {
                                    $sta->bindParam(':departement', $r['departement']);
                                } else {
                                    $sta->bindParam(':departement', $_POST['departement_stagiaire']);
                                }

                                //execution de la requête		
                                $resultat_sta = $sta->execute();
                                // parcourir le tableau 
                                $tab_r_v = $sta->fetchAll();
                                // parcourir le tableau avec les résultats
                                //Rempli la liste des villes en sélectionnant celle de l'étudiant récupéré
                                foreach ($tab_r_v as $r_v) {

                                    if ($r_v['id'] == $r['id_ville'])
                                        echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                    else
                                        echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ville'] . " " . $r_v['code_postal'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Téléphone :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="tel" name="telephone_stagiaire" maxlength="10"  value ="' . $_POST['telephone_stagiaire'] . '"/>';
                        } else {
                            echo '<td><input type="tel" name="telephone_stagiaire" maxlength="10"  value ="' . $r['telephone_sta'] . '"/>';
                        }
                        ?>
                        </td>

                    </tr>
                    <tr>
                        <th>Email :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_stagiaire" placeholder="pauldurand@gmail.com"  value ="' . $_POST['email_stagiaire'] . '"/></td>';
                        } else {
                            echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_stagiaire" placeholder="pauldurand@gmail.com"  value ="' . $r['email_sta'] . '"/></td>';
                        }
                        ?>

                    </tr>
                    <tr>
                        <th>Caisse primaire d'assurance maladie :</th>
                        <?php
                        if (isset($_POST['departement_stagiaire'])) {
                            echo '<td><input type="text" name="caisse_stagiaire" value ="' . $_POST['caisse_stagiaire'] . '" /></td>';
                        } else {
                            echo '<td><input type="text" name="caisse_stagiaire" value ="' . $r['caisse_sta'] . '" /></td>';
                        }
                        ?>

                    </tr>
                    <tr>
                        <th>Mot de passe :</th>
                        <td>

                            <input type="password" name="mdp_stagiaire" />
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td>
                        <button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>
                    </td>
                    <td>
                        <p><a href="etudiants.php?action=liste&amp;id=<?php echo $id_classe; ?> ">retour</a></p>
                    </td>


                    <?php
                } elseif (!isset($_SESSION['type'])) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez choisir un stagiaire';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="etudiants.php">Retour</a>';
                    echo '</td>';
                }
                ?>
            </tr>
        </table>

    </form>
</body>
</html>
