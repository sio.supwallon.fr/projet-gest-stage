<!DOCTYPE html>
<?php
require 'scripts/constante.php';
session_start();
$id_tut = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer un tuteur</title>
    </head>
    <body>

        <form action="tuteur_suppr.php?id=<?php echo $id_tut; ?>&amp;id_ent=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || ($_SESSION['type'] == "P" ))) {
                echo '<p>êtes vous sûr de vouloir suprimer le tuteur ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>

            </form>

            <?php
            if (isset($_GET['id']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_tut = ' DELETE FROM `tuteur` WHERE id=:id ;';

                //préparation de la requête
                $tut = $dbs->prepare($req_tut);
                $tut->bindParam(':id', $id_tut);
                //execution de la requête
                $resultat_tut = $tut->execute();

                if ($resultat_tut == true) {
                    header('location:tuteur.php?id=' . $id_ent . '&id_ville=' . $id_ville);
                } else {
                    echo '<pre>';
                    var_dump($resultat_tut);
                    print_r($dbs->errorInfo());
                    var_dump($req_tut);
                    print_r($tut);
                    var_dump($id_tut);
                    echo '</pre>';
                    die();
                }
            } elseif (isset($_GET['id']) && isset($_POST['btn_non'])) {
                header('location:tuteur.php?id_ville=' . $id_ville . '&id=' . $id_ent);
            } elseif (!isset($_GET['id'])) {
                echo'<p>veuillez sélectionner un tuteur</p>';
            }
        } elseif ($_SESSION['type'] != "A" || ($_SESSION['type'] != "P" )) {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour cette manipulation';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        } else {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</table>';
        }
        ?>




    </body>
</html>

