<!DOCTYPE html>
<?php
require 'scripts/constante.php';
$id_conv = filter_input(INPUT_GET, 'id_conv', FILTER_SANITIZE_SPECIAL_CHARS);
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_SPECIAL_CHARS);

session_start();
//ouverture de la connexion
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="scripts/style.css" />
        <title>supprimer une convention</title>
    </head>
    <body>

        <form action="convention_supprimer.php?id_conv=<?php echo $id_conv; ?>&amp;id_classe=<?php echo $id_classe; ?>" method="post">
            <?php
            if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P" )) {
                echo '<p>êtes vous sûr de vouloir suprimer la convention ?</p>';
                echo '<p><input type="submit" name="btn_oui" value="oui"></p>';
                echo '<p><input type="submit" name="btn_non" value="non"></p>';
                ?>
            </form>
            <?php
            if (isset($_GET['id_conv']) && isset($_POST['btn_oui'])) {
                //faire la requête de suppression
                $req_ent = ' DELETE FROM `convention` WHERE convention.id=:id ;';

                //préparation de la requête
                $ent = $dbs->prepare($req_ent);
                $ent->bindParam(':id', $id_conv);
                //execution de la requête
                $resultat_sta = $ent->execute();
                if ($resultat_sta == true) {
                    header("location:etudiants.php?action=liste&id=" . $id_classe);
                } else {
                    print_r($dbs->errorInfo());
                }
            } elseif (isset($_GET['id_conv']) && isset($_POST['btn_non'])) {
                header("location:etudiants.php?action=liste&id=" . $id_classe);
            } elseif (!isset($_GET['id_conv'])) {
                echo'<p>veuillez sélectionner une convention</p>';
            }
        } elseif (($_SESSION['type'] != "A" || $_SESSION['type'] != "P")) {
            echo '<tr>';
            echo '<td>';
            echo 'Vous n\'avez pas les droits pour supprimer une convention';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">Retour</a>';
            echo '</td>';
        } else {
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">Retour</a>';
            echo '</td>';
        }
        ?>




    </body>
</html>
