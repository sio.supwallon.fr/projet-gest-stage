<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_classe = filter_input(INPUT_GET, 'id_classe', FILTER_SANITIZE_NUMBER_INT);
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Gestion des conventions </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>


        <?php


        if (isset($_SESSION['type']) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {


            $req = ' SELECT intitule FROM classe WHERE id = :id;';
            $sth = $dbs->prepare($req);
            $sth->bindParam(':id', $id_classe);
            $sth->execute();
            $classe = $sth->fetch();
            ?>
            <h1> Importation d'un fichier CSV</h1>
            <h2>Pour la classe <?php echo $classe['intitule']; ?></h2>





            <h3> veuillez choisir un fichier .csv</h3>
            <form class="form" action="importation_etudiant.php?id_classe=<?php echo $id_classe; ?>" method="post" enctype="multipart/form-data">

                <input name="fichier" type="file" value="table" accept="text/csv">
                <input name="importer" type="submit" value="importer">
            </form>
            <?php
            if (isset($_POST['importer'])) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                //vérifions que le fichier est bien un fichier cvs
                //récupération du fichier 
                $fichier_csv = $_FILES['fichier']['tmp_name'];
                //faire la verif 
                $verif = finfo_file($finfo, $fichier_csv);
                //si la vérification et fausse afficher un message et vider la variable
                if ($verif != "text/plain") {
                    echo '<p>le fichier sélectionné n\'est pas un fichier csv</p>';
                    unlink($fichier_csv);
                } else {
                    // ouverture temporaire
                    $fp = fopen($fichier_csv, "r");
                    $ligne = fgets($fp); //premiere ligne (nom des champs)
                    $fich = true;
                    $ligne = fgets($fp);  // aller la la deuxieme ligne (1ere valeurs)
                    while (!feof($fp)) {
                        $tab = explode(';', $ligne);
                        $nombre_ligne = count($tab);
                        if ($nombre_ligne != 2) {
                            $fich = false;
                        }
                        $ligne = fgets($fp);
                    }
                    fclose($fp);
                    if ($fich == false) {
                        echo '<p>le fichier csv est invalide car le nombre de champs n\'est pas celui attendu</p>';
                    } else {
                        $fp = fopen($fichier_csv, "r");
                        $ligne = fgets($fp); // aller a la premiere ligne 
                        $ligne = fgets($fp); // aller a la deuxieme ligne 
                        $tab = 0;
                        $count = 0;
                        echo '<table class="listnewetudiant">';
                        echo '<tr>';
                        echo '<th>';
                        echo 'Nom :';
                        echo '</th>';
                        echo '<th>';
                        echo 'prenom :';
                        echo '</th>';
                        echo '<th>';
                        echo 'login :';
                        echo '</th>';
                        echo '<th>';
                        echo 'mot de passe :';
                        echo '</th>';
                        echo '</tr>';
                        $lesnom = "";
                        $lesprenom = "";
                        $lesmdp = "";
                        while (!feof($fp)) {
                            $tab++;
                            $count++;
                            $list = explode(';', $ligne);
                            //affecter les valeurs 
                            $champs1 = $list[0];

                            $champs2 = $list[1];

                            //enleve les espaces de la chaîne
                            $champs1 = trim($champs1);
                            $champs2 = trim($champs2);


                            $mdp = uniqid();




                            $mdp_in_bdd = md5($mdp);
 
                            //verif qu'il n y a pas déjà une personne du même nom et prenom dans la classe
                            $req_verif = ' SELECT COUNT(*) AS nb FROM stagiaire WHERE nom_sta=:nom AND prenom_sta=:prenom;'; //AND id_classe=:classe;';
                            $sth_verif = $dbs->prepare($req_verif);
                            $sth_verif->bindParam(':nom', $champs1);
                            $sth_verif->bindParam(':prenom', $champs2);
//                            $sth_verif->bindParam(':classe', $id_classe);
                            $result_verif = $sth_verif->execute();

                            if ($result_verif == true) {
                              $result = $sth_verif->fetchAll();
                              foreach ($result as $r) {
                                $res = $r['nb'];
                              }
                              if ($res != 0) {
                                  $res++;
                                  $log = strtolower($champs2 . '.' . $champs1) . $res;
                              } else {
                                  $log = strtolower($champs2 . '.' . $champs1);
                              }
                            }

                            $tableau[]['nom'] = $champs1;
                            $tableau[]['prenom'] = $champs2;
                            $tableau[]['login'] = $log;
                            $tableau[]['mdp'] = $mdp;     


                            $req_insert = ' INSERT INTO stagiaire(login_sta, nom_sta ,prenom_sta ,id_classe ,mdp_sta) VALUES(:login, :nom, :prenom, :classe, :mdp);';
                            $sth = $dbs->prepare($req_insert);
                            $sth->bindParam(':login', $log);
                            $sth->bindParam(':nom', $champs1);
                            $sth->bindParam(':prenom', $champs2);
                            $sth->bindParam(':classe', $id_classe);
                            $sth->bindParam(':mdp', $mdp_in_bdd);
                            $result = $sth->execute();

                            if ($result == false) {
                                echo '<tr>';
                                echo '<td>';
                                echo $log;
                                echo '</td>';
                                echo '<td>';
                                echo $champs1;
                                echo '</td>';
                                echo '<td>';
                                echo $champs2;
                                echo '</td>';
                                echo '<td>';
                                echo $mdp;
                                echo '</td>';
                                echo '<td>';
                                echo '<p>compte non créé</p>';
                                echo '</td>';
                                echo '</tr>';
                            } else {
                                echo '<tr>';
                                echo '<td>';
                                echo $log;
                                echo '</td>';
                                echo '<td>';
                                echo $champs1;
                                echo '</td>';
                                echo '<td>';
                                echo $champs2;
                                echo '</td>';
                                echo '<td>';
                                echo $mdp;
                                echo '</td>';
                                echo '<td>';
                                echo '<p>compte créé</p>';
                                echo '</td>';
                                echo '</tr>';
                            }

                            $ligne = fgets($fp);
                        }
                        echo '</table>';
                        $json = json_encode($tableau);

//                        echo "<pre>";
//                        var_dump($tableau);
//                        var_dump($json);
//                        echo "</pre>";
                        ?>


                        <form target="_blank" class="listnewetudiant" action="pdf_new_etudiant.php?id=<?php echo $id_classe; ?>" method="post">
                            <p><input  type="submit" name="pdf" value="liste au format pdf"></p>
                            <p>(action recommandé pour ne pas perdre les mots de passe)</p>
                            <input type="hidden" name="lesvaleurs" value='<?php echo $json; ?>'>

                        </form>           
                        <?php
                    }
                }
            }

            echo '<p><a href = "etudiants.php?action=liste&amp;id=' . $id_classe . '">retour</a></p>';
        } elseif (($_SESSION['type'] != "A" || $_SESSION['type'] != "P")) {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter avec un compte abilité';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</tr>';
            echo '</table>';
        } else {
            echo '<table>';
            echo '<tr>';
            echo '<td>';
            echo 'Vous devez vous connecter';
            echo '</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>';
            echo '<a href="connexion.php">connexion</a>';
            echo '</td>';
            echo '</tr>';
            echo '</table>';
        }
        ?>



    </form>
</body>
</html>

