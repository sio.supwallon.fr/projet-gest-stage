<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

session_start();
//Récupère l'id de la classe choisie dans la page précédente
//Si le bouton enregistrer a été utilisé
if (isset($_POST['enregistrer'])) {
    //Récupération du code permettant d'ajouter un étudiant dans la base de données
    //initialisation des variables
    $req = true;

    //vérification du remplissage des champs de l'élève
    if (empty($_POST['nom_professeur'])) {
        $req = false;
        echo 'le champs nom de l\'enseignant est vide';
    }

    if (empty($_POST['prenom_professeur'])) {
        $req = false;
        echo 'le champs prénom de l\'enseignant est vide';
    }

    /*  	if (empty($_POST['discipline_professeur']))
      {
      $req=false;
      echo 'le champs discipline de l\'enseignant est vide';

      }

      if (empty($_POST['telephone_professeur']))
      {
      $req=false;
      echo 'le champs téléphone de l\'enseignant est vide';

      }

      if (empty($_POST['email_professeur']))
      {
      $req=false;
      echo 'le champs email de l\'enseignant est vide';
      } */

    if (empty($_POST['mdp_professeur'])) {
        $req = false;
        echo 'le champs mot de passe de l\'enseignant est vide';
    }

    //insertion des valeurs du stagiaires dans la table stagiaire
    if ($req != false) {
        //déclaration des variables des infos sur l'enseignant
        $gauche = 'INSERT INTO `professeur` (`login_prof`,`nom_prof`, `prenom_prof`';
        $droite = ' Values (:login, :nom_prof, :prenom_prof';

        if (!empty($_POST['discipline_professeur'])) {
            $gauche = $gauche . ', `discipline_prof`';
            $droite = $droite . ', :discipline_prof';
        }

        if (!empty($_POST['telephone_professeur'])) {
            $gauche = $gauche . ',`telephone_prof`';
            $droite = $droite . ', :telephone_prof';
        }

        if (!empty($_POST['email_professeur'])) {
            $gauche = $gauche . ',`email_prof`';
            $droite = $droite . ', :email_prof';
        }

        $gauche = $gauche . ', `mdp_prof`)';
        $droite = $droite . ', :mdp_prof);';

        $req_prof = $gauche . $droite;

        $nom_prof = filter_input(INPUT_POST, 'nom_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        $prenom_prof = filter_input(INPUT_POST, 'prenom_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        $discipline_prof = filter_input(INPUT_POST, 'discipline_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        $telephone_prof = filter_input(INPUT_POST, 'telephone_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        $email_prof = filter_input(INPUT_POST, 'email_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        $mdp_prof = filter_input(INPUT_POST, 'mdp_professeur', FILTER_SANITIZE_SPECIAL_CHARS);
        //$req_prof = 'INSERT INTO `professeur` (`nom_prof`, `prenom_prof`, `discipline_prof`,`telephone_prof`,`email_prof`,`mdp_prof`) Values (:nom_prof, :prenom_prof, :discipline_prof, :telephone_prof, :email_prof, :mdp_prof);';
        //préparation de la requête

        $req_login = ' SELECT COUNT(*) AS nb FROM professeur WHERE nom_prof=:nom AND prenom_prof=:prenom;';
        $sth_log = $dbs->prepare($req_login);

        $sth_log->bindParam(':nom', $nom_prof);
        $sth_log->bindParam(':prenom', $prenom_prof);

        $result_verif = $sth_log->execute();

        if ($result_verif == true) {
            $res = intval($sth_log->fetch('nb'));
            if ($res != 0) {
                $res++;
                $log = $prenom_prof . '.' . $nom_prof . $res;
            } else {
                $log = $prenom_prof . '.' . $nom_prof;
            }
        }
         //0327193040


        $prof = $dbs->prepare($req_prof);

        //envoyent des paramètres
        $prof->bindParam(':login', $log);
        $prof->bindParam(':nom_prof', $nom_prof);
        $prof->bindParam(':prenom_prof', $prenom_prof);

        if (!empty($_POST['discipline_professeur']))
            $prof->bindParam(':discipline_prof', $discipline_prof);

        if (!empty($_POST['telephone_professeur']))
            $prof->bindParam(':telephone_prof', $telephone_prof);

        if (!empty($_POST['email_professeur']))
            $prof->bindParam(':email_prof', $email_prof);

        $mdp = md5($mdp_prof);
        $prof->bindParam(':mdp_prof', $mdp);

        //execution de la requête		
        $resultat_prof = $prof->execute();
         echo '<pre>';
          var_dump($resultat_prof);
          print_r($dbs->errorInfo());
          var_dump($req_prof);
          print_r($prof);
          echo '</pre>';

         
        header('location:professeurs.php');
    }
}
?>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Ajout d'un enseignant </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <form action="professeur_ajout.php" method="post">
            <?php
//http://localhost/Stage_Jeremy3/
//Prépare les zones de saisie vide
            ?>
            <table>
                <tr>
                    <?php
                    if (isset($_SESSION['type']) && $_SESSION['type'] == "A") {
                        ?>
                        <th>Nom :</th>
                        <td><input type="text" name="nom_professeur" title="A remplir impérativement" placeholder="Entrer le nom de l'enseignant" value ="" /></td>
                    </tr>
                    <tr>
                        <th>Prénom :</th>
                        <td><input type="text" name="prenom_professeur" placeholder="Entrer le prénom de l'enseignant" value ="" /></td>
                    </tr>
                    <tr>
                        <th>Discipline :</th>
                        <td><input type="text" name="discipline_professeur" placeholder="Entrer la discipline de l'enseignant" value ="" /></td>
                    </tr>
                    <tr>
                        <th>Téléphone :</th>
                        <td><input type="tel" name="telephone_professeur" maxlength="10" placeholder="Entrer le numéro de téléphone de l'enseignant" value ="0327193040" />
                        </td>

                    </tr>
                    <tr>
                        <th>Email :</th>
                        <td><input type="email" name="email_professeur" placeholder="pauldurand@gmail.com" /></td>

                    </tr>
                    <tr>
                        <th>Mot de passe :</th>
                        <td>
                            <input type="password" name="mdp_professeur" value ="" />
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <button class="bouton" type="submit" name="enregistrer">Enregistrer l'enseignant</button>
                        </td>
                        <td>
                            <a href="professeurs.php">retour</a>
                        </td>
                        <?php
                    } elseif (!isset($_SESSION['type'])) {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez vous connecter';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="connexion.php">connexion</a>';
                        echo '</td>';
                    } else {
                        echo '<tr>';
                        echo '<td>';
                        echo 'Vous devez n\'avez pas le droit d\'ajouter un enseignant';
                        echo '</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td>';
                        echo '<a href="professeurs.php">Retour</a>';
                        echo '</td>';
                    }
                    ?>
            </table>
        </form>
    </body>
</html>
