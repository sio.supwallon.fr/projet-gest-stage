<?php

class PDF extends FPDF {

// Pied de page
    function Footer() {
        // Positionnement à 1,5 cm du bas
        $this->SetY(-15);

        $this->SetX(180);
        // Police Arial italique 8
        $this->SetFont('Arial', 'I', 8);
        $this->SetTextColor(0, 0, 0);
        // Numéro de page
        $this->Cell(0, 10, 'Page ' . $this->PageNo(), 0, 0, 'C');
    }

    function BasicTable($y, $x, $c1, $c2, $header) {
        // En-tête
        $this->SetY($y);
        $this->SetX($x);
        foreach ($header as $col)
            $this->Cell($c1, $c2, $col, 1);

        $this->Ln();
    }

    function BasicTable2($header, $tab1, $tab2, $tab3, $tab4, $nombre) {
        // En-tête
        foreach ($header as $col) {
            $this->Cell(90, 5, $col, 1);
        }
        $this->Ln();
        $this->Cell(90, 5, $tab1, 1);
        $this->Cell(90, 5, $tab2, 1);
        $this->Ln();
        $this->Cell(90, 5, $tab3, 1);
        $this->Cell(90, 5, $tab4, 1);
        $this->Ln();
        $this->Cell(90, 5, '', 1);
        $this->Cell(90, 5, '', 1);
        $this->Ln();
        $this->Cell(180, 4, utf8_decode('Soit une durée totale en jours ouvrables de : ' . $nombre . ' jours.'), 1);

        $this->Ln();
    }

    function BasicTable1($header, $tab1, $tab2, $nombre) {
        // En-tête
        foreach ($header as $col) {
            $this->Cell(90, 5, $col, 1);
        }
        $this->Ln();
        $this->Cell(90, 5, $tab1, 1);
        $this->Cell(90, 5, $tab2, 1);
        $this->Ln();
        $this->Cell(90, 5, '', 1);
        $this->Cell(90, 5, '', 1);
        $this->Ln();
        $this->Cell(90, 5, '', 1);
        $this->Cell(90, 5, '', 1);
        $this->Ln();
        $this->Cell(180, 4, utf8_decode('Soit une durée totale en jours ouvrables de : ' . $nombre . ' jours.'), 1);

        $this->Ln();
    }

    function BasicTable3($header, $tab1, $tab2, $tab3, $tab4, $tab5, $tab6, $nombre) {
        // En-tête
        foreach ($header as $col) {
            $this->Cell(90, 5, $col, 1);
        }
        $this->Ln();
        $this->Cell(90, 5, $tab1, 1);
        $this->Cell(90, 5, $tab2, 1);
        $this->Ln();
        $this->Cell(90, 5, $tab3, 1);
        $this->Cell(90, 5, $tab4, 1);
        $this->Ln();
        $this->Cell(90, 5, $tab5, 1);
        $this->Cell(90, 5, $tab6, 1);
        $this->Ln();
        $this->Cell(180, 4, utf8_decode('Soit une durée totale en jours ouvrables de : ' . $nombre . ' jours.'), 1);

        $this->Ln();
    }

}

?>