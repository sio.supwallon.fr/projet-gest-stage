<?php
require 'scripts/constante.php';
// défini l'UTF-8 comme encodage par défaut (à placer dans le fichier de configuration par exemple)
//  mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');
$id_ent = filter_input(INPUT_GET, 'id_ent', FILTER_SANITIZE_SPECIAL_CHARS);
$id_resp = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_SPECIAL_CHARS);
$id_ville = filter_input(INPUT_GET, 'id_ville', FILTER_SANITIZE_SPECIAL_CHARS);
session_start();
if (isset($_GET['id'])) {
    //Récupère l'id de la classe sélectionnée dans la page "etudiants.php"
    $num = $id_resp;
    $nom_resp = filter_input(INPUT_POST, 'nom_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    $nom_resp = strtoupper($nom_resp);
    $prenom_resp = filter_input(INPUT_POST, 'prenom_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    $prenom_resp = strtolower($prenom_resp);
    $prenom_resp = ucwords($prenom_resp);
    $pos = strpos($prenom_resp, "-");
    if($pos !== false)
    {
      $prenom_resp = substr($prenom_resp,0,$pos+1) . strtoupper(substr($prenom_resp,$pos+1,1)) . substr($prenom_resp,$pos+2);
    }
    $fonction_resp = filter_input(INPUT_POST, 'fonction_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    $telephone_resp = filter_input(INPUT_POST, 'telephone_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    $email_resp = filter_input(INPUT_POST, 'email_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    $entreprise_resp = filter_input(INPUT_POST, 'entreprise_resp', FILTER_SANITIZE_SPECIAL_CHARS);
    //Si le bouton enregistrer a été utilisé
    if (isset($_POST['enregistrer'])) {


        //faire la requête de modification
        $req_resp = ('UPDATE responsable SET nom_resp= :nom_resp, prenom_resp=:prenom_resp,fonction_resp=:fonction_resp, telephone_resp=:telephone_resp,email_resp=:email_resp,id_ent=:id_ent WHERE id =:id;'); //Récupère le tuteur choisi précédemment
        //préparation de la requête
        $resp = $dbs->prepare($req_resp);
        //récupère l'adresse sans les espaces               
        //envoyent des paramètres
        $resp->bindParam(':nom_resp', $nom_resp);
        $resp->bindParam(':prenom_resp', $prenom_resp);
        $resp->bindParam(':fonction_resp', $fonction_resp);
        $resp->bindParam(':telephone_resp', $telephone_resp);
        $resp->bindParam(':email_resp', $email_resp);
        $resp->bindParam(':id_ent', $entreprise_resp);
        $resp->bindParam(':id', $num);

        //execution de la requête		
        $resultat_resp = $resp->execute();
        if ($resultat_resp == true) {
            echo '<p>données enregistrées</p>';
        } else {
            print_r($dbs->errorInfo()[2]);
        }
    }
} else
    $num = 0;
?>





<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Modification d'un responsable </title>

        <link rel="stylesheet" type="text/css" href="scripts/style.css" />
    </head>
    <body>
        <?php
        echo '<form action="responsable_modif.php?action=liste&amp;id_ent=' . $id_ent . '&amp;id_ville=' . $id_ville . '&amp;id=' . $num . '" method="post">';
        ?>
        <table>

            <?php
            if ((isset($_SESSION['type'])) && ($_SESSION['type'] == "A" || $_SESSION['type'] == "P")) {

                //faire la requête 
                $req = ('SELECT `nom_resp`,`prenom_resp`,`fonction_resp` ,`telephone_resp`,`email_resp`,`nom_ent` FROM responsable INNER JOIN entreprise ON entreprise.id = responsable.id_ent WHERE responsable.id=:id;');
                //récupérer les résultats de la requête

                $sth = $dbs->prepare($req);
                $sth->bindParam(':id', $num);
                $res = $sth->execute();
                if ($res != True) {
                    echo '<pre>';
                    var_dump($res);
                    print_r($dbs->errorInfo());
                    var_dump($req);
                    print_r($res);
                    var_dump($num);
                    echo '</pre>';
                } else {
                    // parcourir ces résultats
                    $tab_r = $sth->fetch();
                    // parcourir le tableau avec les résultats
                    while ($tab_r != null) {
                        //Affiche les informations de l'étudiant récupérées
                        ?>

                        <tr>
                            <th>Nom :</th>
                            <?php
                            echo '<td><input type="text" name="nom_resp" value ="' . $tab_r['nom_resp'] . '" ></td>';
                            ?>
                        </tr>

                        <tr>
                            <th>Prenom :</th>

                            <td><input type="text" name="prenom_resp" value ="<?php echo $tab_r['prenom_resp']; ?> " ></td>


                        </tr>
                        <tr>
                            <th>fonction :</th>
                            <td><input type="text" name="fonction_resp" value ="<?php echo $tab_r['fonction_resp']; ?> " ></td>
                        </tr>
                        <tr>
                            <th>Téléphone :</th>
                            <?php
                            echo '<td><input type="tel" name="telephone_resp" size="20" minlength="10" maxlength="10" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value ="' . $tab_r['telephone_resp'] . '">';
                            ?>
                            </td>

                        </tr>
                        <tr>
                            <th>Email :</th>
                            <?php
                            echo '<td><input type="email" size="30" pattern="^[a-zA-Z0-9_]+[.]*[a-zA-Z0-9_]*[@]+([a-zA-Z0-9_]+)*[.]+([a-zA-Z0-9_]+){2,3}$" name="email_resp" placeholder="pauldurand@gmail.com"  value ="' . $tab_r['email_resp'] . '"></td>';
                            ?>

                        </tr>
                        <tr>
                            <th>entreprise :</th>
                            <td>
                                <select name="entreprise_resp">
                                    <option value=''>Faites votre choix</option>
                                    <?php
                                    //faire la requête
                                    $req_v = ('SELECT * FROM `entreprise` ORDER BY `nom_ent`;');
                                    //récupérer les résultats de la requête
                                    $result_v = $dbs->query($req_v);
                                    // parcourir ces résultats
                                    $tab_r_v = $result_v->fetchAll();
                                    // parcourir le tableau avec les résultats
                                    //Rempli la liste des villes en sélectionnant celle de l'étudiant récupéré
                                    foreach ($tab_r_v as $r_v) {
                                        if ($r_v['id'] == $id_ent)
                                            echo '<option value="' . $r_v['id'] . '"  selected>' . $r_v['nom_ent'] . '</option>';
                                        else
                                            echo '<option value="' . $r_v['id'] . '">' . $r_v['nom_ent'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        $tab_r = $sth->fetch();
                    }
                    ?>
                    <tr>
                        <td>
                            <button class="bouton" type="submit" name="enregistrer">Enregistrer les modifications</button>
                        </td>
                        <td>
                            <a href="responsable.php?action=liste&amp;id=<?php echo $id_ent; ?>&amp;id_ville=<?php echo $id_ville; ?>">retour</a>
                        </td>
                        <?php
                    }
                }elseif (($_SESSION['type'] != "A" || $_SESSION['type'] != "P")) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter avec un compte abilité';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } elseif (!isset($_SESSION['type'])) {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez vous connecter';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="connexion.php">connexion</a>';
                    echo '</td>';
                } else {
                    echo '<tr>';
                    echo '<td>';
                    echo 'Vous devez choisir une entreprise';
                    echo '</td>';
                    echo '</tr>';
                    echo '<tr>';
                    echo '<td>';
                    echo '<a href="entreprises.php">Retour</a>';
                    echo '</td>';
                }
                ?>
            </tr>
        </table>

    </form>
</body>
</html>
