# Configuration sendmail

Pour tester l'envoie de mail avec Wamp utiliser la démarche suivante :

1. **Implanter sendmail**

    dézipper le fichier sendmail.zip dans le dossier :

        C:\wamp64\sendmail

2. **configurer sendmail**

    éditer le fichier ```sendmail.ini``` et mettre les informations suivantes :

        [sendmail]
        smtp_server=smtp.gmail.com
        smtp_port=587
        auth_username=g.stage.hdf@gmail.com
        auth_password=g!St4g3-HdF-DR-bo-al

3. **configurer php**

    éditer le fichier ```php.ini``` et mettre les informations suivantes :

        [mail function]
        sendmail_path = "\"C:\Wamp64\sendmail\sendmail.exe\" -t -i"
    
    **Attention** à l'échappement du caractère ```"``` avec ```\"```.

